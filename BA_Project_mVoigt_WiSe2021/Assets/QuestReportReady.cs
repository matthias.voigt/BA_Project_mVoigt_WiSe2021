using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestReportReady : MonoBehaviour
{
    public AIStateManager ai;
    public GameObject readyToReport;
    public GameObject followPlayer;
    public GameObject Sleeping;
    public Camera cam;

    private void Start()
    {
        cam = Camera.main;
    }


    // Update is called once per frame
    void Update()
    {


        if (GameManager.singleton.currentPlaystate != GameManager.PlayState.inOverworld)
        {
            readyToReport.SetActive(false);
            followPlayer.SetActive(false);
            Sleeping.SetActive(false);
            return;
        }

        if (ai.followState.objToFollow != null)
        {
            if (ai.followState.objToFollow == GameManager.singleton.playerObj)
            {
                if (ai.currentState == ai.followState)
                    followPlayer.SetActive(true);
            }
        }
        else
        {
            followPlayer.SetActive(false);
        }

        if ((ai.currentState == ai.questEndState && !followPlayer.activeSelf) || ai.questEndState.readyToReport)
        {
            readyToReport.SetActive(true);
        }
        else if(ai.currentState == ai.healedState)
        {
            readyToReport.SetActive(true);
        }
        else
        {
            readyToReport.SetActive(false);
        }

        if (ai.currentState == ai.sleepState)
        {
            Sleeping.SetActive(true);
        }
        else
        {
            Sleeping.SetActive(false);
        }

        if (Sleeping.activeSelf) readyToReport.transform.LookAt(cam.transform);
        if (readyToReport.activeSelf) readyToReport.transform.LookAt(cam.transform);
        if (followPlayer.activeSelf) followPlayer.transform.LookAt(cam.transform);

    }

}
