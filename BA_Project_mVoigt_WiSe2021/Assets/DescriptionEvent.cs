using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newEventDesc", menuName = "New Event Description")]
public class DescriptionEvent : ScriptableObject
{
    public int eventID;
    public bool unlockedFromStart;
    public bool unlocked;
    public string eventTitle;
    public string desc;
}
