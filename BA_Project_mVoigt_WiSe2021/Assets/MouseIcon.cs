using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseIcon : MonoBehaviour
{
    private void Update()
    {
        if (GameManager.singleton.currentPlaystate == GameManager.PlayState.inOverworld)
        {
            Cursor.visible = false;
        }
        else
            Cursor.visible = true;
    }
}
