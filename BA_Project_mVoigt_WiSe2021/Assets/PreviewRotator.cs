using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewRotator : MonoBehaviour
{
    [SerializeField] private GameObject target = null;
    [SerializeField] private float rotateSpeed = 1f;

    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            float spd = rotateSpeed * -1;
            RotateObj(spd);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            RotateObj(rotateSpeed);
        }
    }

    private void RotateObj(float speed)
    {
        target.transform.Rotate(Vector3.up * speed * Time.deltaTime);
    }
}
