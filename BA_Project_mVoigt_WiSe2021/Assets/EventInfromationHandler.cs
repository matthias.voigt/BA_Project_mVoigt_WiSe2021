using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class EventInfromationHandler : MonoBehaviour
{
    public GameObject EventPanel;
    public Image eventImage;
    public Material previewMat;
    public List<Sprite> eventSprites = new List<Sprite>();
    public List<DescriptionEvent> descriptionEvent = new List<DescriptionEvent>();
    public TMP_Text tmp_desc;
    public TMP_Text tmp_title;

    public int currentEventID = 0;
    public static EventInfromationHandler singleton;
    private void Awake()
    {
        singleton = this;
    }
    private void Start()
    {
        int l = PlayerPrefs.GetInt("load", 0);
        if (l == 0)
        {
            Debug.Log("Setting Event Descs to default.");
            SetEventDefaults();
        }
    }
    private void SetEventDefaults()
    {
        foreach(DescriptionEvent dE in descriptionEvent)
        {
            if (dE.unlockedFromStart) dE.unlocked = true;
            else dE.unlocked = false;
        }
    }
    public void OpenEventPanel()
    {
        EventPanel.SetActive(true);
        currentEventID = 0;
        DisplayEventDesc(descriptionEvent[0]);
    }

    private void DisplayEventDesc(DescriptionEvent dE)
    {

        if (dE.unlocked || dE.unlockedFromStart)
        {
            previewMat.SetFloat("_BlurIntensity", 0f);
            eventImage.gameObject.SetActive(true);
            tmp_desc.alignment = TextAlignmentOptions.Left;
            tmp_desc.text = dE.desc;
            tmp_title.text = dE.eventTitle;
            eventImage.sprite = eventSprites[dE.eventID];
        }
        else
        {
            tmp_title.text = "???";
            eventImage.sprite = eventSprites[dE.eventID];
            previewMat.SetFloat("_BlurIntensity", 10f);
            tmp_desc.alignment = TextAlignmentOptions.Center;
            tmp_desc.text = "There are many things happening on this island.\nAnnounce a Scout Quest to let your villagers scout the area to find new things!";           
        }
    }

    public void ShowNextEvent(int value)
    {
        AudioManager.singleton.PlayClickSFX();
        currentEventID++;
        if (currentEventID > descriptionEvent.Count -1) currentEventID = 0;
        else if (currentEventID < 0) currentEventID = descriptionEvent.Count;
        DisplayEventDesc(descriptionEvent[currentEventID]);
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire2"))
            EventPanel.SetActive(false);
    }

}
