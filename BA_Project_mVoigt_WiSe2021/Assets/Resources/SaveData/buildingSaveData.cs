using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class BuildingSaveData 
{
    public int id;
    public Vector3 position;
    public Quaternion rotation;
    public string objectTag;
    public int index;
}
