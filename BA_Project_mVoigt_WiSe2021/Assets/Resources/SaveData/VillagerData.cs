using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class VillagerData 
{
    [Header("Character")]
    public string cName;
    public int raceID;
    public int raceTypeID;
    public int jobID;
    public List<int> traitID = new List<int>();

    [Header("Stats")]
    public float satisfaction;
    public int authority;
    public int prestige;
    public int qExp;

    public bool isHero;



    [Header("Apperance")]
    public float meshColor1;
    public float meshColor2;
    public float meshColor3;
    public float meshBrightness;
    public Color c1;
    public Color c2;
    public Color c3;

    public int beltID;
    public int clothsID;
    public int glovesID;
    public int shoesID;
    public int shouldersID;
    public int hairID;
    public int faceID;
    public int hatsID;

    public int factionID;
    public int currentStateID;
    public int currentDisplayMode;
    public int currentTextType;

    public Vector3 position;
    public Quaternion rotation;

    public int money;
    public List<int> items = new List<int>();

    public int houseOwnerIndex = -1;


}
