using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SaveData 
{
    [Header("Time")]
    public float currentTime;
    public int currentDay;
    public int currentTimeState;

    [Header("Player")]
    public List<int> playerItems = new List<int>();
    public int playerMoney;
    public Vector3 playerPos = new Vector3();

    [Header("Cutscene Progress")]
    public int cutSceneID;

    [Header("Buildings")]
    public List<BuildingSaveData> bsData = new List<BuildingSaveData>();

    [Header("Villagers")]
    public List<VillagerData> vData = new List<VillagerData>();
    public List<VillagerData> eData = new List<VillagerData>();


    //Skull data is missing!
}
