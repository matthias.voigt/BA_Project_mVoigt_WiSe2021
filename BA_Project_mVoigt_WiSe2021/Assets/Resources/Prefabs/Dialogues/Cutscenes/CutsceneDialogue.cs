using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newDialogue", menuName = "New Cutscene Dialogue")]
public class CutsceneDialogue : ScriptableObject
{
    [TextArea(3, 10)]
    public string[] sentences;

    public EventList[] eventList;

    public AnimationClip[] animation;
    public AudioClip[] audioClips;
    
    public Vector3[] CameraPos;
    public Vector3[] CameraRot;

    public SceneCharacter[] sceneCharacter;
    public Vector3[] sceneCharacterPos;
    public Vector3[] sceneCharacterRot;

   
    public enum EventList
    {
        wait,
        disablePlayer,
        setSceneCharacter,
        setCharacterPos,
        setCharacterRot,
        nextSentence,
        playCamAnim,
        changeCameraPos,
        changeCameraRot,
        fadeIn,
        fadeOut,
        disableAiState,
        enableAiState,
        setCharacterAnimator,
        playCharAnim,
        stopTime,
        startTime,
        playBGM,
        playTitle,
        end,
        spawnTransfParticles,
        changeSkyboxToBoss,
        changeSkyboxToNormal,
        enableTornadoEffect,
        disableTornadoEffect,
        changeChurchScene,
        enableEvilMageAI,
        enablePostProcessing,
        setFinalePos,
        enableSecondTornado,
        checkVillagerCount,
        changeCamToHuman,
        changeCamToMonster,
        checkSequenceHumanTalk,
        checkSequenceMonsterTalk,
        playerdecisionSide,
        enableDecisionButtons,
        setbossfightHero,
        setbossfighMage,
        scaleMage,
        scaleHero,
        enableBossBoxTrigger,
        disableGameObjectTornado,
        playSFXBackground,
        loadCredits,
        spawnVillagers,
        disableBookIcon,
        press1Info
    }

    public enum SceneCharacter
    {
        player,
        hero,
        evilmage,
        evilmageAI
    }
}
