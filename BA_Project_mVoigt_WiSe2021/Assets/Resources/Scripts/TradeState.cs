using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TradeState : AIState
{
    public AIStateManager ai;
    public AIStateManager aiToTradeWith;

    public override AIState RunCurrentState()
    {
        SetAgentSpeed();
        SetDisplaymode();

        //Fill in merchant
        if(aiToTradeWith == null)
        {
            aiToTradeWith = GetNeareastMerchant();
        }

        //No merchant found
        if(aiToTradeWith == null)
        {
            ai.rP.AddReport($"I wish we had a market where I could trade my items.");
            return ai.TryChangeState(ai.idleState);
        }

        //Go to merchant
        if(ai.IsCharacterTooFarAwayFromObject(ai.gameObject, aiToTradeWith.gameObject, 3f))
        {
            ai.rP.AddReport($"Let's go to the market!");
            ai.agent.SetDestination(aiToTradeWith.merchState.selectedBuild.tradePosition.position);
        }
        //Nearby merchant
        else if(ai.IsCharacterNearbyObject(ai.gameObject, aiToTradeWith.gameObject, 3f))
        {
            int rA = RandomAction();

            //AI buys something
            if(rA == 0)
            {
                ai.rP.AddReport($"Hm, this item looks interesting. Let's buy it!");
                GetItemFrom(ai.inventory, aiToTradeWith.inventory);
            }
            //AI sells something
            else if(rA == 1)
            {
                ai.rP.AddReport($"It was a please to sell my items to {aiToTradeWith.character.GetCharacterName()}");
                GetItemFrom(aiToTradeWith.inventory, ai.inventory);
            }


            return ai.TryChangeState(ai.idleState);

        }

        return this;
    }

    private void GetItemFrom(Inventory buyer, Inventory seller)
    {
        if (buyer.GetCurrentMoney() <= 0) return;
        else if (seller.items.Count == 0) return;

        ObjectHandler item = seller.items[Random.Range(0, seller.items.Count)];

        seller.items.Remove(item);
        buyer.AddItemToInventory(item);

        seller.AddMoney(item.sellPrice);
        buyer.AddMoney(-item.sellPrice);

        Debug.Log($"Item trade successful: {item.objectTag}.");
    } 
    
    public int RandomAction()
    {
        return Random.Range(0, 2);
    }

    private AIStateManager GetNeareastMerchant()
    {
        AIStateManager[] allMerchs = FindObjectsOfType<AIStateManager>();
        AIStateManager closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = ai.gameObject.transform.position;

        foreach (AIStateManager go in allMerchs)
        {
            //AI is in merch state
            if(go.currentState == go.merchState)
            {
                //AI owns a building
                if(go.merchState.ownedMerchBuild != null)
                {
                    //AIs shop is open
                    if (go.merchState.ownedMerchBuild.isOpen)
                    {
                        Vector3 diff = go.transform.position - position;
                        float curDistance = diff.sqrMagnitude;

                        if (curDistance < distance)
                        {
                            closest = go;
                            distance = curDistance;
                        }
                    }
                }
            }
            if (go.faction == AIStateManager.Faction.neutral && (go.currentState == go.idleState || go.currentState == go.wanderState))
            {
                if (go.character.jobClass == TraitManager.JobClass.Merchant)
                {
                    Vector3 diff = go.transform.position - position;
                    float curDistance = diff.sqrMagnitude;

                    if (curDistance < distance)
                    {
                        closest = go;
                        distance = curDistance;
                    }
                }
            }
        }

        return closest;
    }

    public void SetAgentSpeed()
    {
        ai.agent.speed = ai.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplaymode()
    {
        ai.canTalk = true;
        switch (ai.faction)
        {
            case AIStateManager.Faction.villager:
                if (ai.character.jobClass == TraitManager.JobClass.Hero)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.none;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                else
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.character;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                break;
            case AIStateManager.Faction.enemy:
                ai.currentDisplayMode = Dialogue.DisplayMode.none;
                ai.currentTextType = Dialogue.TextType.standard;
                break;
            case AIStateManager.Faction.neutral:
                if (ai.character.jobClass == TraitManager.JobClass.Adventurer)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.adventurer;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                else if (ai.character.jobClass == TraitManager.JobClass.Merchant)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.merchant;
                    ai.currentTextType = Dialogue.TextType.shop;
                }
                else if (ai.character.race == Character.Race.skeleton)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.ressurect;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                break;
        }
    }
}
