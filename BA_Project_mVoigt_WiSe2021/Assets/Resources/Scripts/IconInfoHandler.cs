using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class IconInfoHandler : MonoBehaviour
{

    public TMP_Text infoText;
    public InfoType infoType = InfoType.satisfaction;
    public GameObject infoPanel;

    private Image traitImage;

    public enum InfoType
    {
        satisfaction,
        prestige,
        authority,
        money,
        faction,
        job,
        trait,
        home,
        spectate,
        showDetails,
        next,
        previous,
        displayEnemies,
        displayNeutrals,
        displayVillagers,
    }

    private void Start()
    {
        if(infoType == InfoType.trait || infoType == InfoType.job)
        {
            traitImage = GetComponent<Image>();
        }
        DisableInformation();
    }

    public void ShowInformation()
    {
        Debug.Log("Enter");
        infoText.alignment = TextAlignmentOptions.Left;

        switch (infoType)
        {
            case InfoType.next:
                infoText.alignment = TextAlignmentOptions.Center;
                infoText.text = "Show next resident.";
                break;
            case InfoType.previous:
                infoText.alignment = TextAlignmentOptions.Center;
                infoText.text = "Show previous resident.";
                break;
            case InfoType.displayEnemies:
                infoText.text = $"Click to display all enemy residents.\n\nThese residents are hostile against your villagers.\nWhen they see a villager of yours, they might attack!";
                break;
            case InfoType.displayNeutrals:
                infoText.text = "Click to display all neutral residents.\n\nThese residents are independent.\nThey will not attack any other residents, as long as they don't have to.\n";
                break;
            case InfoType.displayVillagers:
                infoText.text = "Click to display all your residents.\n\nThese are your villagers. These residents are hostile against all enemies. When they see an enemy, they might attack!";
                break;
            case InfoType.home:
                infoText.text = $"Home:\nShows current home of this villager.\nAt sleeping time, a villager tends to sleep at home and recovers satisfaction.\nThe bigger the house, the better they can recover.\nAssign a villager to a house by interacting with a built house.";
                break;
            case InfoType.spectate:
                infoText.text = $"Spectation:\nSpectate the current selected resident to see what the resident is up to.";
                break;
            case InfoType.showDetails:
                infoText.text = $"Details:\nShows details of the current selected villager.\nSometimes it might help to take a look at what they are thinking.";
                    break;
            case InfoType.satisfaction:
                infoText.text = $"Satisfaction:\nDetermines stamina of a character. With high stamina a character can do tasks longer. Below 0, the character will faint!";
                break;
            case InfoType.authority:
                infoText.text = $"Authority:\nDetermines how good a character will do a task. Low authority means less effectiveness. High authority means high effectiveness.";
                break;
            case InfoType.prestige:
                infoText.text = $"Prestige:\nA character with high prestige wants higher rewards for a quest. Every Quest below this prestige will be ignored by this character!";
                break;
            case InfoType.money:
                infoText.text = $"Money:\nUsed to buy items at the market. The more money a character carries, the higher its prestige will get!";
                break;
            case InfoType.faction:                
                infoText.text = $"Faction: Determines attitude. Press [TAB] to see a characters faction.\nHeart: This character is a villager of your town and is hostile against enemies. \n Shield: This character is neutral towards others. \n Skull: This character is an enemy and is hostile against villagers.";
                break;
            case InfoType.trait:
                infoText.alignment = TextAlignmentOptions.Center;
                //Pacifist
                if (traitImage.sprite == TraitManager.singleton.TraitSprites[0])
                {
                    infoText.text = "A pacifist avoids violence at all cost. If a fight arises, a pacifist will rather run away than hurt others.";
                }
                //Aggressive
                else if (traitImage.sprite == TraitManager.singleton.TraitSprites[1])
                {
                    infoText.text = "Violence is a language everyone understands. If something happens, violence is the first and only option!";
                }
                //NightOwl
                else if(traitImage.sprite == TraitManager.singleton.TraitSprites[2])
                {
                    infoText.text = "The night is a beautiful time to be awake! This character wakes up at 8:00 and goes to sleep at 2:00.";
                }
                //EarlyBird
                else if(traitImage.sprite == TraitManager.singleton.TraitSprites[3])
                {
                    infoText.text = "The early bird catches the worm! This character wakes up at 4:00 and goes to bed at 22:00.";
                }
                //SleepyHead
                else if(traitImage.sprite == TraitManager.singleton.TraitSprites[4])
                {
                    infoText.text = "Some people just love sleeping. This character wakes up at 10:00 and goes to bed at 22:00.";
                }
                //Sleepless
                else if(traitImage.sprite == TraitManager.singleton.TraitSprites[5])
                {
                    infoText.text = "Some people just can't fall asleep. This character wakes up at 4:00 and goes to bed at 00:00.";
                }
                //Workaholic
                else if(traitImage.sprite == TraitManager.singleton.TraitSprites[6])
                {
                    infoText.text = "Work hard! This character looses less satisfaction from tasks than others.";
                }
                //Lazyhead
                else if(traitImage.sprite == TraitManager.singleton.TraitSprites[7])
                {
                    infoText.text = "Yawn! This character looses more satisfaction from tasks than others.";
                }
                //Generous
                else if(traitImage.sprite == TraitManager.singleton.TraitSprites[8])
                {
                    infoText.text = "Do you need a helping hand? Take two! This character is more generous to others!";
                }
                //Miser
                else if(traitImage.sprite == TraitManager.singleton.TraitSprites[9])
                {
                    infoText.text = "This is mine! This character trades only for its advantages.";
                }
                //Grumpy
                else if(traitImage.sprite == TraitManager.singleton.TraitSprites[10])
                {
                    infoText.text = "Hmpf. This character recovers less satisfaction from sleeping than others.";
                }
                else if(traitImage.sprite == TraitManager.singleton.TraitSprites[11])
                {
                    infoText.text = "The secret to a happy life? A healty sleep! This character recovers more satisfaction from sleeping than others.";
                }
                else
                {
                    Debug.LogWarning("No Sprite found for " + this.gameObject.name);
                }
                break;
            case InfoType.job:
                if (traitImage.sprite == TraitManager.singleton.JobSprites[1])
                {
                    infoText.alignment = TextAlignmentOptions.Center;
                    infoText.text = $"Hero\nA powerful adventurer!";
                }
                else if (traitImage.sprite == TraitManager.singleton.JobSprites[2])
                    infoText.text = $"Villager\nOrdinary resident.\nVillagers do not do quests, unless they are asked to.";
                else if (traitImage.sprite == TraitManager.singleton.JobSprites[3])
                    infoText.text = $"Adventurer\nAdventurers love to do quests and check the quest board regulary on their own.";
                else if (traitImage.sprite == TraitManager.singleton.JobSprites[4])
                    infoText.text = $"Merchant\nMerchants love to trade. With a market built in town, they can open a shop to sell and buy goods.";
                else if (traitImage.sprite == TraitManager.singleton.JobSprites[5])
                    infoText.text = $"\nGuard\nGuards are powerful protectors. With a watchtower built in town, they can guard at a better seight.";
                else if (traitImage.sprite == TraitManager.singleton.JobSprites[6])
                    infoText.text = $"Researcher\nResearchers loves to find new things. With a research lab built, they can research new buildings.";
                else
                {
                    infoText.alignment = TextAlignmentOptions.Center; 
                    infoText.text = $"Character class of a resident.";
                }
                    
                break;
        }

        infoPanel.SetActive(true);
    }

    public void DisableInformation()
    {
        infoPanel.SetActive(false);
    }
}
