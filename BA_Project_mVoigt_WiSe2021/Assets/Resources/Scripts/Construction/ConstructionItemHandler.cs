using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ConstructionItemHandler : MonoBehaviour
{
    [SerializeField] private Construction construct = null;
    [SerializeField] private TMP_Text constructName = null;

    private void Start()
    {
        if (construct != null)
            SetConstruct(construct);
    }

    public void SetConstruct(Construction construct)
    {
        this.construct = construct;
        constructName.text = construct.GetConstructName();

        if(!construct.GetUnlockBool())
        {
            constructName.text = "???";
        }
    }

    public void OnEnable()
    {
        if (construct != null)
            SetConstruct(construct);
    }

    public void ShowPreview()
    {
        AudioManager.singleton.PlayClickSFX();
        ConstructionManager.singleton.SetConstruct(construct);
        ConstructionManager.singleton.TryChangeConstructState(ConstructionManager.ConstructionState.inPreview);
    }
}
