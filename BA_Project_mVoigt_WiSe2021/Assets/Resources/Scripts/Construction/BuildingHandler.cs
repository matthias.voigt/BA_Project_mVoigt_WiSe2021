using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingHandler : MonoBehaviour
{
    [SerializeField] public Character owner = null;
    [SerializeField] private Construction construction = null;
    [SerializeField] private SleepItemHandler sleepHandler = null;

    public int prestigeBoost = 0;

    public int buildIndex = -1;

    private void Start()
    {
        //NavigationBaker.singleton.UpdateNavMesh();

        if (TryGetComponent(out SleepItemHandler sIH))
        {
            sleepHandler = sIH;
        }
    }

    public Character GetOwner()
    {
        return owner;
    }

    public Construction GetConstruction() { return construction; }
    

    public void SetOwner(Character character)
    {
        if (owner != null)
        {
            owner.SetHome(null);
            Debug.Log($"Owner {owner.GetCharacterName()} is not amused! Implement consequences!");
        }

        if (character == null)
        {
            Debug.Log($"{character.GetCharacterName()} is moving out from {construction.GetConstructName()}");
            owner = null;
        }
        else
        {
            character.SetHome(this);
            owner = character;
        }
        
    }

    public void SetBuildingInfo(Construction constructData)
    {
        construction = constructData;
    }

    public void MoveOut()
    {
        Debug.Log($"{owner.GetCharacterName()} is moving out from {construction.GetConstructName()}");
        owner = null;
    }

    public SleepItemHandler GetSleepHandlerItem()
    {
        return sleepHandler;
    }

}
