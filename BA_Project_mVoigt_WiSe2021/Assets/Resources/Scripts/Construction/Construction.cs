using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newConstruct", menuName = "New Construction")]
public class Construction : ScriptableObject
{
    [SerializeField] private int id = 0;
    [SerializeField] private string constructName = "default construct";
    [SerializeField] private GameObject constructPrefab = null;
    [SerializeField] private string description = "This is a default description.";
    [SerializeField] private int constructionCost = 5;
    [SerializeField] private bool unlocked = true;
    [SerializeField] private int researchCost = 100;

    public bool unlockedFromStart = false;
    public int authorityBoost = 0;
    public int prestigeBoost = 0;

    public float researchTime = 18000f;

    public int GetId() { return id; }
    public string GetConstructName() { return constructName; }
    public string GetConstructDesc() { return description; }

    public int GetResearchCost() { return researchCost; }

    public bool GetUnlockBool() { return unlocked; }
    public void UnlockBuilding(bool isUnlocked) { unlocked = isUnlocked; }

    public int GetConstructCost() { return constructionCost; }
    public GameObject GetConstruct() { return constructPrefab; }

    public void SetID(int newID) => id = newID;
}
