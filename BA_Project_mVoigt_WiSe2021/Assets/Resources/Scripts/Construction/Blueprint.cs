using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class Blueprint : MonoBehaviour
{
    [Header("Blueprint Material")]
    [SerializeField] private Material material;
    [Header("Layers")]
    [SerializeField] int affectedLayerForRaycast = 6;
    [Header("Properties")]
    [SerializeField] private float rotationSpeed = 10f;
    [Header("AudioSource")]
    [SerializeField] private AudioSource buildSFX = null;

    private Camera cam;
    private Construction construct;
    private bool canBuild = false;
    private bool enoughMoney = true;

    void Start()
    {
        BuildingEnabled();

        if(cam == null)
        {
            Debug.Log($"No camera set for {gameObject.name}. Setting reference from ConstructionManager.");
            cam = ConstructionManager.singleton.GetConstructionCamera();
        }
        CheckForMoney();
        //InstallCollisions();
    }

    /// <summary>
    /// Adds BoxCollider to this gameObject and copies value from assigned construction GameObject reference.
    /// </summary>
    private void InstallCollisions()
    {
        Debug.Log("Installing collisions for blueprint by copying referenced BoxCollider from construction class.");
        BoxCollider boxcol = GetComponent<BoxCollider>();
        BoxCollider constCol = construct.GetConstruct().GetComponent<BoxCollider>();

        boxcol.center = new Vector3(constCol.center.x, constCol.center.y, constCol.center.z);
        boxcol.size = new Vector3(constCol.size.x, constCol.size.y, constCol.size.z);
        boxcol.isTrigger = true;
        constCol.enabled = false;
    }

    void Update()
    {
        if (construct == null) return;

        Raycasting();
        RotateBlueprint();

        if (Input.GetButtonDown("Fire1"))
            BuildBluePrint();
    }

    private void Raycasting()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 500000, (1 << affectedLayerForRaycast)))
        {
            transform.position = hit.point;      
        }
    }

    private void CheckForMoney()
    {
        if (InventoryManager.singleton.playerInventory.money < construct.GetConstructCost())
        {
            Debug.Log("No money available for building!");
            enoughMoney = false;
            BuildingDisabled();
        }
        else
        {
            enoughMoney = true;
        }
    }

    /// <summary>
    /// Sets Construction class reference and instantiates blueprinting version of a GameObject.
    /// </summary>
    /// <param name="construct"></param>
    public void SetConstruct(Construction construct)
    {
        if(construct == null)
        {
            Debug.LogError("No construction reference set!");
            return;
        }

        Debug.Log($"Instantiating blueprint version of {construct.GetConstructName()}");

        this.construct = construct;
        GameObject blueprint =  Instantiate(construct.GetConstruct());
        blueprint.transform.parent = transform;
        SetAllMaterials(blueprint);
    }

    /// <summary>
    /// Replaces all set materials of gameobject and children with material reference.
    /// </summary>
    private void SetAllMaterials(GameObject obj)
    {
        Debug.Log($"Replacing all matterials of {obj.name} for blueprinting.");

        if (obj.TryGetComponent(out Renderer _rend))
        {
            var mats = new Material[_rend.materials.Length];

            for (int i = 0; i < _rend.materials.Length; i++)
            {
                mats[i] = material;
            }

            _rend.materials = mats;
        }

        Debug.Log($"Replacing all matterials of {obj.name}'s children for blueprinting.");

        foreach (Transform child in obj.transform)
        {
            if(child.TryGetComponent(out Renderer rend))
            {
                var mats = new Material[rend.materials.Length];

                for (int i = 0; i < rend.materials.Length; i++)
                {
                    mats[i] = material;
                }

                rend.materials = mats;
            }
        }       
    }

    /// <summary>
    /// Instantiates GameObject from Construction class assigned GameObject.
    /// </summary>
    private void BuildBluePrint()
    {
        if (!enoughMoney) return;

        if (!canBuild)
        {
            Debug.Log("Cannot build construct as it is colliding with another gameObject.");
            return;
        }

        if (construct == null)
        {
            Debug.LogError("No construction possible. Missing reference!");
            return;
        }

        InventoryManager.singleton.playerInventory.AddMoney(-construct.GetConstructCost());

        
        Debug.Log($"Instantiating blueprint as set gameobject: {construct.GetConstructName()} ");
        GameObject objToConstruct = Instantiate(construct.GetConstruct(), transform.position, transform.rotation);
        //objToConstruct.GetComponent<BoxCollider>().enabled = true;
        

        if (objToConstruct.tag == "NoCollision")
        {
            foreach (Transform child in objToConstruct.transform)
            {
                child.gameObject.layer = 7;
            }

            objToConstruct.layer = 7;
        }
        else if (objToConstruct.tag == "Terrain")
        {
            foreach (Transform child in objToConstruct.transform)
            {
                child.gameObject.layer = 6;
            }

            objToConstruct.layer = 6;
        }
        else
        {
            foreach (Transform child in objToConstruct.transform)
            {
                child.gameObject.layer = 8;
            }

            objToConstruct.layer = 8;

            BuildingHandler _bH = objToConstruct.AddComponent<BuildingHandler>();
            _bH.SetBuildingInfo(construct);
        }

        //Save Data to Cache
        if (objToConstruct.TryGetComponent<BuildingHandler>(out BuildingHandler bH))
        {
            SaveManager.singleton.AddBuildID(construct.GetId(), transform.position, transform.rotation, construct.GetConstruct().tag, bH);
        }
        else
        {
            SaveManager.singleton.AddBuildID(construct.GetId(), transform.position, transform.rotation, construct.GetConstruct().tag, null);
        }



        /*
        if (objToConstruct.tag == "Bench")
        {
            GameManager.singleton.AddBenchToList(objToConstruct.GetComponent<SleepItemHandler>());
        }
        */
        buildSFX.Play();
        objToConstruct.transform.DOShakeScale(0.5f, 0.2f, 10, 90, true);
        CheckForMoney();
    }

    private void RotateBlueprint()
    {
        if (CameraMovement.singleton.cM != CameraMovement.ConstructMode.RotateObject) return;

        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(-Vector3.up * rotationSpeed / 50, Space.World);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(Vector3.up * rotationSpeed / 50, Space.World);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            transform.Rotate(Vector3.up * rotationSpeed, Space.Self);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            transform.Rotate(Vector3.down * rotationSpeed, Space.Self);
        }
    }

    #region Material Changes
    /// <summary>
    /// Enables building. Changes material color of this gameObject to white.
    /// </summary>
    private void BuildingEnabled()
    {
        canBuild = true;
        material.color = Color.white;
    }

    /// <summary>
    /// Disables building. Changes material color of this gameObject to red.
    /// </summary>
    private void BuildingDisabled()
    {
        canBuild = false;
        material.color = Color.red;
    }
    #endregion

    #region Collision Checks
    /// <summary>
    /// Checks if this gameObjects collides with anything except layer 6. Disables building when there is a collision.
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        if (!enoughMoney) return;

        if (other.gameObject.layer == 7) return;    //Ignoring Blueprint Layer
        if (other.gameObject.layer == 6) return;    //Ignoring Ground Layer
        if (other.gameObject.tag == "NoCollision") return; 

        Debug.Log($"Colliding with {other.gameObject.name}.");
        BuildingDisabled();
    }

    /// <summary>
    /// Checks if this gameObjects exits a collision with anything except layer 6. Enables building.
    /// </summary>
    private void OnTriggerExit(Collider other)
    {
        if (!enoughMoney) return;

        if (other.gameObject.layer == 7) return;    //Ignoring Blueprint Layer
        if (other.gameObject.layer == 6) return;    //Ignoring Ground Layer
        BuildingEnabled();
    }

    /// <summary>
    /// Checks if this gameObjects collides with anything except layer 6. Disables building when there is a collision.
    /// </summary>
    private void OnTriggerStay(Collider other)
    {
        if (!enoughMoney) return;

        if (other.gameObject.layer == 7) return;    //Ignoring Blueprint Layer
        if (other.gameObject.layer == 6) return;    //Ignoring Ground Layer
        if (other.gameObject.tag == "NoCollision") return;

        Debug.Log($"Colliding with {other.gameObject.name}.");
        BuildingDisabled();
    }
    #endregion


}
