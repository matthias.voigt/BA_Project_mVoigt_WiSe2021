using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public bool randomInventory = true;
    public int money = 0;
    public List<ObjectHandler> items = new List<ObjectHandler>();
    public Transform InvTransform;

    public int pumpkins;
    public int tomatos;
    public int potatoes;
    public int iron;
    public int silver;
    public int gold;
    public int skull;
    public int potions;

    public ObjectHandler favoriteItem;
    public int favorAmount = 0;

    private void Start()
    {
        if (randomInventory)
            ItemManager.singleton.GenerateRandomInventory(this);

        GenerateRandomFavoriteItem();
    }

    private void GenerateRandomFavoriteItem()
    {
        GameObject obj = ItemManager.singleton.items[Random.Range(1, ItemManager.singleton.items.Length)];
        favoriteItem = obj.GetComponent<ObjectHandler>();
        favorAmount = Random.Range(1, 10);
    }
    public void AddItemToInventory(ObjectHandler item)
    {
        items.Add(item);
        item.gameObject.transform.SetParent(InvTransform);

        if (item.objectTag == "pumpkin") pumpkins++;
        if (item.objectTag == "tomato") tomatos++;
        if (item.objectTag == "potato") potatoes++;
        if (item.objectTag == "iron") iron++;
        if (item.objectTag == "silver") silver++;
        if (item.objectTag == "gold") gold++;
        if (item.objectTag == "skull") skull++;
        if (item.objectTag == "potion") potions++;
    }

    public void ChangeObjectAmount(int id, int value)
    {
        switch (id)
        {
            case 0:
                potatoes += value;
                if (potatoes < 0) potatoes = 0;
                break;
            case 1:
                pumpkins += value;
                if (pumpkins < 0) pumpkins = 0;
                break;
            case 2:
                tomatos += value;
                if (tomatos < 0) tomatos = 0;
                break;
            case 3:
                iron += value;
                if (iron < 0) iron = 0;
                break;
            case 4:
                silver += value;
                if (silver < 0) iron = 0;
                break;
            case 5:
                gold += value;
                if (gold < 0) iron = 0;
                break;
            case 6:
                skull += value;
                if (skull < 0) iron = 0;
                break;
            case 7:
                potions += value;
                if (potions < 0) potions = 0;
                break;
            default:
                Debug.LogError("Something went wrong!");
                break;
        }
    }

    public void AddMoney(int addValue)
    {
        Debug.Log($"Adding {addValue} coins to {gameObject.name}'s inventory.");
        money += addValue;
        if (money < 0)
        {
            money = 0;
        }
    }
    public int GetCurrentMoney() { return money; }

    public void DropItem(ObjectHandler oH)
    {
        oH.gameObject.transform.SetParent(null);
        Vector3 pos = gameObject.transform.position;
        pos = new Vector3(pos.x, pos.y + 2.5f, pos.z);
        oH.gameObject.transform.position = pos;
        oH.gameObject.SetActive(true);
        items.Remove(oH);
    }
}
