using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxPP : MonoBehaviour
{
    public GameObject PP;
    public AudioClip audioclip;
    private AudioClip previewsAudio;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PP.SetActive(false);
            AudioManager.singleton.PlayEvilMageAmbient(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            AudioManager.singleton.PlayEvilMageAmbient(false);
            PP.SetActive(true);
        }
    }
}
