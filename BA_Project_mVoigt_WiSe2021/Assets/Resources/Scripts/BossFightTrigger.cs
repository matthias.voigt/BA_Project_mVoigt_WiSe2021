using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFightTrigger : MonoBehaviour
{
    public BoxCollider bc;
    public CutsceneDialogue bossfightIntro;

    private void Awake()
    {
        bc.GetComponent<BoxCollider>();
        bc.enabled = false;
    }

    private void Start()
    {
        //CutsceneManager.singleton.StartEvent(bossfightIntro);
    }

    public void EnableBoxCollider()
    {
        bc.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(FlyManager.singleton.transformation != FlyManager.Transfomation.bat)
            {
                DisableAllTalk();
                bc.enabled = false;
                CutsceneManager.singleton.StartEvent(bossfightIntro);
            }
        }
    }

    public void DisableAllTalk()
    {
        foreach(AIStateManager ai in GameManager.singleton.enemies)
        {
            ai.canTalk = false;
        }
        foreach (AIStateManager ai in GameManager.singleton.neutrals)
        {
            ai.canTalk = false;
        }
        foreach (AIStateManager ai in GameManager.singleton.villagers)
        {
            ai.canTalk = false;
        }
    }
}
