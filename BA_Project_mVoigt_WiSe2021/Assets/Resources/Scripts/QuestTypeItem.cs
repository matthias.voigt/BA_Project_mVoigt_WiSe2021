using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class QuestTypeItem : MonoBehaviour
{
    public TMP_Text title;
    public Image icon;
    public Image factionIcon;
    public Button button;

    public QuestManager.QuestType questType = QuestManager.QuestType.undefined;
    public ItemManager.Items questObj = ItemManager.Items.undefined;
    public QuestManager.Location questLocation = QuestManager.Location.undefined;
    public TraitManager.JobClass questJob = TraitManager.JobClass.undefined;
    public QuestManager.Interaction questInteraction = QuestManager.Interaction.undefined;

    public AIStateManager rescueNPC;

   

    public void SetQuestTypeItem(Sprite s, QuestManager.QuestType qt)
    {
        Debug.Log($"Setting up QuestTypeItem: sprite: {s.name},  questType: {qt}");

        gameObject.name = qt.ToString();

        title.text = qt.ToString();
        icon.sprite = s;

        questType = qt;

        factionIcon.gameObject.SetActive(false);
    }

    public void SetQuestObjectItem(Sprite s, ItemManager.Items i)
    {
        Debug.Log($"Setting up QuestObjectItem: sprite: {s.name},  questObject: {i}");

        gameObject.name = i.ToString();

        title.text = i.ToString();
        icon.sprite = s;

        questObj = i;

        factionIcon.gameObject.SetActive(false);
    }

    public void SetQuestLocationItem(Sprite s, QuestManager.Location l)
    {
        Debug.Log($"Setting up QuestLocationItem: sprite: {s.name},  questLocation: {l}");

        gameObject.name = l.ToString();

        title.text = l.ToString();
        icon.sprite = s;

        questLocation = l;

        factionIcon.gameObject.SetActive(false);
    }


    public void SetQuestRescueItem(Sprite s, AIStateManager ai)
    {
        Debug.Log($"Setting up QuestAIItem: sprite: {s.name},  ai: {ai.character.GetCharacterName()}");

        if (ai.isCurrentlyMissing)
            button.interactable = false;
        else
        {
            button.interactable = true;
        }

        rescueNPC = ai;

        gameObject.name = ai.character.GetCharacterName().ToString();

        title.text = ai.character.GetCharacterName().ToString();
        icon.sprite = s;

        factionIcon.sprite = GameManager.singleton.factionIcons[(int)ai.faction];
        factionIcon.gameObject.SetActive(true);
    }

    public void SetQuestJobItem(Sprite s, TraitManager.JobClass j)
    {
        Debug.Log($"Setting up QuestJobItem: sprite: {s.name},  questJob: {j}");

        gameObject.name = j.ToString();

        title.text = j.ToString();
        icon.sprite = s;

        questJob = j;

        factionIcon.gameObject.SetActive(false);
    }

    public void SetQuestInteractionItem(Sprite s, QuestManager.Interaction i)
    {
        Debug.Log($"Setting up QuestInteractionItem: sprite: {s.name},  questInteraction: {i}");

        gameObject.name = i.ToString();

        title.text = i.ToString();
        icon.sprite = s;

        questInteraction = i;

        factionIcon.gameObject.SetActive(false);
    }

    public void OnButtonClick()
    {
        //Button: QuestType
        if (questType != QuestManager.QuestType.undefined)
        {
            Debug.Log("Clicking on QuestType Button.");
            QuestTypeButton();
        }
        //Button: Item
        else if(questObj != ItemManager.Items.undefined)
        {
            Debug.Log("Clicking on QuestObject Button.");
            SetQuestObject();
        }
        //Button: Location
        else if(questLocation != QuestManager.Location.undefined)
        {
            Debug.Log("Clicking on Location Button.");
            SetLocation();
        }
        //Button: Job
        else if(questJob != TraitManager.JobClass.undefined)
        {
            Debug.Log("Clicking on Job Button.");
            SetJobChange();
        }
        //Button: Event Interaction
        else if(questInteraction != QuestManager.Interaction.undefined)
        {
            Debug.Log("Clicking on Interaction Button.");
        }      
        else if(rescueNPC != null)
        {
            Debug.Log("Clicking on Rescue Button.");
            SetRescueNPC(rescueNPC);
        }
    }

    private void QuestTypeButton()
    {
        switch (questType)
        {
            case QuestManager.QuestType.undefined:
                Debug.LogError("No questType defined!");
                break;
            case QuestManager.QuestType.Gather:
                Debug.Log("Starting quest with questtype: Gather");
                QuestManager.singleton.SetQuestType(QuestManager.QuestType.Gather);
                break;
            case QuestManager.QuestType.Become:
                Debug.Log("Starting quest with questtype: Become");
                QuestManager.singleton.SetQuestType(QuestManager.QuestType.Become);
                break;
            case QuestManager.QuestType.Scout:
                Debug.Log("Starting quest with questtype: Scout");
                QuestManager.singleton.SetQuestType(QuestManager.QuestType.Scout);
                break;
            case QuestManager.QuestType.Rescue:
                Debug.Log("Starting quest with questtype: Rescue");
                QuestManager.singleton.SetQuestType(QuestManager.QuestType.Rescue);
                break;
            case QuestManager.QuestType.Events:
                Debug.Log("Starting quest with questtype: Events");
                QuestManager.singleton.SetQuestType(QuestManager.QuestType.Events);
                break;
            default:
                Debug.LogError("No questType defined!");
                break;
        }
    }

    private void SetQuestObject()
    {
        QuestManager.singleton.SetQuestObject(questObj);      
    }

    private void SetJobChange()
    {
        QuestManager.singleton.SetJobChange(questJob);
    }

    private void SetLocation()
    {
        QuestManager.singleton.SetLocation(questLocation);
    }

    private void SetRescueNPC(AIStateManager ai)
    {
        QuestManager.singleton.SetRescueQuest(ai);
    }

    private void OnEnable()
    {

        if (questObj == ItemManager.Items.Skull) gameObject.SetActive(false);
        if(questType == QuestManager.QuestType.Events) gameObject.SetActive(false);

        if (questObj == ItemManager.Items.undefined) return;
        string tag = questObj.ToString().ToLower();
        List<ObjectHandler> oHs = new List<ObjectHandler>(FindObjectsOfType<ObjectHandler>());
        int amount = 0;

        foreach(ObjectHandler oH in oHs)
        {
            if(oH.objectTag == tag)
            {
                amount++;
            }
        }

        if(amount == 0)
        {
            button.interactable = false;
        }
        else
        {
            title.text = $"{tag} ({amount})";
            button.interactable = true;
        }
    }
}
