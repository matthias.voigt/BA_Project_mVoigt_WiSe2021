using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestEndState : AIState
{
    public AIStateManager aiStateManager;
    public GameObject finishPlace;
    public bool readyToReport = false;
    public bool sendStatus = false;

    public override AIState RunCurrentState()
    {
        SetAgentSpeed();
        SetDisplayMode();

        if(finishPlace == null)
        {
            finishPlace = GameObject.FindGameObjectWithTag("QuestBoard");
        }
        else if (aiStateManager.IsCharacterNearbyObject(aiStateManager.character.gameObject, finishPlace, 10f))
        {
            Debug.Log($"{aiStateManager.character.GetCharacterName()} is back from questing.");

            //REPORT
            aiStateManager.rP.AddReport($"I am ready to report my quest!");
            if(aiStateManager.currentQuest.questType == QuestManager.QuestType.Become)
            {
                aiStateManager.rP.AddQuestReport("Well, thank you for my new opportunity!");
            }
            else if(aiStateManager.currentQuest.questType == QuestManager.QuestType.Gather)
            {
                if(aiStateManager.gatherState.gatheredItems.Count < 0)
                    aiStateManager.rP.AddQuestReport("Well, here are the items you asked for! Thank you and see you!");
                else
                {
                    aiStateManager.rP.AddQuestReport("Next time I try to be better prepared!");
                }
            }
            else if(aiStateManager.currentQuest.questType == QuestManager.QuestType.Rescue)
            {
                aiStateManager.rP.AddQuestReport("Anyways, that is about it. Take care.");
            }
            else if(aiStateManager.currentQuest.questType == QuestManager.QuestType.Scout)
            {
                aiStateManager.rP.AddQuestReport("Anyways, that is about it. Take care.");
            }
            
            if (!sendStatus)
            {
                sendStatus = true;
                StatusManager.singleton.InsertNewStatus(GetFinishStatus());                             
            }
            return this;
            //return aiStateManager.TryChangeState(aiStateManager.idleState);
        }
        else
        {
            //REPORT
            aiStateManager.rP.AddReport($"Let's head to the quest board to report my quest.");
            aiStateManager.rP.AddQuestReport("As I was done with my quest, I headed back to report!");

            //aiStateManager.currentDisplayMode = Dialogue.DisplayMode.character;
            //aiStateManager.currentTextType = Dialogue.TextType.standard;

            //readyToReport = false;
            aiStateManager.agent.SetDestination(finishPlace.transform.position);
        }

        if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
            aiStateManager.anim.Play("Walk");

        return this;
    }

    private string GetFinishStatus()
    {
        string status = "null";

        switch (aiStateManager.currentQuest.questType)
        {
            case QuestManager.QuestType.Gather:
                 status = $"{aiStateManager.character.GetCharacterName()} finished quest and is ready to report.";
                break;
            case QuestManager.QuestType.Become:
                status = $"{aiStateManager.character.GetCharacterName()} is now a {aiStateManager.character.jobClass}.";
                break;
            case QuestManager.QuestType.Scout:
                status = $"{aiStateManager.character.GetCharacterName()} finished scouting and is ready to report.";
                break;
            case QuestManager.QuestType.Rescue:
                status = $"{aiStateManager.character.GetCharacterName()} is back from rescuing {aiStateManager.rescueState.rescuedAi.character.GetCharacterName()} and is ready to report.";
                break;
        }
        return status;
    }

    public void SetAgentSpeed()
    {
        aiStateManager.agent.speed = aiStateManager.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplayMode()
    {
        aiStateManager.canTalk = true;
        readyToReport = true;

        aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
        aiStateManager.currentTextType = Dialogue.TextType.QG_Report;
    }
}
