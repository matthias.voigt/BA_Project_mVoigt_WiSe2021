using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEventManager : MonoBehaviour
{
    public Transform[] randomEventPos;
    public Transform enemyBase;
    public Transform dockBeach;
    public float timeTillEvent;
    private AudioSource alarm;

    public GameObject villager;

    public static RandomEventManager singleton;

    private void Awake()
    {
        singleton = this;
    }
    public enum RandomEvent
    {
        travelingMerchant,
        travelingAdventurer,
        travelingEnemy,
        villager,
        getmoney
    }

    private void Start()
    {
        alarm = GetComponent<AudioSource>();
        SetTimer();
        OptionsManager.singleton.SetOption += ChangeVolume;
        ChangeVolume();
    }

    private void OnDestroy()
    {
        OptionsManager.singleton.SetOption -= ChangeVolume;
    }

    private void ChangeVolume()
    {
        alarm.volume = OptionsManager.singleton.sfxValue;
    }

    private void SetTimer()
    {
        timeTillEvent = Random.Range(GameManager.singleton.minTimeTillEvent, GameManager.singleton.maxTimeTillEvent);
    }

    private void Update()
    {
        if (CutsceneManager.singleton.currentState != CutsceneManager.CutsceneState.none) return;
        if (CutsceneManager.singleton.tornadoEnabled) return;

        if (GameManager.singleton.currentPlaystate != GameManager.PlayState.inOverworld) return;

        timeTillEvent -= Time.deltaTime * DayManager.singleton.speed;
        if(timeTillEvent <= 0)
        {
            SetTimer();
            Debug.Log("Get Random Event!");
            if(GameManager.singleton.villagers.Count > 6 && GameManager.singleton.enemies.Count < 15)
            {
                SetUpRandomEvent(RandomEvent.travelingEnemy);
            }
            else if(GameManager.singleton.villagers.Count < 4)
            {
                SetUpRandomEvent(RandomEvent.villager);
            }
            else if(GameManager.singleton.PlayerInventory.money < 1000)
            {
                SetUpRandomEvent(RandomEvent.getmoney);
            }           
        }
    }

    public void SetUpRandomEvent(RandomEvent rE)
    {
        switch (rE)
        {
            case RandomEvent.travelingEnemy:

                StatusManager.singleton.InsertNewStatus("Enemies are approaching!");
                alarm.Play();


                int amountOfEnemies = Random.Range(1, 4);

                for(int i = 0; i < amountOfEnemies; i++)
                {
                    Debug.Log("Spawing random enemies!");

                    if(GameManager.singleton.enemies.Count < 15)
                    {
                        GameObject enemy = Instantiate(ScoutManager.singleton.evp_small[1], enemyBase.position, Quaternion.identity);
                        AIStateManager ai = enemy.transform.GetComponentInChildren<AIStateManager>();

                        enemy.transform.DetachChildren();
                        Destroy(enemy);

                        ai.goHomeState.attackTown = true;
                        ai.currentState = ai.TryChangeState(ai.goHomeState);
                    }
                }
                break;
            case RandomEvent.villager:
                StatusManager.singleton.InsertNewStatus("A new villager arrives at the island!");
                Debug.Log("Spawing random villager!");
                GameObject _villager = Instantiate(villager, dockBeach.position, Quaternion.identity);
                AIStateManager _ai = villager.transform.GetComponentInChildren<AIStateManager>();
                //GameManager.singleton.AddNPCToList(_ai, AIStateManager.Faction.villager);
                _villager.transform.DetachChildren();
                Destroy(_villager);

                _ai.goHomeState.attackTown = false;
                _ai.currentState = _ai.TryChangeState(_ai.goHomeState);

                break;
            case RandomEvent.getmoney:
                int random = Random.Range(500, 1500);
                StatusManager.singleton.InsertNewStatus($"Villagers donated {random} gold for the town.");
                GameManager.singleton.PlayerInventory.AddMoney(random);
                break;
            default:
                Debug.LogError("Something went wrong!");
                break;
        }
    }
}
