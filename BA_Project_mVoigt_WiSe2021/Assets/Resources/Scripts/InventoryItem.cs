using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class InventoryItem : MonoBehaviour
{
    [Header("Item Property")]
    public string itemName;
    public int objectID;
    public int amount;

    [Header("Image")]
    public Image itemIcon;
   
    [Header("Button GameObjects")]
    public Button interactButton;
    public Button drinkButton;

    [Header("Texts")]
    public TMP_Text itemName_TMP;
    public TMP_Text itemAmount_TMP;
    public TMP_Text bText;

    [Header("Inventory")]
    public Inventory currentInventory;


    private void OnEnable()
    {
        interactButton.gameObject.SetActive(false);
        drinkButton.gameObject.SetActive(false);
    }

    public void SetInventory(Inventory i) => currentInventory = i;
    public void SetItem(ObjectHandler oH, Inventory inv)
    {
        SetInventory(inv);
        objectID = oH.objectID;
        amount = InventoryManager.singleton.GetAmount(objectID, inv);
        itemName = InventoryManager.singleton.GetItemName(objectID);
        itemIcon.sprite = InventoryManager.singleton.GetItemSprite(objectID);

        itemAmount_TMP.text = amount.ToString();
        itemName_TMP.text = itemName.ToString();
    }

    private void SellItem()
    {
        if (amount <= 0)
        {
            interactButton.gameObject.SetActive(false);
            return;
        }
        if (amount - 1 <= 0)
        {
            interactButton.gameObject.SetActive(false);
        }

        ObjectHandler oH = GetObjectHandler(currentInventory);
        InventoryManager.singleton.SellItem(oH, this);
    }

    private void DropItem()
    {
        if (amount <= 0)
        {
            interactButton.gameObject.SetActive(false);
            return;
        }
        if(amount - 1 <= 0)
        {
            interactButton.gameObject.SetActive(false);
        }

        InventoryManager.singleton.DropItem(GetObjectHandler(currentInventory), this);
    }  

    public void ShowMenu()
    {
        if (!interactButton.gameObject.activeSelf && amount > 0)
        {
            interactButton.gameObject.SetActive(true);
        }
        else
        {
            interactButton.gameObject.SetActive(false);
            return;
        }

        switch (InventoryManager.singleton.displayMode)
        {
            case InventoryManager.DisplayMode.ownInventory:
                bText.text = "Drop";  
                
                if(objectID == 7 && FlyManager.singleton.transformation != FlyManager.Transfomation.bat)
                {
                    drinkButton.gameObject.SetActive(true);
                }
                else
                {
                    drinkButton.gameObject.SetActive(false);
                }
                break;
            case InventoryManager.DisplayMode.sellMode:
                bText.text = "Sell for " + GetObjectHandler(currentInventory).sellPrice;
                break;
            case InventoryManager.DisplayMode.buyMode:
                ObjectHandler oH = GetObjectHandler(currentInventory);
                int addPrice = Mathf.RoundToInt(oH.sellPrice / GameManager.singleton.merchRiseFactor);
                int sellPrice = oH.sellPrice + addPrice;
                bText.text = "Buy for " + sellPrice;
                break;
            case InventoryManager.DisplayMode.npcInventory:
                interactButton.gameObject.SetActive(false);
                break;
            default:
                Debug.LogError("Something went wrong!");
                break;
        }
    }

    public void Interact()
    {
        switch (InventoryManager.singleton.displayMode)
        {
            case InventoryManager.DisplayMode.ownInventory:
                DropItem();
                break;
            case InventoryManager.DisplayMode.buyMode:
                SellItem();
                break;
            case InventoryManager.DisplayMode.sellMode:
                SellItem();
                break;
        }
    }

    public void DrinkPotion()
    {
        InventoryManager.singleton.CloseInventory();
        InventoryManager.singleton.RemoveItem(GetObjectHandler(currentInventory), this);
        FlyManager.singleton.TransformToBat();
    }

    private ObjectHandler GetObjectHandler(Inventory inv)
    {
        foreach (ObjectHandler oH in inv.items)
        {
            if (oH.objectID == objectID)
            {
                return oH;
            }
        }

        Debug.LogError("No Objecthandler found!");
        return null;
    }
}
