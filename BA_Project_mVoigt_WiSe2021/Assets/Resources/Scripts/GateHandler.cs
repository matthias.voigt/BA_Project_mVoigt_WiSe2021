using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateHandler : MonoBehaviour
{
    public Transform RightDoor;
    public Transform LeftDoor;

    public float yRotSpeed = 5f;

    public void CloseGate()
    {

    }
    public void OpenGate()
    {
        RightDoor.Rotate(Vector3.up * yRotSpeed * Time.deltaTime, Space.Self);
        RightDoor.Rotate(Vector3.down * yRotSpeed * Time.deltaTime, Space.Self);
    }

    private void Update()
    {
        RightDoor.Rotate(Vector3.up * yRotSpeed * Time.deltaTime, Space.Self);
        RightDoor.Rotate(Vector3.down * yRotSpeed * Time.deltaTime, Space.Self);
    }
}
