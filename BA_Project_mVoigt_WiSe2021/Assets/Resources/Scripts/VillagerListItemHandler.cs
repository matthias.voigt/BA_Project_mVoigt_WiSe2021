using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VillagerListItemHandler : MonoBehaviour
{
    [SerializeField] private Button villagerButton = null;
    [SerializeField] private Button homeButton = null;

    [SerializeField] private TMP_Text villagerName = null;
    [SerializeField] private TMP_Text homeName = null;

    [SerializeField] private Character charData = null;
    [SerializeField] private BuildingHandler bH = null;


    public void SetData(Character character)
    {
        charData = character;
        villagerName.text = character.GetCharacterName();
        villagerButton.interactable = true;

        if(character.GetCurrentHome() != null)
        {
            homeName.text = character.GetCurrentHome().GetConstruction().GetConstructName();
        }
        else
        {
            homeName.text = "homeless";
            homeButton.interactable = false;
        }
    }

    public void AssignSelector()
    {
        BuildingInfoManager.singleton.DisplayAssignDesc(charData);
    }
}
