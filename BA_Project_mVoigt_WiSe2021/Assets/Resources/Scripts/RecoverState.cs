using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecoverState : AIState
{
    public AIStateManager ai;
    public HospitalBed hp;

    public float timeTillRecovery = 18000;
    public override AIState RunCurrentState()
    {

        ai.canTalk = false;
        SetAnimation();
        ai.untargetable = true;
        timeTillRecovery -= DayManager.singleton.speed * Time.deltaTime;

        if(timeTillRecovery <= 0)
        {
            Debug.Log($"{ai.character.GetCharacterName()} recovered!");
            hp.ExitHospitalBed();
            ai.rP.AddReport("Ah...I feel so much better now! I could've been dead! I am glad I got rescued.");
            ai.rP.AddQuestReport("Suddenly I woke up at our hospital...You send help right? You rescued me, thank you so much!");
            StatusManager.singleton.InsertNewStatus($"{ai.character.GetCharacterName()} recovered from injuries.");
            return ai.TryChangeState(ai.healedState);
        }

        ai.rP.AddReport("Urgs...I need some rest...My injuries are hurting...");
        return this;
    }

    private void SetAnimation()
    {
        if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Sleep"))
            ai.anim.Play("Sleep");
    }
}
