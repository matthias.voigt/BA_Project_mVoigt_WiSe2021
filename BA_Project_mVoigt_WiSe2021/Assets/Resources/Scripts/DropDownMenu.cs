using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DropDownMenu : MonoBehaviour
{
    public BuildSection bs = BuildSection.none;
    public DropDownType ddT = DropDownType.build;
    public enum DropDownType
    {
        build,
        research
    }
 
    private TMP_Dropdown dd;

    private void Start()
    {
        dd = GetComponent<TMP_Dropdown>();
    }

    private void OnDisable()
    {
        if(dd != null)
            dd.value = 0;
    }
    public enum BuildSection
    {
        none,
        Base,
        Nature,
        Miscellaneous,
    }
    public void HandleInputData(int val)
    {
        switch (bs)
        {
            case BuildSection.Base:
                ShowBaseSection(val);
                break;
            case BuildSection.Nature:
                ShowNatureSection(val);
                break;
            case BuildSection.Miscellaneous:
                ShowMiscellaneousSection(val);
                break;
            default:
                Debug.LogError("Something went wrong!");
                break;
        }
    }

    private void ShowBaseSection(int val)
    {
        if (val == 0)
        {         
            Debug.Log("Select something!");
        }
        else if (val == 1)
        {
            Debug.Log("Go to Walls");

            if (ddT == DropDownType.build)
            {             
                ConstructionManager.singleton.ShowBuildSectionWalls();
            }
            else if(ddT == DropDownType.research)
            {
                ResearchWalls();
            }

        }
        else if (val == 2)
        {
            Debug.Log("Go to Fences");
            if (ddT == DropDownType.build)
            {
                ConstructionManager.singleton.ShowBuildSectionFences();
            }
            else if(ddT == DropDownType.research)
            {
                ResearchFences();
            }
        }
        else if (val == 3)
        {
            Debug.Log("Go to Paths");
            if (ddT == DropDownType.build)
            {
                ConstructionManager.singleton.ShowBuildSectionPaths();
            }
            else if(ddT == DropDownType.research)
            {
                ResearchPaths();
            }
        }
    }

    private void ShowNatureSection(int val)
    {
        if (val == 0)
        {
            Debug.Log("Select something!");
        }
        else if (val == 1)
        {
            Debug.Log("Go to Trees");
            if (ddT == DropDownType.build)
            {
                ConstructionManager.singleton.ShowBuildSectionTrees();
            }
            else if(ddT == DropDownType.research)
            {
                ResearchTrees();
            }
        }
        else if (val == 2)
        {
            Debug.Log("Go to Flowers");
            if (ddT == DropDownType.build)
            {
                ConstructionManager.singleton.ShowBuildSectionFlowers();
            }
            else if (ddT == DropDownType.research)
            {
                ResearchFlowers();
            }
        }
        else if (val == 3)
        {
            Debug.Log("Go to Stones");
            if (ddT == DropDownType.build)
            {
                ConstructionManager.singleton.ShowBuildSectionStones();
            }
            else if (ddT == DropDownType.research)
            {
                ResearchStones();
            }
        }
        else if (val == 4)
        {
            Debug.Log("Go to Rocks");
            if (ddT == DropDownType.build)
            {
                ConstructionManager.singleton.ShowBuildSectionRocks();
            }
            else if (ddT == DropDownType.research)
            {
                ResearchRocks();
            }
        }
    }

    private void ShowMiscellaneousSection(int val)
    {
        if (val == 0)
        {
            Debug.Log("Select something!");
        }
        else if (val == 1)
        {
            Debug.Log("Go to Benches");
            if (ddT == DropDownType.build)
            {
                ConstructionManager.singleton.ShowBuildSectionBenches();
            }
            else if (ddT == DropDownType.research)
            {
                ResearchBenches();
            }
        }
        else if (val == 2)
        {
            Debug.Log("Go to Lanterns");
            if (ddT == DropDownType.build)
            {
                ConstructionManager.singleton.ShowBuildSectionLanterns();
            }
            else if (ddT == DropDownType.research)
            {
                ResearchLanterns();
            }
        }
        else if (val == 3)
        {
            Debug.Log("Go to Diverse");
            if (ddT == DropDownType.build)
            {
                ConstructionManager.singleton.ShowBuildSectionDiverse();
            }
            else if (ddT == DropDownType.research)
            {
                ResearchDiverse();
            }
        }
    }


    #region Research Buttons
    public void ShowHomeSection()
    {
        ConstructionManager.singleton.ShowBuildSectionHome();
        Debug.Log("Go to home!");
    }

    public void ShowSpecialSection()
    {
        ConstructionManager.singleton.ShowBuildSectionSpecial();
        Debug.Log("Go to Special!");
    }

    public void ResearchHomes() 
    {
        Debug.Log("Researching building of type: home");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.homes);
    }

    public void ResearchWalls()
    {
        Debug.Log("Researching building of type: Walls");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.walls);
        dd.value = 0;
    }

    public void ResearchFences()
    {
        Debug.Log("Researching building of type: Fences");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.fences);
        dd.value = 0;
    }

    public void ResearchPaths()
    {
        Debug.Log("Researching building of type: Paths");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.paths);
        dd.value = 0;
    }

    public void ResearchTrees()
    {
        Debug.Log("Researching building of type: Trees");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.trees);
        dd.value = 0;
    }

    public void ResearchFlowers()
    {
        Debug.Log("Researching building of type: Flowers");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.flowers);
        dd.value = 0;
    }

    public void ResearchStones()
    {
        Debug.Log("Researching building of type: Stones");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.stones);
        dd.value = 0;
    }

    public void ResearchRocks()
    {
        Debug.Log("Researching building of type: Rocks");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.rocks);
        dd.value = 0;
    }

    public void ResearchBenches()
    {
        Debug.Log("Researching building of type: Benches");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.benches);
        dd.value = 0;
    }

    public void ResearchLanterns()
    {
        Debug.Log("Researching building of type: Lanterns");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.lanterns);
        dd.value = 0;
    }

    public void ResearchDiverse()
    {
        Debug.Log("Researching building of type: Diverse");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.diverse);
        dd.value = 0;
    }

    public void ResearchSpecial()
    {
        Debug.Log("Researching building of type: Special");
        ResearchManager.singleton.ShowResearchPreview(ResearchManager.singleton.special);
    }
    #endregion


}
