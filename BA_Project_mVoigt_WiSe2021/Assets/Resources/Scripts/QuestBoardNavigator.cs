using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuestBoardNavigator : MonoBehaviour
{
    [Header("===Quest Board Main Page===")]
    public GameObject QuestBoardPanel;
    public GameObject MainPagePanel;

    [Header("===Announce Page===")]
    public GameObject AnnouncePanel;

    [Header("===Item Page===")]
    public GameObject ItemPanel;

    [Header("===Item Amount Page===")]
    public GameObject ItemAmountPanel;
    public TMP_Text currentAmountTxt;

    [Header("===Location Page===")]
    public GameObject LocationPanel;

    [Header("===Job Page===")]
    public GameObject JobPanel;

    [Header("===Interaction Page===")]
    public GameObject InteractionPanel;

    [Header("===Event Page===")]
    public GameObject EventPanel;

    [Header("===Reward Page===")]
    public GameObject RewardPanel;
    public TMP_Text currentRewardTxt;
    public TMP_Text currentPrestigeTxt;

    [Header("===Result Page===")]
    public GameObject ResultPanel;

    [Header("===Quest List Page===")]
    public GameObject QuestListPanel;

    [Header("===Quest Details Page===")]
    public GameObject QuestDetailsPanel;
    public GameObject QuestDetailsList;
    public List<GameObject> QuestDetailItems;
    public GameObject CraftingIcons;

    [Header("Rescue Page")]
    public GameObject RescuePanel;

    
    public GameObject QuestDetailsPrefab;


    public List<QuestCraftHandler> questCraftHandlers = new List<QuestCraftHandler>();
    public List<NavAction> NavActions = new List<NavAction>();

    public static QuestBoardNavigator singleton;

    private void Awake()
    {
        singleton = this;
    }

    private void Start()
    {
        DeactivatePanels();
        QuestBoardPanel.SetActive(false);
    }

    public enum NavAction
    {
        none,
        mainPage,
        announce,
        gather,
        become,
        scout,
        events,
        itemAmount,
        interaction,
        reward,
        result,
        questList,
        questDetails,
        showRescueNPCs
    }

    public void OpenQuestBoard()
    {
        NavActions.Clear();
        NavActions.Add(NavAction.mainPage);
    }

    public void DeactivatePanels()
    {
        MainPagePanel.SetActive(false);
        AnnouncePanel.SetActive(false);
        ItemPanel.SetActive(false);
        JobPanel.SetActive(false);
        LocationPanel.SetActive(false);
        EventPanel.SetActive(false);
        ItemAmountPanel.SetActive(false);
        InteractionPanel.SetActive(false);
        RewardPanel.SetActive(false);
        ResultPanel.SetActive(false);
        QuestListPanel.SetActive(false);
        QuestDetailsPanel.SetActive(false);
        CraftingIcons.SetActive(false);
        RescuePanel.SetActive(false);
    }

    public void ChangeQuestBoardPanel(NavAction nA)
    {
        Debug.Log("Changing Quest Board Display to: " + nA.ToString());

        DeactivatePanels();
        QuestBoardPanel.SetActive(true);

        switch (nA)
        {
            case NavAction.none:
                Debug.Log("Closing QuestBoard");
                NavActions.Clear();
                QuestBoardPanel.SetActive(false);
                break;
            case NavAction.mainPage:              
                MainPagePanel.SetActive(true);              
                break;
            case NavAction.announce:
                Debug.Log("Resetting Quest");
                QuestManager.singleton.quest = new Quest();
                AnnouncePanel.SetActive(true);
                break;
            case NavAction.gather:
                ItemPanel.SetActive(true);
                break;
            case NavAction.become:
                JobPanel.SetActive(true);
                break;
            case NavAction.scout:
                LocationPanel.SetActive(true);
                break;
            case NavAction.events:
                EventPanel.SetActive(true);
                break;
            case NavAction.itemAmount:
                SetCurrentItemAmount(QuestManager.singleton.quest);
                ItemAmountPanel.SetActive(true);
                break;
            case NavAction.interaction:
                InteractionPanel.SetActive(true);
                break;
            case NavAction.reward:
                SetCurrentReward(QuestManager.singleton.quest);
                RewardPanel.SetActive(true);
                break;
            case NavAction.result:
                DisplayQuestDetails(QuestManager.singleton.quest, false);
                CraftingIcons.SetActive(true);
                ResultPanel.SetActive(true);
                CancelMissionButton.gameObject.SetActive(false);
                break;
            case NavAction.questList:
                QuestListPanel.SetActive(true);
                break;
            case NavAction.questDetails:
                ResultPanel.SetActive(true);
                CraftingIcons.SetActive(false);
                CancelMissionButton.gameObject.SetActive(true);
                break;
            case NavAction.showRescueNPCs:
                QuestManager.singleton.UpdateFaintedNPCs();
                RescuePanel.SetActive(true);
                break;
        }
    }
    public void AddNavAction(NavAction nA)
    {
        Debug.Log("Adding nav action: " + nA.ToString());
        NavActions.Add(nA);
        ChangeQuestBoardPanel(NavActions[NavActions.Count - 1]);
    }

    public void ResumeNavAction()
    {
        if(NavActions.Count - 1 == 0)
        {
            Debug.Log("Quitting Quest Board Panel.");
            DeactivatePanels();
            QuestBoardPanel.SetActive(false);
            NavActions.Clear();
            GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
        }
        else
        {
            NavAction latestAction = NavActions[NavActions.Count - 1];
            NavAction previewsAction = NavActions[NavActions.Count - 2];

            NavActions.Remove(latestAction);
            ChangeQuestBoardPanel(previewsAction);
        }
    }

    private void Update()
    {
        if (!QuestBoardPanel.activeSelf) return;

        if (Input.GetButtonDown("Fire2"))
        {
            ResumeNavAction();
        }
    }
    
    public void SetCurrentItemAmount(Quest q)
    {
        currentAmountTxt.text = q.amountToGet.ToString();
    }

    public void SetCurrentReward(Quest q)
    {
        currentRewardTxt.text = q.moneyReward.ToString();
        currentPrestigeTxt.text = q.questPrestige.ToString();
    }

    public QuestCraftHandler CancelMissionButton;
    public void DisplayQuestDetails(Quest q, bool displayHolder)
    {
        CancelMissionButton.SetQuest(q);

        if (q.QuestHolder == null)
            CancelMissionButton.gameObject.SetActive(true);
        else CancelMissionButton.gameObject.SetActive(false);
        

        if(q.questType == QuestManager.QuestType.Gather)
        {
            questCraftHandlers[0].SetUp(q, QuestCraftHandler.DisplayMode.gather);
            questCraftHandlers[1].SetUp(q, QuestCraftHandler.DisplayMode.itemAmount);
            questCraftHandlers[2].SetUp(q, QuestCraftHandler.DisplayMode.item);
            questCraftHandlers[3].SetUp(q, QuestCraftHandler.DisplayMode.none);
        }
        else if(q.questType == QuestManager.QuestType.Become)
        {
            questCraftHandlers[0].SetUp(q, QuestCraftHandler.DisplayMode.none);
            questCraftHandlers[1].SetUp(q, QuestCraftHandler.DisplayMode.become);
            questCraftHandlers[2].SetUp(q, QuestCraftHandler.DisplayMode.job);
            questCraftHandlers[3].SetUp(q, QuestCraftHandler.DisplayMode.none);
        }
        else if(q.questType == QuestManager.QuestType.Scout)
        {
            questCraftHandlers[0].SetUp(q, QuestCraftHandler.DisplayMode.none);
            questCraftHandlers[1].SetUp(q, QuestCraftHandler.DisplayMode.scout);
            questCraftHandlers[2].SetUp(q, QuestCraftHandler.DisplayMode.location);
            questCraftHandlers[3].SetUp(q, QuestCraftHandler.DisplayMode.none);
        }
        else if(q.questType == QuestManager.QuestType.Events)
        {
            questCraftHandlers[0].SetUp(q, QuestCraftHandler.DisplayMode.interaction);
            questCraftHandlers[1].SetUp(q, QuestCraftHandler.DisplayMode.events);           //<-- NEEDS proper Implementation
            questCraftHandlers[2].SetUp(q, QuestCraftHandler.DisplayMode.location);
            questCraftHandlers[3].SetUp(q, QuestCraftHandler.DisplayMode.none);
        }
        else if(q.questType == QuestManager.QuestType.Rescue)
        {
            questCraftHandlers[0].SetUp(q, QuestCraftHandler.DisplayMode.none);
            questCraftHandlers[1].SetUp(q, QuestCraftHandler.DisplayMode.rescue);           
            questCraftHandlers[2].SetUp(q, QuestCraftHandler.DisplayMode.npcToRescue);
            questCraftHandlers[3].SetUp(q, QuestCraftHandler.DisplayMode.none);
        }

        questCraftHandlers[4].SetUp(q, QuestCraftHandler.DisplayMode.reward);
        questCraftHandlers[5].SetUp(q, QuestCraftHandler.DisplayMode.prestige);

        if (displayHolder)
        {
            questCraftHandlers[6].SetUp(q, QuestCraftHandler.DisplayMode.questHolder);
          
        }
        else
        {
            questCraftHandlers[6].SetUp(q, QuestCraftHandler.DisplayMode.none);
        }
            
    }

    public void InstantiateQuestDetailsItem(Quest q)
    {
        GameObject questItem = Instantiate(QuestDetailsPrefab);
        questItem.transform.SetParent(QuestDetailsList.transform);
        QuestDisplayer qD = questItem.GetComponent<QuestDisplayer>();
        q.questDisplayer = qD;

        qD.SetQuest(q);
        QuestDetailItems.Add(questItem);
    }
}
