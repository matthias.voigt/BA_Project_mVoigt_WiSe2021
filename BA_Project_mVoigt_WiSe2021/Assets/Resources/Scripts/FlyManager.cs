using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;

public class FlyManager : MonoBehaviour
{
    public CinemachineFreeLook cam;
    public GameObject flyObject;
    public GameObject flyCamTarget;
    public GameObject humanCamTarget;
    public GameObject Particles;
    public Sprite transformBackSprite;
    public Sprite menuSprite;
    public Image menuImage;

    public bool devMode = false;

    public Transfomation transformation = Transfomation.human;

    public static FlyManager singleton;

    private void Awake()
    {
        singleton = this;
    }
    public enum Transfomation
    {
        human,
        bat
    }
  

    public void TransformToBat()
    {
        if (transformation == Transfomation.bat) return;

        menuImage.sprite = transformBackSprite;

        transformation = Transfomation.bat;
        Instantiate(Particles, GameManager.singleton.playerObj.transform.position, Quaternion.identity);
        cam.Follow = flyCamTarget.transform;
        cam.LookAt = flyCamTarget.transform;

        flyObject.transform.position = GameManager.singleton.playerObj.transform.position;
        GameManager.singleton.playerObj.SetActive(false);
        flyObject.SetActive(true);
    }

    public void TransformToHuman()
    {
        if (transformation == Transfomation.human) return;

        menuImage.sprite = menuSprite;

        transformation = Transfomation.human;
        Instantiate(Particles, flyObject.transform.position, Quaternion.identity);
        cam.Follow = humanCamTarget.transform;
        cam.LookAt = humanCamTarget.transform;

        GameManager.singleton.playerObj.transform.position = flyObject.transform.position;
        GameManager.singleton.playerObj.SetActive(true);
        flyObject.SetActive(false);
    }

    private void Update()
    {
        if (transformation != Transfomation.bat) return;

        if (Input.GetKeyUp(KeyCode.Alpha1) && GameManager.singleton.currentPlaystate == GameManager.PlayState.inOverworld)
        {
            TransformToHuman();
        }
    }



}
