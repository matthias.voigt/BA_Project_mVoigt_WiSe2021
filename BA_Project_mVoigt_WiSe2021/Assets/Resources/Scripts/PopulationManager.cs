using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Cinemachine;

public class PopulationManager : MonoBehaviour
{
    public GameObject populationPanel;
    public GameObject overviewItem;
    public GameObject spectationObjects;
    public TMP_Text updatedspecText;
    public TMP_Text updatedNameText;

    public TMP_Text villagerAmount;
    public TMP_Text neutralAmount;
    public TMP_Text enemyAmount;

    public GameObject emptyMessage;

    [SerializeField] private TMP_Text cName;
    [SerializeField] private Image job;
    [SerializeField] private TMP_Text satisfaction;
    [SerializeField] private TMP_Text authority;
    [SerializeField] private TMP_Text prestige;
    [SerializeField] private TMP_Text money;
    [SerializeField] private SpecialBuilding home;
    [SerializeField] private Button homeButton;
    [SerializeField] private Image faction;
    [SerializeField] private Image trait0;
    [SerializeField] private Image trait1;
    [SerializeField] private Image trait2;
    [SerializeField] private TMP_Text status;

    [SerializeField] private CinemachineFreeLook cfL;
    [SerializeField] private Transform playerTransform;

    public AIStateManager currentAI;
    public AIState currentState;
    public int currentID = 0;

    public bool inSpectationMode = false;

    public CurrentList cL = CurrentList.villager;
    public enum CurrentList
    {
        villager,
        neutral,
        enemy
    }


    private void Start()
    {
        spectationObjects.SetActive(false);
        emptyMessage.SetActive(false);
        currentID = 0;
    }

    public void DisplayPopulationPanel()
    {

        Debug.Log("Opening Population Panel.");
        populationPanel.SetActive(true);
        overviewItem.SetActive(true);

        if(currentAI == null)
        {
           if(cL == CurrentList.villager)
           {
                if(currentAI == null)
                {
                    if(GameManager.singleton.villagers.Count <= 0)
                    {
                        emptyMessage.SetActive(true);
                        overviewItem.SetActive(false);
                    }
                    else
                        currentAI = GameManager.singleton.villagers[0];                
                }
           }
        }

        villagerAmount.text = GameManager.singleton.villagers.Count.ToString();
        neutralAmount.text = GameManager.singleton.neutrals.Count.ToString();
        enemyAmount.text = GameManager.singleton.enemies.Count.ToString();

        FillInData(currentAI);
    }


    public void ShowCharacterProfile()
    {
        ClosePanel();
        CharacterDisplayManager.singleton.TryChangeCharacterDisplayState(CharacterDisplayManager.CharacterDisplayStates.jobClass, currentAI.character);
    }

    private void FillInData(AIStateManager ai)
    {
        if(ai == null)
        {
            Debug.LogError("No AI found!");
            return;
        }
        job.sprite = TraitManager.singleton.JobSprites[(int)ai.character.jobClass];
        cName.text = ai.character.GetCharacterName();
        satisfaction.text = Mathf.RoundToInt(ai.character.GetProsperitySatisfaction()).ToString();
        authority.text = ai.character.GetAuthority().ToString();
        prestige.text = ai.character.GetProsperityPrestige().ToString();
        faction.sprite = GameManager.singleton.factionIcons[(int)ai.faction];
        trait0.sprite = TraitManager.singleton.TraitSprites[(int)ai.character.traits[0]];
        trait1.sprite = TraitManager.singleton.TraitSprites[(int)ai.character.traits[1]];
        trait2.sprite = TraitManager.singleton.TraitSprites[(int)ai.character.traits[2]];
        money.text = ai.inventory.money.ToString();
        status.text = GetStatus();
        updatedspecText.text = GetStatus();

        currentState = ai.currentState;
        if(ai.character.GetCurrentHome() == null)
        {
            homeButton.interactable = false;
        }
        else
        {
            homeButton.interactable = true;
        }
    }

    public void ClosePanel()
    {
        populationPanel.SetActive(false);
    }


    public void SpectateCurrentAI()
    {
        spectationObjects.SetActive(true);
        inSpectationMode = true;
        populationPanel.SetActive(false);
        cfL.Follow = currentAI.camTransform.transform;
        cfL.LookAt = currentAI.camTransform.transform;
        GameManager.singleton.TryChangePlayState(GameManager.PlayState.spectationMode);
    }

    public void ExitSpectation()
    {
        spectationObjects.SetActive(false);
        cfL.Follow = playerTransform;
        cfL.LookAt = playerTransform;
        populationPanel.SetActive(true);
        inSpectationMode = false;
        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
    }

    private void Update()
    {
        if (populationPanel.activeSelf && Input.GetButton("Fire2")) ClosePanel();
        else if(inSpectationMode && Input.GetButtonUp("Fire2"))
        {
            ExitSpectation();
        }

        if (inSpectationMode)
        {
            updatedNameText.text = currentAI.character.GetCharacterName();

            if (currentState != currentAI.currentState)
            {
                updatedspecText.text = GetStatus();               
            }


            if (Input.GetKeyDown(KeyCode.Q))
            {
                GetNextNPC(-1);
                SpectateCurrentAI();
            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                GetNextNPC(+1);
                SpectateCurrentAI();
            }
        }
    }

    public string GetStatus()
    {
        if (CutsceneManager.singleton.tornadoEnabled) return "What is going on?!";
        else if (currentAI.currentTextType == Dialogue.TextType.healed) return "Wants to talk to you.";
        else if (currentAI.currentDisplayMode == Dialogue.DisplayMode.ressurect) return "Awaken from the dead.";
        else if (currentAI.currentTextType == Dialogue.TextType.shop) return "Wants to sell stuff.";
        else if (currentAI.questEndState.readyToReport) return "Wants to report quest to you.";
        else if (currentAI.currentState == currentAI.idleState)
        {
            return GetIdleString();
        }

        else if (currentAI.currentState == currentAI.wanderState)
        {
            return GetIdleString();
        }
        else if (currentAI.currentState == currentAI.findSleepState) return "Wants to go to sleep.";
        else if (currentAI.currentState == currentAI.talkingState) return "Deep in conversation.";
        else if (currentAI.currentState == currentAI.followState)
        {
            if (currentAI.followState.objToFollow.TryGetComponent<AIStateManager>(out AIStateManager followAI))
            {
                if (followAI.faction != currentAI.faction)
                {
                    return "Is getting kidnapped.";
                }
                else
                    return "Follows someone.";
            }
            else
                return "Follows someone.";
        }
        else if (currentAI.currentState == currentAI.healedState) return "Feels like a newborn!";
        else if (currentAI.currentState == currentAI.qBoardState) return "Checks quest board.";
        else if (currentAI.currentState == currentAI.watchTowerState) return "Currently on alert.";
        else if (currentAI.currentState == currentAI.merchState) return "Selling items.";
        else if (currentAI.currentState == currentAI.attackState) return "In Combat!";
        else if (currentAI.currentState == currentAI.faintState) return "Fainted...";
        else if (currentAI.currentState == currentAI.researchState) return "Currently researching.";
        else if (currentAI.currentState == currentAI.escortPrisonerState) return "Escorts prisoner.";
        else if (currentAI.currentState == currentAI.imprisonedState) return "In Prison.";
        else if (currentAI.currentState == currentAI.fleeState) return "Flees from attacker!";
        else if (currentAI.currentState == currentAI.recoverState) return "Recovers injuries.";
        else if (currentAI.currentState == currentAI.goHomeState)
        {
            if (currentAI.faction == AIStateManager.Faction.villager)
                return "Goes home.";
            else if (currentAI.faction == AIStateManager.Faction.neutral)
                return "Leaves the island";
            else if (currentAI.faction == AIStateManager.Faction.enemy)
            {
                if (currentAI.goHomeState.attackTown)
                {
                    return "Attacking town!";
                }
                else
                {
                    return "Goes back to The Evil Mage";
                }
            }
            else
                return "???";
        }
        else if (currentAI.currentState == currentAI.cookState) return "Wants to cook something.";
        else if (currentAI.currentState == currentAI.gatherState) return "Gathers resources.";
        else if (currentAI.currentState == currentAI.questEndState) return "Wants to report quest.";
        else if (currentAI.currentState == currentAI.scoutState) return "Scouts the island.";
        else if (currentAI.currentState == currentAI.rescueState) return "On a rescue mission!";
        else if (currentAI.currentState == currentAI.tradeState) return "Trades at the market.";
        else if (currentAI.currentState == currentAI.sleepState) return "Zzz...";
        else return "Currently doing nothing.";
    }

    public string GetIdleString()
    {
        List<string> sList = new List<string>();


        foreach(TraitManager.Traits t in currentAI.character.traits)
        {
            sList.Add(GetTraitString(t));
        }

        sList.Add("Stares holes into space.");
        sList.Add("Takes a walk.");
        return sList[Random.Range(0, sList.Count)];
    }

    private string GetTraitString(TraitManager.Traits t)
    {
        switch (t)
        {
            case TraitManager.Traits.Pacifist:
                return "Enjoys the peace.";
            case TraitManager.Traits.Aggressive:
                return "Is looking for trouble.";
            case TraitManager.Traits.EarlyBird:
                return "Wants to go to bed early today.";
            case TraitManager.Traits.Generous:
                return "Wants to help somebody.";
            case TraitManager.Traits.Grumpy:
                return "Is having a bad day.";
            case TraitManager.Traits.HappyNature:
                return "Hums happily.";
            case TraitManager.Traits.Lazyhead:
                return "Wants to take a nap.";
            case TraitManager.Traits.Miser:
                return "Wants to find a treasure.";
            case TraitManager.Traits.NightOwl:
                if (DayManager.singleton.currentTimeState == DayManager.CurrentTimeState.night)
                {
                    return "Enjoys the night.";
                }
                else
                    return "Wants to observe the moon.";
            case TraitManager.Traits.Sleepless:
                return "Had a bad dream.";
            case TraitManager.Traits.SleepyHead:
                return "Is getting sleepy.";
            case TraitManager.Traits.Workaholic:
                return "Wants to do something!";
            default:
                return "Stares holes into space.";
        }
    }

    private void SetNextNPC(List<AIStateManager> aiList, int value)
    {

        currentID += value;
        
        if(currentID == aiList.Count)
        {
            currentID = 0;
        }
        else if(currentID < 0)
        {
            currentID = aiList.Count - 1;
        }

        currentAI = aiList[currentID];

        FillInData(currentAI);
    }

    public void GetNextNPC(int value)
    {
        emptyMessage.SetActive(false);
        overviewItem.SetActive(true);

        if (cL == CurrentList.villager)
        {
            if(GameManager.singleton.villagers.Count <= 0)
            {
                emptyMessage.SetActive(true);
                overviewItem.SetActive(false);
            }
            else
            {
                SetNextNPC(GameManager.singleton.villagers, value);
            }          
        }
        else if (cL == CurrentList.enemy)
        {
            if (GameManager.singleton.enemies.Count <= 0)
            {
                overviewItem.SetActive(false);
                emptyMessage.SetActive(true);
            }
            else
            {
                SetNextNPC(GameManager.singleton.enemies, value);
            }
            
        }
        else if (cL == CurrentList.neutral)
        {
            if (GameManager.singleton.neutrals.Count <= 0)
            {
                overviewItem.SetActive(false);
                emptyMessage.SetActive(true);
            }
            else
            {
                SetNextNPC(GameManager.singleton.neutrals, value);
            }
                
        }
    }

    public void SetList(CurrentList _cl)
    {
        cL = _cl;
        currentID = 0;
        GetNextNPC(0);
    }

    public void SetVillagerList()
    {
        SetList(CurrentList.villager);
    }

    public void SetEnemyList()
    {
        SetList(CurrentList.enemy);
    }

    public void SetNeutralList()
    {
        SetList(CurrentList.neutral);
    }

    public void ShowHome()
    {
        GameManager.singleton.ingameMenuPanel.SetActive(false);
        populationPanel.SetActive(false);
        BuildingInfoManager.singleton.TryChangeBuildingInfoState(BuildingInfoManager.CurrentBuildingInfoState.main, currentAI.character.GetCurrentHome());
    }


}
