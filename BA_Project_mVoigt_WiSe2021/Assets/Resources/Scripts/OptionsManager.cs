using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class OptionsManager : MonoBehaviour
{
    [Header("References")]
    public GameObject OptionPanel = null;
    public Slider SFX_Slider = null;
    public Slider BGM_Slider = null;
    public Slider MouseSensitivity_SliderX = null;
    public Slider MouseSensitivity_SliderY = null;

    public TMP_Text sfxTxt = null;
    public TMP_Text bgmTxt = null;
    public TMP_Text xInvertTxt = null;
    public TMP_Text yInvertTxt = null;
    public TMP_Text XmsTxt = null;
    public TMP_Text YmsTxt = null;

    [Header("Values")]
    public float sfxValue;
    public float bgmValue;
    public float mouseSensitivityValueX;
    public float mouseSensitivityValueY;
    public bool invertX = false;
    public bool invertY = false;

    public Action SetOption;

    public static OptionsManager singleton;

    private void Awake()
    {
        if (singleton == null)
        {
            Debug.Log("Instance is null - using this object:" + gameObject.name);
            singleton = this;
        }
        else
        {
            // Send the game object home that is
            // attached to this script.
            Debug.LogWarning("Instance is already filled and not null - sending this object home: " + gameObject.name);

            GameObject.Destroy(gameObject);
        }

        sfxValue = PlayerPrefs.GetFloat("sfx", 0.8f);
        bgmValue = PlayerPrefs.GetFloat("bgm", 0.6f);

        mouseSensitivityValueX = PlayerPrefs.GetFloat("mXs", 200f);
        mouseSensitivityValueY = PlayerPrefs.GetFloat("mYs", 0.5f);

        UpdateText();
    }

    void UpdateText()
    {
        SFX_Slider.value = sfxValue;
        sfxTxt.text = "Sound: " + (sfxValue * 100).ToString("F0");
        BGM_Slider.value = bgmValue;
        bgmTxt.text = "Music: " + (bgmValue * 100).ToString("F0");

        XmsTxt.text = "Mouse Sensitivity X: " + (mouseSensitivityValueX / 4).ToString("F0");
        MouseSensitivity_SliderX.value = mouseSensitivityValueX;

        MouseSensitivity_SliderY.value = mouseSensitivityValueY;
        YmsTxt.text = "Mouse Sensitivity Y: " + (mouseSensitivityValueY * 100).ToString("F0");
    }

    public void Return()
    {
        OptionPanel.SetActive(false);
    }
    public void OpenOptions()
    {
        OptionPanel.SetActive(true);
    }

    public void SetSFX(float value)
    {
        sfxValue = value;

        sfxTxt.text = "Sound: " + (sfxValue * 100).ToString("F0");

        PlayerPrefs.SetFloat("sfx", value);

        SetOption?.Invoke();
    }
    public void SetBGM(float value)
    {
        PlayerPrefs.SetFloat("bgm", value);

        bgmValue = value;
        bgmTxt.text = "Music: " + (bgmValue * 100).ToString("F0");

        SetOption?.Invoke();
    }

    public void SetMouseSensitivityX(float value)
    {
        PlayerPrefs.SetFloat("mXs", value);

        mouseSensitivityValueX = value;
        XmsTxt.text = "Mouse Sensitivity X: " + (mouseSensitivityValueX / 4).ToString("F0");

        SetOption?.Invoke();
    }

    public void SetMouseSensitivityY(float value)
    {
        PlayerPrefs.SetFloat("mYs", value);

        mouseSensitivityValueY = value;
        YmsTxt.text = "Mouse Sensitivity Y: " + (mouseSensitivityValueY * 100).ToString("F0");

        SetOption?.Invoke();
    }

    public void InvertXMouse()
    {
        if (invertX)
        {
            invertX = false;
            xInvertTxt.text = "Invert X Axis: No";
        }
        else
        {
            invertX = true;
            xInvertTxt.text = "Invert X Axis: Yes";
        }

        SetOption?.Invoke();
    }
    public void InvertYMouse()
    {
        if (invertY)
        {
            invertY = false;
            yInvertTxt.text = "Invert Y Axis: No";
        }
        else
        {
            invertY = true;
            yInvertTxt.text = "Invert Y Axis: Yes";
        }

        SetOption?.Invoke();
    }

    public void ResetToDefault()
    {
        SetSFX(0.8f);
        SFX_Slider.value = 0.8f;
        SetBGM(0.6f);
        BGM_Slider.value = 0.6f;
        SetMouseSensitivityX(200f);
        MouseSensitivity_SliderX.value = 200f;
        SetMouseSensitivityY(0.5f);
        MouseSensitivity_SliderY.value = 0.5f;

        invertX = false;
        xInvertTxt.text = "Invert X Axis: No";
        invertY = false;
        yInvertTxt.text = "Invert Y Axis: No";

        PlayerPrefs.SetFloat("mXs", 200f);
        PlayerPrefs.SetFloat("mYs", 0.5f);
        PlayerPrefs.SetFloat("bgm", 0.6f);
        PlayerPrefs.SetFloat("sfx", 0.8f);

        SetOption?.Invoke();
    }

}
