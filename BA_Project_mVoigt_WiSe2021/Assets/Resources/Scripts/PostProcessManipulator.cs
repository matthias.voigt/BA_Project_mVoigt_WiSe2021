using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PostProcessManipulator : MonoBehaviour
{
    public float bloomSpeed = 0.05f;
    public float maxIntensity = 50f;
    private Bloom bloomLayer;
    public PostProcessVolume ppv;

    public GameObject Canvas;
    public GameObject loadingIcon;

    public int sceneID;

    private float initialIntensity;

    public bool startBloom = false;

    private void Awake()
    {
        loadingIcon.SetActive(false);
        ppv.profile.TryGetSettings(out bloomLayer);
        initialIntensity = bloomLayer.intensity.value;
    }

    private void Update()
    {
        if (startBloom)
        {
            Canvas.SetActive(false);


            bloomLayer.intensity.value += bloomSpeed;

            if(bloomLayer.intensity.value >= maxIntensity)
            {
                startBloom = false;
                Debug.Log("Start Changing Scene!");
            }
        }
        
    }



    IEnumerator SetTransition(float time)
    {
        startBloom = true;
        yield return new WaitForSeconds(time);

        StartCoroutine(LoadAsynchronously(sceneID));
    }

    public void LoadLevel(int sceneIndex)
    {
        sceneID = sceneIndex;
        StartCoroutine(SetTransition(3f));
    }

    IEnumerator LoadAsynchronously (int sceneIndex)
    {
        loadingIcon.SetActive(true);

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            Debug.Log("Loading: " + progress);

            yield return null;
        }
    }
}
