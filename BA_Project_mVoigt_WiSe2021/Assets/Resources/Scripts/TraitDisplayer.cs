using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TraitDisplayer : MonoBehaviour
{
    [SerializeField] private Image traitImage;
    [SerializeField] private TMP_Text traitTitle;

    public void SetTraits(TraitManager.Traits trait)
    {
        traitTitle.text = trait.ToString();
        traitImage.sprite = TraitManager.singleton.TraitSprites[(int)trait];
    }
}
