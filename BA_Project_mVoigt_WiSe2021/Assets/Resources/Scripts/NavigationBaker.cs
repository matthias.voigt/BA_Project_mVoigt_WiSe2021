using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavigationBaker : MonoBehaviour
{

    public NavMeshSurface[] surfaces;
    public NavMeshSurface[] outworld;

    public static NavigationBaker singleton;

    private void Awake()
    {
        singleton = this;
    }

    private void Start()
    {
        UpdateNavMesh();
    }


    public void UpdateNavMesh()
    {
        for(int i = 0; i < surfaces.Length; i++)
        {
            surfaces[i].BuildNavMesh();
        }
    }

    public void UpdateOutworldNavMesh()
    {
        for (int i = 0; i < outworld.Length; i++)
        {
            outworld[i].BuildNavMesh();
        }
    }


}
