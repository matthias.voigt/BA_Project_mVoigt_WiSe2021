using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestReport : MonoBehaviour
{
    public List<string> reports = new List<string>();

    public enum Happenings
    {
        gatheredItem,
        gatheredItemSatisfactionFailure,
        gotAttacked,
        scoutedSomething,
        nothing,
    }

    public void SetReport(Quest q, Happenings h) 
    {
        string s = "";

        switch (h)
        {
            case Happenings.gatheredItem:
                s = $"I gathered some {q.questItem} at the {q.questLocation}.";
                break;
            case Happenings.nothing:
                s = "Nothing really special happened.";
                break;
            case Happenings.gatheredItemSatisfactionFailure:
                s = $"I tried to gather some {q.questItem}, but it was too stressful...";
                break;
        }

        reports.Add(s);
    }
}
