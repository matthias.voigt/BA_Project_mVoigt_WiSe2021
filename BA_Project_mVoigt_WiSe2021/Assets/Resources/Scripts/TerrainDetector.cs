using UnityEngine;

public class TerrainDetector : MonoBehaviour
{
    private TerrainData terrainData;
    private int alphamapWidth;
    private int alphamapHeight;
    private float[,,] splatmapData;
    private int numTextures;

    private void Awake()
    {
        
    }

    public void InsertNewValues(TerrainData tD)
    {
        terrainData = tD;
        alphamapWidth = terrainData.alphamapWidth;
        alphamapHeight = terrainData.alphamapHeight;

        splatmapData = terrainData.GetAlphamaps(0, 0, alphamapWidth, alphamapHeight);
        numTextures = splatmapData.Length / (alphamapWidth * alphamapHeight);
    }

    private Vector3 ConvertToSplatMapCoordinate(Vector3 worldPosition, Terrain t)
    {
        Vector3 splatPosition = new Vector3();
        Terrain ter = t; //Terrain.activeTerrain;
        Vector3 terPosition = ter.transform.position;
        splatPosition.x = ((worldPosition.x - terPosition.x) / ter.terrainData.size.x) * ter.terrainData.alphamapWidth;
        splatPosition.z = ((worldPosition.z - terPosition.z) / ter.terrainData.size.z) * ter.terrainData.alphamapHeight;
        return splatPosition;
    }

    public int GetActiveTerrainTextureIdx(Vector3 position)
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity))
        {
            Debug.Log(hit.collider.gameObject.name);

            //Get ID of Terrain
            if (hit.collider.tag == "Terrain")
            {
                if (hit.collider.gameObject.TryGetComponent(out Terrain t))
                {
                    InsertNewValues(t.terrainData);
                    Vector3 terrainCord = ConvertToSplatMapCoordinate(position, t);
                    int activeTerrainIndex = 0;
                    float largestOpacity = 0f;

                    for (int i = 0; i < numTextures; i++)
                    {
                        if (largestOpacity < splatmapData[(int)terrainCord.z, (int)terrainCord.x, i])
                        {
                            activeTerrainIndex = i;
                            largestOpacity = splatmapData[(int)terrainCord.z, (int)terrainCord.x, i];
                        }
                    }

                    return activeTerrainIndex;
                }
            }
            else if (hit.collider.tag == "wood")
            {
                return 10;
            }
            else if (hit.collider.tag == "stone")
            {
                return 12;
            }
            else
                return 0;

        }

        return 0;
    }  
}
