using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public GameObject[] items;
    public static ItemManager singleton;

    public Sprite[] ItemSprite;

    private void Awake()
    {
        singleton = this;
    }

    public GameObject GetItem(int id)
    {
        return items[id];
    }
    public Sprite GetItemSprite(int id)
    {
        return ItemSprite[id];
    }

    public enum Items
    {
        undefined,
        Potato,
        Pumpkin,
        Tomato,
        Iron,
        Silver,
        Gold,
        Skull
    }

    public void GenerateRandomInventory(Inventory inv)
    {
        int maxCall = Random.Range(0, 10);
        if (maxCall == 0) return;

        for(int i = 0; i <= maxCall; i++)
        {
            int rng = Random.Range(1, items.Length);
            GameObject obj = Instantiate(items[rng]);
            inv.AddItemToInventory(obj.GetComponent<ObjectHandler>());

            obj.SetActive(false);
        }
    }

    public GameObject GetGameObject(int id)
    {
        return items[id];
    }
}
