using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RescueState : AIState
{
    public AIStateManager ai;
    public AIStateManager rescuedAi;

    private Character.RaceType rescueAIRace;

    public HospitalBed hp;

    public SkullHandler skullObject;
    public bool pickedUpSkull = false;
    public bool changedPopularity = false;
    public bool npcDied;
    public override AIState RunCurrentState()
    {
        SetAgentSpeed();
        SetDisplaymode();

        if (ai.hasQuest && !ai.sendQuestStartStatus) ai.SendQuestStartStatus();

        //Find Someone To Rescue
        if (rescuedAi == null)
        {
            rescuedAi = ai.currentQuest.NPCtoRescue;
            rescueAIRace = ai.currentQuest.NPCtoRescue.character.raceType;
            ai.rP.AddReport($"Hold on {ai.currentQuest.NPCtoRescue.character.GetCharacterName()}! I am coming for the rescue!");
            ai.rP.AddQuestReport("Hold on! Thats what I thought. I ran to the rescue!");
        }
        else if (rescuedAi.faintState.rescuer == null && rescuedAi.gameObject.activeSelf)
        {
            rescuedAi.faintState.SetRescuer(ai);
        }

        //Quest Failed: Return QuestEndState
        if (ai.currentQuest.questFailed)
        {
            rescuedAi.isCurrentlyMissing = false;
            if (!changedPopularity)
            {
                changedPopularity = true;
                PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.rescueFailed, rescueAIRace);
            }
            
            return ai.TryChangeState(ai.questEndState);
        }
        //Quest Finished: Return QuestEndState
        else if (ai.currentQuest.questFinished)
        {
            if (!changedPopularity)
            {
                changedPopularity = true;
                PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.rescueSuccessful, rescueAIRace);
            }           
            QuestManager.singleton.RemoveFaintedNPC(rescuedAi);
            return ai.TryChangeState(ai.questEndState);
        }

        //NPC died and go to place to get skull
        else if (rescuedAi != null && rescuedAi.isDead)
        {
            ai.rP.AddReport($"I am too late...I am so sorry, {ai.currentQuest.NPCtoRescue.character.GetCharacterName()}...");
            ai.rP.AddQuestReport($"But when I found {ai.currentQuest.NPCtoRescue.character.GetCharacterName()}, I could not find any living signs anymore...I was too late...");
            
            if(skullObject != null)
            {
                if (skullObject.gameObject.activeSelf)
                {
                    //Go to skull
                    if (ai.IsCharacterTooFarAwayFromObject(ai.gameObject, skullObject.gameObject, 2f))
                    {
                        ai.agent.SetDestination(skullObject.gameObject.transform.position);
                    }
                    //Pick Up Skull
                    else if (ai.IsCharacterNearbyObject(ai.gameObject, skullObject.gameObject, 3f))
                    {
                        ai.rP.AddReport($"What's this? A skull?");
                        ai.rP.AddQuestReport($"I found this skull nearby the corpse...Maybe you can do something with it...");
                        return PickUpObject_Normal();
                    }
                }          
            }
            else
            {
                //Failed to Rescue and failed to pick up skull;
                ai.rP.AddReport($"I could not rescue you, {ai.currentQuest.NPCtoRescue.character.GetCharacterName()}...");
                ai.rP.AddQuestReport($"I am sorry, I couldn't help... ");
                ai.currentQuest.questFailed = true;
                return this;
            }

            if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Sprint"))
                ai.anim.Play("Sprint");

            return this;
        }
        //Find a hospital Bed
        else if (hp == null)
        {
            hp = GetHospitalBed();

            if(hp == null)
            {
                Debug.Log("No empty hospital bed found.");
                ai.rP.AddReport($"I wanted to rescue you, {rescuedAi.character.GetCharacterName()}...But there is no hospital...");
                ai.rP.AddQuestReport($"There's just one propblem. I wanted to rescue {rescuedAi.character.GetCharacterName()} but there was no empty hospital bed...");
                ai.currentQuest.questFailed = true;
                return this;
            }
            else
            {
                //Reserve place for rescuedAI
                hp.currentHolder = rescuedAi;
                return this;
            }
        }

        //Rescued AI is still in faint state
        if(rescuedAi.currentState == rescuedAi.faintState)
        {
            //Go to person to rescue
            if (ai.IsCharacterTooFarAwayFromObject(ai.gameObject, rescuedAi.gameObject, 2f))
            {
                ai.agent.SetDestination(rescuedAi.gameObject.transform.position);
            }
            //Nearby person to rescue
            else if (ai.IsCharacterNearbyObject(ai.gameObject, rescuedAi.gameObject, 2f) && !rescuedAi.isDead)
            {            
                hp.currentHolder = rescuedAi; //Reserve place for rescued AI
                rescuedAi.followState.objToFollow = ai.gameObject;
                rescuedAi.currentState = rescuedAi.TryChangeState(rescuedAi.followState);
            }
        }
        //AI is too far away from hospital
        else if(ai.IsCharacterTooFarAwayFromObject(ai.gameObject, hp.gameObject, 4f))
        {
            //Go to hospital
            ai.rP.AddReport($"Let's bring {rescuedAi.character.GetCharacterName()} to a hospital!");
            ai.agent.SetDestination(hp.gameObject.transform.position);
        }
        else if(ai.IsCharacterNearbyObject(ai.gameObject, hp.gameObject, 5f))
        {
            if(rescuedAi.followState.objToFollow != hp.gameObject)
            {
                rescuedAi.followState.objToFollow = hp.gameObject;
                //rescuedAi.currentState = rescuedAi.TryChangeState(rescuedAi.followState);
            }
            else if(rescuedAi.IsCharacterNearbyObject(rescuedAi.gameObject, hp.gameObject, 4f))
            {
                ai.rP.AddReport($"At the hospital, safe and sound, {rescuedAi.character.GetCharacterName()}!");
                ai.rP.AddQuestReport($"I brought {rescuedAi.character.GetCharacterName()} to hospital safe and sound!");
                Debug.Log($"{ai.character.GetCharacterName()} successfuly rescued {rescuedAi.character.GetCharacterName()}.");
                hp.SleepOnObject(rescuedAi);

                //rescuedAi = null;
                //hp = null;
                ai.currentQuest.questFinished = true;
                return ai.TryChangeState(ai.questEndState);
            }
        }

        if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Sprint"))
            ai.anim.Play("Sprint");
        return this;
    }

    public AIStateManager GetPersonToRescue()
    {
        AIStateManager[] allPersons = FindObjectsOfType<AIStateManager>();
        AIStateManager closest = null;

        float distance = Mathf.Infinity;
        Vector3 position = ai.gameObject.transform.position;

        foreach (AIStateManager go in allPersons)
        {
            if (go.currentState == go.faintState)
            {
                Vector3 diff = go.transform.position - position;
                float curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
        }

        if(closest == null)
        {
            Debug.Log($"{ai.character.GetCharacterName()} could not find any persons to rescue.");
            ai.currentState = ai.TryChangeState(ai.idleState);
            return null;
        }


        Debug.Log($"{ai.character.GetCharacterName()} could find {closest.character.GetCharacterName()} to rescue.");
        return closest;
    }

    public HospitalBed GetHospitalBed()
    {
        HospitalBed[] allPersons = FindObjectsOfType<HospitalBed>();
        HospitalBed closest = null;

        float distance = Mathf.Infinity;
        Vector3 position = ai.gameObject.transform.position;

        foreach (HospitalBed go in allPersons)
        {
            if (go.currentHolder == null)
            {
                Vector3 diff = go.transform.position - position;
                float curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
        }

        if(closest == null)
        {
            Debug.Log($"{ai.character.GetCharacterName()} could not find hospital beds to go to.");
            ai.currentState = ai.TryChangeState(ai.idleState);
            return null;
        }

        Debug.Log($"{ai.character.GetCharacterName()} could  find hospital beds to go to.");
        return closest;
    }


    private AIState PickUpObject_Normal()
    {
        Debug.Log($"{ai.character.GetCharacterName()} is picking up {skullObject.name}.");
        if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("PickUp"))
        {
            ai.anim.Play("PickUp");
            Debug.Log($"{ai.character.GetCharacterName()} picked up object of tag {ai.currentQuest.GetObjectTag()}");
            return this;
        }
        else if (ai.anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            Debug.Log($"{ai.character.GetCharacterName()} picked up an item.");
            ai.inventory.AddItemToInventory(skullObject.GetComponent<ObjectHandler>());
            skullObject.gameObject.SetActive(false);
            pickedUpSkull = true;
            ai.currentQuest.questFailed = true;
            return ai.TryChangeState(ai.idleState);
        }
        else
            return this;
    }

    public void SetAgentSpeed()
    {
        if(rescuedAi != null && rescuedAi.currentState == rescuedAi.talkingState && DialogueManager.singleton.currentDialogueState == DialogueManager.DialogueState.inDialogue)
        {          
                ai.agent.speed *= 0;
        }
        else
            ai.agent.speed = ai.runningSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplaymode()
    {
        ai.canTalk = true;
        switch (ai.faction)
        {
            case AIStateManager.Faction.villager:
                if (ai.character.jobClass == TraitManager.JobClass.Hero)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.none;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                else
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.character;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                break;
            case AIStateManager.Faction.enemy:
                ai.currentDisplayMode = Dialogue.DisplayMode.none;
                ai.currentTextType = Dialogue.TextType.standard;
                break;
            case AIStateManager.Faction.neutral:
                if (ai.character.jobClass == TraitManager.JobClass.Adventurer)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.adventurer;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                else if (ai.character.jobClass == TraitManager.JobClass.Merchant)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.merchant;
                    ai.currentTextType = Dialogue.TextType.shop;
                }
                else if (ai.character.race == Character.Race.skeleton)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.ressurect;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                break;
        }
    }
}
