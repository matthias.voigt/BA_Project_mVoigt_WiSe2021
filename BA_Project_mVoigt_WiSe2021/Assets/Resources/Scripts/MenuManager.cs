using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private AudioSource globalSource;
    [SerializeField] private AudioClip hover_sfx;
    PostProcessManipulator ppm;

    [SerializeField] private GameObject MenuPanel = null;

    public static MenuManager singleton;


    public bool IsMenuOpen()
    {
        if (MenuPanel.activeSelf) return true;
        else return false;
    }

    private void Awake()
    {
        singleton = this;
        ppm = GetComponent<PostProcessManipulator>();
    }

    public void SkipIntro()
    {
        PlayerPrefs.SetInt("load", 2);
        ppm.LoadLevel(3);

    }
    private void Start()
    {
        OptionsManager.singleton.SetOption += SetAudioVolume;
        SetAudioVolume();
    }
    private void OnDestroy()
    {
        OptionsManager.singleton.SetOption -= SetAudioVolume;
    }

    public void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("load", 0);
    }

    public void NewGame()
    {
        Debug.Log("Starting New Game.");
        PlayerPrefs.SetInt("load", 0);
        ppm.LoadLevel(3);
    }

    public void MainMenu()
    {
        Debug.Log("Loading Main Menu.");
        ppm.LoadLevel(0);
    }

    public void Credits()
    {
        Debug.Log("Loading Credits.");
        ppm.LoadLevel(2);
    }
    public void LoadSave()
    {
        PlayerPrefs.SetInt("load", 1);
        Debug.Log("Loading Save File.");
        ppm.LoadLevel(1);
    }
    public void LoadCredits()
    {
        Debug.Log("Loading Credits.");
        ppm.LoadLevel(2);
    }

    public void OpenOptions()
    {
        OptionsManager.singleton.OpenOptions();
    }
    public void Return()
    {
        if(MenuPanel != null)
        {
            GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
            MenuPanel.SetActive(false);
        }
    }
    public void OpenMenu()
    {
        if (MenuPanel == null) return;

        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
        MenuPanel.SetActive(true);
    }




    public void PlayHoverSFX()
    {
        globalSource.clip = hover_sfx;
        globalSource.Play();
    }

    public void Update()
    {
        if (MenuPanel == null) return;
        
        if (CutsceneManager.singleton.currentState != CutsceneManager.CutsceneState.none) return;
        if (GameManager.singleton.currentPlaystate != GameManager.PlayState.inOverworld) return;
        if (Input.GetKeyDown(KeyCode.Escape))
        {


            if (!MenuPanel.activeSelf) 
                OpenMenu();
            else
                Return();
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void SetAudioVolume()
    {
        globalSource.volume = OptionsManager.singleton.sfxValue;
    }


}
