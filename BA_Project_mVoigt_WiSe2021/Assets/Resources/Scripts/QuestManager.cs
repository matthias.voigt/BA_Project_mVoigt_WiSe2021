using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class QuestManager : MonoBehaviour
{
    public static QuestManager singleton;
    private QuestBoardNavigator questBoardNav;

    [Header("Prefabs")]
    public GameObject QuestTypeItemPrefab;

    [Header("Transforms")]
    public GameObject QuestTypeList;
    public GameObject QuestObjectList;
    public GameObject QuestLocationList;
    public GameObject QuestJobList;
    public GameObject QuestInteractionList;
    public GameObject QuestRescueList;

    [Header("Crafted Quest")]
    public Quest quest = new Quest();

    [Header("Rescue Related")]
    public List<AIStateManager> faintedNPCs = new List<AIStateManager>();
    public List<QuestTypeItem> questTypeItemsRescue;

    [Header("Gather Related")]
    public int amountOfObjectsToGet = 1;
    public int minAmount = 1;
    public int maxAmount = 10;
    public List<ObjectHandler> allItemsInOverworld = new List<ObjectHandler>();


    [Header("===Quest Type Values===")]
    public Sprite[] questTypeSprites;

    [Header("===Active Quests")]
    public List<Quest> currentQuestsUnassigned;
    public List<Quest> currentQuestsAssigned;
    public enum QuestType
    {
        undefined,
        Gather,             //Gather Items in Overworld
        Become,             //Changing Job 
        Scout,              //Scouting places
        Events,             //Interacting with events
        Rescue              //Rescuing fainted NPCs
    }

    [Header("===Quest Object Values===")]
    public ItemManager.Items QuestObject = ItemManager.Items.undefined;

    [Header("===Quest Job Change Values===")]
    public TraitManager.JobClass Become;

    [Header("===Quest Location Values===")]
    public Sprite[] questLocationSprites;
    public enum Location
    {
        undefined,
        Beach,
        River,
        Mountain
    }

    [Header("===Quest Interaction Values===")]
    public Sprite[] questInteractionSprites;
    public enum Interaction
    {
        undefined,
        Trade,
        Attack,
        Invite
    }

    [Header("===Quest Event Values===")]
    public Sprite[] questEventSprites;
    public enum EventType
    {
        none,
        Merchants,
        Bandits,
        Traveler
    }

    #region Init
    private void Awake()
    {
        Debug.Log("Filling instance of " + this.name);
        singleton = this;
    }

    public void ResumeAction()
    {
        questBoardNav.ResumeNavAction();
    }

    private void Start()
    {
        Debug.Log(this.name + " getting QuestBoardNavigator component.");
        questBoardNav = GetComponent<QuestBoardNavigator>();

        InstantiateQuestTypeItems();
        InstantiateQuestObjectItems();
        InstantiateQuestLocationItems();
        InstantiateQuestJobItems();
        InstantiateQuestInteractionItems();
        InstantiateRescueNPCItems();
    }
    
    #region Instantiating UI Items
    private void InstantiateQuestTypeItems()
    {
        Debug.Log("Instantiating Quest Type Items.");

        System.Array qT = System.Enum.GetValues(typeof(QuestManager.QuestType));

        foreach (QuestManager.QuestType _qT in qT)
        {
            if (_qT != QuestManager.QuestType.undefined)
            {
                GameObject qtiObj = Instantiate(QuestTypeItemPrefab, QuestTypeList.transform);
                QuestTypeItem qti = qtiObj.GetComponent<QuestTypeItem>();

                qti.SetQuestTypeItem(questTypeSprites[(int)_qT], _qT);
            }
        }
    }
    private void InstantiateQuestObjectItems()
    {
        Debug.Log("Instantiating Quest Object Items.");

        System.Array i = System.Enum.GetValues(typeof(ItemManager.Items));

        foreach (ItemManager.Items _i in i)
        {
            if (_i != ItemManager.Items.undefined)
            {
                GameObject qtiObj = Instantiate(QuestTypeItemPrefab, QuestObjectList.transform);
                QuestTypeItem qti = qtiObj.GetComponent<QuestTypeItem>();

                qti.SetQuestObjectItem(ItemManager.singleton.ItemSprite[(int)_i], _i);
            }
        }
    }

    private void InstantiateQuestLocationItems()
    {
        Debug.Log("Instantiating Quest Location Items.");

        System.Array l = System.Enum.GetValues(typeof(Location));

        foreach (Location _l in l)
        {
            if (_l != Location.undefined)
            {
                GameObject qtiObj = Instantiate(QuestTypeItemPrefab, QuestLocationList.transform);
                QuestTypeItem qti = qtiObj.GetComponent<QuestTypeItem>();

                qti.SetQuestLocationItem(questLocationSprites[(int)_l], _l);
            }
        }
    }

    private void InstantiateRescueNPCItems()
    {
        Debug.Log("Instantiating Quest Rescue Items.");

        for(int i = 0; i < faintedNPCs.Count; i++)
        {
            GameObject qtiObj = Instantiate(QuestTypeItemPrefab, QuestRescueList.transform);
            QuestTypeItem qti = qtiObj.GetComponent<QuestTypeItem>();

            AIStateManager ai = faintedNPCs[i];

            questTypeItemsRescue.Add(qti);
            qti.SetQuestRescueItem(TraitManager.singleton.JobSprites[(int)ai.character.jobClass], ai);
        }
    }
  
    
    public void UpdateFaintedNPCs()
    {
        Debug.Log("Updating fainted NPC list.");

        Debug.Log("Currently fainted NPCs: " + faintedNPCs.Count);
        Debug.Log("Currently available Items: " + questTypeItemsRescue.Count);

        if (faintedNPCs.Count > questTypeItemsRescue.Count)
        {
            Debug.Log("There currently are more fainted NPCs than quest type items. Instantiating more items.");

            for(int i = questTypeItemsRescue.Count; i < faintedNPCs.Count; i++)
            {
                InstantiateRescueItem(faintedNPCs[i]);
            }
        }
        else if(faintedNPCs.Count == 0)
        {
            foreach(QuestTypeItem qti in questTypeItemsRescue)
            {
                qti.gameObject.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < faintedNPCs.Count; i++)
            {
                if (i > questTypeItemsRescue.Count)
                {
                    InstantiateRescueItem(faintedNPCs[i]);
                }
                else
                {
                    Sprite s = TraitManager.singleton.JobSprites[(int)faintedNPCs[i].character.jobClass];
                    AIStateManager ai = faintedNPCs[i];

                    questTypeItemsRescue[i].SetQuestRescueItem(s, ai);
                }

                questTypeItemsRescue[i].gameObject.SetActive(true);
            }
        }
        
    }

    private void InstantiateRescueItem(AIStateManager ai)
    {
        GameObject qtiObj = Instantiate(QuestTypeItemPrefab, QuestRescueList.transform);
        QuestTypeItem qti = qtiObj.GetComponent<QuestTypeItem>();
        qti.SetQuestRescueItem(TraitManager.singleton.JobSprites[(int)ai.character.jobClass], ai);
        questTypeItemsRescue.Add(qti);
    }

    private void InstantiateQuestJobItems()
    {
        Debug.Log("Instantiating Quest Job Items.");

        System.Array j = System.Enum.GetValues(typeof(TraitManager.JobClass));

        foreach (TraitManager.JobClass _j in j)
        {
            if (_j != TraitManager.JobClass.undefined && _j != TraitManager.JobClass.Hero)
            {
                GameObject qtiObj = Instantiate(QuestTypeItemPrefab, QuestJobList.transform);
                QuestTypeItem qti = qtiObj.GetComponent<QuestTypeItem>();

                qti.SetQuestJobItem(TraitManager.singleton.JobSprites[(int)_j], _j);
            }
        }
    }

    private void InstantiateQuestInteractionItems()
    {
        Debug.Log("Instantiating Quest Interaction Items.");

        System.Array i = System.Enum.GetValues(typeof(Interaction));

        foreach (Interaction _i in i)
        {
            if (_i != Interaction.undefined)
            {
                GameObject qtiObj = Instantiate(QuestTypeItemPrefab, QuestInteractionList.transform);
                QuestTypeItem qti = qtiObj.GetComponent<QuestTypeItem>();

                qti.SetQuestInteractionItem(questInteractionSprites[(int)_i], _i);
            }
        }
    }
    #endregion

    #endregion

    public void OpenQuestBoard() 
    {
        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
        questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.mainPage);
    }

    public void OpenAnnouncePanel()
    {
        Debug.Log("Opening Announce Panel. Crafting new quest.");
        quest = new Quest();
        questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.announce);
    }
    public void OpenQuestList()
    {
        Debug.Log("Opening Quest List Panel.");
        questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.questList);
    }

    public void SetQuestType(QuestType qT)
    {
        quest.questType = qT;

        switch (qT)
        {
            case QuestType.Gather:
                questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.gather);
                break;
            case QuestType.Become:
                questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.become);
                break;
            case QuestType.Scout:
                questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.scout);
                break;
            case QuestType.Rescue:
                questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.showRescueNPCs);
                break;
            case QuestType.Events:
                questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.events);
                break;
        }
    }

    public void SetQuestObject(ItemManager.Items i)
    {
        Debug.Log("Setting up quest Item: " + i);
        quest.questItem = i;

        questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.itemAmount);
    }

    public void SetReward()
    {
        Debug.Log("Setting Reward for Quest!");
        questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.reward);
    }
    
    public void ChangeGatherAmount(int value)
    {
        quest.amountToGet += value;

        if(quest.amountToGet > maxAmount)
        {
            quest.amountToGet = minAmount;
        }
        else if(quest.amountToGet < minAmount)
        {
            quest.amountToGet = maxAmount;
        }

        questBoardNav.SetCurrentItemAmount(quest);
    }

    
    public void ChangeRewardAmount(int amount)
    {
        quest.moneyReward += amount;

        if (quest.moneyReward > GameManager.singleton.PlayerInventory.money)
        {
            quest.moneyReward = 0;
        }
        else if(quest.moneyReward < 0)
        {
            quest.moneyReward = GameManager.singleton.PlayerInventory.money;
        }

        quest.questPrestige = CalculateQuestPrestige(quest.moneyReward);

        questBoardNav.SetCurrentReward(quest);
    }

    public int CalculateQuestPrestige(int money)
    {
        return Mathf.RoundToInt(money / 1.25f);
    }
 

    public void ShowResult()
    {
        questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.result);
    }

    public void SetJobChange(TraitManager.JobClass j)
    {
        quest.job = j;
        questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.reward);
    }

    public void SetLocation(Location l)
    {
        quest.questLocation = l;
        questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.reward);
    }

    public void SetRescueQuest(AIStateManager ai)
    {
        quest.NPCtoRescue = ai;
        questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.reward);
    }


    public void GenerateCraftedQuest()
    {
        Quest q = new Quest();
        q = quest;


        if(quest.questType == QuestType.Rescue)
        {
            Debug.Log($"Declaring {quest.NPCtoRescue.character.GetCharacterName()} as missing.");
            quest.NPCtoRescue.isCurrentlyMissing = true;
        }

        currentQuestsUnassigned.Add(q);

        GameManager.singleton.PlayerInventory.AddMoney(-quest.moneyReward);

        if (string.IsNullOrEmpty(quest.questName))
        {
            q.questName = $"{quest.questType}";

            if(q.questType == QuestType.Gather)
            {
                q.questName += $" {q.amountToGet}";
                q.questName += $" {q.questItem}";
            }
            else if(q.questType == QuestType.Become)
            {
                q.questName += $" {q.job}";
            }
            else if(quest.questType == QuestType.Scout)
            {
                q.questName += $" {q.questLocation}";
            }
            else if(quest.questType == QuestType.Rescue)
            {
                q.questName += $" {q.NPCtoRescue.character.GetCharacterName()}";
            }
        }

        questBoardNav.InstantiateQuestDetailsItem(q);

        questBoardNav.AddNavAction(QuestBoardNavigator.NavAction.none);

        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
        StatusManager.singleton.InsertNewStatus("New Quest available: " + q.questName);
    }


    public void SetQuestName(string newName)
    {
        quest.questName = newName;
    }


    public void AddFaintedNPC(AIStateManager ai)
    {
        if(!faintedNPCs.Contains(ai))
        {
            Debug.Log($"Adding {ai.character.GetCharacterName()} to list of fainted NPCs.");
            faintedNPCs.Add(ai);
        }
        else
        {
            Debug.Log($"Trying to add {ai.character.GetCharacterName()} to list of fainted NPCs, but list already contains this AI.");
        }
            
    }

    public void RemoveFaintedNPC(AIStateManager ai)
    {
        if (faintedNPCs.Contains(ai))
        {
            Debug.Log($"Removing {ai.character.GetCharacterName()} from list of fainted NPCs.");
            faintedNPCs.Remove(ai);
        }
        else
        {
            Debug.Log($"Trying to remove {ai.character.GetCharacterName()} from list of fainted NPCs, but list does not contain this AI.");
        }

        UpdateFaintedNPCs();
    }
}
