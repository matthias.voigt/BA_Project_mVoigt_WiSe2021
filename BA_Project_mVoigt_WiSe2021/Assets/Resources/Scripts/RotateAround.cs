using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
    [SerializeField] private bool xAxis = false;
    [SerializeField] private float xRotSpeed = 1f;

    [SerializeField] private bool yAxis = false;
    [SerializeField] private float yRotSpeed = 1f;

    [SerializeField] private bool zAxis = false;
    [SerializeField] private float zRotSpeed = 1f;
    void Update()
    {
        if (xAxis)
            transform.Rotate(Vector3.right * xRotSpeed * Time.deltaTime, Space.Self);
        if (yAxis)
            transform.Rotate(Vector3.up * yRotSpeed * Time.deltaTime, Space.Self);
        if (zAxis)
            transform.Rotate(Vector3.forward * zRotSpeed * Time.deltaTime, Space.Self);
    }
}
