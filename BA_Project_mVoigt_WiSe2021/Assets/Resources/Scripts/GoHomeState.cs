using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoHomeState : AIState
{
    public AIStateManager ai;
    public bool isGoingHome = false;
    public bool attackTown = false;
    private GameObject questBoard;
    public override AIState RunCurrentState()
    {
        SetAgentSpeed();
        SetDisplaymode();
        if (attackTown)
        {
            questBoard = GameObject.FindGameObjectWithTag("QuestBoard");
        }
        else
        {
            questBoard = GetQuestBoard();
        }

               
        if(questBoard == null)
        {
            Debug.LogError("No Questboard found!");
            return ai.TryChangeState(ai.idleState);
        }

        SetAnimation();

        if (ai.IsCharacterTooFarAwayFromObject(ai.gameObject, questBoard, 7f))
        {
            ai.rP.AddReport($"Let's head to town!");
            if (ai.hasQuest) ai.rP.AddQuestReport("So the best I could do was heading home!");
            ai.agent.SetDestination(questBoard.transform.position);
        }
        else
        {          
            if (ai.faction == AIStateManager.Faction.neutral)
            {
                StatusManager.singleton.InsertNewStatus($"{ai.character.GetCharacterName()} left the island.");
                GameManager.singleton.RemoveNPCFromList(ai);

                GameObject deadNPCList = GameObject.FindGameObjectWithTag("DeadNPCs");
                ai.gameObject.transform.SetParent(deadNPCList.transform);
                ai.gameObject.SetActive(false);
            }
            else
            {
                ai.NPCUntargetable(false);
                isGoingHome = false;
                attackTown = false;
                return ai.TryChangeState(ai.idleState);
            }


        }

        return this;
    }

    private void SetAnimation()
    {
        if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("NormalSprit_noWeapon"))
            ai.anim.Play("Sprint");
    }
    public void SetAgentSpeed()
    {
        ai.agent.speed = ai.runningSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public GameObject GetQuestBoard()
    {
        if (ai.faction == AIStateManager.Faction.enemy)
        {
            return GameObject.FindGameObjectWithTag("EnemyQuestBoard");
        }
        else if (ai.faction == AIStateManager.Faction.villager)
        {
            return GameObject.FindGameObjectWithTag("QuestBoard");
        }
        else if (ai.faction == AIStateManager.Faction.neutral)
        {
            return GameObject.FindGameObjectWithTag("NeutralQuestBoard");
        }
        else
            return null;
    }

    public void SetDisplaymode()
    {
        ai.canTalk = true;
        switch (ai.faction)
        {
            case AIStateManager.Faction.villager:
                if (ai.character.jobClass == TraitManager.JobClass.Hero)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.none;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                else
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.character;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                break;
            case AIStateManager.Faction.enemy:
                ai.currentDisplayMode = Dialogue.DisplayMode.none;
                ai.currentTextType = Dialogue.TextType.standard;
                break;
            case AIStateManager.Faction.neutral:
                if (ai.character.jobClass == TraitManager.JobClass.Adventurer)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.adventurer;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                else if (ai.character.jobClass == TraitManager.JobClass.Merchant)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.merchant;
                    ai.currentTextType = Dialogue.TextType.shop;
                }
                else if (ai.character.race == Character.Race.skeleton)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.ressurect;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                break;
        }
    }
}
