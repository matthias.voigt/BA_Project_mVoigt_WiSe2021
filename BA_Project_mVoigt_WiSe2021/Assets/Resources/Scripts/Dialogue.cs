using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "newDialogue", menuName = "New Dialogue")]
public class Dialogue : ScriptableObject
{
    [TextArea(3, 10)]
    public string[] sentences;
    public TextType textType = TextType.standard;

    public DisplayMode displayMode = DisplayMode.none;
    public TraitManager.Traits trait;
    public AIStateManager.Faction faction;
    public Character.RaceType raceType;
    public TraitManager.JobClass job;
    public int id = -1;
    public string fromFile;

    public List<string> sList = new List<string>();

    public TextType GetTextType()
    {
        return textType;
    }

    public enum TextType
    {
        standard,
        shop,
        findSleep,
        QG_Unfinished,
        QG_Failed,
        QG_Success,
        QG_Report,
        recruit,
        free,
        fainted,
        escort,
        healed,
        recruitFailed,
        freeFailed,
        steal,
        recruitFull
    }

    public enum DisplayMode
    {
        none,
        character,
        merchant,
        prisoner,
        researcher,
        exitPrison,
        lootable,
        ressurect,
        exitRuin,
        hospital,
        exitHospital,
        recruitFailed,
        adventurer,
        exitAdventurer
    }

    public void SetTextType(TextType tt)
    {
        textType = tt;
    }

    public void SetDisplayMode(DisplayMode dM)
    {
        displayMode = dM;
    }

    public void SetTrait(TraitManager.Traits t)
    {
        trait = t;
    }

    public void SetFaction(AIStateManager.Faction f)
    {
        faction = f;
    }

    public void SetRace(Character.RaceType rt)
    {
        raceType = rt;
    }
    public void SetJob(TraitManager.JobClass j)
    {
        job = j;
    }

    public void SetSentence(string s)
    {
        string[] splitArray = s.Split(char.Parse("/"));

        for (int i = 0; i < splitArray.Length; i++)
        {
            sList.Add(splitArray[i]);
        }

        sentences = sList.ToArray();
    }
}
