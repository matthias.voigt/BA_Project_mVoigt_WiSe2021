using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class ObjectHandler : MonoBehaviour
{
    public int objectID = 0;
    public string objectTag = "default";
    public int sellPrice = 5;

    private void Start() => objectTag.Trim();

    private void ScaleUp()
    {
        DOTween.Sequence().Append(transform.DOPunchScale(new Vector3(10,10,10), .5f,3,1f));
    }
    private void ScaleDown()
    {
        DOTween.Sequence().Append(transform.DOPunchScale(new Vector3(-5, -5, -5), .5f, 3, 1f));
    }


}
