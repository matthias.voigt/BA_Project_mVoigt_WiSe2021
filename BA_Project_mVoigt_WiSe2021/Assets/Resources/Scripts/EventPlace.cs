using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventPlace : MonoBehaviour
{
    public EventSize eventSize = EventSize.small;
    public EventType eventType = EventType.none;
    public QuestManager.Location location = QuestManager.Location.Beach;

    public GameObject currentEvent = null;

    public float timeTillDestroy = 172800f;
    public float timer;

    private void Start()
    {
        timer = timeTillDestroy;
        //SetRandomItemSpawn();

        timeTillItemSpawn = 0;
    }

    private void SetRandomItemSpawn()
    {
        timeTillItemSpawn = Random.Range(GameManager.singleton.minTimeTillItemSpawn, GameManager.singleton.maxTimeTillItemSpawn);
    }

    public enum EventSize
    {
        vegetables,
        ore,
        small,
        big
    }
    public enum EventType
    {
        none,
        resource,
        bandits,
        merchant,
        traveler,
        ruin
    }

    public void SetEventType(EventType et)
    {
        timer = timeTillDestroy;
        eventType = et;
    }

    public float timeTillItemSpawn = 86400f;

 
    private void Update()
    {
        if (GameManager.singleton.currentPlaystate != GameManager.PlayState.inOverworld) return;
        if (DialogueManager.singleton.currentDialogueState != DialogueManager.DialogueState.none) return;

        if (eventSize == EventSize.ore || eventSize == EventSize.vegetables)
        {
            timeTillItemSpawn -= DayManager.singleton.speed * Time.deltaTime;

            if(timeTillItemSpawn <= 0)
            {
                SetRandomItemSpawn();
                
                if(eventSize == EventSize.vegetables)
                {
                    var i = Random.Range(1, 4);
                    InstantiateItem(ItemManager.singleton.GetGameObject(i));
                }
                else if(eventSize == EventSize.ore)
                {
                    var i = Random.Range(4, 7);
                    InstantiateItem(ItemManager.singleton.GetGameObject(i));
                }

            }
        }
        if(currentEvent != null)
        {
            timer -= DayManager.singleton.speed * Time.deltaTime;

            if(timer <= 0)
            {
                timer = timeTillDestroy;
                
                if(location == QuestManager.Location.River)
                {
                    ScoutManager.singleton.currentEventsAtRiver.Remove(gameObject);
                }
                else if( location == QuestManager.Location.Beach)
                {
                    ScoutManager.singleton.currentEventsAtBeach.Remove(gameObject);
                }
                else if (location == QuestManager.Location.Mountain)
                {
                    ScoutManager.singleton.currentEventsAtMountain.Remove(gameObject);
                }

                if(currentEvent != null)
                    DestroyEvent();               
            }
        }
    }


    private void DestroyEvent()
    {
        Debug.Log("Try getting EventPlaceType");

        if(currentEvent.TryGetComponent<EventPlaceType>(out EventPlaceType ept))
        {
            foreach(SleepItemHandler sIH in ept.sleepHandlers)
            {
                if(sIH.currentObject != null)
                {
                    sIH.ExitSleeping();
                }
            }
            foreach(Prison prison in ept.prisons)
            {
                if(prison.currentObj != null)
                {
                    StatusManager.singleton.InsertNewStatus($"{prison.currentObj.character.GetCharacterName()} vanished in Prison.");
                    prison.currentObj.faintState.DestroyThisObject();
                    prison.currentObj = null;
                }
            }

            foreach(AIStateManager ai in ept.Characters)
            {
                ai.currentState = ai.TryChangeState(ai.goHomeState);
            }

            StartCoroutine(WaitForSeconds());
        }
        else
        {
            Debug.LogError("No EventPlaceTypeFound!");
        }
    }

    IEnumerator WaitForSeconds()
    {
        Debug.Log($"Waiting for 10 seconds destroying {currentEvent.name}.");
        yield return new WaitForSeconds(10f);
        StatusManager.singleton.InsertNewStatus($"{eventType} at the {location} disappeared.");
        Destroy(currentEvent);
        currentEvent = null;
    }

    private void InstantiateItem(GameObject item)
    {
        int amount = Random.Range(1, 6);

        Vector3 pos = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);
        for (int i = 0; i < amount; i++)
        {
            Instantiate(item, pos, Quaternion.identity);
        }
    }
}
