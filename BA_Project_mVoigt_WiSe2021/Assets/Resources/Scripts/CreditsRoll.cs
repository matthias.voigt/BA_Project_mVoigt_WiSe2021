using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsRoll : MonoBehaviour
{
    public float speed = 250f;
    private float spaceBar;
    public GameObject effect;
    public GameObject title;

    private void Start()
    {
        effect.SetActive(true);
        title.SetActive(true);
    }

    void Update()
    {

        if(transform.localPosition.y < 22800)
            transform.position += new Vector3(0, (speed + spaceBar) * Time.deltaTime, 0);

        

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            spaceBar = 750f;
        }
        else
        {
            spaceBar = 0;
        }
    }

    
}
