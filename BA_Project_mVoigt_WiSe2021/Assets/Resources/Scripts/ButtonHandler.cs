using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonHandler : MonoBehaviour
{
    public GameObject descObj;
    public Transform showPos;
    public Transform hidePos;

    private Vector3 startScale;

    private void Start()
    {
        startScale = transform.localScale;
    }

    private void OnDisable()
    {
        if(descObj != null)
        {
            descObj.transform.position = hidePos.transform.position;
        }
    }

    public void OnPointerEnter()
    {
        AudioManager.singleton.PlayQuestBoardHoverPageSFX();

        //BUG: If hovering too fast over object it scales up!
        if(transform.localScale.x == startScale.x)
            DOTween.Sequence().Append(transform.DOPunchScale(new Vector3(.2f, .2f, .2f), .5f, 3, 1f));

        if(descObj != null)
            descObj.transform.DOMove(showPos.position, 0.5f);
    }

    public void OnPointerExit()
    {

        if(descObj != null)
            descObj.transform.DOMove(hidePos.position, 0.5f);
    }
}
