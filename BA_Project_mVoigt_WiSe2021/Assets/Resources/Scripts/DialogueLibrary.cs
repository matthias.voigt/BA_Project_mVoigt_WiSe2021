using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueLibrary : MonoBehaviour
{
    public List<Dialogue> Standard = new List<Dialogue>();
    public List<Dialogue> Villagers = new List<Dialogue>();
    public List<Dialogue> Adventurers = new List<Dialogue>();
    public List<Dialogue> Guards = new List<Dialogue>();
    public List<Dialogue> Researchers = new List<Dialogue>();
    public List<Dialogue> Scouts = new List<Dialogue>();

    [Header("SleepStates")]
    public List<Dialogue> FindSleepAsHomeless = new List<Dialogue>();
    public List<Dialogue> FindSleepAtHome = new List<Dialogue>();

    [Header("Gather Quest")]
    public List<Dialogue> QG_Unfinished = new List<Dialogue>();
    public List<Dialogue> QG_Failed = new List<Dialogue>();
    public List<Dialogue> QG_Report = new List<Dialogue>();


}
