using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Quest 
{

    #region oldCode
    [Header("QuestName")]
    public string questName;
    [Header("Quest Type")]
    public QuestManager.QuestType questType = QuestManager.QuestType.undefined;
    public TraitManager.JobClass job = TraitManager.JobClass.undefined;
    public QuestManager.Interaction interaction = QuestManager.Interaction.undefined;
    [Header("Amount to do")]
    public int amountToGet = 1;
    [Header("Money Reward")]
    public int moneyReward = 0;
    public int questPrestige = 0;
    [Header("Quest target")]
    public ItemManager.Items questItem = ItemManager.Items.undefined;
    [Header("Quest Location")]
    public QuestManager.Location questLocation = QuestManager.Location.undefined;


    [Header("===Rescue Quest===")]
    public AIStateManager NPCtoRescue;

    [Header("Quest Holder")]
    public AIStateManager QuestHolder = null;
    public QuestDisplayer questDisplayer = null;

    public bool questFinished = false;
    public bool questFailed = false;

    public string GetObjectTag()
    {
        string objectTag;

        switch (questItem)
        {
            case ItemManager.Items.undefined:
                objectTag = "none";
                return objectTag;
            case ItemManager.Items.Tomato:
                objectTag = "tomato".Trim();
                return objectTag;
            case ItemManager.Items.Potato:
                objectTag = "potato".Trim();
                return objectTag;
            case ItemManager.Items.Pumpkin:
                objectTag = "pumpkin".Trim();
                return objectTag;
            case ItemManager.Items.Iron:
                objectTag = "iron".Trim();
                return objectTag;
            case ItemManager.Items.Silver:
                objectTag = "silver".Trim();
                return objectTag;
            case ItemManager.Items.Gold:
                objectTag = "gold".Trim();
                return objectTag;
            case ItemManager.Items.Skull:
                objectTag = "skull".Trim();
                return objectTag;
            default:
                objectTag = "None";
                return objectTag;
        }
    }

    #endregion
}
