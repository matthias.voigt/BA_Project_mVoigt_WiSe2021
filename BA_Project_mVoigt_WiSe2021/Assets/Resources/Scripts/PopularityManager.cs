using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopularityManager : MonoBehaviour
{
    public Slider monsterSlider;
    public float monsterPopularity = 50f;

    public Slider humanSlider;
    public float humanPopularity = 50f;

    public static PopularityManager singleton;

    private void Awake()
    {
        singleton = this;
        humanPopularity = humanSlider.value;
        monsterPopularity = humanSlider.value;
    }

    public void ChangeHumanPopularity(int value)
    {
        if (value < 0) StatusManager.singleton.InsertNewStatus("Humans seem to like you less.");
        else if (value > 0) StatusManager.singleton.InsertNewStatus("Humans seem to like you more.");

        humanPopularity += value;
        if (humanPopularity > 100) humanPopularity = 100;
        if (humanPopularity < 0) humanPopularity = 0;
        humanSlider.value = humanPopularity;
        Debug.Log($"Human popularity now is: {humanPopularity}.");
    }

    public void ChangeMonsterPopularity(int value)
    {
        if(value < 0) StatusManager.singleton.InsertNewStatus("Monsters seem to like you less.");
        else if(value > 0) StatusManager.singleton.InsertNewStatus("Monsters seem to like you more.");


        if (monsterPopularity > 100) monsterPopularity = 100;
        if (monsterPopularity < 0) monsterPopularity = 0;
        monsterPopularity += value;
        monsterSlider.value = monsterPopularity;       
        Debug.Log($"Monsters popularity now is: {monsterPopularity}.");
    }

    public enum PopularityAction
    {
        rescueSuccessful,           //+1
        rescueFailed,               //-1
        healedFromInjuries,         //+5
        lootForItems,               //-5
        lootForMoney,               //-5
        imprison,                   //-2
        recruitFromPrison,          //+1
        freeForMoneyFromPrison,     //-1
        recruitFromRuin,            //+3
        releaseSoul,                //+5
        freeFromHospital,           //+3
        steal,                      //-5
        questsuccessful,            //+3
        leveledUp,
        leveledDown,
        questFailed


    }

    public void ChangePopularity(PopularityAction pA, Character.RaceType rT)
    {

        if(pA == PopularityAction.recruitFromRuin)
        {
            if(rT == Character.RaceType.human)
            {
                ChangeHumanPopularity(1);
                ChangeMonsterPopularity(GetPopularityValue(PopularityAction.recruitFromRuin));
            }
            else if(rT == Character.RaceType.monster)
            {
                ChangeMonsterPopularity(1);
                ChangeMonsterPopularity(GetPopularityValue(PopularityAction.recruitFromRuin));
            }
        }
        else if(rT == Character.RaceType.human)
        {
            ChangeHumanPopularity(GetPopularityValue(pA));
        }
        else if(rT == Character.RaceType.monster)
        {
            ChangeMonsterPopularity(GetPopularityValue(pA));
        }
        else
        {
            Debug.LogWarning("Tried to change popularity of undefined racetype.");
        }
    }

    private int GetPopularityValue(PopularityAction pA)
    {
        switch (pA)
        {
            case PopularityAction.rescueSuccessful:
                return 10;
            case PopularityAction.lootForItems:
                return -10;
            case PopularityAction.lootForMoney:
                return -10;
            case PopularityAction.healedFromInjuries:
                return 10;
            case PopularityAction.recruitFromPrison:
                return 10;
            case PopularityAction.freeForMoneyFromPrison:
                return -5;
            case PopularityAction.recruitFromRuin:
                return 10;
            case PopularityAction.releaseSoul:
                return 10;
            case PopularityAction.imprison:
                return -5;
            case PopularityAction.rescueFailed:
                return -3;
            case PopularityAction.steal:
                return -10;
            case PopularityAction.questsuccessful:
                return 5;
            case PopularityAction.leveledUp:
                return 10;
            case PopularityAction.leveledDown:
                return -3;
            case PopularityAction.questFailed:
                return -3;
            default:
                return 0;
        }
    }
}
