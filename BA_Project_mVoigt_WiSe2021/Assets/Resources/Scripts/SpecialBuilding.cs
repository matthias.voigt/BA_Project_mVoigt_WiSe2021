using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialBuilding : MonoBehaviour
{
    public Transform enterPosition;
    public Transform tradePosition;
    private AIStateManager aiState;
    public Character owner;
    public bool isOpen = false;
    public BuildingType buildingType = BuildingType.shop;
    public Camera cam;
    public Camera mainCam;


    [Header("Research")]
    public Construction constructToResearch = null;
    public float researchTime;
    public bool sendStatus = false;

    private void Start()
    {
        mainCam = GameManager.singleton.mainCamera;
    }

    public enum BuildingType
    {
        shop,
        researchLab,
    } 
    public void EnterBuilding(AIStateManager _aiState)
    {
        _aiState.canTalk = false;

        //This building is already owned
        if (owner != null && aiState.character != owner)
        {
            Debug.Log($"{_aiState.character.GetCharacterName()} cannot enter {gameObject.name}. This building belongs to {owner.GetCharacterName()}.");
            _aiState.currentState = aiState.TryChangeState(aiState.idleState);
        }
        //This Building is empty
        else if (owner == null)
        {
            if (_aiState.character.jobClass == TraitManager.JobClass.Merchant)
            {
                //REPORT
                _aiState.rP.AddReport($"Let's open my shop!");
            }
            else if (_aiState.character.jobClass == TraitManager.JobClass.Researcher)
            {
                //REPORT
                _aiState.rP.AddReport($"Let's open the research lab!");
            }

            Debug.Log($"{_aiState.character.GetCharacterName()} now owns {gameObject.name}.");
            owner = _aiState.character;
            _aiState.merchState.ownedMerchBuild = this;
            _aiState.researchState.ownedLabBuild = this;
            aiState = _aiState;
            
            SetUp();
        }
        //This Building is owned by entering character
        else if(owner == aiState.character)
        {
            aiState = _aiState;
            SetUp();
        }       
    }

    private void SetUp()
    {
        if(buildingType == BuildingType.shop)
        {
            aiState.currentTextType = Dialogue.TextType.shop;
            aiState.currentDisplayMode = Dialogue.DisplayMode.merchant;
            aiState.rP.AddReport($"Let's open my shop!");
        }
        else if(buildingType == BuildingType.researchLab)
        {
            aiState.currentTextType = Dialogue.TextType.shop;
            aiState.currentDisplayMode = Dialogue.DisplayMode.researcher;
            aiState.rP.AddReport($"Let's open the research lab!");
        }

        isOpen = true;
        aiState.specialBuilding = this;
        aiState.agent.Warp(enterPosition.position);
        aiState.gameObject.transform.LookAt(gameObject.transform);
    }

    public void ExitShop()
    {
        aiState.merchState.ownedMerchBuild = null;
        aiState.researchState.ownedLabBuild = null;

        aiState.NPCUntargetable(false);
        aiState.currentTextType = Dialogue.TextType.standard;
        aiState.currentDisplayMode = Dialogue.DisplayMode.character;
        aiState.specialBuilding = null;
        aiState.canTalk = true;
        isOpen = false;
       
    }

    public void AbandonShop()
    {      
        if (isOpen)
        {
            isOpen = false;
            aiState.currentDisplayMode = Dialogue.DisplayMode.character;
            aiState.currentState = aiState.TryChangeState(aiState.idleState);
        }

        owner = null;
        aiState = null;      
    }

    public void InteractWithBuilding()
    {
        //mainCam.gameObject.SetActive(false);
        //cam.gameObject.SetActive(true);

        DialogueManager.singleton.TryChangeDialogueState(DialogueManager.DialogueState.inDialogue, aiState, aiState.talkingState);
    }
    public void StopInteractingWithBuilding()
    {
        mainCam.gameObject.SetActive(true);
        cam.gameObject.SetActive(false);
    }

    public void SetResearchConstruct(Construction c)
    {
        Debug.Log("Setting up research construction: " + c.GetConstructName());

        sendStatus = false;
        constructToResearch = c;
        researchTime = c.researchTime;
    }

    private void Update()
    {
        if(isOpen && buildingType == BuildingType.researchLab && constructToResearch != null)
        {        
            //Research available?
            if (constructToResearch.GetUnlockBool()) return;

            //Reduce satisfaction
            owner.ReduceSatisfactionBy(GameManager.singleton.GetSatisfactionReduction(owner));

            //Not enough satisfaction
            if(owner.GetProsperitySatisfaction() <= 0)
            {
                aiState.rP.AddReport("I cannot continue my research. I am too tired.");
                Debug.Log($"{owner.GetCharacterName()} is too unsatisfied to do research.");
                return;
            }

            //Researching
            int authority = owner.GetAuthority();
            researchTime -= Time.deltaTime * (DayManager.singleton.speed + authority);

            aiState.rP.AddReport($"Hm...what about a {constructToResearch.GetConstructName()}? Let's do some research...");

            //Research complete
            if (researchTime <= 0)
            {
                if (!sendStatus)
                {
                    sendStatus = true;

                    aiState.rP.AddReport("I finished my research!");

                    Debug.Log("Research complete: " + constructToResearch.GetConstructName());
                    constructToResearch.UnlockBuilding(true);
                    ResearchManager.singleton.RemoveConstructFromCurrentResearch(constructToResearch);
                    ResearchManager.singleton.GenerateResearchList();
                    StatusManager.singleton.InsertNewStatus("Research complete: " + constructToResearch.GetConstructName());
                }

            }
        }
        else if (isOpen && buildingType == BuildingType.researchLab && constructToResearch == null)
        {
            aiState.rP.AddReport("What shall I research?");
        }
    }
}
