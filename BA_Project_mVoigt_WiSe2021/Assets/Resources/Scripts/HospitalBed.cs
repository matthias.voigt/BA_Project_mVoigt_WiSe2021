using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalBed : MonoBehaviour
{
    public float recoveryRate;
    public AIStateManager currentHolder = null;

    [Header("Transforms")]
    public Transform sleepPosition;
    public Transform enterPosition;
    public Vector3 SleepRotation = new Vector3(0, 90, 0);


    public void SleepOnObject(AIStateManager ai)
    {
        currentHolder = ai;
        ai.recoverState.hp = this;

        ai.currentState = ai.TryChangeState(ai.recoverState);
      
        currentHolder.agent.Warp(sleepPosition.position);

        ai.transform.SetParent(gameObject.transform);

        ai.transform.localEulerAngles = SleepRotation;
    }

    public void ExitHospitalBed()
    {
        PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.healedFromInjuries, currentHolder.character.raceType);

        currentHolder.NPCUntargetable(false);
        currentHolder.previewsState = currentHolder.idleState;

        if (currentHolder.faction == AIStateManager.Faction.villager)
        {

            currentHolder.currentDisplayMode = Dialogue.DisplayMode.exitHospital;
            currentHolder.currentTextType = Dialogue.TextType.healed;
        }
        else if(currentHolder.faction == AIStateManager.Faction.enemy)
        {
            currentHolder.currentTextType = Dialogue.TextType.healed;
            currentHolder.currentDisplayMode = Dialogue.DisplayMode.hospital;
        }
        else if(currentHolder.faction == AIStateManager.Faction.neutral)
        {
            currentHolder.currentTextType = Dialogue.TextType.healed;
            currentHolder.currentDisplayMode = Dialogue.DisplayMode.hospital;
        }
        
        currentHolder.character.AddSatisfaction(Random.Range(50, 100));
        currentHolder.recoverState.hp = null;
        currentHolder.transform.SetParent(null);
        currentHolder.agent.Warp(enterPosition.position);
        currentHolder = null;
    }
}
