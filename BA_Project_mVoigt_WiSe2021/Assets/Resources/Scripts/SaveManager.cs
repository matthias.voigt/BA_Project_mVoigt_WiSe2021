using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SaveManager : MonoBehaviour
{
    public List<BuildingSaveData> buildObjects = new List<BuildingSaveData>();
    public List<Construction> constructions = new List<Construction>();

    public SaveData saveFile = null;
    [SerializeField] private AIStateManager HeroNPC;
    public static SaveManager singleton;

    private void Awake()
    {
        singleton = this;

    }

    private void Start()
    {
        GenerateConstructIDs();

        int l = PlayerPrefs.GetInt("load", 0);

        if (l == 0)
        {
            Debug.Log("Starting new game.");
        }
        else if (l == 1)
        {
            Debug.Log("Loading game data.");

            SaveManager.singleton.LoadSaveFile();
        }
    }


    private void GenerateConstructIDs()
    {
        constructions.Clear();

        foreach(Construction c in ConstructionManager.singleton.constructsHomes) 
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsSpecial)
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsWalls)
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsFences)
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsPaths)
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsFlowers)
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsRocks)
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsStones)
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsTrees)
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsBenches)
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsLanterns)
        {
            constructions.Add(c);
        }
        foreach (Construction c in ConstructionManager.singleton.constructsDiverse)
        {
            constructions.Add(c);
        }

        for (int i = 0; i < constructions.Count; i++)
        {
            constructions[i].SetID(i);
        }
    }

    private int index = 0;
    public void AddBuildID(int id, Vector3 pos, Quaternion rot, string objectTag, BuildingHandler bH)
    {
        BuildingSaveData bsd = new BuildingSaveData();
        bsd.id = id;
        bsd.position = pos;
        bsd.rotation = rot;
        bsd.objectTag = objectTag;

        if(bH != null) { bH.buildIndex = index; }         
        bsd.index = index;
        index++;

        buildObjects.Add(bsd);
    }

    public void SaveFile()
    {
        StatusManager.singleton.InsertNewStatus("Game saved.");
        MenuManager.singleton.Return();
        SaveData sD = new SaveData();


        //Saving Build Data
        sD.bsData = new List<BuildingSaveData>(buildObjects);

        //Saving Time Data
        sD.currentTimeState = (int)DayManager.singleton.currentTimeState;
        sD.currentTime = DayManager.singleton.time;
        sD.currentDay = DayManager.singleton.days;

        //Saving Player Data
        sD.playerPos = GameManager.singleton.playerObj.transform.position;
        Inventory inv = GameManager.singleton.PlayerInventory;
        sD.playerMoney = inv.money;
        for (int i = 0; i < inv.items.Count; i++)
        {
            sD.playerItems.Add(inv.items[i].objectID);
        }


        List<AIStateManager> aiToSave = new List<AIStateManager>(GameManager.singleton.villagers);
        
        foreach(AIStateManager ai in GameManager.singleton.enemies)
        {
            aiToSave.Add(ai);
        }

        //Saving villager Data
        for(int i = 0; i < aiToSave.Count; i++)
        {


            VillagerData vD = new VillagerData();

            //Setting Hero reference
            if (aiToSave[i].character.jobClass == TraitManager.JobClass.Hero)
            {
                Debug.Log("Villager of class hero is Hero or Evil Mage!");
                vD.isHero = true;
            }

            //Saving Name
            vD.cName = aiToSave[i].character.GetCharacterName();

            //Saving Race
            vD.raceID = (int)aiToSave[i].character.race;
            vD.raceTypeID = (int)aiToSave[i].character.raceType;

            //Saving Job
            vD.jobID = (int)aiToSave[i].character.jobClass;
            
            //Saving Traits
            for(int t = 0; t < aiToSave[i].character.traits.Count; t++)
            {
                vD.traitID.Add((int)aiToSave[i].character.traits[t]);
            }

            vD.satisfaction = aiToSave[i].character.GetProsperitySatisfaction();
            vD.authority = aiToSave[i].character.GetAuthority();
            vD.prestige = aiToSave[i].character.GetPrestige();
            vD.qExp = aiToSave[i].character.GetQuestExp();


            //Saving villager inventory
            Inventory vInv = aiToSave[i].inventory;
            vD.money = vInv.money;

            for (int z = 0; z < vInv.items.Count; z++)
            {
                vD.items.Add(vInv.items[z].objectID);
            }

            //Appearance
            vD.meshColor1 = aiToSave[i].character.aM.meshColor1;
            vD.meshColor2 = aiToSave[i].character.aM.meshColor2;
            vD.meshColor3 = aiToSave[i].character.aM.meshColor3;
            vD.c1 = aiToSave[i].character.aM.c1;
            vD.c2 = aiToSave[i].character.aM.c2;
            vD.c3 = aiToSave[i].character.aM.c3;

            vD.beltID = aiToSave[i].character.aM.GetBeltID();
            vD.clothsID = aiToSave[i].character.aM.GetClothID();
            vD.glovesID = aiToSave[i].character.aM.GetGlovesID();
            vD.shoesID  = aiToSave[i].character.aM.GetShoesID();
            vD.shouldersID = aiToSave[i].character.aM.GetShoulderID();
            vD.hairID = aiToSave[i].character.aM.GetHairID();
            vD.faceID = aiToSave[i].character.aM.GetFaceID();
            vD.hatsID = aiToSave[i].character.aM.GetHatID();

            //Save current AI State
            vD.position = aiToSave[i].gameObject.transform.position;
            vD.rotation = aiToSave[i].gameObject.transform.rotation;
            vD.currentStateID = aiToSave[i].GetCurrentStateID();
            vD.currentDisplayMode = (int)aiToSave[i].currentDisplayMode;
            vD.currentTextType = (int)aiToSave[i].currentTextType;
            vD.factionID = (int)aiToSave[i].faction;


            //Saving Home reference
            if (aiToSave[i].character.currentHome != null)
            {
                vD.houseOwnerIndex = aiToSave[i].character.currentHome.buildIndex;
            }
            else
                vD.houseOwnerIndex = -1;

            //Adding saved vData to save Data
            sD.vData.Add(vD);
        }


        string dataToText = JsonUtility.ToJson(sD);
        //System.IO.File.Delete(Application.dataPath + "/Resources/SaveData/saveFile");
        System.IO.File.WriteAllText(Application.dataPath + "/Resources/saveData/saveFile.json", dataToText);

#if (UNITY_EDITOR)
        AssetDatabase.Refresh();
#endif
    }




    public void LoadSaveFile()
    {
        TextAsset tA;
        //tA = (TextAsset)AssetDatabase.LoadAssetAtPath(Application.dataPath + "/saveData/saveFile.json", typeof(TextAsset));

        tA = Resources.Load<TextAsset>("saveData/saveFile");
        saveFile = JsonUtility.FromJson<SaveData>(tA.text);

        InstantiateBuildingsFromSave();
        

        //Loading Time
        DayManager.singleton.days = saveFile.currentDay;
        DayManager.singleton.currentTimeState = (DayManager.CurrentTimeState)saveFile.currentTimeState;
        DayManager.singleton.time = saveFile.currentTime;

        //Loading PlayerData
        Inventory inv = GameManager.singleton.PlayerInventory;
        inv.money = saveFile.playerMoney;
        GameManager.singleton.playerObj.transform.position = saveFile.playerPos;

        for(int i = 0; i < saveFile.playerItems.Count; i++)
        {
            GameObject item = Instantiate(ItemManager.singleton.items[saveFile.playerItems[i]+1]);
            ObjectHandler oH = item.GetComponent<ObjectHandler>();
            inv.AddItemToInventory(oH);
            item.SetActive(false);
        }

        //Enabling loadfrom Data for character component
        PersonalityManager.singleton.characterPrefab.GetComponent<Character>().loadFromData = true;

        //Loading Villager Data
        for (int i = 0; i < saveFile.vData.Count; i++)
        {
            GameObject NPC = HeroNPC.gameObject;

            if (!saveFile.vData[i].isHero)
            {
                //Instantiate Villager
                NPC = (Instantiate(
                    PersonalityManager.singleton.characterPrefab,
                    saveFile.vData[i].position,
                    saveFile.vData[i].rotation));
            }
            else
            {
                HeroNPC.agent.Warp(saveFile.vData[i].position);
                HeroNPC.transform.rotation = saveFile.vData[i].rotation;
            }

            //Get AI Component
            AIStateManager ai = NPC.GetComponent<AIStateManager>();
                       
            //Set Character Data
            ai.character.characterName = saveFile.vData[i].cName;
            NPC.name = ai.character.GetCharacterName();
            ai.character.race = (Character.Race)saveFile.vData[i].raceID;
            ai.character.raceType = (Character.RaceType)saveFile.vData[i].raceTypeID;
            ai.character.jobClass = (TraitManager.JobClass)saveFile.vData[i].jobID;

            ai.character.satisfaction = saveFile.vData[i].satisfaction;
            ai.character.authority = saveFile.vData[i].authority;
            ai.character.prestige = saveFile.vData[i].prestige;
            ai.character.questExperience = saveFile.vData[i].qExp;

            //Set Traits
            ai.character.traits.Clear();
            for (int tID = 0; tID < saveFile.vData[i].traitID.Count; tID++)
            {
                ai.character.traits.Add((TraitManager.Traits)saveFile.vData[i].traitID[tID]);
            }

            //Set Appearance
            ai.character.aM.meshColor1 = saveFile.vData[i].meshColor1;
            ai.character.aM.meshColor2 = saveFile.vData[i].meshColor2;
            ai.character.aM.meshColor3 = saveFile.vData[i].meshColor3;
            ai.character.aM.meshBrightness = saveFile.vData[i].meshBrightness;

            ai.character.aM.c1 = saveFile.vData[i].c1;
            ai.character.aM.c2 = saveFile.vData[i].c2;
            ai.character.aM.c3 = saveFile.vData[i].c3;

            ai.character.aM.hairID = saveFile.vData[i].hairID;
            ai.character.aM.hatsID = saveFile.vData[i].hatsID;
            ai.character.aM.faceID = saveFile.vData[i].faceID;
            ai.character.aM.beltID = saveFile.vData[i].beltID;
            ai.character.aM.clothsID = saveFile.vData[i].clothsID;
            ai.character.aM.glovesID = saveFile.vData[i].glovesID;
            ai.character.aM.shouldersID = saveFile.vData[i].shouldersID;
            ai.character.aM.shoesID = saveFile.vData[i].shoesID;



            //AI States
            ai.currentDisplayMode = (Dialogue.DisplayMode)saveFile.vData[i].currentDisplayMode;
            ai.currentTextType = (Dialogue.TextType)saveFile.vData[i].currentTextType;
            ai.faction = (AIStateManager.Faction)saveFile.vData[i].factionID;


            //Villager Items
            ai.inventory.money = saveFile.vData[i].money;
            for (int items = 0; items < saveFile.vData[i].items.Count; items++)
            {
                GameObject item = Instantiate(ItemManager.singleton.items[saveFile.vData[i].items[items] + 1]);
                ObjectHandler oH = item.GetComponent<ObjectHandler>();
                ai.inventory.AddItemToInventory(oH);
                item.SetActive(false);
            }

            

            ai.character.aM.LoadAppearance(saveFile.vData[i]);

            GameManager.singleton.AddNPCToList(ai, ai.faction);

            //Setting home reference
            if (saveFile.vData[i].houseOwnerIndex != -1)
            {
                List<BuildingHandler> bHs = new List<BuildingHandler>(FindObjectsOfType<BuildingHandler>());

                foreach(BuildingHandler _bH in bHs)
                {
                    if(_bH.buildIndex == saveFile.vData[i].houseOwnerIndex)
                    {
                        _bH.SetOwner(ai.character);
                    }
                }
            }
        }

        //Disabling loadfrom Data for character component
        PersonalityManager.singleton.characterPrefab.GetComponent<Character>().loadFromData = false;
    }


    private void InstantiateBuildingsFromSave()
    {
        for(int i = 0; i < saveFile.bsData.Count; i++)
        {

            GameObject b = Instantiate(
                constructions[saveFile.bsData[i].id].GetConstruct(),
                saveFile.bsData[i].position,
                saveFile.bsData[i].rotation
                );
            b.name = constructions[saveFile.bsData[i].id].GetConstructName();

            if (b.tag == "NoCollision")
            {
                foreach (Transform child in b.transform)
                {
                    child.gameObject.layer = 7;
                }

                b.layer = 7;
            }
            else if (b.tag == "Terrain")
            {
                foreach (Transform child in b.transform)
                {
                    child.gameObject.layer = 6;
                }

                b.layer = 6;
            }
            else
            {
                foreach (Transform child in b.transform)
                {
                    child.gameObject.layer = 8;
                }

                b.layer = 8;

                BuildingHandler bH = b.AddComponent<BuildingHandler>();
                bH.SetBuildingInfo(constructions[saveFile.bsData[i].id]);
            }

            if (b.TryGetComponent<BuildingHandler>(out BuildingHandler _bH))
            {
                SaveManager.singleton.AddBuildID(saveFile.bsData[i].id, saveFile.bsData[i].position, saveFile.bsData[i].rotation, saveFile.bsData[i].objectTag, _bH);
            }
            else
            {
                SaveManager.singleton.AddBuildID(saveFile.bsData[i].id, saveFile.bsData[i].position, saveFile.bsData[i].rotation, saveFile.bsData[i].objectTag, null);
            }

        }
        
        NavigationBaker.singleton.UpdateNavMesh();
    }

}
