using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{

    private Animator anim;
    public bool opened = false;
    private AudioSource audiosource;
    [SerializeField] private ChestReward chestReward;
    [SerializeField] private int minAmountOfGold = 100;
    [SerializeField] private int maxAmountOfGold = 500;
    private enum ChestReward
    {
        randomItem,
        gold
    }


    void Start()
    {
        audiosource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }


    public void OpenChest()
    {
        
        if (opened) return;
        opened = true;
        Debug.Log("Opening chest! Changing layer to prevent raycast targeting.");
        gameObject.layer = 0;
        anim.SetBool("open", true);

        audiosource.volume = OptionsManager.singleton.sfxValue;
        audiosource.Play();

        switch (chestReward)
        {
            case ChestReward.gold:
                int g = Random.Range(minAmountOfGold, maxAmountOfGold + 1);
                GameManager.singleton.PlayerInventory.AddMoney(g);
                StatusManager.singleton.InsertNewStatus($"Obtained {g} gold.");               
                break;

            case ChestReward.randomItem:
                GameObject o = ItemManager.singleton.GetItem(Random.Range(1, ItemManager.singleton.items.Length));
                Vector3 pos = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
                Instantiate(o, pos, Quaternion.identity);
                break;
        }

        gameObject.tag = "Untagged";

        StartCoroutine(DestroyThis());
    }

    IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);
    }

}
