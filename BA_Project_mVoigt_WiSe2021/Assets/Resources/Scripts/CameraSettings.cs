using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cinemachine;

public class CameraSettings : MonoBehaviour
{

    public CinemachineFreeLook cam;

    private void Start()
    {
        cam = GetComponent<CinemachineFreeLook>();

        OptionsManager.singleton.SetOption += SetCameraSettings;
        SetCameraSettings();
    }
    private void OnDestroy()
    {
        OptionsManager.singleton.SetOption -= SetCameraSettings;
    }

    private void SetCameraSettings()
    {
        cam.m_YAxis.m_MaxSpeed = OptionsManager.singleton.mouseSensitivityValueY;
        cam.m_XAxis.m_MaxSpeed = OptionsManager.singleton.mouseSensitivityValueX;

        cam.m_YAxis.m_InvertInput = OptionsManager.singleton.invertY;
        cam.m_XAxis.m_InvertInput = OptionsManager.singleton.invertX;
    }
}
