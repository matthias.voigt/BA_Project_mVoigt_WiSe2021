using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BGM : MonoBehaviour
{
    public AudioSource bgm;
  

    private void Start()
    {
        bgm = GetComponent<AudioSource>();
        OptionsManager.singleton.SetOption += SetAudio;

        SetAudio();
    }
    private void OnDestroy()
    {
        OptionsManager.singleton.SetOption -= SetAudio;
    }

    private void SetAudio()
    {
        bgm.volume = OptionsManager.singleton.bgmValue;
    }
}
