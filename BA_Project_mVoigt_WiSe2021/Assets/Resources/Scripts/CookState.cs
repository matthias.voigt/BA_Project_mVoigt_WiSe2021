using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookState : AIState
{
    public AIStateManager ai;
    public CookingStation cookStation = null;
    public ObjectHandler currentItem = null;
    public override AIState RunCurrentState()
    {
        SetAnimation();
        SetAgentSpeed();
        SetDisplayMode();

        if (DoCookStationsExist())
        {
            ai.rP.AddReport("I am hungry...");

            if(cookStation == null)
            {
                cookStation = GetNeareastCookStation();
            }

            //Going to cooking station
            if(ai.IsCharacterTooFarAwayFromObject(ai.gameObject, cookStation.gameObject, 2f))
            {
                ai.agent.SetDestination(cookStation.gameObject.transform.position);
            }
            //Nearby cooking station
            else if(ai.IsCharacterNearbyObject(ai.gameObject, cookStation.gameObject, 2f))
            {
                //NPC has items to cook
                if(ai.inventory.pumpkins > 0 || ai.inventory.potatoes > 0 || ai.inventory.tomatos > 0)
                {
                    if(currentItem == null)
                    {
                        currentItem = GetIngredientFromInventory();
                        return this;                     
                    }
                    else
                    {
                        StartCoroutine(CookingSomething());
                        return ai.TryChangeState(ai.idleState);
                    }

                }
                else
                {
                    if (ai.hasQuest)
                    {
                        ai.rP.AddQuestReport("I wanted to cook a delicious soup, but I had no ingredients...So I went on with my quest with an empty stomach...");
                        return ai.TryChangeState(ai.ChangeToQuestingState());
                    }
                    ai.rP.AddReport("I wanted to cook something, but I have no ingredients!");
                    return ai.TryChangeState(ai.idleState);
                }
            }
            
        }
        else
        {
            if (ai.hasQuest)
            {
                ai.rP.AddQuestReport("I wanted to cook a delicious soup, but we have no campfire in town...So I went on with my quest with an empty stomach...");
                return ai.TryChangeState(ai.ChangeToQuestingState());
            }
            ai.rP.AddReport("I wish we had a campfire to cook something.");
            return ai.TryChangeState(ai.idleState);
        }

        return this;
    }

    private bool DoCookStationsExist()
    {
        if (FindObjectOfType<CookingStation>()) return true;
        else return false;
    }

    private CookingStation GetNeareastCookStation()
    {
        CookingStation[] allCookStations = FindObjectsOfType<CookingStation>();
        CookingStation closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = ai.gameObject.transform.position;

        foreach (CookingStation go in allCookStations)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;

            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }

        return closest;
    }

    private ObjectHandler GetIngredientFromInventory()
    {
        List<int> l = new List<int>();

        if (ai.inventory.potatoes > 0) l.Add(0);
        if (ai.inventory.pumpkins > 0) l.Add(1);
        if (ai.inventory.tomatos > 0) l.Add(2);

        int rng = l[Random.Range(0, l.Count)];

        ObjectHandler oH = null;

        for(int i = 0; i < ai.inventory.items.Count; i++)
        {
            if(ai.inventory.items[i].objectID == rng)
            {
                oH = ai.inventory.items[i];
            }
        }

        Debug.Log("Cooking " + oH.gameObject.name);

        return oH;
    }

    private void SetAnimation()
    {
        if (ai.agent.velocity.magnitude < 0.15f)
        {
            if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                ai.anim.Play("Idle");
        }
        else
        {
            if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                ai.anim.Play("Walk");
        }
    }

    public void SetAgentSpeed()
    {
        ai.agent.speed = ai.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    IEnumerator CookingSomething()
    {
        ai.rP.AddReport($"Let's cook a {currentItem.objectTag} soup.");

        if (ai.hasQuest)
        {
            ai.rP.AddQuestReport($"I cooked a tasty {currentItem.objectTag} soup! It helped me a lot to regain some energy!");
        }
        ai.inventory.items.Remove(currentItem);
        ai.inventory.ChangeObjectAmount(currentItem.objectID, -1);

        yield return new WaitForSeconds(3f);

        ai.character.AddSatisfaction(5);
        ai.rP.AddReport("Hmmm...That was delicious.");
        Destroy(currentItem.gameObject);
        currentItem = null;       
    }

    public void SetDisplayMode()
    {
        ai.canTalk = true;
        switch (ai.faction)
        {
            case AIStateManager.Faction.villager:
                if (ai.character.jobClass == TraitManager.JobClass.Hero)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.none;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                else
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.character;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                break;
            case AIStateManager.Faction.enemy:
                ai.currentDisplayMode = Dialogue.DisplayMode.none;
                ai.currentTextType = Dialogue.TextType.standard;
                break;
            case AIStateManager.Faction.neutral:
                if (ai.character.jobClass == TraitManager.JobClass.Adventurer)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.adventurer;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                else if (ai.character.jobClass == TraitManager.JobClass.Merchant)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.merchant;
                    ai.currentTextType = Dialogue.TextType.shop;
                }
                else if (ai.character.race == Character.Race.skeleton)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.ressurect;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                break;
        }
    }
}
