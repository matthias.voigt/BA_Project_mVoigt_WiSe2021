using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventPlaceType : MonoBehaviour
{
    public EventPlace.EventType eventType = EventPlace.EventType.none;
    public EventPlace.EventSize eventSize = EventPlace.EventSize.small;

    public List<AIStateManager> Characters = new List<AIStateManager>();
    public List<TraitManager.JobClass> jobs = new List<TraitManager.JobClass>();

    public AIStateManager.Faction faction = AIStateManager.Faction.neutral;
    public Character.Race race = Character.Race.human;


    public List<SleepItemHandler> sleepHandlers = new List<SleepItemHandler>();
    public List<Prison> prisons = new List<Prison>();

    private void Start()
    {
        if (Characters.Count <= 0) return;

        for(int i = 0; i < Characters.Count; i++)
        {
            Characters[i].faction = faction;
            Characters[i].character.race = race;
            Characters[i].character.jobClass = jobs[i];
            Characters[i].gameObject.GetComponent<AppearanceManager>().GenerateRandomAppearance(race);
            Characters[i].SetFactionIcon();

            Characters[i].gameObject.transform.SetParent(null);
            GameManager.singleton.AddNPCToList(Characters[i], faction);
        }
    }
}
