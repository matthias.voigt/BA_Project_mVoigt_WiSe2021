using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WatchTower : MonoBehaviour
{
    public Transform enterPosition;
    public Transform insidePosition;
    public GameObject currentObj;
    public AIStateManager.Faction faction = AIStateManager.Faction.villager;
    public void EnterTower(GameObject obj)
    {
        if(currentObj != null)
        {
            return;
        }
        else if(obj.TryGetComponent(out AIStateManager aiState))
        {
            Debug.Log($"Disable talking for {aiState.character.GetCharacterName()} while in tower.");
            aiState.canTalk = false;
            aiState.agent.Warp(insidePosition.position);
            currentObj = obj;
            aiState.gameObject.transform.rotation = insidePosition.transform.rotation;

            //REPORT
            aiState.rP.AddReport($"On guard at the watchtower!");
        }
        else
        {
            obj.transform.position = insidePosition.position;
            currentObj = obj;
        }
    }

    public void ExitTower()
    {
        if(currentObj.TryGetComponent(out AIStateManager aiState))
        {
            Debug.Log($"Enable talking for {aiState.character.GetCharacterName()}");
            aiState.canTalk = true;
            aiState.agent.Warp(enterPosition.position);            
            aiState.watchTowerState.currentWatchTower = null;
            aiState.watchTowerState.isInTower = false;
            currentObj = null;
        }
        else
        currentObj.transform.position = enterPosition.position;
        currentObj = null;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z)) ExitTower();
    }
}
