using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadDialougeData : MonoBehaviour
{
    public List<Dialogue> dialogue = new List<Dialogue>();
    public TextAsset[] dialogueData;
    
    public static LoadDialougeData singleton;

    private void Awake()
    {
        singleton = this;
    }
    private void Start()
    {
        foreach(TextAsset tA in dialogueData)
        {
            string loadPath = "DialogueData/" + tA.name;
            ReadingDialogueData(tA);
        }
    }

    void ReadingDialogueData(TextAsset tData)
    {
        string[] data;

        data = tData.text.Split(new char[] { '\n' });

        for (int i = 1; i < data.Length - 1; i++)
        {
            string[] row = data[i].Split(new char[] { ';' });


            if (row[1] != "")
            {
                Dialogue d = new Dialogue();

                //Set ID
                d.id = i + 1;
                d.fromFile = tData.name;
                d.name = $"#{d.id} {tData.name}";
                //Set DialogueType
                string dT = row[0];
                d.SetTextType(GetTextType(dT));

                //Set DisplayMode
                string dM = row[1];
                d.SetDisplayMode(GetDisplayMode(dM));

                //Set Trait
                string t = row[2];
                d.SetTrait(GetTrait(t));

                //Set Faction
                string f = row[3];
                d.SetFaction(GetFaction(f));

                //Set Race
                string r = row[4];
                d.SetRace(GetRace(r));

                //Set job
                string j = row[5];
                d.SetJob(GetJob(j));

                //Set Sentence
                string s = row[6];
                d.SetSentence(s);

                dialogue.Add(d);
            }
        }
    }

    public Dialogue.TextType GetTextType(string s)
    {
        switch (s)
        {
            case "standard":                                    //Standard Dialogues
                return Dialogue.TextType.standard;
            case "findSleep":                           //Searching for a place to sleep as homeless
                return Dialogue.TextType.findSleep;
            case "qgUnfinished":
                return Dialogue.TextType.QG_Unfinished;         //Having an unfinished Gather Quest
            case "qgFailed":
                return Dialogue.TextType.QG_Failed;
            case "qgSuccess":
                return Dialogue.TextType.QG_Success;    
            case "qgReport":
                return Dialogue.TextType.QG_Report;
            case "shop":
                return Dialogue.TextType.shop;
            case "recruit":
                return Dialogue.TextType.recruit;
            case "free":
                return Dialogue.TextType.free;
            case "fainted":
                return Dialogue.TextType.fainted;
            case "escort":
                return Dialogue.TextType.escort;
            case "healed":
                return Dialogue.TextType.healed;
            case "recruitFailed":
                return Dialogue.TextType.recruitFailed;
            case "freeFailed":
                return Dialogue.TextType.freeFailed;
            case "steal":
                return Dialogue.TextType.steal;
            case "recruitFull":
                return Dialogue.TextType.recruitFull;

            default:
                Debug.LogError("No TextType found!");
                return Dialogue.TextType.standard;
        }
    }

    public Dialogue.DisplayMode GetDisplayMode(string s)
    {
        switch (s)
        {
            case "none":
                return Dialogue.DisplayMode.none;
            case "character":
                return Dialogue.DisplayMode.character;
            case "merchant":
                return Dialogue.DisplayMode.merchant;
            case "researcher":
                return Dialogue.DisplayMode.researcher;
            case "prisoner":
                return Dialogue.DisplayMode.prisoner;
            case "exitPrison":
                return Dialogue.DisplayMode.exitPrison;
            case "lootable":
                return Dialogue.DisplayMode.lootable;
            case "ressurect":
                return Dialogue.DisplayMode.ressurect;
            case "exitRuin":
                return Dialogue.DisplayMode.exitRuin;
            case "hospital":
                return Dialogue.DisplayMode.hospital;
            case "exitHospital":
                return Dialogue.DisplayMode.exitHospital;
            case "adventurer":
                return Dialogue.DisplayMode.adventurer;
            case "exitAdventurer":
                return Dialogue.DisplayMode.exitAdventurer;
            default:
                Debug.LogError("No DisplayMode found!");
                return Dialogue.DisplayMode.none;
                    
        }
    }

    public TraitManager.Traits GetTrait(string s)
    {
        switch (s)
        {
            case "pacifist":
                return TraitManager.Traits.Pacifist;
            case "aggressive":
                return TraitManager.Traits.Aggressive;
            case "nightowl":
                return TraitManager.Traits.NightOwl;
            case "earlybird":
                return TraitManager.Traits.EarlyBird;
            case "sleepyhead":
                return TraitManager.Traits.SleepyHead;
            case "sleepless":
                return TraitManager.Traits.Sleepless;
            case "workaholic":
                return TraitManager.Traits.Workaholic;
            case "lazyhead":
                return TraitManager.Traits.Lazyhead;
            case "generous":
                return TraitManager.Traits.Generous;
            case "miser":
                return TraitManager.Traits.Miser;
            case "grumpy":
                return TraitManager.Traits.Grumpy;
            case "happynature":
                return TraitManager.Traits.HappyNature;
            case "null":
                return TraitManager.Traits.undefined;
            default:
                Debug.LogError("No Trait found!");
                return TraitManager.Traits.undefined;
        }
    }

    public Character.RaceType GetRace(string s)
    {
        switch (s)
        {
            case "null":
                return Character.RaceType.undefined;
            case "human":
                return Character.RaceType.human;
            case "monster":
                return Character.RaceType.monster;          
            default:
                Debug.LogError("No racetype found!");
                return Character.RaceType.undefined;
        }
    }

    public AIStateManager.Faction GetFaction(string s)
    {
        switch (s)
        {
            case "neutral":
                return AIStateManager.Faction.neutral;
            case "villager":
                return AIStateManager.Faction.villager;
            case "enemy":
                return AIStateManager.Faction.enemy;
            default:
                return AIStateManager.Faction.neutral;
        }
    }

    public TraitManager.JobClass GetJob(string s)
    {
        switch (s)
        {
            case "villager":
                return TraitManager.JobClass.Villager;
            case "adventurer":
                return TraitManager.JobClass.Adventurer;
            case "merchant":
                return TraitManager.JobClass.Merchant;
            case "guard":
                return TraitManager.JobClass.Guard;
            case "researcher":
                return TraitManager.JobClass.Researcher;
            case "hero":
                return TraitManager.JobClass.Hero;
            default:
                return TraitManager.JobClass.undefined;             
        }
    }
}