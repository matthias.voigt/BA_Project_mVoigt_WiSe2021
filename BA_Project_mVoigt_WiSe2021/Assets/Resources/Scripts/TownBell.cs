using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownBell : MonoBehaviour
{

    public GameObject InteractionPanel;
    private AudioSource audiosource;
    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
        audiosource = GetComponent<AudioSource>();
        OptionsManager.singleton.SetOption += ChangeVolume;
        ChangeVolume();
    }
    private void OnDestroy()
    {
        OptionsManager.singleton.SetOption -= ChangeVolume;
    }

    private void ChangeVolume()
    {
        audiosource.volume = OptionsManager.singleton.sfxValue;
    }

    public void Interact()
    {
        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
        InteractionPanel.SetActive(true);
    }

    public void FollowMe()
    {
        if (GameManager.singleton.villagers.Count <= 0)
        {
            EndInteraction();
            return;
        }

        foreach(AIStateManager ai in GameManager.singleton.villagers)
        {
            if(ai.currentState != ai.faintState &&
                ai.currentState != ai.recoverState && ai.currentState != ai.followState)
            {
                //Currently in escort prisoner state?
                if(ai.currentState == ai.escortPrisonerState)
                {
                    //Prisoner is not null - releasing prisoner
                    if(ai.escortPrisonerState.prisoner != null)
                    {
                        ai.escortPrisonerState.prisoner.followState.objToFollow = null;
                    }
                   
                }
                else if(ai.currentState == ai.sleepState)
                {
                    if(ai.sleepState.sleepingPlace != null)
                    {
                        ai.sleepState.sleepingPlace.ExitSleeping();
                    }
                }
                else if(ai.currentState == ai.watchTowerState)
                {
                    if(ai.watchTowerState.currentWatchTower != null)
                    {
                        ai.watchTowerState.currentWatchTower.ExitTower();
                    }
                }
                
                if(ai.currentState != ai.merchState && ai.currentState != ai.researchState)
                {
                    ai.followState.objToFollow = GameManager.singleton.playerObj;
                    ai.currentState = ai.TryChangeState(ai.followState);
                }             
            }
                     
        }

        anim.Play("TownBellRing");
        audiosource.Play();

        EndInteraction();
    }
    public void UnfollowMe()
    {
        if (GameManager.singleton.villagers.Count <= 0)
        {
            EndInteraction();
            return;
        }

        foreach (AIStateManager ai in GameManager.singleton.villagers)
        {
            if(ai.currentState == ai.followState && ai.followState.objToFollow == GameManager.singleton.playerObj)
            {
                ai.followState.objToFollow = null;             
            }
        }

        anim.Play("TownBellRing");
        audiosource.Play();
        EndInteraction();
    }

    public void GatherAtQuestBoard()
    {
        if (GameManager.singleton.villagers.Count <= 0)
        {
            EndInteraction();
            return;
        }

        foreach (AIStateManager ai in GameManager.singleton.villagers)
        {
            if (ai.currentState != ai.faintState &&
                ai.currentState != ai.recoverState && ai.currentState)
            {
                //Currently in escort prisoner state?
                if (ai.currentState == ai.escortPrisonerState)
                {
                    //Prisoner is not null - releasing prisoner
                    if (ai.escortPrisonerState.prisoner != null)
                    {
                        ai.escortPrisonerState.prisoner.followState.objToFollow = null;
                    }

                }

                //AI is following someone
                if(ai.currentState == ai.followState)
                {
                    //Follow is not null
                    if(ai.followState.objToFollow != null)
                    {
                        //Only reset those who do follow player
                        if(ai.followState.objToFollow.tag == "Player")
                        {
                            ai.followState.objToFollow = null;
                            ai.followState.objToFollow = GameManager.singleton.playerObj;
                            ai.currentState = ai.TryChangeState(ai.goHomeState);
                        }
                    }
                }
            }

        }

        anim.Play("TownBellRing");
        audiosource.Play();
        EndInteraction();
    }

    public void EndInteraction()
    {
        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
        InteractionPanel.SetActive(false);
    }
}
