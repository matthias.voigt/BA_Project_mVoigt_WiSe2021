using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class Footsteps : MonoBehaviour
{
    [Header("Grass")]
    [SerializeField] private AudioClip[] grassClips_Walk;
    [SerializeField] private AudioClip[] grassClips_Run;
    [Header("Sand")]
    [SerializeField] private AudioClip[] sandClips_Walk;
    [SerializeField] private AudioClip[] sandClips_Run;

    [Header("Gravel")]
    [SerializeField] private AudioClip[] gravelClips_Walk;
    [SerializeField] private AudioClip[] gravelClips_Run;

    [Header("Wood: ID 10")]
    [SerializeField] private AudioClip[] woodClips_Walk;
    [SerializeField] private AudioClip[] woodClips_Run;

    [Header("Swim: ID 11")]
    [SerializeField] private AudioClip[] swimClips_Walk;
    [SerializeField] private AudioClip[] swimClips_Run;

    [Header("Stone: ID 12")]
    [SerializeField] private AudioClip[] stoneClips_Walk;
    [SerializeField] private AudioClip[] stoneClips_Run;


    private AudioSource audioSource;
    private TerrainDetector terrainDetector;
    private bool isRunning = false;
    [SerializeField] ThirdPersonMovement tpm = null;


    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        terrainDetector = GetComponent<TerrainDetector>();
        OptionsManager.singleton.SetOption += ChangeVolume;
    }
    private void OnDestroy()
    {
        OptionsManager.singleton.SetOption -= ChangeVolume;
    }

    void ChangeVolume()
    {
        audioSource.volume = OptionsManager.singleton.sfxValue/3;
    }

    private void Step()
    {
        if (audioSource.volume == 0) return;

        if (!audioSource.isPlaying)
        {
            AudioClip clip = GetRandomClip();
            audioSource.PlayOneShot(clip);
        }
    }

    private AudioClip GetRandomClip()
    {
        int terrainTextureIndex = terrainDetector.GetActiveTerrainTextureIdx(transform.position);
        Debug.Log("Current terrain texture ID: " + terrainTextureIndex);


        if (tpm.IsSwimming())
        {
            if (isRunning)
                return swimClips_Run[UnityEngine.Random.Range(0, swimClips_Run.Length)];
            else
            {
                return swimClips_Walk[UnityEngine.Random.Range(0, swimClips_Walk.Length)];
            }
        }
        else if (isRunning)
        {
            switch (terrainTextureIndex)
            {
                case 0:
                    return grassClips_Run[UnityEngine.Random.Range(0, grassClips_Run.Length)];
                case 1:
                    return sandClips_Run[UnityEngine.Random.Range(0, sandClips_Run.Length)];
                case 2:
                    return gravelClips_Run[UnityEngine.Random.Range(0, gravelClips_Run.Length)];
                case 3:
                    return gravelClips_Run[UnityEngine.Random.Range(0, gravelClips_Run.Length)];
                case 4:
                    return stoneClips_Run[UnityEngine.Random.Range(0, stoneClips_Run.Length)];
                case 5:
                    return stoneClips_Run[UnityEngine.Random.Range(0, stoneClips_Run.Length)];
                case 6:
                    return stoneClips_Run[UnityEngine.Random.Range(0, stoneClips_Run.Length)];
                case 7:
                    return stoneClips_Run[UnityEngine.Random.Range(0, stoneClips_Run.Length)];
                case 8:
                    return gravelClips_Run[UnityEngine.Random.Range(0, gravelClips_Run.Length)];
                case 9:
                    return gravelClips_Run[UnityEngine.Random.Range(0, gravelClips_Run.Length)];
                case 10:
                    return woodClips_Run[UnityEngine.Random.Range(0, woodClips_Run.Length)];
                case 11:
                    return swimClips_Run[UnityEngine.Random.Range(0, swimClips_Run.Length)];
                case 12:
                    return stoneClips_Run[UnityEngine.Random.Range(0, stoneClips_Run.Length)];

                default:
                    return grassClips_Run[UnityEngine.Random.Range(0, grassClips_Run.Length)];
            }
        }
        else
        {
            switch (terrainTextureIndex)
            {
                case 0:
                    return grassClips_Walk[UnityEngine.Random.Range(0, grassClips_Walk.Length)];
                case 1:
                    return sandClips_Walk[UnityEngine.Random.Range(0, sandClips_Walk.Length)];
                case 2:
                    return gravelClips_Walk[UnityEngine.Random.Range(0, gravelClips_Walk.Length)];
                case 3:
                    return gravelClips_Walk[UnityEngine.Random.Range(0, gravelClips_Walk.Length)];
                case 4:
                    return stoneClips_Walk[UnityEngine.Random.Range(0, stoneClips_Walk.Length)];
                case 5:
                    return stoneClips_Walk[UnityEngine.Random.Range(0, stoneClips_Walk.Length)];
                case 6:
                    return stoneClips_Walk[UnityEngine.Random.Range(0, stoneClips_Walk.Length)];
                case 7:
                    return stoneClips_Walk[UnityEngine.Random.Range(0, stoneClips_Walk.Length)];
                case 8:
                    return gravelClips_Walk[UnityEngine.Random.Range(0, gravelClips_Walk.Length)];
                case 9:
                    return gravelClips_Walk[UnityEngine.Random.Range(0, gravelClips_Walk.Length)];
                case 10:
                    return woodClips_Walk[UnityEngine.Random.Range(0, woodClips_Walk.Length)];
                case 11:
                    return swimClips_Walk[UnityEngine.Random.Range(0, swimClips_Walk.Length)];
                case 12:
                    return stoneClips_Walk[UnityEngine.Random.Range(0, stoneClips_Walk.Length)];
                default:
                    return grassClips_Walk[UnityEngine.Random.Range(0, grassClips_Walk.Length)];
            }
        }      
    }

    private void Update()
    {
        if (GameManager.singleton.currentPlaystate != GameManager.PlayState.inOverworld) return;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            isRunning = true;
        }
        else
        {
            isRunning = false;
        }

        if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
        {
            if(tpm.IsPlayerGrounded())
                Step();
        }
    }

    
}
