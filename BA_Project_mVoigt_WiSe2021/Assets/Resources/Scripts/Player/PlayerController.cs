using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Animator anim = null;
    CameraManager camManager = null;

    void Start() // Initiallizing camera, animator, rigidboy
    {
        ResetAnimatorToDefault();
    }
    private void ResetAnimatorToDefault()
    {
        anim.SetBool("isIdling", true);
        anim.SetBool("isGettingHit", false);
        anim.SetBool("isPickingUp", false);
        anim.SetBool("isDrinking", false);
        anim.SetBool("isVictorious", false);
        anim.SetBool("isWalking", false);
        anim.SetBool("isRunning", false);
        anim.SetBool("isSleeping", false);
        anim.SetBool("isDieing", false);
        anim.SetBool("isRecovering", false);
        anim.SetBool("isDizzy", false);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * Time.deltaTime * 10f;
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }
    }

}
