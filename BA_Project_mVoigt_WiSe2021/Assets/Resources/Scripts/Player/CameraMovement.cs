using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float movSpeed = 10f;
    [SerializeField] private float scrollSpeed = 10f;

    public float maxZoom = 50;
    public float minZoom = 15;

    public TMP_Text tmp;

    public static CameraMovement singleton;

    public ConstructMode cM = ConstructMode.RotateObject;

    public enum ConstructMode
    {
        RotateObject,
        Zoom,
        RotateCamera,
        Destroy,
        Move
    }

    private void Awake()
    {
        singleton = this;
    }

    private void OnEnable()
    {
        SetTMP();
    }

    void SetTMP()
    {
        switch (cM)
        {
            case ConstructMode.RotateCamera:
                tmp.text = "Mode: Rotate Camera";
                break;
            case ConstructMode.RotateObject:
                tmp.text = "Mode: Rotate Object";
                break;
            case ConstructMode.Zoom:
                tmp.text = "Mode: Zoom";
                break;
            case ConstructMode.Destroy:
                tmp.text = "Mode: Destroy";
                break;
            case ConstructMode.Move:
                tmp.text = "Mode: Move";
                break;
        }
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (cM == ConstructMode.Zoom) cM = ConstructMode.RotateObject;
            else if (cM == ConstructMode.RotateObject) cM = ConstructMode.RotateCamera;
            else if (cM == ConstructMode.RotateCamera) cM = ConstructMode.Zoom;
            else if (cM == ConstructMode.Move || cM == ConstructMode.Destroy) cM = ConstructMode.RotateObject;

            EditConstructions.singleton.EnableCurrentBluePrint(ConstructionManager.singleton.currentBluePrintInScene);

            SetTMP();
        }  
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (cM != ConstructMode.Destroy && cM != ConstructMode.Move)
            {
                cM = ConstructMode.Move;               
            }
            else if (cM == ConstructMode.Destroy) cM = ConstructMode.Move;
            else if (cM == ConstructMode.Move) cM = ConstructMode.Destroy;

            SetTMP();
            EditConstructions.singleton.DisableCurrentBluePrint(ConstructionManager.singleton.currentBluePrintInScene);
        }


        float xMov = Input.GetAxis("Horizontal");
        float yMov = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(xMov, 0f, yMov);

        transform.Translate(direction * movSpeed * Time.deltaTime, Space.Self);

        Zoom();
        RotateCamera();
        
    }

    void Zoom()
    {
        if (cM != ConstructMode.Zoom) return;

        if (Input.GetKey(KeyCode.PageUp))
        {
            transform.position -= new Vector3(0, scrollSpeed/50, 0);

            if (transform.position.y > maxZoom) transform.position = new Vector3(transform.position.x, maxZoom, transform.position.z);
        }
        else if (Input.GetKey(KeyCode.PageDown))
        {
            transform.position += new Vector3(0, scrollSpeed/50, 0);

            if (transform.position.y < minZoom) transform.position = new Vector3(transform.position.x, minZoom, transform.position.z);
        }
        else
        {
            transform.position -= new Vector3(0, Input.GetAxis("Mouse ScrollWheel") * scrollSpeed, 0);

            if (transform.position.y > maxZoom) transform.position = new Vector3(transform.position.x, maxZoom, transform.position.z);
            else if (transform.position.y < minZoom) transform.position = new Vector3(transform.position.x, minZoom, transform.position.z);
        }
            
    }

    void RotateCamera()
    {
        if (cM != ConstructMode.RotateCamera) return;

        if (Input.GetKey(KeyCode.Q))    //Rotate Left
        {
            transform.Rotate(-Vector3.up * scrollSpeed / 50, Space.World);
        }
        else if (Input.GetKey(KeyCode.E)) //Rotate Right
        {
            transform.Rotate(Vector3.up * scrollSpeed / 50, Space.World);
        }
        else //Rotate with Mousewheel
        {
            transform.Rotate(0, Input.GetAxis("Mouse ScrollWheel") * scrollSpeed, 0, Space.World);
        }
    }


}
