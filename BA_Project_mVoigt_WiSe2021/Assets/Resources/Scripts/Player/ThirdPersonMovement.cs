using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour
{
    public CharacterController controller;

    [SerializeField] private float walkingSpeed = 2.5f;
    [SerializeField] private float sprintingSpeed = 8f;
    [SerializeField] private float jumpForce = 3f;
    public float test = -0.2f;
    public bool flyMode = false;

    [SerializeField] private Transform cam;
    
    private float turnSmoothTime = 0.1f;
    private float turnSmoothVelocity;

    [SerializeField] private Animator anim = null;

    [SerializeField] private float gravityValue = -9.81f;
    private bool isGrounded;


    Vector3 playerVelocity;
    Vector3 moveDirection;

    private bool isSprinting = false;

    private void Start()
    {
        if(cam == null)
        {
            cam = GameObject.FindGameObjectWithTag("MainCamera").transform;
        }
    }

    private void FixedUpdate()
    {
        if (GameManager.singleton.currentPlaystate != GameManager.PlayState.inOverworld) return;

        //Get Input
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        float speed = GetSpeed();

        //Get boolean of grounded
        isGrounded = controller.isGrounded;
        

        //Reset velocity.y
        if(isGrounded && playerVelocity.y < 0)
        {
            anim.SetBool("isJumping", false);
            playerVelocity.y = 0f;
        }
        
        //Get Direction
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;  

        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            controller.Move(moveDirection.normalized * speed * Time.deltaTime);;

            anim.SetBool("isSwimIdling", false);
            anim.SetBool("isIdling", false);
        }
        else
        {
            if (IsSwimming())
            {
                anim.SetBool("isSwimIdling", true);
                anim.SetBool("isSwimmingFwd", false);
                anim.SetBool("isIdling", false);
            }
            else if(!IsSwimming())
            {
                anim.SetBool("isSwimmingFwd", false);
                anim.SetBool("isSwimIdling", false);
                anim.SetBool("isIdling", true);
            }
            
            anim.SetBool("isWalking", false);
            anim.SetBool("isRunning", false);
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }

    private void Update()
    {
        if (GameManager.singleton.currentPlaystate != GameManager.PlayState.inOverworld) return;

        if (Input.GetButton("Jump") && flyMode)
        {
            if(playerVelocity.y < 10)
                playerVelocity.y += Mathf.Sqrt(1);
        }

        // Changes the height position of the player..
        else if (Input.GetButtonDown("Jump") && isGrounded)
        {
            anim.SetBool("isJumping", true);
            playerVelocity.y += Mathf.Sqrt(jumpForce * -3.0f * gravityValue);
        }

    }

    public void ResetAnimations()
    {
        anim.SetBool("isJumping", false);
        anim.SetBool("isSwimmingFwd", false);
        anim.SetBool("isRunning", false);
        anim.SetBool("isWalking", false);
        anim.SetBool("isSwimIdling", false);
        anim.SetBool("isIdling", true);
    }

    private float GetSpeed()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (!IsSwimming())
            {
                anim.SetBool("isSwimmingFwd", false);
                anim.SetBool("isSwimIdling", false);
                anim.SetBool("isRunning", true);
                anim.SetBool("isWalking", false);
            }
            else if (IsSwimming())
            {
                anim.SetBool("isSwimmingFwd", true);
                anim.SetBool("isSwimIdling", false);
                anim.SetBool("isRunning", false);
                anim.SetBool("isWalking", false);
            }

            return sprintingSpeed;
        }
        else
        {
            if (!IsSwimming())
            {
                anim.SetBool("isSwimmingFwd", false);
                anim.SetBool("isSwimIdling", false);
                anim.SetBool("isRunning", false);
                anim.SetBool("isWalking", true);
            }
            else if(IsSwimming())
            {
                anim.SetBool("isSwimmingFwd", true);
                anim.SetBool("isSwimIdling", false);
                anim.SetBool("isRunning", false);
                anim.SetBool("isWalking", false);
            }

            return walkingSpeed;
        }      
    }

    public bool IsPlayerGrounded()
    {
        return isGrounded;
    }


    public bool IsSwimming()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, Vector3.down);

        if (Physics.Raycast(ray, out hit, 0.2f, LayerMask.GetMask("Water")))
        {
            anim.SetBool("isSwimming", true);
            return true;
        }
        else
        {
            anim.SetBool("isSwimming", false);
            return false;
        }
    }
}




