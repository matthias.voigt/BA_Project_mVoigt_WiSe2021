using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    public AudioSource sfx;
    public bool sfxInHalf = false;
    public bool isBGM = false;
    private void Awake()
    {
        sfx = GetComponent<AudioSource>();
        OptionsManager.singleton.SetOption += SetAudio;
        SetAudio();
    }
    private void OnDestroy()
    {
        OptionsManager.singleton.SetOption -= SetAudio;
    }

    private void SetAudio()
    {
        if(!isBGM)
            sfx.volume = OptionsManager.singleton.sfxValue;
        else
            sfx.volume = OptionsManager.singleton.bgmValue;

    }
}
