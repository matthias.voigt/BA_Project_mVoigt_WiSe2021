using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCutsceneHandler : MonoBehaviour
{
    [SerializeField] private CutsceneDialogue firstTalk;
    [SerializeField] private CutsceneDialogue secondTalk;
    [SerializeField] private CutsceneDialogue enemiesDefeatedTalk;
    [SerializeField] private CutsceneDialogue bossFightIntro;
    [SerializeField] private GameObject BossfightObject;

    private bool firstMeet = true;

    public void Interact()
    {
        if (GameManager.singleton.AllEnemiesDefeated())
        {
            BossfightObject.SetActive(true);
            CutsceneManager.singleton.StartEvent(enemiesDefeatedTalk);
        }           
        else if (firstMeet)
        {
            firstMeet = false;
            CutsceneManager.singleton.StartEvent(firstTalk);
        }
        else
        {
            CutsceneManager.singleton.StartEvent(secondTalk);
        }
    }   
}
