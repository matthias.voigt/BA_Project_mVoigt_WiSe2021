using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class InventoryManager : MonoBehaviour
{
    [Header("Components")]
    public GameObject itemGrid;
    public GameObject InventoryPanel;

    public GameObject itemPrefab;
    public Sprite GatherIcon;
    public Sprite MiningIcon;
    public Sprite SkullIcon;
    
    public GameObject ingameMenu;

    private Character currentMerchant = null;

    public GameObject inventoryMoneyDisplay;
    public TMP_Text inventoryMoneyTxt;

    public GameObject merchantMoneyDisplay;
    public TMP_Text merchantMoneyTxt;

    List<InventoryItem> invItems = new List<InventoryItem>();
    public DisplayMode displayMode = DisplayMode.ownInventory;

    public Inventory playerInventory;
    public static InventoryManager singleton;


    public enum DisplayMode
    {
        none,
        ownInventory,
        npcInventory,
        sellMode,
        buyMode

    }

    private void Awake() => singleton = this;

    private void Start()
    {
        if(playerInventory == null)
        {
            playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        }

        InventoryPanel.SetActive(false);
        CreateItemInventory();
    }

    public void SellToMerchant(AIStateManager ai)
    {
        currentMerchant = ai.character;
        DisplayInventory(playerInventory, DisplayMode.sellMode);
        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
    }

    public void BuyFromMerchant(AIStateManager ai)
    {
        currentMerchant = ai.character;
        DisplayInventory(ai.inventory, DisplayMode.buyMode);
        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
    }

    public void CreateItemInventory()
    {
        for(int i = 0; i < 28; i++)
        {
            GameObject obj = Instantiate(itemPrefab, transform.position, Quaternion.identity);
            obj.transform.SetParent(itemGrid.transform);

            InventoryItem ii = obj.GetComponent<InventoryItem>();
            
            invItems.Add(ii);

            obj.SetActive(false);
        }
    }

    public void DisplayInventory(Inventory inv, DisplayMode dM)
    {
        //Disable all Item slots
        foreach(InventoryItem ii in invItems)
        {
            ii.gameObject.SetActive(false);
            ii.objectID = -1;
            ii.currentInventory = null;
            ii.amount = 0;
        }

        List<ObjectHandler> itemList = new List<ObjectHandler>();
        List<int> itemIDs = new List<int>();

        foreach(ObjectHandler oH in inv.items)
        {
            if (!itemIDs.Contains(oH.objectID))
            {
                itemList.Add(oH);
                itemIDs.Add(oH.objectID);
            }
        }

        for(int i = 0; i < itemList.Count; i++)
        {
            invItems[i].SetItem(itemList[i], inv);
            invItems[i].gameObject.SetActive(true);
        }

        inventoryMoneyTxt.text = inv.money.ToString();
        displayMode = dM;

        OpenInventory(inv, dM);
    }

    public void OpenPlayerInventory()
    {      
        DisplayInventory(playerInventory, DisplayMode.ownInventory);
    }
    public void CloseInventory()
    {       
        InventoryPanel.SetActive(false);
        currentMerchant = null;
        inventoryMoneyDisplay.SetActive(false);
        merchantMoneyDisplay.SetActive(false);
        displayMode = DisplayMode.none;

        if(DialogueManager.singleton.currentDialogueState == DialogueManager.DialogueState.none)
            GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
    }

    private void Update()
    {
        if(GameManager.singleton.currentPlaystate == GameManager.PlayState.inMenu && InventoryPanel.activeSelf)
        {
            if (Input.GetButtonDown("Fire2"))
                CloseInventory();
        }
    }

    public Sprite GetItemSprite(int id)
    {
        return ItemManager.singleton.ItemSprite[id + 1];
        /*
        switch (id)
        {
            case 0:
                return GatherIcon;
            case 1:
                return GatherIcon;
            case 2:
                return GatherIcon;
            case 3:
                return MiningIcon;
            case 4:
                return MiningIcon;
            case 5:
                return MiningIcon;
            case 6:
                return SkullIcon;
            default:
                return GatherIcon;
        
        }

        */
    }
    public string GetItemName(int id)
    {
        switch (id)
        {
            case 0:
                return "Potatoes";
            case 1:
                return "Pumpkins";
            case 2:
                return "Tomatos";
            case 3:
                return "Iron";
            case 4:
                return "Silver";
            case 5:
                return "Gold";
            case 6:
                return "Skull";
            case 7:
                return "Potions";
            default:
                return "???";
        }
    }

    public int GetAmount(int id, Inventory inv)
    {
        switch (id)
        {
            case 0:
                return inv.potatoes;
            case 1:
                return inv.pumpkins;
            case 2:
                return inv.tomatos;
            case 3:
                return inv.iron;
            case 4:
                return inv.silver;
            case 5:
                return inv.gold;
            case 6:
                return inv.skull;
            case 7:
                return inv.potions;
            default:
                return 0;
        }
    }

    public void SellItem(ObjectHandler oH, InventoryItem ii)
    {
        Inventory buyer = null;
        Inventory seller = null;

        if (displayMode == DisplayMode.sellMode)
        {
            buyer = currentMerchant.gameObject.GetComponent<Inventory>();
            seller = playerInventory;

            if (buyer.money < oH.sellPrice)
            {
                Debug.Log($"{buyer.gameObject.name} does not have enough money to buy {oH.objectTag}.");
                return;
            }
            else
            {
                //Reduce money of buyer
                buyer.AddMoney(-oH.sellPrice);

                //Add money to seller
                seller.AddMoney(oH.sellPrice);
            }
        }
        else if(displayMode == DisplayMode.buyMode)
        {
            buyer = playerInventory;
            seller = currentMerchant.gameObject.GetComponent<Inventory>();

            int addPrice = Mathf.RoundToInt(oH.sellPrice / GameManager.singleton.merchRiseFactor);
            int sellPrice = oH.sellPrice + addPrice;

            if (buyer.money < sellPrice)
            {
                Debug.Log($"{buyer.gameObject.name} does not have enough money to buy {oH.objectTag}.");
                return;
            }
            else
            {
                //Reduce money of buyer
                buyer.AddMoney(-sellPrice);

                //Add money to seller
                seller.AddMoney(sellPrice);
            }
        }
        else
        {
            Debug.LogError("Something went wrong!");
        }
   
        //Set new parent of sold item
        oH.gameObject.transform.SetParent(buyer.InvTransform);

        //Change amount
        buyer.ChangeObjectAmount(oH.objectID, +1);
        seller.ChangeObjectAmount(oH.objectID, -1);

        //Remove item from seller list
        seller.items.Remove(oH);

        //Add item to buyer list
        buyer.items.Add(oH);

        //Update texts
        if(buyer.gameObject.tag == "Player")
        {
            //Player is buying
            inventoryMoneyTxt.text = buyer.money.ToString();
            merchantMoneyTxt.text = seller.money.ToString();
        }
        else
        {
            //Merchant is buying
            merchantMoneyTxt.text = buyer.money.ToString();
            inventoryMoneyTxt.text = seller.money.ToString();
        }

        ii.amount = GetAmount(ii.objectID, ii.currentInventory);
        ii.itemAmount_TMP.text = ii.amount.ToString();
    }

    public void DropItem(ObjectHandler oH, InventoryItem ii)
    {
        ii.currentInventory.items.Remove(oH);
        ii.currentInventory.ChangeObjectAmount(oH.objectID, -1);
        ii.amount = GetAmount(ii.objectID, ii.currentInventory);
        ii.itemAmount_TMP.text = ii.amount.ToString();
        oH.gameObject.transform.SetParent(null);
        Vector3 pos = ii.currentInventory.gameObject.transform.position;
        pos = new Vector3(pos.x, pos.y + 2.5f, pos.z);
        oH.gameObject.transform.position = pos;
        oH.gameObject.SetActive(true);
    }

    public void RemoveItem(ObjectHandler oH, InventoryItem ii)
    {
        ii.currentInventory.items.Remove(oH);
        ii.currentInventory.ChangeObjectAmount(oH.objectID, -1);
        ii.amount = GetAmount(ii.objectID, ii.currentInventory);
        ii.itemAmount_TMP.text = ii.amount.ToString();

        Destroy(oH.gameObject);
    }

    private void OpenInventory(Inventory i, DisplayMode dM)
    {
        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
        InventoryPanel.SetActive(true);

        switch (dM)
        {
            case DisplayMode.ownInventory:             
                ingameMenu.SetActive(false);

                inventoryMoneyDisplay.SetActive(true);
                inventoryMoneyTxt.text = i.money.ToString();

                merchantMoneyDisplay.SetActive(false);
                break;
            case DisplayMode.npcInventory:
                inventoryMoneyDisplay.SetActive(false);

                merchantMoneyDisplay.SetActive(true);
                merchantMoneyTxt.text = i.money.ToString();
                break;
            case DisplayMode.buyMode:
                merchantMoneyDisplay.SetActive(true);
                merchantMoneyTxt.text = currentMerchant.gameObject.GetComponent<Inventory>().money.ToString();

                inventoryMoneyDisplay.SetActive(true);
                inventoryMoneyTxt.text = playerInventory.money.ToString();
                break;
            case DisplayMode.sellMode:
                merchantMoneyDisplay.SetActive(true);
                merchantMoneyTxt.text = currentMerchant.gameObject.GetComponent<Inventory>().money.ToString();

                inventoryMoneyDisplay.SetActive(true);
                inventoryMoneyTxt.text = playerInventory.money.ToString();
                break;
        }

    }
}
