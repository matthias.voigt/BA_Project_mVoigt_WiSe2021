using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Bossfight : MonoBehaviour
{
    public AIStateManager boss;
    public Slider bossSlider;
    public TMP_Text bossName;
    public GameObject bossPanel;
    public bool startFight = false;

    public CutsceneDialogue humanEnding;
    public CutsceneDialogue mageEnding;


    public float timer;

    private void Start()
    {
        //CutsceneManager.singleton.StartEvent(mageEnding);
    }

    public void SetBossFight(AIStateManager ai)
    {
        ai.character.boss = true;
        boss = ai;

        int hPBuffPerVillager = 0;

        for(int i = 0; i < GameManager.singleton.villagers.Count; i++)
        {
            hPBuffPerVillager += 100;
        }

        ai.character.satisfaction =  100 + hPBuffPerVillager;
        bossSlider.maxValue = ai.character.satisfaction;
        bossSlider.minValue = 0;


        bossName.text = ai.character.GetCharacterName();

        bossSlider.value = boss.character.GetProsperitySatisfaction();
        boss.character.invincible = false;

        bossPanel.SetActive(true);
        startFight = true;

    }
    private void Update()
    {
        if (!startFight) return;

        timer += Time.deltaTime;


        bossSlider.value = boss.character.satisfaction;

        if(boss.character.satisfaction <= 0)
        {
            startFight = false;
            Debug.Log("Boss defeated!");
            EnemiesFaint();
            bossPanel.SetActive(false);

            
        }
    }

    private void EnemiesFaint()
    {
        foreach(AIStateManager ai in GameManager.singleton.enemies)
        {
            ai.character.satisfaction = 0;
        }

        foreach (AIStateManager ai in GameManager.singleton.villagers)
        {
            ai.attackState.objToAttack = null;
            ai.currentState = ai.TryChangeState(ai.goHomeState);
        }

        StartCoroutine(WaitForSeconds());
    }

    IEnumerator WaitForSeconds()
    {
        yield return new WaitForSeconds(3f);

        if (boss.character.raceType == Character.RaceType.human)
        {
            CutsceneManager.singleton.StartEvent(mageEnding);
        }
        else
        {
            CutsceneManager.singleton.StartEvent(humanEnding);
        }
    }
}
