using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckRecruitableOption : MonoBehaviour
{
    public Button RecruitButton;
    private void OnEnable()
    {
        if (GameManager.singleton.villagers.Count >= 15)
            RecruitButton.interactable = false;
        else
            RecruitButton.interactable = true;
    }
}
