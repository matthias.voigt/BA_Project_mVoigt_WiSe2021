using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestCraftHandler : MonoBehaviour
{
    public GameObject displayObject;
    public TMP_Text desc;
    public TMP_Text amountTxt;
    public Image icon;
    
    public enum DisplayMode
    {
        none, 
        gather,
        rescue,
        interaction,
        become,
        scout,
        events, //not implemented yet
        job,
        location,
        item,
        itemAmount,
        prestige,
        reward,
        questHolder,
        npcToRescue,
        cancelButton
    }

    public void SetUp(Quest q, DisplayMode dM)
    {
        if(amountTxt != null)
            amountTxt.gameObject.SetActive(false);

        if(icon != null)
            icon.gameObject.SetActive(true);

        //None
        if (dM == DisplayMode.none)
        {
            displayObject.SetActive(false);
        }
        //Quest Type
        else if (dM == DisplayMode.gather || dM == DisplayMode.become || dM == DisplayMode.scout)
        {
            icon.sprite = icon.sprite = QuestManager.singleton.questTypeSprites[(int)q.questType];
            desc.text = q.questType.ToString();
        }
        //Interaction
        else if(dM == DisplayMode.interaction)
        {
            icon.sprite = QuestManager.singleton.questInteractionSprites[(int)q.interaction];
            desc.text = q.interaction.ToString();
        }
        //Job
        else if (dM == DisplayMode.job)
        {
            icon.sprite = TraitManager.singleton.JobSprites[(int)q.job];
            desc.text = q.job.ToString();
        }
        //Location
        else if(dM == DisplayMode.location)
        {
            icon.sprite = QuestManager.singleton.questLocationSprites[(int)q.questLocation];
            desc.text = q.questLocation.ToString();
        }
        //Item Amount
        else if(dM == DisplayMode.itemAmount)
        {
            icon.gameObject.SetActive(false);
            amountTxt.text = q.amountToGet.ToString();
            amountTxt.gameObject.SetActive(true);

        }
        //Item
        else if(dM == DisplayMode.item)
        {
            icon.sprite = ItemManager.singleton.ItemSprite[(int)q.questItem];
            desc.text = q.questItem.ToString();
        }
        //Prestige
        else if(dM == DisplayMode.prestige)
        {
            amountTxt.gameObject.SetActive(true);
            amountTxt.text = q.questPrestige.ToString();
        }
        //Reward
        else if(dM == DisplayMode.reward)
        {
            amountTxt.gameObject.SetActive(true);
            amountTxt.text = q.moneyReward.ToString();
        }
        //Event
        else if(dM == DisplayMode.events)
        {
            Debug.LogError("NEEDS TO BE IMPLEMENTED YET!");
        }
        else if(dM == DisplayMode.questHolder)
        {
            displayObject.SetActive(true);

            if (q.QuestHolder != null)
            {
                desc.text = q.QuestHolder.character.GetCharacterName();                
            }
            else
            {
                desc.text = "unassigned";
            }           
        }
        else if(dM == DisplayMode.npcToRescue)
        {
            if(q.NPCtoRescue == null)
            {
                displayObject.SetActive(false);
            }
            else
            {
                desc.text = q.NPCtoRescue.character.GetCharacterName();
                icon.sprite = TraitManager.singleton.JobSprites[(int)q.NPCtoRescue.character.jobClass];
            }
        }
        else if(dM == DisplayMode.rescue)
        {
            desc.text = QuestManager.QuestType.Rescue.ToString();
            icon.sprite = icon.sprite = QuestManager.singleton.questTypeSprites[(int)q.questType];
        }
    }

    public bool isCancelButton = false;
    Quest quest = null;
    public void SetQuest(Quest q)
    {
        quest = q;
    }

    private void OnEnable()
    {
        if (isCancelButton && quest != null)
        {
            if(quest.QuestHolder != null)
            {
                displayObject.SetActive(false);
            }
            else
            {
                displayObject.SetActive(true);
            }
        } 
        
    }

    public void CancelQuest()
    {
        QuestManager.singleton.currentQuestsAssigned.Remove(quest);
        quest.questDisplayer.DestroySelf();
        quest = null;
        QuestManager.singleton.ResumeAction();     
    }

}
