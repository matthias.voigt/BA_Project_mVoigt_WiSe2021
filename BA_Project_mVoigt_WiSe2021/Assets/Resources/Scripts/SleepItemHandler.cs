using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SleepItemHandler : MonoBehaviour
{
    [Header("Recovery Rate")]
    public float recoveryRate;

    [Header("Current Holder")]
    public GameObject currentObject = null;

    [Header("Animation Object")]
    public GameObject[] animationObject = null;

    [Header("Transforms")]
    public Transform exitPosition;
    public Transform sleepPosition;
    public Transform enterPosition;
    public Transform lastParent;

    private Animator currentAnim = null;
    private Character character;

    public Vector3 SleepRotation = new Vector3(0, 0, 0);

    public void SleepOnObject(GameObject obj, Animator anim)
    {
        if(animationObject != null)
        {
            foreach(GameObject animObj in animationObject)
            {
                animObj.SetActive(true);
            }
        }
        lastParent = obj.transform.parent;
        obj.transform.SetParent(transform);
        currentObject = obj;
        currentAnim = anim;

        currentAnim.Play("Sleep");

        if(obj.TryGetComponent(out NavMeshAgent agent))
        {
            agent.isStopped = true;
        }

        currentObject.transform.position = sleepPosition.position;
        currentObject.transform.localEulerAngles = SleepRotation;

        character = obj.GetComponent<Character>();

        
        if(obj.TryGetComponent(out AIStateManager ai))
        {
            Debug.Log("Sending a thought report.");
            ai.rP.AddReport("Zzz.");
            if (ai.hasQuest) ai.rP.AddQuestReport("Well, I went sleeping to recover some energy.");
        }

        if(obj.tag == "Player")
        {
            DayManager.singleton.speed = 360;
        }
    }

    public void ExitSleeping()
    {
        if(currentObject != null)
        {
            Debug.Log(currentObject.name + " is exiting " + gameObject.name);

            if (animationObject != null)
            {
                foreach (GameObject animObj in animationObject)
                {
                    animObj.SetActive(false);
                }
            }
            currentObject.transform.position = exitPosition.position;

            currentAnim.Play("Idle");
            currentObject.transform.SetParent(lastParent);

            if (currentObject.TryGetComponent(out AIStateManager ai))
            {
                ai.agent.Warp(exitPosition.position);
                Debug.Log("Sending a thought report.");
                ai.rP.AddReport("Yawn!");

                if (ai.hasQuest)
                {
                    if (ai.currentQuest.questFinished)
                    {
                        ai.rP.AddQuestReport("Well, I headed back to the quest board for report!");
                    }
                    else
                        ai.rP.AddQuestReport("After a good sleep, I continued my quest.");
                }
            }

            if (currentObject.tag == "Player")
            {
                DayManager.singleton.speed = 60;
            }


            lastParent = null;
            character = null;
            currentObject = null;
            currentAnim = null;
        }
    }

    public void Interact(GameObject obj, Animator anim)
    {
        if(currentObject == null)
        {
            Debug.Log("Bench is empty. Sleeping on it!");
            obj.GetComponent<ThirdPersonMovement>().enabled = false;
            SleepOnObject(obj, anim);
        }
        else
        {
            Debug.Log("Bench is not empty! Start dialogue with NPC!");
        }
    }
    private void Update()
    {
        if (currentObject == null) return;

        if (Input.GetButtonDown("Fire2") && currentObject.gameObject.tag == "Player")
        {
            currentObject.GetComponent<ThirdPersonMovement>().enabled = true;
            ExitSleeping();
        }

        if(currentObject != null)
        {
            currentObject.transform.localPosition = new Vector3(0.2f, 0.4f, 0);
            currentObject.transform.localEulerAngles = SleepRotation;
        } 
    }

}
