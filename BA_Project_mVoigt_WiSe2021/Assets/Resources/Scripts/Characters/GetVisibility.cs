using UnityEngine;

public class GetVisibility : MonoBehaviour
{
    [SerializeField] private bool isFaceVisible = true;
    [SerializeField] private bool isHairVisible = true;
    [SerializeField] private bool isFullHairVisible = true;
    private AppearanceManager appearance = null;

    private void Awake()
    {
        if(appearance == null)
        {
            appearance = GetComponentInParent<AppearanceManager>();
        }
    }
    private void Start()
    {
        CheckForVisibilities();
    }

    private void OnEnable()
    {
        CheckForVisibilities();
    }

    private void OnDisable()
    {
        HatDisabled();
    }

    /// <summary>
    /// Adjust visibility of face and hair according to set booleans.
    /// </summary>
    private void CheckForVisibilities()
    {
        if (isFaceVisible)
        {
            appearance.EnableFace(appearance.GetFaceID());
        }
        else
        {
            appearance.DisableAllFaces();
        }

        if (isHairVisible)
        {
            if (isFullHairVisible)
            {
                appearance.EnableHair(appearance.GetHairID());
            }
            else
            {
                appearance.DisableAllHairs();
                appearance.EnableHair(appearance.GetHairID() + appearance.GetAmountOfHairs / 2); 
            }
        }
        else
        {
            appearance.DisableAllHairs();
        }
    }

    /// <summary>
    /// Disables characters hats and turns of hair and face.
    /// </summary>
    private void HatDisabled()
    {
        appearance.DisableAllHairs();
        appearance.EnableHair(appearance.GetHairID());
        appearance.EnableFace(appearance.GetFaceID());       
    }
}
