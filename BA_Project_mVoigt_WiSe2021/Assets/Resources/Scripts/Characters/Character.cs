using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    #region Character Stats Declaration
    public int villagerID = 0;
    
    public string characterName = "Bobby Bob";
    private string characterDescription = "Normal Being";

    //Traits
    public TraitManager.JobClass jobClass = TraitManager.JobClass.Villager;
    public List<TraitManager.Traits> traits = new List<TraitManager.Traits>();

    [SerializeField] public float satisfaction = 0;      //Actions cost satisfaction, less satisfaction means less success.
    [SerializeField] public int authority = 0;           //Authority determines skill of jobclass.
    [SerializeField] public int prestige = 0;            //Prestige influences amount of taxes.   
    [SerializeField] public int questExperience;

    public AppearanceManager aM;

    public Race race = Race.human;
    public bool loadFromData = false;
    public bool randomName = true;
    public bool randomTraits = true;
    public bool randomRace = true;
    public bool randomFaction = true;
    public bool randomJob = true;
    public bool randomValues = true;
    public bool randomApperance = true;
    public bool startWithFaint = false;
    public bool invincible = false;
    #endregion

    public RaceType formerRaceType = RaceType.undefined;

    public List<Race> randomRaceList = new List<Race>();

    [SerializeField] public BuildingHandler currentHome = null;

    
    public float wakeTime = 21600f;
    public float sleepTime = 79200f;

    public RaceType raceType = RaceType.human;

    public enum RaceType
    {
        undefined,
        human,
        monster
    }

    public void SetRaceType()
    {
        if (race == Race.human)
        {
            raceType = RaceType.human;
        }
        else
            raceType = RaceType.monster;
    }

    public enum Race
    {
        none,
        human,
        skeleton,
        orc,
        bat,
        dragon,
        plant,
        slime,
        spider,
        turtle,
        mage,
        golem
    }

    private void Awake()
    {
        if (loadFromData)
        {
            randomName = false;
            randomRace = false;
            randomTraits = false;
            randomValues = false;
            randomFaction = false;
            randomJob = false;
            randomApperance = false;
        }

        int l = PlayerPrefs.GetInt("load", 0);

   
        if (l == 1)
        {
            Debug.Log("Removing non saved entities.");
            if (jobClass != TraitManager.JobClass.Hero)
                Destroy(gameObject);

        }
    }

    private void Start() 
    {
        if (randomName)
        {
            characterName = PersonalityManager.singleton.GetRandomName();
            gameObject.name = characterName;
        }

        if (randomTraits)
        {
            TraitManager.singleton.ReturnRandomTraits(traits, GetComponent<AIStateManager>());
        }

        if (randomFaction)
        {
            GetComponent<AIStateManager>().SetRandomFaction();
        }

        if (randomRace)
        {
            aM.GetRandomRace(this, randomRaceList);
        }

        if (randomJob)
        {
            jobClass = TraitManager.singleton.ReturnRandomClass();
        }

        if (randomValues)
        {
            SetRandomValues();
        }

        if (randomApperance)
        {
            aM.GenerateRandomAppearance(race);
        }
        SetRaceType();
        GetComponent<AIStateManager>().SetFactionIcon();

        //GameManager.singleton.AddVillagerToList(this);

        if (startWithFaint)
        {
            satisfaction = 0;
        }

        AIStateManager ai = GetComponent<AIStateManager>();

        GameManager.singleton.AddNPCToList(ai, ai.faction);      
    }

    #region Get values

    //Strings
    public string GetCharacterName() { return characterName; }
    public string GetCharacterDescription() { return characterDescription; }

    //Prosperity
    public float GetProsperitySatisfaction() { return satisfaction; }
    public int GetProsperityPrestige() { return prestige; }
    public int GetQuestExp() { return questExperience; }
    public void AddQuestExp(int value) 
    {
        questExperience += value;

        if (questExperience >= 3)
        {
            questExperience = 0;
            int boost = Random.Range(1, 6);
            GetComponent<AIStateManager>().rP.AddReport("I feel stronger now!");
            StatusManager.singleton.InsertNewStatus($"{characterName} is doing great at quests! Leveled up authority by {boost}!");
            AddAuthority(boost);
            AddPrestige(boost);

            PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.leveledUp, raceType);
        }
        else if(questExperience <= -3)
        {
            questExperience = 0;
            int boost = Random.Range(1, 6);
            GetComponent<AIStateManager>().rP.AddReport("I feel weaker now...");
            StatusManager.singleton.InsertNewStatus($"{characterName} failed to many quests and lost authority by {boost}!");
            AddAuthority(boost);
            AddPrestige(boost);
            PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.leveledDown, raceType);
        }
        

    
    }

    public BuildingHandler GetCurrentHome() { return currentHome; }
    #endregion


    //Personality
    public void SetCharacterName(string newName) => characterName = newName;
    public void SetCharacterDescription(string newDesc) => characterDescription = newDesc;


    public void SetVillagerID(int id) => villagerID = id;

    public bool boss = false;
    public void AddSatisfaction(float addValue)
    {
        satisfaction += addValue;

        if (!boss)
        {
            if (satisfaction > 100)
                satisfaction = 100;
        }

    }

    public void AddPrestige(int addValue) => prestige += addValue;
    public void AddAuthority(int addValue) => authority += addValue;
    

    public void ReduceSatisfactionBy(float satisfactionLossPerSeconds)
    {
        if (invincible) return;

        satisfaction -= Time.deltaTime * satisfactionLossPerSeconds;
        if (satisfaction <= 0) satisfaction = 0;
    }
    public void SetHome(BuildingHandler bH) => currentHome = bH;

    public int GetPrestige()
    {
        AIStateManager aM = gameObject.GetComponent<AIStateManager>();

        int prestigeBoostFromHome = 0;
        if (aM.character.GetCurrentHome() != null)
        {
            prestigeBoostFromHome = aM.character.GetCurrentHome().GetConstruction().prestigeBoost;
        }
        float moneyPrestige = aM.inventory.GetCurrentMoney() / 25;

        int prestige = Mathf.RoundToInt(moneyPrestige + prestigeBoostFromHome + aM.character.GetQuestExp());

        return prestige;
    }

    public int GetAuthority()
    {
        return authority;
    }

    void SetRandomValues()
    {
        int randomSatisfaction = Random.Range(40, 100);
        AddSatisfaction(randomSatisfaction);

        int randomAuthority = Random.Range(1, 75);
        AddAuthority(randomAuthority);

        //int randomPrestige = Random.Range(0, 30);
        //profile.AddPrestige(randomPrestige);

        int randomMoney = Random.Range(0, 501);
        GetComponent<Inventory>().AddMoney(randomMoney);

        AddPrestige(GetPrestige());

    }


}


