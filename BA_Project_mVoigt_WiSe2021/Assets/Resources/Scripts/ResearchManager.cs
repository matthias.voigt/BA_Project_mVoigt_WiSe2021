using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResearchManager : MonoBehaviour
{
    public GameObject ResearchPanel;

    [Header("Preview Scene")]
    [SerializeField] private Transform previewTransform = null;
    [SerializeField] private TMP_Text constructionNameText = null;
    [SerializeField] private TMP_Text constructionProgress = null;

    [Header("Homes")]
    public List<Construction> homes = new List<Construction>();

    [Header("Base")]
    public List<Construction> walls = new List<Construction>();
    public List<Construction> fences = new List<Construction>();
    public List<Construction> paths = new List<Construction>();

    [Header("Nature")]
    public List<Construction> trees = new List<Construction>();
    public List<Construction> flowers = new List<Construction>();
    public List<Construction> stones = new List<Construction>();
    public List<Construction> rocks = new List<Construction>();

    [Header("Miscellaneous")]
    public List<Construction> benches = new List<Construction>();
    public List<Construction> lanterns = new List<Construction>();
    public List<Construction> diverse = new List<Construction>();

    [Header("Special")]
    public List<Construction> special = new List<Construction>();

    [Header("Current Research")]
    SpecialBuilding currentLab;
    private GameObject currentConstructPreview = null;
    public List<Construction> constructsToResearch;
    public Slider slider;
    public RawImage rawImage;
    public TMP_Text costToReseachTxt = null;
    public GameObject ResearchButton = null;
    private List<Construction> currentResearchList;
    private Construction previewConstruct;
    [SerializeField] private GameObject Overview;
    [SerializeField] private GameObject currentMoneyObj;
    [SerializeField] private TMP_Text currentMoneyTxt;
    public static ResearchManager singleton;

    private void Awake()
    {
        singleton = this;
    }

    private void Start()
    {
        GenerateResearchList();

    }

    public void GenerateResearchList()
    {
        //Home
        homes = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsHomes));
        
        //Base
        walls = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsWalls));
        fences = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsFences));
        paths = new List<Construction>(ConstructionManager.singleton.constructsPaths);

        //Nature
        flowers = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsFlowers));
        rocks = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsRocks));
        stones = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsStones));
        trees = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsTrees));

        //Miscellaneous
        benches = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsBenches));
        lanterns = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsLanterns));
        diverse = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsDiverse));

        special = new List<Construction>(RemoveUnlockedConstructsFromList(ConstructionManager.singleton.constructsSpecial));
    }

    private List<Construction> RemoveUnlockedConstructsFromList(List<Construction> c)
    {
        List<Construction> _newC = new List<Construction>(c);

        foreach(Construction _c in c)
        {
            if (_c.GetUnlockBool())
            {
                _newC.Remove(_c);
            }
        }

        return _newC;
    }
    
    public Construction GetConstructToResearch(List<Construction> c)
    {
        List<Construction> _l = new List<Construction>(c);

        foreach(Construction cItem in c)
        {
            if (!cItem.GetUnlockBool())
            {
                _l.Add(cItem);
            }
        }

        Construction _c = _l[Random.Range(0, _l.Count)];
        return _c;
    }

    public void RemoveConstructFromCurrentResearch(Construction c)
    {
        Debug.Log("Removing " + c.GetConstructName() + " from current research.");
        constructsToResearch.Remove(c);
    }
    public void AddConstructToCurrentResearch(Construction c)
    {
        constructsToResearch.Add(c);
    }

    public void DisplayCurrentResearch(SpecialBuilding sb)
    {
        if(sb.constructToResearch == null)
        {
            ResearchButton.SetActive(false);

            slider.gameObject.SetActive(false);
            Debug.LogWarning("No construct to research!");

            constructionNameText.text = "Currently no research";
            constructionProgress.text = "???";
            slider.value = 0;
        }
        else
        {
            ResearchButton.SetActive(true);

            slider.gameObject.SetActive(true);
            ResearchButton.SetActive(false);
            constructionNameText.text = sb.constructToResearch.GetConstructName();
            constructionProgress.text = SetPercentage(currentLab.constructToResearch.researchTime, currentLab.researchTime).ToString();

            InstantiatePreview(sb.constructToResearch);
        }       
    }

    //Start
    public void OpenResearchPanel(SpecialBuilding sb)
    {
        Debug.Log("Opening ResearchPanel");
        currentLab = sb;
        ResearchPanel.SetActive(true);
        currentMoneyTxt.text = GameManager.singleton.PlayerInventory.money.ToString();
        currentMoneyObj.gameObject.SetActive(true);
        DisplayCurrentResearch(sb);
    }

    public void StartResearch()
    {
        if(GameManager.singleton.PlayerInventory.money < previewConstruct.GetResearchCost())
        {
            Debug.Log("Player has not enough money for research.");
            constructionNameText.text = "Not enough money!";
            return;
        }

        GameManager.singleton.PlayerInventory.AddMoney(-previewConstruct.GetResearchCost());
        currentMoneyTxt.text = GameManager.singleton.PlayerInventory.money.ToString();
    
        Debug.Log("Starting Research!");
        AddConstructToCurrentResearch(previewConstruct);

        currentLab.SetResearchConstruct(previewConstruct);
        DisplayCurrentResearch(currentLab);
    }


    public void ShowResearchPreview(List<Construction> cList)
    {
        if(cList.Count <= 0 && currentLab.constructToResearch == null)
        {
            Debug.Log("No List found for research.");
            ResearchButton.SetActive(false);
            constructionNameText.text = "Research complete";

            if (currentConstructPreview != null)
            {
                Debug.Log("Deleting preview cache.");
                DestroyImmediate(currentConstructPreview, true);
            }
            return;
        }
        else if(cList.Count <= 0 && currentLab.researchTime > 0)
        {
            Debug.Log("No List found for research and currently researching.");
            return;
        }
        else if(cList.Count <= 0 && currentLab.researchTime <= 0)
        {
            Debug.Log("No List found for research.");
            ResearchButton.SetActive(false);
            constructionNameText.text = "Research complete";

            if (currentConstructPreview != null)
            {
                Debug.Log("Deleting preview cache.");
                DestroyImmediate(currentConstructPreview, true);
            }
            return;
        }

        if (currentLab.constructToResearch == null || currentLab.researchTime <= 0)
        {
            ResearchButton.SetActive(true);
            slider.gameObject.SetActive(false);
            currentResearchList = cList;
            previewConstruct = GetConstruct(cList);

            costToReseachTxt.text = previewConstruct.GetResearchCost().ToString();
            InstantiatePreview(previewConstruct);
        }
        else
        {
            ResearchButton.SetActive(false);
            slider.gameObject.SetActive(true);
            DisplayCurrentResearch(currentLab);
        }
    }

    public Construction GetConstruct(List<Construction> cList)
    {
        if (cList.Count <= 0)
        {
            Debug.Log("No research available. List is empty.");
            return null;
        }

        if (currentLab.constructToResearch != null)
        {
            Debug.Log("Removing current Research.");
            RemoveConstructFromCurrentResearch(currentLab.constructToResearch);
        }

        Construction c = GetConstructToResearch(cList);

        return c;
    }


    private void Update()
    {
        if (!ResearchPanel.activeSelf) return;

        if (Input.GetButtonDown("Fire2"))
        {
            Debug.Log("Closing Research Panel.");
            ResearchPanel.SetActive(false);
            currentMoneyObj.gameObject.SetActive(false);
        }

        if (currentLab.constructToResearch == null) return;

        if(!currentLab.constructToResearch.GetUnlockBool())
        {
            float value = SetPercentage(currentLab.constructToResearch.researchTime, currentLab.researchTime);
            float progress = 100 - value;
            slider.value = progress;
            constructionProgress.text = progress.ToString("F2") +"%";
        }
        else
        {
            slider.value = 100;
            constructionProgress.text = "Research Complete!";
        }

        if(previewConstruct.GetUnlockBool()) ChangeRawImageColor(Color.white);
        else ChangeRawImageColor(Color.black);
    }

    public float SetPercentage(float maxValue, float percentageValue)
    {
        percentageValue = Mathf.Abs(percentageValue);

        float percentage = percentageValue / maxValue;

        return percentage * 100;
    }

    public void ChangeRawImageColor(Color c)
    {
        rawImage.color = c;
    }

    private void InstantiatePreview(Construction construct)
    {
        if (currentConstructPreview != null)
        {
            Debug.Log("Deleting preview cache.");
            DestroyImmediate(currentConstructPreview, true);
        }

        Debug.Log($"Instantiating preview gameObject: {construct.GetConstructName()}");
        currentConstructPreview = Instantiate(construct.GetConstruct(), previewTransform);

        constructionNameText.text = construct.GetConstructName();

        currentConstructPreview.layer = 9;
        foreach (Transform child in currentConstructPreview.transform)
        {
            child.gameObject.layer = 9;

            if (child.transform.childCount > 0)
            {
                foreach (Transform _child in child.transform)
                {
                    _child.gameObject.layer = 9;
                }
            }
        }
    }
}
