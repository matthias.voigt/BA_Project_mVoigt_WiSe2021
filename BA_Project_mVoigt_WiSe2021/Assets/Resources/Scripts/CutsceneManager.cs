using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Cinemachine;
using UnityEngine.AI;
using UnityEngine.UI;

public class CutsceneManager : MonoBehaviour
{
    public Camera cam;
    public CinemachineBrain cB;
    public Animator camAnim;
    public Animator fadeAnim;
    public Animator sceneCharAnim;

    public CutsceneDialogue[] cs00;
    public CutsceneDialogue currentDialogue;

    public TMP_Text dialogueText;
    private Queue<string> sentences;
    public GameObject DialoguePanel;

    public AIStateManager hero;
    public GameObject evilMage;
    public GameObject SceneCharacter;
    public GameObject player;
    public GameObject canvasObjects;
    public GameObject TransformParticles;
    public GameObject tornadoEffect;
    public GameObject tornadoEffect2;
    public GameObject churchScene;
    public AIStateManager evilMageAI;
    public GameObject ppVolume;

    public BossFightTrigger bft;

    public Material skyboxNormal;
    public Material skyboxBoss;

    public Animator titleAnim;

    public int eventID;
    public int stringID;
    public int animID;
    public int camPosID;
    public int camRotID;
    public int charPosID;
    public int charRotID;
    public int charID;
    public int bgmID;

    public bool skipIntro = false;

    public CutsceneState currentState;
    public bool tornadoEnabled;
    public static CutsceneManager singleton;


    public CutsceneDialogue noVillagersAtBoss;
    public CutsceneDialogue VillagersAtBoss;

    private void Awake()
    {
        singleton = this;
    }
    public enum CutsceneState
    {
        none,
        transit,
        inDialogue
    }

    public ThirdPersonMovement tpm;
    private void Start()
    {
        currentState = CutsceneState.none;

        int l = PlayerPrefs.GetInt("load", 0);

        if (l == 0 && !skipIntro)
            StartEvent(cs00[0]);
    }
    public Animator playerAnim;
    public void StartEvent(CutsceneDialogue d)
    {
        DialogueManager.singleton.DisableAllButtons();
        GameManager.singleton.CloseAllPanels();


        tpm.ResetAnimations();
        Debug.Log("Playing: " + d.name);
        GameManager.singleton.currentPlaystate = GameManager.PlayState.inMenu;
        cB.enabled = false;

        eventID = 0;
        stringID = 0;
        camPosID = 0;
        camRotID = 0;
        charRotID = 0;
        charPosID = 0;
        animID = 0;
        charID = 0;
        bgmID = 0;
        currentDialogue = d;

        canvasObjects.SetActive(false);

        EventSwitch();
    }

    void EventSwitch()
    {
        if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.wait)
        {
            currentState = CutsceneState.transit;
            StartCoroutine(Wait(2f));
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.nextSentence)
        {
            currentState = CutsceneState.inDialogue;
            DisplayText();

        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.playCamAnim)
        {
            currentState = CutsceneState.transit;
            PlayCamAnimation();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.changeCameraPos)
        {
            ChangeCameraPos();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.changeCameraRot)
        {
            ChangeCameraRot();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.setSceneCharacter)
        {
            SetSceneCharacter();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.setCharacterPos)
        {
            currentState = CutsceneState.transit;
            ChangeCharacterPos();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.setCharacterRot)
        {
            ChangeCharacterRot();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.disablePlayer)
        {
            Debug.Log($"{eventID}: Disable Player.");
            player.SetActive(false);
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.fadeIn)
        {
            Debug.Log($"{eventID}: Fade in.");
            fadeAnim.Play("FadeIn");
            currentState = CutsceneState.transit;
            StartCoroutine(Wait(2f));
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.fadeOut)
        {
            Debug.Log($"{eventID}: Fade out.");
            fadeAnim.Play("FadeOut");
            currentState = CutsceneState.transit;
            StartCoroutine(Wait(2f));
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.disableAiState)
        {
            Debug.Log($"{eventID}: Disable AI state of: {SceneCharacter.gameObject.name}.");
            if (SceneCharacter.TryGetComponent(out AIStateManager ai))
            {
                ai = SceneCharacter.GetComponent<AIStateManager>();
                ai.enabled = false;
                ai.anim.Play("Idle");
                ai.agent.enabled = false;
            }
            else
            {
                Debug.LogError("Cannot disable SceneCharacters AI at eventID: " + eventID);
            }

            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.enableAiState)
        {
            Debug.Log($"{eventID}: Ensable AI state of: {SceneCharacter.gameObject.name}.");
            if (SceneCharacter.TryGetComponent(out AIStateManager ai))
            {
                ai = SceneCharacter.GetComponent<AIStateManager>();
                ai.enabled = true;
                ai.currentState = ai.TryChangeState(ai.idleState);
                ai.agent.enabled = true;
            }
            else
            {
                Debug.LogError("Cannot enable SceneCharacters AI at eventID: " + eventID);
            }

            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.playCharAnim)
        {
            currentState = CutsceneState.transit;
            PlayCharAnimation();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.stopTime)
        {
            Debug.Log($"{eventID}: Stop time.");
            DayManager.singleton.speed = 0;
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.startTime)
        {
            Debug.Log($"{eventID}: Start Time.");
            DayManager.singleton.speed = 60;
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.playBGM)
        {
            PlayBGM();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.playSFXBackground)
        {
            PlaySFXBackground();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.end)
        {
            EndCutscene();
            Debug.Log("Cutscene Finished!");
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.playTitle)
        {
            PlayTitleAnimation();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.spawnTransfParticles)
        {
            SpawnTransformParticles();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.changeSkyboxToBoss)
        {
            Debug.Log($"{eventID}: Change Skybox to Boss.");
            RenderSettings.skybox = skyboxBoss;
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.changeSkyboxToNormal)
        {
            Debug.Log($"{eventID}: Change Skybox to normal.");
            RenderSettings.skybox = skyboxNormal;
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.enableTornadoEffect)
        {
            NavigationBaker.singleton.UpdateNavMesh();
            Debug.Log($"{eventID}: Enabling Tornado.");
            tornadoEnabled = true;
            tornadoEffect.SetActive(true);
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.disableTornadoEffect)
        {
            Debug.Log($"{eventID}: Disabling Tornado.");
            tornadoEnabled = false;
            tornadoEffect.gameObject.GetComponent<RFX4_EffectSettings>().IsVisible = false;
            tornadoEffect2.SetActive(false);

            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.disableGameObjectTornado)
        {
            Debug.Log($"{eventID}: Disabling Tornado.");
            tornadoEffect.SetActive(false);
            tornadoEffect2.SetActive(false);

            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.enableSecondTornado)
        {
            tornadoEffect2.SetActive(true);
            tornadoEnabled = true;
            tornadoEffect.gameObject.GetComponent<RFX4_EffectSettings>().IsVisible = true;
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.changeChurchScene)
        {
            Debug.Log($"{eventID}: Disabling Church.");

            if (churchScene.activeSelf)
                churchScene.SetActive(false);
            else
                churchScene.SetActive(true);

            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.enableEvilMageAI)
        {
            Debug.Log($"{eventID}: Enabling Evil Mage AI");
            evilMage.SetActive(false);
            evilMageAI.gameObject.SetActive(true);
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.enablePostProcessing)
        {
            Debug.Log($"{eventID}: En/Dis-abling global Post Processing");

            if (ppVolume.activeSelf)
            {
                ppVolume.SetActive(false);
            }
            else
            {
                ppVolume.SetActive(true);
            }
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.setFinalePos)
        {
            Debug.Log($"{eventID}: Setting finale positions.");
            SetPositionFinalePosition();
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.checkVillagerCount)
        {
            Debug.Log($"{eventID}: Checking Villager Count.");
            CheckVillagerCount();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.changeCamToHuman)
        {
            Debug.Log($"{eventID}: Change Cam to random Human villager.");
            SetPosToHuman();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.changeCamToMonster)
        {
            Debug.Log($"{eventID}: Change Cam to random monster villager.");
            SetPosToMonster();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.checkSequenceHumanTalk)
        {
            Debug.Log($"{eventID}: Check next sequence: Human.");
            CheckForNextCutsceneHuman();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.checkSequenceMonsterTalk)
        {
            Debug.Log($"{eventID}: Check next sequence: Monster.");
            CheckForNextCutsceneMonster();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.playerdecisionSide)
        {
            Debug.Log($"{eventID}: Start new dialogue: Playerdecision.");
            StartEvent(PlayerDecision);
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.enableDecisionButtons)
        {
            Debug.Log($"{eventID}: Enable Decision buttons.");
            EnableDecisionButtons();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.setbossfighMage)
        {
            Debug.Log($"{eventID}: Boss fight: Evil Mage.");
            SetFinalBossFightEvilMage();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.setbossfightHero)
        {
            Debug.Log($"{eventID}: Boss fight: Hero.");
            SetFinalBossFightHero();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.scaleHero)
        {
            Debug.Log($"{eventID}: Scale Hero.");
            ScaleHero();
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.scaleMage)
        {
            Debug.Log($"{eventID}: Scale Evil Mage.");
            ScaleEvilMage();
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.enableBossBoxTrigger)
        {
            Debug.Log($"{eventID}: Enable boss finale trigger box.");
            bft.EnableBoxCollider();
            ContinueEvent();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.loadCredits)
        {
            Debug.Log($"{eventID}: Loading Credits Using save before.");
            SaveManager.singleton.SaveFile();
            MenuManager.singleton.LoadCredits();
        }
        else if (currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.spawnVillagers)
        {
            Debug.Log($"{eventID}: Spawn Villagers.");
            RandomEventManager.singleton.SetUpRandomEvent(RandomEventManager.RandomEvent.villager);
            ContinueEvent();

        }
        else if(currentDialogue.eventList[eventID] == CutsceneDialogue.EventList.press1Info)
        {
            Debug.Log($"{eventID}: Press 1 info.");
            StatusManager.singleton.InsertNewStatus("Press 1 to open inventory.");
            ContinueEvent();
        }
    }



    private void SpawnTransformParticles()
    {
        Debug.Log($"{eventID}: Spawn Transformation Particles");

        Instantiate(TransformParticles, SceneCharacter.transform.position, Quaternion.identity);
        ContinueEvent();
    }

    private void PlayTitleAnimation()
    {
        Debug.Log($"{eventID}: Play Title animation");
        titleAnim.Play("TitleAnim");

        StartCoroutine(Wait(6f));
    }

    void PlayBGM()
    {
        Debug.Log($"{eventID}: Set BGM: " + currentDialogue.audioClips[bgmID].name);
        AudioManager.singleton.PlayBGM(currentDialogue.audioClips[bgmID]);
        bgmID++;
        ContinueEvent();
    }

    void SetSceneCharacter()
    {
        Debug.Log($"{eventID}: Set SceneCharacter.");
        if (currentDialogue.sceneCharacter[charID] == CutsceneDialogue.SceneCharacter.hero)
        {
            SceneCharacter = hero.gameObject;
            hero.enabled = false;
        }
        else if (currentDialogue.sceneCharacter[charID] == CutsceneDialogue.SceneCharacter.player)
        {
            SceneCharacter = player;
        }
        else if (currentDialogue.sceneCharacter[charID] == CutsceneDialogue.SceneCharacter.evilmage)
        {
            SceneCharacter = evilMage;
        }
        else if (currentDialogue.sceneCharacter[charID] == CutsceneDialogue.SceneCharacter.evilmageAI)
        {
            SceneCharacter = evilMageAI.gameObject;
        }

        charID++;
        ContinueEvent();
    }

    void PlayCamAnimation()
    {
        camAnim.enabled = true;
        Debug.Log($"{eventID}: Set Camera Animation.");
        float time = currentDialogue.animation[animID].length;
        string animation = currentDialogue.animation[animID].ToString().Replace("(UnityEngine.AnimationClip)", "");
        Debug.Log("Animation played: " + animation);
        camAnim.Play(animation.Trim());
        animID++;
        StartCoroutine(Wait(time));
    }
    void PlayCharAnimation()
    {
        Debug.Log($"{eventID}: Set Character Animation.");
        float time = currentDialogue.animation[animID].length;
        string animation = currentDialogue.animation[animID].ToString().Replace("(UnityEngine.AnimationClip)", "");
        Debug.Log("Animation played: " + animation);

        sceneCharAnim = SceneCharacter.GetComponent<Animator>();

        sceneCharAnim.Play(animation.Trim());
        animID++;
        StartCoroutine(Wait(time));

    }

    void ChangeCameraPos()
    {
        cam.transform.SetParent(null);
        Debug.Log($"{eventID}: Change Camera Position.");
        cam.transform.position = currentDialogue.CameraPos[camPosID];
        camPosID++;
        ContinueEvent();
    }
    void ChangeCameraRot()
    {
        cam.transform.SetParent(null);
        Debug.Log($"{eventID}: Change Camera Rotation.");
        cam.transform.eulerAngles = currentDialogue.CameraRot[camRotID];
        camRotID++;
        ContinueEvent();
    }
    void ChangeCharacterPos()
    {
        Debug.Log($"{eventID}: Change Character Position.");
        if (SceneCharacter.tag == "NPC")
        {
            SceneCharacter.GetComponent<NavMeshAgent>().Warp(currentDialogue.sceneCharacterPos[charPosID]);
        }
        else
        {
            SceneCharacter.transform.position = currentDialogue.sceneCharacterPos[charPosID];
        }
        charPosID++;
        ContinueEvent();
    }
    void ChangeCharacterRot()
    {
        Debug.Log($"{eventID}: Change Character Rotation.");
        SceneCharacter.transform.eulerAngles = currentDialogue.sceneCharacterRot[charRotID];
        charRotID++;
        ContinueEvent();
    }


    void DisplayText()
    {
        Debug.Log($"{eventID}: Set next sentence with String ID: " + stringID);
        DialoguePanel.SetActive(true);

        sentences = new Queue<string>();
        sentences.Clear();

        sentences.Enqueue(currentDialogue.sentences[stringID]);
        stringID++;
        DisplayNextSentence();
    }
    void ContinueEvent()
    {
        eventID++;
        EventSwitch();
    }

    IEnumerator Wait(float time)
    {
        Debug.Log($"{eventID}: Wait for {time} seconds.");
        DialoguePanel.SetActive(false);
        currentState = CutsceneState.transit;

        yield return new WaitForSeconds(time);

        ContinueEvent();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            ContinueEvent();
            return;
        }

        DialogueManager.singleton.nextSentenceSFX.Play();

        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        dialogueText.text = sentence;

        StartCoroutine(TypeSentence(sentence));

    }

    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    private void Update()
    {

        if (currentState != CutsceneState.inDialogue) return;

        if (Input.GetButtonDown("Fire1"))
        {
            DisplayNextSentence();
        }
    }

    void EndCutscene()
    {
        cam.transform.SetParent(null);
        cam.transform.position = player.transform.position;
        camAnim.enabled = false;
        GameManager.singleton.currentPlaystate = GameManager.PlayState.inOverworld;
        currentState = CutsceneState.none;
        cB.enabled = true;
        DialoguePanel.SetActive(false);
        canvasObjects.SetActive(true);
    }

    public List<Transform> villagerPos;
    public Transform heroFinalePos;
    public Transform mageFinalPos;
    public Transform playerFinalPos;
    public void SetPositionFinalePosition()
    {
        for (int i = 0; i < GameManager.singleton.villagers.Count; i++)
        {
            GameManager.singleton.villagers[i].agent.isStopped = true;
            GameManager.singleton.villagers[i].enabled = false;
            GameManager.singleton.villagers[i].anim.Play("Idle");
            GameManager.singleton.villagers[i].agent.Warp(villagerPos[i].position);
            GameManager.singleton.villagers[i].transform.eulerAngles = villagerPos[i].eulerAngles;

        }

        player.transform.position = playerFinalPos.position;
        player.transform.eulerAngles = playerFinalPos.eulerAngles;
        evilMageAI.enabled = false;
        evilMageAI.anim.Play("Idle");
        evilMageAI.agent.Warp(mageFinalPos.position);
        evilMageAI.gameObject.transform.eulerAngles = mageFinalPos.eulerAngles;
        evilMageAI.agent.isStopped = true;

        hero.enabled = false;
        hero.agent.isStopped = true;
        hero.agent.Warp(heroFinalePos.position);
        hero.gameObject.transform.eulerAngles = heroFinalePos.eulerAngles;
        hero.anim.Play("Idle");

    }

    public void CheckVillagerCount()
    {
        if (GameManager.singleton.villagers.Count > 1)
        {
            Debug.Log("There are villagers to show next sequence.");
            StartEvent(VillagersAtBoss);
        }
        else
        {
            Debug.Log("There are no villagers. Skip Villager sequence");
            StartEvent(noVillagersAtBoss);
        }
    }

    public void SetPosToMonster()
    {
        foreach (AIStateManager ai in GameManager.singleton.villagers)
        {
            if (ai.character.jobClass != TraitManager.JobClass.Hero)
            {
                if (ai.character.raceType == Character.RaceType.monster)
                {
                    cam.transform.SetParent(ai.camTransform.transform);
                    cam.transform.position = new Vector3(0, 0, 0);
                    cam.transform.eulerAngles = new Vector3(0, 0, 0);
                    cam.transform.localPosition = new Vector3(0, 0f, 3f);
                    cam.transform.localEulerAngles = new Vector3(0, 180, 0);
                }
            }
        }
    }

    public void SetPosToHuman()
    {
        foreach (AIStateManager ai in GameManager.singleton.villagers)
        {
            if (ai.character.jobClass != TraitManager.JobClass.Hero)
            {
                if (ai.character.raceType == Character.RaceType.human)
                {
                    cam.transform.SetParent(ai.camTransform.transform);
                    cam.transform.position = new Vector3(0, 0, 0);
                    cam.transform.eulerAngles = new Vector3(0, 0, 0);
                    cam.transform.localPosition = new Vector3(0, 0f, 3f);
                    cam.transform.localEulerAngles = new Vector3(0, 180, 0);
                }
            }
        }
    }

    public CutsceneDialogue HumansSideWithHero;
    public CutsceneDialogue HumansSideWithPlayer;
    public CutsceneDialogue MonstersSideWithMage;
    public CutsceneDialogue MonstersSideWithPlayer;
    public CutsceneDialogue PlayerDecision;
    public void CheckForNextCutsceneHuman()
    {
        bool humansAvailable = false;
        foreach (AIStateManager ai in GameManager.singleton.villagers)
        {
            if (ai.character.raceType == Character.RaceType.human && ai.character.jobClass != TraitManager.JobClass.Hero)
                humansAvailable = true;
        }

        if (humansAvailable)
        {
            if(PopularityManager.singleton.humanPopularity >= 70)
            {
                StartEvent(HumansSideWithPlayer);
            }
            else
            {
                StartEvent(HumansSideWithHero);
            }

        }
        else
        {
            CheckForNextCutsceneMonster();
        }
    }
    public void CheckForNextCutsceneMonster()
    {
        bool monstersAvailable = false;
        foreach (AIStateManager ai in GameManager.singleton.villagers)
        {
            if (ai.character.raceType == Character.RaceType.monster && ai.character.jobClass != TraitManager.JobClass.Hero)
                monstersAvailable = true;
        }

        if (monstersAvailable)
        {
            if (PopularityManager.singleton.monsterPopularity >= 70)
            {
                StartEvent(MonstersSideWithPlayer);
            }
            else
            {
                StartEvent(MonstersSideWithMage);
            }
        }
        else
        {
            StartEvent(PlayerDecision);
        }
    }

    public GameObject DecisionButtons;
    public void EnableDecisionButtons()
    {
        DecisionButtons.SetActive(true);
    }

    public CutsceneDialogue sideWithMage;
    public CutsceneDialogue sideWithHero;
    public void SidewithMage()
    {
        PopularityManager.singleton.ChangeMonsterPopularity(100);
        DecisionButtons.SetActive(false);
        StartEvent(sideWithMage);
    }
    public void SideWithHero()
    {
        PopularityManager.singleton.ChangeHumanPopularity(100);
        DecisionButtons.SetActive(false);
        StartEvent(sideWithHero);
    }

    public Bossfight bossfight;
    public void SetFinalBossFightHero()
    {
        List<AIStateManager> newVillagers = new List<AIStateManager>(GameManager.singleton.villagers);
        List<AIStateManager> newEnemies = new List<AIStateManager>(GameManager.singleton.enemies);

        GameManager.singleton.enemies.Clear();
        GameManager.singleton.villagers.Clear();

        foreach(AIStateManager ai in newVillagers)
        {
            ai.agent.isStopped = false;
            ai.enabled = true;

            if (ai.character.raceType == Character.RaceType.human && PopularityManager.singleton.humanPopularity < 70)
            {
                Debug.Log($"{ai.character.GetCharacterName()} sides with Hero!");
                GameManager.singleton.AddNPCToList(ai, AIStateManager.Faction.enemy);
            }
            else
            {
                Debug.Log($"{ai.character.GetCharacterName()} sides with Evil Mage!");
                GameManager.singleton.AddNPCToList(ai, AIStateManager.Faction.villager);
            }
        }

        foreach (AIStateManager ai in newEnemies)
        {
            ai.agent.isStopped = false;
            ai.enabled = true;

            if (ai.character.raceType == Character.RaceType.human && PopularityManager.singleton.humanPopularity < 70)
            {
                Debug.Log($"{ai.character.GetCharacterName()} sides with Hero!");
                GameManager.singleton.AddNPCToList(ai, AIStateManager.Faction.enemy);
            }
            else
            {
                Debug.Log($"{ai.character.GetCharacterName()} sides with Evil Mage!");
                GameManager.singleton.AddNPCToList(ai, AIStateManager.Faction.villager);
            }
        }


        GameManager.singleton.AddNPCToList(hero, AIStateManager.Faction.enemy);
        GameManager.singleton.AddNPCToList(evilMageAI, AIStateManager.Faction.villager);

        hero.attackState.objToAttack = evilMageAI;
        hero.currentState = hero.TryChangeState(hero.attackState);

        evilMageAI.attackState.objToAttack = hero;
        evilMageAI.currentState = evilMageAI.TryChangeState(evilMageAI.attackState);

        bossfight.SetBossFight(hero);
    }

   

    public void SetFinalBossFightEvilMage()
    {
        List<AIStateManager> newVillagers = new List<AIStateManager>(GameManager.singleton.villagers);
        List<AIStateManager> newEnemies = new List<AIStateManager>(GameManager.singleton.enemies);

        GameManager.singleton.enemies.Clear();
        GameManager.singleton.villagers.Clear();

        foreach (AIStateManager ai in newVillagers)
        {
            ai.agent.isStopped = false;
            ai.enabled = true;

            if (ai.character.raceType == Character.RaceType.monster && PopularityManager.singleton.monsterPopularity < 70)
            {
                Debug.Log($"{ai.character.GetCharacterName()} sides with EvilMage!");
                GameManager.singleton.AddNPCToList(ai, AIStateManager.Faction.enemy);
            }
            else
            {
                Debug.Log($"{ai.character.GetCharacterName()} sides with Hero!");
                GameManager.singleton.AddNPCToList(ai, AIStateManager.Faction.villager);
            }
        }

        foreach (AIStateManager ai in newEnemies)
        {
            ai.agent.isStopped = false;
            ai.enabled = true;

            if (ai.character.raceType == Character.RaceType.monster && PopularityManager.singleton.monsterPopularity < 70)
            {
                Debug.Log($"{ai.character.GetCharacterName()} sides with EvilMage!");
                GameManager.singleton.AddNPCToList(ai, AIStateManager.Faction.enemy);
            }
            else
            {
                Debug.Log($"{ai.character.GetCharacterName()} sides with Hero!");
                GameManager.singleton.AddNPCToList(ai, AIStateManager.Faction.villager);
            }
        }


        GameManager.singleton.AddNPCToList(hero, AIStateManager.Faction.villager);
        GameManager.singleton.AddNPCToList(evilMageAI, AIStateManager.Faction.enemy);

        hero.attackState.objToAttack = evilMageAI;
        hero.currentState = hero.TryChangeState(hero.attackState);

        evilMageAI.attackState.objToAttack = hero;
        evilMageAI.currentState = evilMageAI.TryChangeState(evilMageAI.attackState);

        bossfight.SetBossFight(evilMageAI);

    }

    public void ScaleHero()
    {
        hero.gameObject.transform.localScale = new Vector3(2, 2, 2);
    }
    public void ScaleEvilMage()
    {
        evilMageAI.gameObject.transform.localScale = new Vector3(2, 2, 2);
    }

    public void PlaySFXBackground()
    {
        Debug.Log($"{eventID}: Set SFX Ambience: " + currentDialogue.audioClips[bgmID].name);
        AudioManager.singleton.PlaySFX(currentDialogue.audioClips[bgmID]);
        bgmID++;
        ContinueEvent();
    }

}
