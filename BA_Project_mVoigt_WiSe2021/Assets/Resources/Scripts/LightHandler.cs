using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LightHandler : MonoBehaviour
{
    [SerializeField] private GameObject lightObject = null;

    private void Start()
    {
        DayManager.EveningTime += TurnOnLights;
        DayManager.MorningTime += TurnOffLights;

        if(DayManager.singleton.time > 64800 || DayManager.singleton.time < 21600)
        {
            TurnOnLights();
        }
    }

    private void OnDestroy()
    {
        DayManager.EveningTime -= TurnOnLights;
        DayManager.MorningTime -= TurnOffLights;
    }

    private void TurnOnLights()
    {
        lightObject.SetActive(true);
    }

    private void TurnOffLights()
    {
        lightObject.SetActive(false);
    }
}
