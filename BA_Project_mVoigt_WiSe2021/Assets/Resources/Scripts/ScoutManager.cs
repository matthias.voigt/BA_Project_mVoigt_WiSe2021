using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoutManager : MonoBehaviour
{
    [Header("EventPlaces")]
    public List<EventPlace> eventPlaces = new List<EventPlace>();

    [Header("Events")]
    public List<GameObject> evp_ore = new List<GameObject>();
    public List<GameObject> evp_vegetables = new List<GameObject>();
    public List<GameObject> evp_small = new List<GameObject>();
    public List<GameObject> evp_big = new List<GameObject>();

    [Header("Current Events")]
    public List<GameObject> currentEventsAtBeach = new List<GameObject>();
    public List<GameObject> currentEventsAtRiver = new List<GameObject>();
    public List<GameObject> currentEventsAtMountain = new List<GameObject>();

    public static ScoutManager singleton;

    private void Awake()
    {
        singleton = this;
    }

    /// <summary>
    /// Instantiates an Event
    /// </summary>
    /// <param name="eventObj"></param>
    /// <param name="eventPlace"></param>
    public void InstantiateEvent(GameObject eventObj, EventPlace eventPlace, ReportHandler rp)
    {
        if(eventObj.tag == "Collectable")
        {
            int amount = Random.Range(3, 9);

            for(int i = 0; i < amount; i++)
            {
                Vector3 pos = eventPlace.gameObject.transform.position;
                pos = new Vector3(pos.x, pos.y + Random.Range(1f, 3f), pos.z);

                Instantiate(eventObj, pos, Quaternion.identity);
            }

            if(eventPlace.eventSize == EventPlace.EventSize.ore)
            {
                StatusManager.singleton.InsertNewStatus($"Some items appeared at the {eventPlace.location}.");
                rp.AddScoutResult("I found some ore nearby! You could announce a quest to gather them. Maybe they are worth selling.");
            }
            else if(eventPlace.eventSize == EventPlace.EventSize.vegetables)
            {
                StatusManager.singleton.InsertNewStatus($"Some items appeared at the {eventPlace.location}.");
                rp.AddScoutResult("I found some vegetables nearby! You could announce a quest to gather them.");
            }
        }
        else
        {
            GameObject e = Instantiate(eventObj, eventPlace.gameObject.transform.position, Quaternion.identity);
            
            if(e.TryGetComponent<EventPlaceType>(out EventPlaceType ept))
            {
                eventPlace.currentEvent = e;
                ept = e.GetComponent<EventPlaceType>();
                eventPlace.eventType = ept.eventType;

                if(ept.eventType == EventPlace.EventType.bandits)
                {
                    EventInfromationHandler.singleton.descriptionEvent[4].unlocked = true;
                    StatusManager.singleton.InsertNewStatus($"Bandits appeared at the {eventPlace.location}.");
                    rp.AddScoutResult("Finally, I scouted some bandits! Stay cautious! Maybe do not go alone there!");
                }
                else if(ept.eventType == EventPlace.EventType.merchant)
                {
                    EventInfromationHandler.singleton.descriptionEvent[3].unlocked = true;
                    StatusManager.singleton.InsertNewStatus($"Merchant appeared at the {eventPlace.location}.");
                    rp.AddScoutResult("Finally, I scouted some traveling merchants. They seem harmless. Maybe you can trade with them?");
                }
                else if(ept.eventType == EventPlace.EventType.ruin)
                {
                    EventInfromationHandler.singleton.descriptionEvent[6].unlocked = true;
                    StatusManager.singleton.InsertNewStatus($"A mysterious ruin appeared at the {eventPlace.location}.");
                    rp.AddScoutResult("Finally, I scouted a strange ruin with circled stones! I don't know where it came from or what it is.");
                }
                else if(ept.eventType == EventPlace.EventType.traveler)
                {
                    EventInfromationHandler.singleton.descriptionEvent[2].unlocked = true;
                    StatusManager.singleton.InsertNewStatus($"A traveler appeared at the {eventPlace.location}.");
                    rp.AddScoutResult("Finally, I scouted a traveler with a small tent. He seemed harmless. Maybe you can go for a visit?");
                }

                switch (eventPlace.location)
                {
                    case QuestManager.Location.Beach:
                        currentEventsAtBeach.Add(e);
                        break;
                    case QuestManager.Location.River:
                        currentEventsAtRiver.Add(e);
                        break;
                    case QuestManager.Location.Mountain:
                        currentEventsAtMountain.Add(e);
                        break;
                    default:
                        Debug.LogError("No location found!");
                        return;
                }
            }
            else if (e.tag == "Enemy")
            {
                EventInfromationHandler.singleton.descriptionEvent[1].unlocked = true;
                StatusManager.singleton.InsertNewStatus($"An enemy appeared at the {eventPlace.location}.");
                Debug.Log("Event is an NPC, detaching event Object and destroying it.");
                rp.AddScoutResult("Finally, I scouted an enemy nearby! Stay cautious! He might attack us!");
                e.transform.DetachChildren();
                Destroy(e);
            }
            else if(e.tag == "Fainted")
            {
                EventInfromationHandler.singleton.descriptionEvent[5].unlocked = true;
                StatusManager.singleton.InsertNewStatus($"A fainted resident appeared at the {eventPlace.location}.");
                Debug.Log("Event is an NPC, detaching event Object and destroying it.");
                rp.AddScoutResult("Finally, I scouted a body on the ground! I think the person was still breathing...Maybe we should send help?");
                e.transform.DetachChildren();
                Destroy(e);
            }

        }


    }

    /// <summary>
    /// Returns a list of EventPlaces equal to inserted parameter
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    public List<EventPlace> GetEventPlaces(QuestManager.Location location)
    {
        List<EventPlace> ep = new List<EventPlace>(eventPlaces);
        List<EventPlace> getList = new List<EventPlace>();

        foreach(EventPlace _ep in ep)
        {
            if(_ep.location == location)
            {
                getList.Add(_ep);
            }
        }
        return getList;
    }

    /// <summary>
    /// Get a random event for a place
    /// </summary>
    /// <param name="es"></param>
    /// <returns></returns>
    public GameObject GetRandomEvent(EventPlace.EventSize es)
    {
        switch (es)
        {
            case EventPlace.EventSize.vegetables:           
                return evp_vegetables[Random.Range(0, evp_vegetables.Count)];
            case EventPlace.EventSize.ore:
                return evp_ore[Random.Range(0, evp_ore.Count)];
            case EventPlace.EventSize.small:
                return evp_small[Random.Range(0, evp_small.Count)];
            case EventPlace.EventSize.big:
                return evp_big[Random.Range(0, evp_big.Count)];
            default:
                Debug.Log("No proper size given!");
                return evp_small[Random.Range(0, evp_small.Count)];
        }
    }
    public void SetRandomEvent(List<EventPlace> ep, ReportHandler rp)
    {
        bool randomEventSet = false;

        foreach (EventPlace _ep in ep)
        {
            if (_ep.eventType == EventPlace.EventType.none && !randomEventSet)
            {
                randomEventSet = true;
                InstantiateEvent(GetRandomEvent(_ep.eventSize), _ep, rp);
            }
            else if (_ep.eventType == EventPlace.EventType.resource && !randomEventSet)
            {
                randomEventSet = true;
                InstantiateEvent(GetRandomEvent(_ep.eventSize), _ep, rp);
            }
        }
    }

}
