using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MerchantState : AIState
{
    public AIStateManager aiStateManager;
    public SpecialBuilding ownedMerchBuild;

    public SpecialBuilding selectedBuild;

    public override AIState RunCurrentState()
    {
        SetAgentSpeed();
        SetDisplayMode();

        //There is a merchant building to inhabit
        if (DoMerchantBuildsExist())
        {
            //Get nearest merchant building or own building
            selectedBuild = GetNeareastObject();

            //Return if selectedBuild is still empty
            if(selectedBuild == null)
            {
                Debug.Log("No empty merchant buildings available.");
                //REPORT
                aiStateManager.rP.AddReport($"I wanted sell my goods at the market, but I could not find any available markets. We need more markets!");
                return aiStateManager.TryChangeState(aiStateManager.idleState);
            }

            //Character is nearby build
            if(aiStateManager.IsCharacterNearbyObject(aiStateManager.character.gameObject, selectedBuild.gameObject, 3f))
            {
                //Build is not open yet
                if (!selectedBuild.isOpen)
                {
                    selectedBuild.EnterBuilding(aiStateManager);
                }
                else //Build is open
                {
                    if(DayManager.singleton.time > GameManager.singleton.merchWorkEndTime)
                    {
                        //REPORT
                        aiStateManager.rP.AddReport($"That's it for today. Time to close my shop.");
                        Debug.Log($"{aiStateManager.character.GetCharacterName()} is done working.");
                        ownedMerchBuild.ExitShop();
                        return aiStateManager.currentState = aiStateManager.TryChangeState(aiStateManager.idleState);
                    }
                    else if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("StandingIdle_noWeapon"))
                        aiStateManager.anim.Play("StandingIdle_noWeapon");

                    return this;
                }
            }
            else //Still walking to merchant built
            {
                if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                    aiStateManager.anim.Play("Walk");

                aiStateManager.agent.SetDestination(selectedBuild.enterPosition.position);
            }
        }
        else //No merch builds available
        {
            //REPORT
            aiStateManager.rP.AddReport($"I cannot sell my items without a market...");
            return aiStateManager.TryChangeState(aiStateManager.idleState);
        }

        return this;
    }


    public bool DoMerchantBuildsExist()
    {
        if (FindObjectOfType<SpecialBuilding>())
            return true;
        else
            return false;
    }

    private SpecialBuilding GetNeareastObject()
    {
        if(ownedMerchBuild != null)
        {
            return ownedMerchBuild;
        }
        SpecialBuilding[] allMerchs = FindObjectsOfType<SpecialBuilding>();
        SpecialBuilding closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = aiStateManager.gameObject.transform.position;

        foreach (SpecialBuilding go in allMerchs)
        {
            if (go.owner == null && go.buildingType == SpecialBuilding.BuildingType.shop)
            {
                Vector3 diff = go.transform.position - position;
                float curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
        }

        return closest;
    }

    public void SetAgentSpeed()
    {
        aiStateManager.agent.speed = aiStateManager.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplayMode()
    {
        if (ownedMerchBuild != null)
        {
            if (ownedMerchBuild.isOpen == true)
            {
                aiStateManager.canTalk = false;
                aiStateManager.currentDisplayMode = Dialogue.DisplayMode.merchant;
                aiStateManager.currentTextType = Dialogue.TextType.shop;
            }
        }
        else
        {
            aiStateManager.canTalk = true;
            aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
            aiStateManager.currentTextType = Dialogue.TextType.standard;
        }
    }
}
