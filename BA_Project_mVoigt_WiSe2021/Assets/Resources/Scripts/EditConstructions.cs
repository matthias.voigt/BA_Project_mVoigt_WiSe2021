using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditConstructions : MonoBehaviour
{
    public GameObject currentSelection;
    public static EditConstructions singleton;
    private Camera cam;
    public int affectedLayerForRaycast = 8;
    private void Awake()
    {
        singleton = this;
    }

    private void Start()
    {
        cam = ConstructionManager.singleton.GetConstructionCamera();
    }
    public void DisableCurrentBluePrint(GameObject bP)
    {
        bP.SetActive(false);
    }

    public void EnableCurrentBluePrint(GameObject bP)
    {
        bP.SetActive(true);
    }

    private void Update()
    {
        Raycasting();

        if (Input.GetButton("Fire1"))
        {
            if (CameraMovement.singleton.cM == CameraMovement.ConstructMode.Destroy) DestroyObject();
            else if (CameraMovement.singleton.cM == CameraMovement.ConstructMode.Move) MoveObject();
        }
     
    }
    private void Raycasting()
    {
        if (CameraMovement.singleton.cM != CameraMovement.ConstructMode.Destroy && CameraMovement.singleton.cM != CameraMovement.ConstructMode.Move) return;

        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 500000, (1 << affectedLayerForRaycast)))
        {
            Debug.Log(hit.collider.gameObject.name);
            currentSelection = hit.collider.gameObject;
        }
        else
        {
            currentSelection = null;
        }
    }

    void DestroyObject()
    {
        Debug.Log($"Destroying {currentSelection}");

        if(currentSelection != null)
        {
            Destroy(currentSelection);
        }
    }

    private void MoveObject()
    {
        if (currentSelection == null) return;
        RotateBlueprint();
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 500000, (1 << 6)))
        {
            currentSelection.transform.position = hit.point;
        }
    }

    private void RotateBlueprint()
    {
        float rotationSpeed = 10f;

        if (CameraMovement.singleton.cM != CameraMovement.ConstructMode.Move) return;
        if (currentSelection == null) return;

        if (Input.GetKey(KeyCode.Q))
        {
            currentSelection.transform.Rotate(-Vector3.up * rotationSpeed / 50, Space.World);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            currentSelection.transform.Rotate(Vector3.up * rotationSpeed / 50, Space.World);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            currentSelection.transform.Rotate(Vector3.up * rotationSpeed, Space.Self);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            currentSelection.transform.Rotate(Vector3.down * rotationSpeed, Space.Self);
        }
    }
}
