using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SkeletonSpawner : MonoBehaviour
{
    public GameObject NPC;
    public GameObject spawnEffect;
    public GameObject nightScene;
    private bool inProgress = false;
    private AudioSource aS;

    private void Start()
    {
        aS = GetComponent<AudioSource>();
        OptionsManager.singleton.SetOption += ChangeVolume;
    }
    private void OnDestroy()
    {
        OptionsManager.singleton.SetOption += ChangeVolume;
    }

    void ChangeVolume()
    {
        aS.volume = OptionsManager.singleton.bgmValue;
    }

    private void Update()
    {
        if(DayManager.singleton.currentTimeState == DayManager.CurrentTimeState.night)
        {
            if (!nightScene.activeSelf) nightScene.SetActive(true);

            if (!aS.isPlaying)
            {
                aS.Play();
            }
        }
        else
        {
            if (nightScene.activeSelf) nightScene.SetActive(false);

            if (aS.isPlaying)
            {
                aS.Stop();
            }
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (DayManager.singleton.currentTimeState != DayManager.CurrentTimeState.night) return;

        if(other.gameObject.tag == "Collectable")
        {
            if(other.gameObject.TryGetComponent(out SkullHandler sH))
            {
                if (!inProgress)
                {
                    inProgress = true;
                    StartCoroutine(SpawnSkeleton(sH));
                }

            }
        }
    }

    public void InstantiateEffect(Vector3 pos)
    {
        Instantiate(spawnEffect, pos, Quaternion.identity);
    }


    IEnumerator SpawnSkeleton(SkullHandler sH)
    {
        GameObject o = Instantiate(NPC, sH.gameObject.transform.position, Quaternion.identity);       
        AIStateManager ai = o.GetComponent<AIStateManager>();
        //Name
        if(sH.cName != "")
        {
            ai.character.characterName = sH.cName;
        }
        else
        {
            ai.character.SetCharacterName(PersonalityManager.singleton.GetRandomName());
        }
        //Traits
        if(sH.traits.Count > 0)
        {
            ai.character.traits = new List<TraitManager.Traits>(sH.traits);
        }
        else
        {
            TraitManager.singleton.ReturnRandomTraits(ai.character.traits, ai);
        }

        if(sH.formerRaceType == Character.RaceType.undefined)
        {
            sH.formerRaceType = (Character.RaceType)UnityEngine.Random.Range(1, System.Enum.GetValues(typeof(Character.RaceType)).Length);
        }

        ai.currentDisplayMode = Dialogue.DisplayMode.ressurect;
        ai.currentTextType = Dialogue.TextType.standard;

        ai.character.jobClass = sH.job;
        ai.faction = sH.faction;
        o.name = ai.character.GetCharacterName();
        ai.character.formerRaceType = sH.formerRaceType;
        
        yield return new WaitForSeconds(3f);
        InstantiateEffect(sH.gameObject.transform.position);
        o.SetActive(true);
        Destroy(sH.gameObject);
        inProgress = false;
    }
}
