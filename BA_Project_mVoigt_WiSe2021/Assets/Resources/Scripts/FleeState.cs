using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FleeState : AIState
{
    public AIStateManager ai;
    public AIStateManager fleeFromObj;
    public bool destReached = true;
    Vector3 pos;
    Vector3 initPos;
    public override AIState RunCurrentState()
    {
        ai.canTalk = false;

        if (ai.character.GetProsperitySatisfaction() <= 0)
        {
            ai.NPCUntargetable(true);
            return ai.TryChangeState(ai.faintState);
        }
        else if (fleeFromObj.currentState != fleeFromObj.attackState)
        {
            Debug.Log($"{ai.character.GetCharacterName()} successfully escaped!");

            ai.rP.AddReport("Phew...I could shake my chaser off...");
            if (ai.hasQuest)
            {
                ai.rP.AddQuestReport("I ran so fast, that I could shake off my attacker! I don't know what would've happened if I was caught.");
            }
            return ai.TryChangeState(ai.idleState);
        }


        SetAnimation();
        SetAgentSpeed();

        if (destReached)
        {
            
            pos = RandomNavSphere(transform.position, 30f, -1);
            if(ai.agent.enabled)
                ai.agent.SetDestination(pos);
            else { Debug.LogWarning($"Agent of {ai.character.GetCharacterName()} is disabled! Can not set position!"); }
            destReached = false;
        }
        else if(Vector3.Distance(transform.position, pos) <= 2f)
        {
            destReached = true;
        }

        ai.rP.AddReport("HELP! I am getting attacked! I don't want to fight!");
        
        if(ai.hasQuest)
            ai.rP.AddQuestReport("I tried to escape from the enemy and ran for my life!");


        return this;
    }

    private static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }


    private void SetAnimation()
    {
        if (ai.agent.velocity.magnitude < 0.15f)
        {
            if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                ai.anim.Play("Idle");
        }
        else
        {
            if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Sprint"))
                ai.anim.Play("Sprint");
        }
    }

    public void SetAgentSpeed()
    {
        ai.agent.speed = 4;
    }
}
