using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelfAfterTime : MonoBehaviour
{
    public float selfDestroyTime = 10f;


    void Update()
    {
        selfDestroyTime -= Time.deltaTime;

        if (selfDestroyTime <= 0) Destroy(gameObject);
    }
}
