using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostilityState : AIState
{
    public AIStateManager aiState;
    public LayerMask layerMask;

    public GameObject currentHitObj;

    public override AIState RunCurrentState()
    {
        if(aiState.attackState.objToAttack != null)
        {
            return aiState.TryChangeState(aiState.attackState);
        }
        else
        {
            GetTargetInSeight();
        }
        return this;
    }

    public void GetTargetInSeight()
    {
        RaycastHit hit;
        Vector3 origin = aiState.gameObject.transform.position;
        Vector3 dir = transform.position;
        float sphereRad = 10f;
        float maxDist = 10f;

        if (Physics.SphereCast(origin, sphereRad, dir, out hit, maxDist, layerMask, QueryTriggerInteraction.UseGlobal))
        {
            currentHitObj = hit.collider.gameObject;
            aiState.attackState.objToAttack = currentHitObj.GetComponent<AIStateManager>();
        }
    }


}
