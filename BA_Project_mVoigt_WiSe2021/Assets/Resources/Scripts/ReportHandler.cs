using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReportHandler : MonoBehaviour
{
    public List<string> reports = new List<string>();
    public List<string> questReports = new List<string>();
    AIStateManager ai;
    private void Start()
    {
        ai = GetComponent<AIStateManager>();
    }
    public void AddReport(string s)
    {
        if(reports.Count > 0)
        {
            if (reports[reports.Count - 1] == s)
                return;
        }

        reports.Add(s);

        if(reports.Count > 6)
        {
            reports.RemoveAt(0);
        }
    }

    public void AddScoutResult(string s)
    {
        questReports.Add(s);
    }

    public void AddQuestReport(string s)
    {
        if (ai.questEndState.readyToReport) return;
        if (questReports.Contains(s)) return;

        questReports.Add(s);
    }
    public void ClearQuestReport()
    {
        questReports.Clear();
    }
}
