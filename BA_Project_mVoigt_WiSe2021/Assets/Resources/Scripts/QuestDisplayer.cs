using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class QuestDisplayer : MonoBehaviour
{
    public Quest quest = new Quest();
    public Image icon;
    public TMP_Text questName;



    private void SetQuestDisplay()
    {
        if(quest == null)
        {
            Debug.Log("No Quest available for " + gameObject.name);
            return;
        }

        icon.sprite = QuestManager.singleton.questTypeSprites[(int)quest.questType];

        questName.text = quest.questName;
    }

    public void SetQuest(Quest q)
    {
        quest = q;
        SetQuestDisplay();
    }

    public void ShowDetails()
    {
        QuestBoardNavigator.singleton.AddNavAction(QuestBoardNavigator.NavAction.questDetails);
        QuestBoardNavigator.singleton.DisplayQuestDetails(quest, true);
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

}
