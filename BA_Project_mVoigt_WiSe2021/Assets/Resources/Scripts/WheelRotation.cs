using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelRotation : MonoBehaviour
{
    public AIStateManager aiState;
    public float xRotSpeed = 5f;

    private void Update()
    {
        transform.Rotate(Vector3.right * aiState.agent.velocity.magnitude * xRotSpeed * Time.deltaTime, Space.Self);
    }
}
