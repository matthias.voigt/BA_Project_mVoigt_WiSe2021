using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoutState : AIState
{
    public AIStateManager ai;

    public List<EventPlace> eventPlaces = null;
    public List<EventPlace.EventType> eventTypes;

    public List<bool> destinationReached;    
    public int currentDestID = 0;
    public override AIState RunCurrentState()
    {
        SetDisplaymode();
        SetAgentSpeed();
        SetAnimation();
        ai.character.ReduceSatisfactionBy(GameManager.singleton.GetSatisfactionReduction(ai.character));

        if (!ai.IsActionSuccessful())
        {
            ai.rP.AddReport($"I wanted to scout the {ai.currentQuest.questLocation}, but I am too tired...");
            ai.rP.AddQuestReport($"I wanted to scout the {ai.currentQuest.questLocation}, but I was too tired to go on...");
            
            Debug.Log($"{ai.character.GetCharacterName()} failed an action due to lack of satisfaction.");
            ai.currentQuest.questFailed = true;
            return ai.TryChangeState(ai.questEndState);
        }

        if (ai.hasQuest && !ai.sendQuestStartStatus) ai.SendQuestStartStatus();

        //Initiate EventPlace List of current Location
        if (eventPlaces.Count == 0)
        {
            Initiate();
            return this;
        }
        
        //Return home if every place was scouted
        if (currentDestID >= eventPlaces.Count)
        {
            ai.rP.AddReport($"I finished scouting the {ai.currentQuest.questLocation}. Let's go back to the quest board.");
            ai.rP.AddQuestReport("So I finished scouting the area...I headed back home.");
            return ai.TryChangeState(ai.questEndState);
        }
        //Character is nearby eventplace
        if(ai.IsCharacterNearbyObject(ai.gameObject, eventPlaces[currentDestID].gameObject, 20f))
        {
            ai.rP.AddReport("Hm...is there something interesting here?");

            if(eventPlaces[currentDestID].eventType == EventPlace.EventType.bandits)
            {
                ai.rP.AddQuestReport("Nearby I found some bandits, I also found a prison there...I couldn't see if someone was inside or not...Stay cautious!");
            }
            if (eventPlaces[currentDestID].eventType == EventPlace.EventType.merchant)
            {
                ai.rP.AddQuestReport("I found some traveling merchants nearby! Maybe you can trade items with them?");
            }
            if (eventPlaces[currentDestID].eventType == EventPlace.EventType.ruin)
            {
                ai.rP.AddQuestReport("I found a strange ruin with circled stones nearby...Maybe you should check it out?");
            }
            if (eventPlaces[currentDestID].eventType == EventPlace.EventType.traveler)
            {
                ai.rP.AddQuestReport("I found a traveler nearby...Maybe you should go for a visit.");
            }
            if (eventPlaces[currentDestID].eventType == EventPlace.EventType.resource)
            {
                ai.rP.AddQuestReport("I found a place where some resources might be.");
            }


            eventTypes.Add(eventPlaces[currentDestID].eventType);
            destinationReached[currentDestID] = true;
            currentDestID++;
            return ai.TryChangeState(ai.idleState);
        }
        //Go to event place
        else if (!destinationReached[currentDestID])
        {
            ai.rP.AddReport("Let's check some other places!");
            ai.rP.AddQuestReport($"So, I scouted the area along the {ai.currentQuest.questLocation}.");
            ai.agent.SetDestination(eventPlaces[currentDestID].gameObject.transform.position);
        }
        
        return this;
        
    }

    public void Initiate()
    {
        List<EventPlace> _eventPlaces = new List<EventPlace>(ScoutManager.singleton.GetEventPlaces(ai.currentQuest.questLocation));
        eventTypes = new List<EventPlace.EventType>();
        eventPlaces.Clear();
        destinationReached.Clear();
        currentDestID = 0;

        int count = _eventPlaces.Count;
        if (count > 3) count = 3;
        if(count <= 0)
        {
            Debug.LogError("No event found!");
            return;
        }
        
        for(int i = 0; i < count; i++)
        {
            EventPlace ep = _eventPlaces[Random.Range(0, _eventPlaces.Count)];
            eventPlaces.Add(ep);
            _eventPlaces.Remove(ep);
        }

        for (int i = 0; i < eventPlaces.Count; i++)
        {
            destinationReached.Add(false);
        }
    }

    public void ResetSettings()
    {
        currentDestID = 0;
        destinationReached.Clear();
        eventPlaces.Clear();
        eventTypes.Clear();
    }

    private void SetAnimation()
    {
        if (ai.agent.velocity.magnitude < 0.15f)
        {
            if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                ai.anim.Play("Idle");
        }
        else if (ai.agent.velocity.magnitude < 0.15f)
        {
            if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                ai.anim.Play("Walk");
        }
        else if (ai.agent.velocity.magnitude < 0.5f)
        {
            if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Sprint"))
                ai.anim.Play("Sprint");
        }
    }

    public void SetAgentSpeed()
    {
        ai.agent.speed = ai.runningSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplaymode()
    {
        ai.canTalk = true;
        switch (ai.faction)
        {
            case AIStateManager.Faction.villager:
                if (ai.character.jobClass == TraitManager.JobClass.Hero)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.none;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                else
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.character;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                break;
            case AIStateManager.Faction.enemy:
                ai.currentDisplayMode = Dialogue.DisplayMode.none;
                ai.currentTextType = Dialogue.TextType.standard;
                break;
            case AIStateManager.Faction.neutral:
                if (ai.character.jobClass == TraitManager.JobClass.Adventurer)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.adventurer;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                else if (ai.character.jobClass == TraitManager.JobClass.Merchant)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.merchant;
                    ai.currentTextType = Dialogue.TextType.shop;
                }
                else if (ai.character.race == Character.Race.skeleton)
                {
                    ai.currentDisplayMode = Dialogue.DisplayMode.ressurect;
                    ai.currentTextType = Dialogue.TextType.standard;
                }
                break;
        }
    }
}
