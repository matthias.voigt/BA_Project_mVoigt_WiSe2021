using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class DayManager : MonoBehaviour
{
    public float time;
    public TimeSpan currenttime;
    public Transform SunTransform;
    public Light Sun;
    public TMP_Text timetext;
    public int days;

    public float intensity;
    public Color fogday = Color.gray;
    public Color fognight = Color.black;

    private bool calledMorningTime = false;
    private bool calledEveningTime = false;
    private bool calledMiddayTime = false;
    private bool calledNightTime = false;
    private bool showDayCount = false;

    [SerializeField] private Animator dayNightAnimText = null;
    [SerializeField] private Animator dayCountAnim = null;

    public CurrentTimeState currentTimeState = CurrentTimeState.morning;

    public static Action EveningTime;
    public static Action MorningTime;


    public enum CurrentTimeState
    {
        morning,
        midday,
        evening,
        night,
    }


    public int speed;

    [SerializeField] private TMP_Text daytimeText = null;
    [SerializeField] private TMP_Text dayCountText = null;

    public static DayManager singleton;

    private void Awake()
    {
        singleton = this;
    }

    private void Start()
    {
        calledMorningTime = false;
        calledMiddayTime = false;
        calledEveningTime = false;
        calledNightTime = false;

        if (!showDayCount)
            dayCountAnim.Play("DayNightText");
    }

    void Update()
    {
        ChangeTime();
    }

    public void ChangeTimeSpeedValue(int value)
    {
        speed = value;
    }

    public void ChangeTime()
    {
        time += Time.deltaTime * speed;

        if (time > 86400)
        {
            days += 1;
            time = 0;
            
            calledMorningTime = false;
            calledMiddayTime = false;
            calledEveningTime = false;
            calledNightTime = false;
            dayCountText.text = "Day " + days.ToString();
            dayCountAnim.Play("DayNightText");

        }

        currenttime = TimeSpan.FromSeconds(time);
        string[] temptime = currenttime.ToString().Split(":"[0]);
        timetext.text = temptime[0] + ":" + temptime[1];

        SunTransform.rotation = Quaternion.Euler(new Vector3((time - 21600) / 86400 * 360, 0, 0));

        if(time > 21600 && !calledMorningTime)
        {
            calledMorningTime = true;
            Debug.Log("It is morning time!");
            MorningTime?.Invoke();
            daytimeText.text = "Morning";
            dayNightAnimText.Play("DayNightText");

            currentTimeState = CurrentTimeState.morning;
        }
        if (time > 64800 && !calledEveningTime)
        {
            calledEveningTime = true;
            Debug.Log("It is evening time!");
            EveningTime?.Invoke();
            daytimeText.text = "Evening";
            dayNightAnimText.Play("DayNightText");

            currentTimeState = CurrentTimeState.evening;

        }

        if (time > 43200 && !calledMiddayTime)
        {
            calledMiddayTime = true;
            Debug.Log("It is midday time!");
            daytimeText.text = "Midday";
            dayNightAnimText.Play("DayNightText");

            currentTimeState = CurrentTimeState.midday;
        }

        if (time >79200 && !calledNightTime)
        {
            calledNightTime = true;
            Debug.Log("It is night time!");
            daytimeText.text = "Night";
            dayNightAnimText.Play("DayNightText");

            currentTimeState = CurrentTimeState.night;
        }


        if (time > 43200)
            intensity = 1 - (43200 - time) / 43200;
        else
            intensity = 1 - ((43200 - time) / 43200 * -1);

        RenderSettings.fogColor = Color.Lerp(fognight, fogday, intensity * intensity);

        Sun.intensity = intensity;

    }

    public CurrentTimeState GetCurrentTimeState()
    {
        return currentTimeState;
    }
    public float GetCurrentTimeFloat()
    {
        return time;
    }
}
