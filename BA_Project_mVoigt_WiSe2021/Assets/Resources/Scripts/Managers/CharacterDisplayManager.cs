using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CharacterDisplayManager : MonoBehaviour
{
    [Header("Ingame Menu Panel")]
    [SerializeField] private GameObject ingameMenuPanel;

    [Header("UI Element")]
    [SerializeField] private GameObject characterPanel;
    [SerializeField] private GameObject JobClassPage;
    [SerializeField] private GameObject TraitsPage;
    [SerializeField] private GameObject ProsperityPage;
    [SerializeField] private GameObject ThoughtsPage;

    [Header("Character")]
    [SerializeField] private TMP_Text characterName;
    [SerializeField] private TMP_Text statusText = null;
    [SerializeField] private Image factionImg = null;

    [Header("JobClass References")]
    [SerializeField] private Image jobClassImage;
    [SerializeField] private TMP_Text jobName;

    [Header("Trait References")]
    [SerializeField] private List<TraitDisplayer> traitDisplayers = new List<TraitDisplayer>();

    [Header("Prosperity")]
    [SerializeField] private TMP_Text satisfaction_value;
    [SerializeField] private TMP_Text authority_value;
    [SerializeField] private TMP_Text prestige_value;
    [SerializeField] private TMP_Text money_value;

    [Header("Thoughts")]
    [SerializeField] private List<TMP_Text> thoughtTxt = new List<TMP_Text>();

    [Header("Character Doll")]
    [SerializeField] private AppearanceManager humanDoll = null;
    [SerializeField] private GameObject skeletonDoll = null;
    [SerializeField] private GameObject orcDoll = null;
    [SerializeField] private GameObject batDoll = null;
    [SerializeField] private GameObject evilMageDoll = null;
    [SerializeField] private GameObject golemDoll = null;
    [SerializeField] private GameObject plantDoll = null;
    [SerializeField] private GameObject slimeDoll = null;
    [SerializeField] private GameObject spiderDoll = null;
    [SerializeField] private GameObject turtleDoll = null;
    [SerializeField] private GameObject dragonDoll = null;
    [SerializeField] private SkinnedMeshRenderer sklt_renderer;
    [SerializeField] private SkinnedMeshRenderer orc_renderer;
    [SerializeField] private SkinnedMeshRenderer bat_renderer;
    [SerializeField] private SkinnedMeshRenderer eM_renderer;
    [SerializeField] private SkinnedMeshRenderer glm_renderer;
    [SerializeField] private SkinnedMeshRenderer plnt_renderer;
    [SerializeField] private SkinnedMeshRenderer spdr_renderer;
    [SerializeField] private SkinnedMeshRenderer trtl_renderer;
    [SerializeField] private SkinnedMeshRenderer slm_renderer;
    [SerializeField] private SkinnedMeshRenderer drg_renderer;
    public static CharacterDisplayManager singleton;

    public bool cameFromBuilding = false;
    public bool cameFromBuildingAssign = false;
    public bool cameFromDialogue = false;
    private Character currentChar = null;

    public CharacterDisplayStates currentDisplayState = CharacterDisplayStates.none;
    public enum CharacterDisplayStates 
    { 
        none,
        jobClass,
        traits,
        prosperity,
        thoughts
    }


    private void Awake()
    {
        singleton = this;
    }

    void DisableAllDolls()
    {
        skeletonDoll.SetActive(false);
        orcDoll.SetActive(false);
        humanDoll.gameObject.SetActive(false);
        batDoll.SetActive(false);
        dragonDoll.SetActive(false);
        golemDoll.SetActive(false);
        plantDoll.SetActive(false);
        slimeDoll.SetActive(false);
        spiderDoll.SetActive(false);
        turtleDoll.SetActive(false);
        evilMageDoll.SetActive(false);
    }

    public void DisplayCharacterValues(Character profile)
    {
        currentChar = profile;
        
        AudioManager.singleton.PlayBookPageSFX();

        DisableAllDolls();

        if (profile.race == Character.Race.human)
        {
            AppearanceManager aM = profile.gameObject.GetComponent<AppearanceManager>();
            humanDoll.SetManualApperance(
                aM.GetBeltID(), aM.GetClothID(),
                aM.GetShoulderID(), aM.GetGlovesID(), aM.GetShoesID(), aM.GetHairID(), aM.GetHatID(), aM.GetFaceID());

            humanDoll.gameObject.SetActive(true);
        }
        else if(profile.race == Character.Race.skeleton)
        {          
            sklt_renderer.material = profile.GetComponent<AppearanceManager>().GetCurrentMat();
            skeletonDoll.SetActive(true);
        }
        else if(profile.race == Character.Race.orc)
        {
            orc_renderer.material = profile.GetComponent<AppearanceManager>().GetCurrentMat();
            orcDoll.SetActive(true);
        }
        else if (profile.race == Character.Race.bat)
        {
            bat_renderer.material = profile.GetComponent<AppearanceManager>().GetCurrentMat();
            batDoll.SetActive(true);
        }
        else if (profile.race == Character.Race.mage)
        {
            eM_renderer.material = profile.GetComponent<AppearanceManager>().GetCurrentMat();
            evilMageDoll.SetActive(true);
        }
        else if (profile.race == Character.Race.golem)
        {
            glm_renderer.material = profile.GetComponent<AppearanceManager>().GetCurrentMat();
            golemDoll.SetActive(true);
        }
        else if (profile.race == Character.Race.plant)
        {
            plnt_renderer.material = profile.GetComponent<AppearanceManager>().GetCurrentMat();
            plantDoll.SetActive(true);
        }
        else if (profile.race == Character.Race.slime)
        {
            slm_renderer.material = profile.GetComponent<AppearanceManager>().GetCurrentMat();
            slimeDoll.SetActive(true);
        }
        else if (profile.race == Character.Race.spider)
        {
            spdr_renderer.material = profile.GetComponent<AppearanceManager>().GetCurrentMat();
            spiderDoll.SetActive(true);
        }
        else if (profile.race == Character.Race.turtle)
        {
            trtl_renderer.material = profile.GetComponent<AppearanceManager>().GetCurrentMat();
            turtleDoll.SetActive(true);
        }
        else if (profile.race == Character.Race.dragon)
        {
            drg_renderer.material = profile.GetComponent<AppearanceManager>().GetCurrentMat();
            dragonDoll.SetActive(true);
        }

        characterName.text = profile.GetCharacterName();

        ingameMenuPanel.SetActive(false);
        characterPanel.SetActive(true);
        ShowJobClassPanel();

        //Prosperity
        satisfaction_value.text = profile.GetProsperitySatisfaction().ToString("F0");
        authority_value.text = profile.GetAuthority().ToString();
        prestige_value.text = profile.GetPrestige().ToString();
        money_value.text = profile.gameObject.GetComponent<Inventory>().money.ToString();

        if (profile.gameObject.tag != "Player")
        {
            statusText.text = GetCharacterStatus(profile, profile.gameObject.GetComponent<AIStateManager>());
            statusText.gameObject.SetActive(true);
        }
        else
        {
            statusText.gameObject.SetActive(false);
        }

        //Set JobImage
        jobName.text = profile.jobClass.ToString();
        jobClassImage.sprite = TraitManager.singleton.JobSprites[(int)profile.jobClass];      

        //Set Traits
        for(int i = 0; i < traitDisplayers.Count; i++)
        {
            traitDisplayers[i].SetTraits(profile.traits[i]);
        }

        //Thoughts
        if(profile.gameObject.TryGetComponent(out AIStateManager ai))
        {
            factionImg.sprite = GameManager.singleton.factionIcons[(int)ai.faction];

            for (int i = 0; i < ai.rP.reports.Count; i++)
            {
                thoughtTxt[i].text = ai.rP.reports[i];
            }
        }

    }

    public void CloseDisplay()
    {
        AudioManager.singleton.PlayBookPageSFX();
        characterPanel.SetActive(false);
        JobClassPage.SetActive(false);
        TraitsPage.SetActive(false);
        ProsperityPage.SetActive(false);
        ThoughtsPage.SetActive(false);
    }

    private void Update()
    {
        if (currentDisplayState == CharacterDisplayStates.none) return;

        if(Input.GetButtonDown("Fire2"))
        {
            if (currentDisplayState == CharacterDisplayStates.jobClass)
                TryChangeCharacterDisplayState(CharacterDisplayStates.none, currentChar);

            else if (currentDisplayState == CharacterDisplayStates.prosperity)
                TryChangeCharacterDisplayState(CharacterDisplayStates.none, currentChar);

            else if (currentDisplayState == CharacterDisplayStates.traits)
                TryChangeCharacterDisplayState(CharacterDisplayStates.none, currentChar);

            else if (currentDisplayState == CharacterDisplayStates.thoughts)
                TryChangeCharacterDisplayState(CharacterDisplayStates.none, currentChar);
        }
    }

    private void ShowJobClassPanel()
    {
        AudioManager.singleton.PlayBookPageSFX();
        JobClassPage.SetActive(true);
        TraitsPage.SetActive(false);
        ProsperityPage.SetActive(false);
        ThoughtsPage.SetActive(false);

    }
    private void ShowTraitsPanel()
    {
        AudioManager.singleton.PlayBookPageSFX();
        JobClassPage.SetActive(false);
        TraitsPage.SetActive(true);
        ProsperityPage.SetActive(false);
        ThoughtsPage.SetActive(false);

    }
    private void ShowProsperityPanel()
    {
        AudioManager.singleton.PlayBookPageSFX();
        JobClassPage.SetActive(false);
        TraitsPage.SetActive(false);
        ProsperityPage.SetActive(true);
        ThoughtsPage.SetActive(false);

    }

    private void ShowThoughtsPanel()
    {
        AudioManager.singleton.PlayBookPageSFX();
        JobClassPage.SetActive(false);
        TraitsPage.SetActive(false);
        ProsperityPage.SetActive(false);
        ThoughtsPage.SetActive(true);
    }

    public void ButtonShowProsperity() => TryChangeCharacterDisplayState(CharacterDisplayStates.prosperity, currentChar);
    public void ButtonShowJobClass() => TryChangeCharacterDisplayState(CharacterDisplayStates.jobClass, currentChar);
    public void ButtonShowTraits() => TryChangeCharacterDisplayState(CharacterDisplayStates.traits, currentChar);
    public void ButtonShowThoughts() => TryChangeCharacterDisplayState(CharacterDisplayStates.thoughts, currentChar);


    private string GetCharacterStatus(Character profile, AIStateManager aiStateManager)
    {
        string statusDesc = "...";
        if(aiStateManager.currentState == aiStateManager.idleState)
        {
            statusDesc = "Stares holes into space.";
        }
        else if(aiStateManager.currentState == aiStateManager.wanderState)
        {
            statusDesc = "Takes a walk...";
        }
        else if(aiStateManager.currentState == aiStateManager.talkingState)
        {
            statusDesc = "Deep in conversation...";
        }

        if (aiStateManager.rP.reports.Count > 0)
        {
            return aiStateManager.rP.reports[aiStateManager.rP.reports.Count - 1];
        }
        else
            return statusDesc;
    }

    public void TryChangeCharacterDisplayState(CharacterDisplayStates requestedState, Character character)
    {
        #region Init
        Debug.Log($"Try changing CharacterDisplayState from {currentDisplayState} to {requestedState}");

        if(currentDisplayState == requestedState)
        {
            Debug.Log("Already in requested State. Return.");
            return;
        }

        if (character != null)
        {
            Debug.Log("Displaying character values");
            DisplayCharacterValues(character);
        }
        else
        {
            Debug.LogError("No character to be displayed!");
        }
        #endregion

        #region Current State: NONE
        if (currentDisplayState == CharacterDisplayStates.none)
        {
            if (requestedState == CharacterDisplayStates.jobClass)
            {
                characterPanel.SetActive(true);
                ShowJobClassPanel();
                currentDisplayState = requestedState;
            }
            else if (requestedState == CharacterDisplayStates.traits)
            {
                characterPanel.SetActive(true);
                ShowTraitsPanel();
                currentDisplayState = requestedState;
            }
            else if (requestedState == CharacterDisplayStates.prosperity)
            {
                characterPanel.SetActive(true);
                ShowProsperityPanel();
                currentDisplayState = requestedState;
            }
            else if(requestedState == CharacterDisplayStates.thoughts)
            {
                characterPanel.SetActive(true);
                ShowThoughtsPanel();
                currentDisplayState = requestedState;
            }
        }
        #endregion
        #region Current State: JobClass
        else if (currentDisplayState == CharacterDisplayStates.jobClass)
        {
            if(requestedState == CharacterDisplayStates.none)
            {
                CloseDisplay();
                CloseState();
                    
                currentDisplayState = requestedState;             
            }
            else if(requestedState == CharacterDisplayStates.traits)
            {
                ShowTraitsPanel();
                currentDisplayState = requestedState;
            }
            else if(requestedState == CharacterDisplayStates.prosperity)
            {
                ShowProsperityPanel();
                currentDisplayState = requestedState;
            }
            else if(requestedState == CharacterDisplayStates.thoughts)
            {
                ShowThoughtsPanel();
                currentDisplayState = requestedState;
            }
        }
        #endregion
        #region Current State: Traits
        else if (currentDisplayState == CharacterDisplayStates.traits)
        {
            if (requestedState == CharacterDisplayStates.none)
            {
                CloseDisplay();
                CloseState();

                currentDisplayState = requestedState;
            }
            else if (requestedState == CharacterDisplayStates.jobClass)
            {
                ShowJobClassPanel();
                currentDisplayState = requestedState;
            }
            else if (requestedState == CharacterDisplayStates.prosperity)
            {
                ShowProsperityPanel();
                currentDisplayState = requestedState;
            }
            else if (requestedState == CharacterDisplayStates.thoughts)
            {
                ShowThoughtsPanel();
                currentDisplayState = requestedState;
            }
        }
        #endregion
        #region Current State: Prosperity
        else if (currentDisplayState == CharacterDisplayStates.prosperity)
        {
            if (requestedState == CharacterDisplayStates.none)
            {
                CloseDisplay();
                CloseState();

                currentDisplayState = requestedState;
            }
            else if (requestedState == CharacterDisplayStates.traits)
            {
                ShowTraitsPanel();
                currentDisplayState = requestedState;
            }
            else if (requestedState == CharacterDisplayStates.jobClass)
            {
                ShowJobClassPanel();
                currentDisplayState = requestedState;
            }
            else if (requestedState == CharacterDisplayStates.thoughts)
            {
                ShowThoughtsPanel();
                currentDisplayState = requestedState;
            }
        }
        else if(currentDisplayState == CharacterDisplayStates.thoughts)
        {
            if(requestedState == CharacterDisplayStates.jobClass)
            {
                ShowJobClassPanel();
                currentDisplayState = requestedState;
            }
            else if(requestedState == CharacterDisplayStates.prosperity)
            {
                ShowProsperityPanel();
                currentDisplayState = requestedState;
            }
            else if(requestedState == CharacterDisplayStates.traits)
            {
                ShowTraitsPanel();
                currentDisplayState = requestedState;
            }
            else if(requestedState == CharacterDisplayStates.none)
            {
                CloseDisplay();
                CloseState();

                currentDisplayState = requestedState;
            }
        }
        #endregion
    }

    private void CloseState()
    {
        //Going back to BuildingInfoManager
        if (BuildingInfoManager.singleton.currentInfoState != BuildingInfoManager.CurrentBuildingInfoState.none)
        {
            Debug.Log("Closing Character Display and returning to build info panel.");
            BuildingInfoManager.singleton.TryChangeBuildingInfoState(GetBuildingInfoState(), BuildingInfoManager.singleton.currentBuilding);
        }
        //Going Back to Dialogue
        else if (DialogueManager.singleton.currentDialogueState != DialogueManager.DialogueState.none)
        {
            Debug.Log("Closing Character Display and returning to dialogue.");
        }
        else
        {
            Debug.Log("Closing Character Display and returning to Overworld.");
            GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
        }
    }

    private BuildingInfoManager.CurrentBuildingInfoState GetBuildingInfoState()
    {
        BuildingInfoManager.CurrentBuildingInfoState cbi = BuildingInfoManager.singleton.currentInfoState;

        switch (cbi)
        {
            case BuildingInfoManager.CurrentBuildingInfoState.assignListProfile:
                return BuildingInfoManager.CurrentBuildingInfoState.assignList;
            case BuildingInfoManager.CurrentBuildingInfoState.owner:
                return BuildingInfoManager.CurrentBuildingInfoState.main;
            default:
                return BuildingInfoManager.CurrentBuildingInfoState.main;
        }
    }

    public void ShowCharacterInventory()
    {
        Inventory i = currentChar.gameObject.GetComponent<Inventory>();
        InventoryManager.singleton.DisplayInventory(i, InventoryManager.DisplayMode.npcInventory);
    }
}
