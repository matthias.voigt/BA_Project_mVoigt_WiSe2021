using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearanceManager : MonoBehaviour
{
    #region Appearance Objects
    public bool isPlayer = false;
    [SerializeField] private List<GameObject> Belts = new List<GameObject>();
    [SerializeField] private List<GameObject> Cloths = new List<GameObject>();
    [SerializeField] private List<GameObject> Shoulders = new List<GameObject>();
    [SerializeField] private List<GameObject> Gloves = new List<GameObject>();
    [SerializeField] private List<GameObject> Shoes = new List<GameObject>();

    [SerializeField] private List<GameObject> Hairs = new List<GameObject>();
    [SerializeField] private List<GameObject> Hats = new List<GameObject>();
    [SerializeField] private List<GameObject> Faces = new List<GameObject>();
    [SerializeField] private List<GameObject> HairsHalf = new List<GameObject>();

    [SerializeField] private List<GameObject> unusedAssets = new List<GameObject>();

    public int hairID;
    public int hatsID;
    public int faceID;

    public int beltID;
    public int clothsID;
    public int shouldersID;
    public int shoesID;
    public int glovesID;
    #endregion
    #region Models
    [SerializeField] private GameObject Human;
    [SerializeField] private GameObject Skeleton;
    [SerializeField] private GameObject Orc;
    [SerializeField] private GameObject Golem;
    [SerializeField] private GameObject EvilMage;
    [SerializeField] private GameObject Bat;
    [SerializeField] private GameObject Dragon;
    [SerializeField] private GameObject Plant;
    [SerializeField] private GameObject Slime;
    [SerializeField] private GameObject Spider;
    [SerializeField] private GameObject Turtle;
    #endregion
    #region Avatars
    [SerializeField] private Avatar HumanAvatar;
    [SerializeField] private Avatar OrcAvatar;
    [SerializeField] private Avatar SkeletonAvatar;
    [SerializeField] private Avatar GolemAvatar;
    [SerializeField] private Avatar EvilMageAvatar;
    [SerializeField] private Avatar BatAvatar;
    [SerializeField] private Avatar DragonAvatar;
    [SerializeField] private Avatar PlantAvatar;
    [SerializeField] private Avatar SlimeAvatar;
    [SerializeField] private Avatar SpiderAvatar;
    [SerializeField] private Avatar TurtleAvatar;
    #endregion

    #region Meshes
    [SerializeField] private Material tintMaterial;
    [SerializeField] private SkinnedMeshRenderer orcMesh;
    [SerializeField] private SkinnedMeshRenderer skeletonMesh;
    [SerializeField] private SkinnedMeshRenderer batMesh;
    [SerializeField] private SkinnedMeshRenderer dragonMesh;
    [SerializeField] private SkinnedMeshRenderer plantMesh;
    [SerializeField] private SkinnedMeshRenderer slimeMesh;
    [SerializeField] private SkinnedMeshRenderer spiderMesh;
    [SerializeField] private SkinnedMeshRenderer turtleMesh;
    [SerializeField] private SkinnedMeshRenderer golemMesh;
    [SerializeField] private SkinnedMeshRenderer mageMesh;
    [SerializeField] private Material currentMat = null;
    #endregion

    public float meshColor1;
    public float meshColor2;
    public float meshColor3;
    public float meshBrightness;

    public Color c1;
    public Color c2;
    public Color c3;

    private void Start()
    {
        if(isPlayer)
            LoadPlayerAppearance();
    }

    private void DisableAllObjects()
    {
        Human.SetActive(false);
        Skeleton.SetActive(false);
        Orc.SetActive(false);
        Golem.SetActive(false);
        EvilMage.SetActive(false);
        Bat.SetActive(false);
        Dragon.SetActive(false);
        Plant.SetActive(false);
        Slime.SetActive(false);
        Spider.SetActive(false);
        Turtle.SetActive(false);
    }
    public void GenerateRandomAppearance(Character.Race race)
    {
        DisableAllObjects();
        Animator anim = GetComponent<Animator>();

        switch (race)
        {
            case Character.Race.human:
                GenerateRandomHuman();
                anim.avatar = HumanAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/AI Controller") as RuntimeAnimatorController;
                break;
            case Character.Race.skeleton:
                Skeleton.SetActive(true);
                anim.avatar = SkeletonAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/AI Controller") as RuntimeAnimatorController;
                GenerateRandomMeshColor(tintMaterial, Character.Race.skeleton);
                break;
            case Character.Race.mage:
                EvilMage.SetActive(true);
                anim.avatar = EvilMageAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/EvilMage") as RuntimeAnimatorController;
                GenerateRandomMeshColor(tintMaterial, Character.Race.mage);
                break;
            case Character.Race.orc:
                Orc.SetActive(true);
                anim.avatar = OrcAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/AI Controller") as RuntimeAnimatorController;
                GenerateRandomMeshColor(tintMaterial, Character.Race.orc);
                break;
            case Character.Race.golem:
                Golem.SetActive(true);
                anim.avatar = GolemAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/Golem") as RuntimeAnimatorController;
                GenerateRandomMeshColor(tintMaterial, Character.Race.golem);
                break;
            case Character.Race.bat:
                Bat.SetActive(true);
                anim.avatar = BatAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/Bat") as RuntimeAnimatorController;
                GenerateRandomMeshColor(tintMaterial, Character.Race.bat);
                break;
            case Character.Race.dragon:
                Dragon.SetActive(true);
                anim.avatar = DragonAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/Dragon") as RuntimeAnimatorController;
                GenerateRandomMeshColor(tintMaterial, Character.Race.dragon);
                break;
            case Character.Race.plant:
                Plant.SetActive(true);
                anim.avatar = PlantAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/MonsterPlant") as RuntimeAnimatorController;
                GenerateRandomMeshColor(tintMaterial, Character.Race.plant);
                break;
            case Character.Race.slime:
                Slime.SetActive(true);
                anim.avatar = SlimeAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/Slime") as RuntimeAnimatorController;
                GenerateRandomMeshColor(tintMaterial, Character.Race.slime);
                break;
            case Character.Race.spider:
                Spider.SetActive(true);
                anim.avatar = SpiderAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/Spider") as RuntimeAnimatorController;
                GenerateRandomMeshColor(tintMaterial, Character.Race.spider);
                break;
            case Character.Race.turtle:
                Turtle.SetActive(true);
                anim.avatar = TurtleAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/TurtleShell") as RuntimeAnimatorController;
                GenerateRandomMeshColor(tintMaterial, Character.Race.turtle);
                break;
            default:
                race = Character.Race.human;
                anim.avatar = HumanAvatar;
                GenerateRandomHuman();
                break;
        }

        if (TryGetComponent(out AIStateManager aiState))
        {
            aiState.anim = anim;
        }
    }

    private void GenerateRandomMeshColor(Material _mat, Character.Race race)
    {
        Material mat = new Material(_mat);
        meshColor1 = Random.Range(0, 2);
        meshColor2 = Random.Range(0, 4);
        meshColor3 = Random.Range(0, 2);
        meshBrightness = Random.Range(0, 4);

        mat.SetFloat("_Color01Power", meshColor1);
        mat.SetFloat("_Color02Power", meshColor2);
        mat.SetFloat("_Color03Power", meshColor3);
        mat.SetFloat("_Brightness", meshBrightness);

        
        c1 = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        c2 = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        c3 = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);

        mat.SetColor("_Color01", c1);
        mat.SetColor("_Color02", c2);
        mat.SetColor("_Color03", c3);

        currentMat = mat;

        switch (race)
        {
            case Character.Race.orc:
                orcMesh.material = mat;
                break;
            case Character.Race.skeleton:
                skeletonMesh.material = mat;
                break;
            case Character.Race.bat:
                batMesh.material = mat;
                break;
            case Character.Race.dragon:
                dragonMesh.material = mat;
                break;
            case Character.Race.plant:
                plantMesh.material = mat;
                break;
            case Character.Race.slime:
                slimeMesh.material = mat;
                break;
            case Character.Race.spider:
                spiderMesh.material = mat;
                break;
            case Character.Race.turtle:
                turtleMesh.material = mat;
                break;
            case Character.Race.golem:
                golemMesh.material = mat;
                break;
            case Character.Race.mage:
                mageMesh.material = mat;
                break;


            default:
                Debug.LogWarning("Cannot randomize values of Material!");
                break;
        }
    }

    public Material GetCurrentMat()
    {
        return currentMat;
    }

    private void GenerateRandomHuman()
    {
        Human.SetActive(true);

        DisableAllBelts();
        DisableAllCloths();
        DisableAllFaces();
        DisableAllGloves();
        DisableAllHairs();
        DisableAllHats();
        DisableAllShoes();
        DisableAllShoulders();

        beltID = Random.Range(0, Belts.Count);
        EnableBelt(beltID);
        clothsID = Random.Range(0, Cloths.Count);
        EnableCloth(clothsID);
        glovesID = Random.Range(0, Gloves.Count);
        EnableGloves(glovesID);
        shoesID = Random.Range(0, Shoes.Count);
        EnableShoes(shoesID);
        shouldersID = Random.Range(0, Shoulders.Count);
        EnableShoulders(shouldersID);

        hairID = Random.Range(0, Hairs.Count / 2);
        EnableHair(hairID);
        faceID = Random.Range(0, Faces.Count);
        EnableFace(faceID);
        hatsID = Random.Range(0, Hats.Count);
        EnableHat(hatsID);
    }

    public void GetRandomRace(Character c, List<Character.Race> r)
    {
        c.race = r[Random.Range(0, r.Count)];

        //c.race = (Character.Race)Random.Range(1, System.Enum.GetValues(typeof(Character.Race)).Length);
        
        Debug.Log($"Generating random race for {c.GetCharacterName()}: {c.race}");
    }

    #region Enabling and disabling Object Parts

    public void EnableBelt(int objectID) => Belts[objectID].SetActive(true);
    public void EnableCloth(int objectID) => Cloths[objectID].SetActive(true);
    public void EnableShoulders(int objectID) => Shoulders[objectID].SetActive(true);
    public void EnableGloves(int objectID) => Gloves[objectID].SetActive(true);
    public void EnableShoes(int objectID) => Shoes[objectID].SetActive(true);
    public void EnableHair(int objectID) => Hairs[objectID].SetActive(true);
    public void EnableHat(int objectID) => Hats[objectID].SetActive(true);
    public void EnableFace(int objectID) => Faces[objectID].SetActive(true);


    public void DisableAllBelts()
    {
        for (int i = 0; i < Belts.Count; i++)
        {
            Belts[i].SetActive(false);
        }
    }

    public void DisableAllCloths()
    {
        for (int i = 0; i < Cloths.Count; i++)
        {
            Cloths[i].SetActive(false);
        }
    }
    public void DisableAllShoulders()
    {
        for (int i = 0; i < Shoulders.Count; i++)
        {
            Shoulders[i].SetActive(false);
        }
    }
    public void DisableAllFaces()
    {
        for (int i = 0; i < Faces.Count; i++)
        {
            Faces[i].SetActive(false);
        }
    }
    public void DisableAllGloves()
    {
        for (int i = 0; i < Gloves.Count; i++)
        {
            Gloves[i].SetActive(false);
        }
    }
    public void DisableAllHairs()
    {
        for (int i = 0; i < Hairs.Count; i++)
        {
            Hairs[i].SetActive(false);
        }
    }
    public void DisableAllHats()
    {
        for (int i = 0; i < Hats.Count; i++)
        {
            Hats[i].SetActive(false);
        }
    }
    public void DisableAllShoes()
    {
        for (int i = 0; i < Shoes.Count; i++)
        {
            Shoes[i].SetActive(false);
        }
    }
    #endregion

    #region Get Apperance IDs
    public int GetHairID() { return hairID; }
    public int GetHatID() { return hatsID; }
    public int GetFaceID() { return faceID; }
    public int GetShoesID() { return shoesID; }
    public int GetClothID() { return clothsID; }
    public int GetShoulderID() { return shouldersID; }
    public int GetBeltID() { return beltID; }
    public int GetGlovesID() { return glovesID; }
    public int GetAmountOfHairs => Hairs.Count;
    #endregion

    public void SetManualApperance(int newBeltID, int newClothID, int newShoulderID, int newGloveID, int newShoeID, int newHairID, int newHatsID, int newFaceID)
    {
        DisableAllBelts();
        DisableAllCloths();
        DisableAllFaces();
        DisableAllGloves();
        DisableAllHairs();
        DisableAllHats();
        DisableAllShoes();
        DisableAllShoulders();

        beltID = newBeltID;
        Belts[newBeltID].SetActive(true);
        clothsID = newClothID;
        Cloths[newClothID].SetActive(true);
        shouldersID = newShoulderID;
        Shoulders[newShoulderID].SetActive(true);
        glovesID = newGloveID;
        Gloves[newGloveID].SetActive(true);
        shoesID = newShoeID;
        Shoes[newShoeID].SetActive(true);
        hairID = newHairID;
        Hairs[hairID].SetActive(true);
        faceID = newFaceID;
        Faces[newFaceID].SetActive(true);
        hatsID = newHairID;
        Hats[newHatsID].SetActive(true);

        if(Human != null)
            Human.SetActive(true);
    }


    public void LoadAppearance(VillagerData vD)
    {
        DisableAllObjects();
        Animator anim = GetComponent<Animator>();
        Character.Race r = (Character.Race)vD.raceID;
        switch (r)
        {
            case Character.Race.human:
                SetManualApperance(vD.beltID, vD.clothsID, vD.shouldersID, vD.glovesID, vD.shoesID, vD.hairID, vD.hatsID, vD.faceID);
                anim.avatar = HumanAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/AI Controller") as RuntimeAnimatorController;
                break;
            case Character.Race.skeleton:
                Skeleton.SetActive(true);
                anim.avatar = SkeletonAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/AI Controller") as RuntimeAnimatorController;
                SetManualMonsterApperance(tintMaterial, Character.Race.skeleton);             
                break;
            case Character.Race.mage:
                EvilMage.SetActive(true);
                anim.avatar = EvilMageAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/EvilMage") as RuntimeAnimatorController;
                SetManualMonsterApperance(tintMaterial, Character.Race.mage);
                break;
            case Character.Race.orc:
                Orc.SetActive(true);
                anim.avatar = OrcAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/AI Controller") as RuntimeAnimatorController;
                SetManualMonsterApperance(tintMaterial, Character.Race.orc);
                break;
            case Character.Race.golem:
                Golem.SetActive(true);
                anim.avatar = GolemAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/Golem") as RuntimeAnimatorController;
                SetManualMonsterApperance(tintMaterial, Character.Race.golem);
                break;
            case Character.Race.bat:
                Bat.SetActive(true);
                anim.avatar = BatAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/Bat") as RuntimeAnimatorController;
                SetManualMonsterApperance(tintMaterial, Character.Race.bat);
                break;
            case Character.Race.dragon:
                Dragon.SetActive(true);
                anim.avatar = DragonAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/Dragon") as RuntimeAnimatorController;
                SetManualMonsterApperance(tintMaterial, Character.Race.dragon);
                break;
            case Character.Race.plant:
                Plant.SetActive(true);
                anim.avatar = PlantAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/MonsterPlant") as RuntimeAnimatorController;
                SetManualMonsterApperance(tintMaterial, Character.Race.plant);
                break;
            case Character.Race.slime:
                Slime.SetActive(true);
                anim.avatar = SlimeAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/Slime") as RuntimeAnimatorController;
                SetManualMonsterApperance(tintMaterial, Character.Race.slime);
                break;
            case Character.Race.spider:
                Spider.SetActive(true);
                anim.avatar = SpiderAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/Spider") as RuntimeAnimatorController;
                SetManualMonsterApperance(tintMaterial, Character.Race.spider);
                break;
            case Character.Race.turtle:
                Turtle.SetActive(true);
                anim.avatar = TurtleAvatar;
                anim.runtimeAnimatorController = Resources.Load("Animation/Characters/Monsters/TurtleShell") as RuntimeAnimatorController;
                SetManualMonsterApperance(tintMaterial, Character.Race.turtle);
                break;
            default:
                r = Character.Race.human;
                anim.avatar = HumanAvatar;
                GenerateRandomHuman();
                break;
        }

        if (TryGetComponent(out AIStateManager aiState))
        {
            aiState.anim = anim;
        }


    }

    private void SetManualMonsterApperance(Material _mat, Character.Race race)
    {
        Material mat = new Material(_mat);
        mat.SetFloat("_Color01Power", meshColor1);
        mat.SetFloat("_Color02Power", meshColor2);
        mat.SetFloat("_Color03Power", meshColor3);
        mat.SetFloat("_Brightness", meshBrightness);

        mat.SetColor("_Color01", c1);
        mat.SetColor("_Color02", c2);
        mat.SetColor("_Color03", c3);

        currentMat = mat;

        switch (race)
        {
            case Character.Race.orc:
                orcMesh.material = mat;
                break;
            case Character.Race.skeleton:
                skeletonMesh.material = mat;
                break;
            case Character.Race.bat:
                batMesh.material = mat;
                break;
            case Character.Race.dragon:
                dragonMesh.material = mat;
                break;
            case Character.Race.plant:
                plantMesh.material = mat;
                break;
            case Character.Race.slime:
                slimeMesh.material = mat;
                break;
            case Character.Race.spider:
                spiderMesh.material = mat;
                break;
            case Character.Race.turtle:
                turtleMesh.material = mat;
                break;
            case Character.Race.golem:
                golemMesh.material = mat;
                break;
            case Character.Race.mage:
                mageMesh.material = mat;
                break;


            default:
                Debug.LogWarning("Cannot randomize values of Material!");
                break;
        }
    }



    #region PlayerOnly
    public void LoadPlayerAppearance()
    {
        foreach(GameObject obj in unusedAssets)
        {
            obj.SetActive(false);
        }
        Debug.Log("Loading character appearance.");

        int helmetID = PlayerPrefs.GetInt("helmetID", 0);
        int faceID = PlayerPrefs.GetInt("faceID", 0);
        int hairID = PlayerPrefs.GetInt("hairID", 0);
        int clothID = PlayerPrefs.GetInt("clothID", 0);
        int shoeID = PlayerPrefs.GetInt("shoeID", 0);
        int gloveID = PlayerPrefs.GetInt("gloveID", 0);
        int beltID = PlayerPrefs.GetInt("beltID", 0);
        int shoulderID = PlayerPrefs.GetInt("shoulderID", 0);
        //--------------------------------------
        //Helmet
        DeactivateAllHelmets();

        if (helmetID != -1)
        {
            Hats[helmetID].SetActive(true);
        }
        //--------------------------------------
        //Hair
        DeactivateAllHair();

        if (helmetID == -1)
        {
            Hairs[hairID].SetActive(true);
        }
        else if (helmetID < 7)
        {
            HairsHalf[hairID].SetActive(true);
        }
        //--------------------------------------
        //Face
        DeactivateAllFaces();
        if (helmetID < 7)
        {
            Faces[faceID].SetActive(true);
        }
        //--------------------------------------
        //Cloth
        DeactivateAllCloths();
        Cloths[clothID].SetActive(true);
        //--------------------------------------
        //Glove
        DeactivateAllGloves();
        Gloves[gloveID].SetActive(true);
        //--------------------------------------
        //Belt
        DeactivateAllBelts();
        if (beltID != -1)
        {
            Belts[beltID].SetActive(true);
        }
        //--------------------------------------
        //Shoe
        DeactivateAllShoes();
        Shoes[shoeID].SetActive(true);
        //--------------------------------------
        //--------------------------------------
        //Shoulder
        DeactivateAllShoulderPads();
        if (shoulderID != -1)
        {
            Shoulders[shoulderID].SetActive(true);
        }
    }
    private void DeactivateAllShoulderPads()
    {
        foreach (GameObject shoulder in Shoulders)
        {
            shoulder.SetActive(false);
        }
    }
    private void DeactivateAllHelmets()
    {
        foreach (GameObject helmet in Hats)
        {
            helmet.SetActive(false);
        }
    }
    private void DeactivateAllFaces()
    {
        foreach (GameObject face in Faces)
        {
            face.SetActive(false);
        }
    }
    private void DeactivateAllHair()
    {
        foreach (GameObject hair in Hairs)
        {
            hair.SetActive(false);
        }

        foreach (GameObject hair in HairsHalf)
        {
            hair.SetActive(false);
        }
    }
    private void DeactivateAllCloths()
    {
        foreach (GameObject cloth in Cloths)
        {
            cloth.SetActive(false);
        }
    }
    private void DeactivateAllShoes()
    {
        foreach (GameObject shoe in Shoes)
        {
            shoe.SetActive(false);
        }
    }
    private void DeactivateAllGloves()
    {
        foreach (GameObject glove in Gloves)
        {
            glove.SetActive(false);
        }
    }
    private void DeactivateAllBelts()
    {
        foreach (GameObject belt in Belts)
        {
            belt.SetActive(false);
        }
    }
    #endregion
}
