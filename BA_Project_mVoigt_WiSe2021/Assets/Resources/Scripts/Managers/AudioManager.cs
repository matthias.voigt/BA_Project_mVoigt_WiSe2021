using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource globalSource;
    [SerializeField] private AudioSource bgmSource;
    [SerializeField] private AudioSource bgmAmbienceSource;
    [Header("BGM")]
    [SerializeField] private AudioClip townBGM;
    [SerializeField] private AudioClip oceanBGM;
    [SerializeField] private AudioClip nightBGM;

    [Header("SFX")]
    [SerializeField] private AudioClip[] book_sfx;
    [SerializeField] private AudioClip pickUpSound;
    [SerializeField] private AudioClip statusSFX;
    [SerializeField] private AudioClip questBoardHoverSFX;
    [SerializeField] private AudioClip questBoardClickSFX;
    [SerializeField] private AudioClip clickSFX;

    [Header("Ambience")]
    [SerializeField] private AudioClip mageAmbientBGM;
    [SerializeField] private AudioClip dayTimeForest;

    public static AudioManager singleton;

    private void Awake()
    {
        singleton = this;
    }

    private void Start()
    {
        globalSource = GetComponent<AudioSource>();

        OptionsManager.singleton.SetOption += SetAudioVolume;
    }

    private void OnDestroy()
    {
        OptionsManager.singleton.SetOption -= SetAudioVolume;
    }

    public void PlaySFX(AudioClip ac)
    {
        globalSource.clip = ac;
        SetAudioVolume();
        globalSource.Play();
    }

    public void PlayAmbience(AudioClip ac)
    {
        SetAudioVolume();
        bgmAmbienceSource.clip = ac;
        bgmAmbienceSource.Play();
    }

    

    private AudioClip GetRandomBookPageSFX()
    {
        return book_sfx[UnityEngine.Random.Range(0, book_sfx.Length)];
    }

    public void PlayBookPageSFX()
    {
        globalSource.clip = GetRandomBookPageSFX();
        globalSource.Play();
    }

    public void PlayPickUpSFX()
    {
        globalSource.clip = pickUpSound;
        globalSource.Play();
    }

    public void PlayNightBGM()
    {
        globalSource.clip = nightBGM;
        globalSource.Play();
    }
    public void PlayDayBGM()
    {
        globalSource.clip = townBGM;
        globalSource.Play();
    }

    public void PlayEvilMageAmbient(bool turn)
    {

        if (turn)
        {
            bgmSource.Stop();
            bgmAmbienceSource.clip = mageAmbientBGM;
            bgmAmbienceSource.Play();
        }
        else
        {
            bgmSource.Play();
            bgmAmbienceSource.clip = dayTimeForest;
            bgmAmbienceSource.Play();
        }
    }
    public void PlayBGM(AudioClip ac)
    {
        bgmSource.volume = OptionsManager.singleton.bgmValue;
        bgmSource.clip = ac;
        bgmSource.Play();
    }

    public void PlayStatusSFX()
    {
        globalSource.clip = statusSFX;
        globalSource.Play();
    }

    public void PlayQuestBoardHoverPageSFX ()
    {
        globalSource.clip = questBoardHoverSFX;
        globalSource.Play();
    }
    public void PlayQuestBoardClickSFX()
    {
        globalSource.clip = questBoardClickSFX;
        globalSource.Play();
    }

    public void PlayClickSFX()
    {
        globalSource.clip = clickSFX;
        globalSource.Play();
    }

    public void PlayTownBGM()
    {
        bgmSource.clip = townBGM;
        bgmSource.Play();
    }

    public void PlayOceanBGM()
    {
        bgmSource.clip = oceanBGM;
        bgmSource.Play();
    }

    public void SetAudioVolume()
    {
       globalSource.volume = OptionsManager.singleton.sfxValue;
       bgmSource.volume = OptionsManager.singleton.bgmValue;
       bgmAmbienceSource.volume = OptionsManager.singleton.bgmValue;
    }

}
