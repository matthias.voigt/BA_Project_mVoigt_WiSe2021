using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionManager : MonoBehaviour
{
    [Header("Raycast")]
    [SerializeField] private float rayDistance = 2f;
    [Header("Icons")]
    [SerializeField] private GameObject talkingBubble;
    [SerializeField] private GameObject interactionIcon;

    [Header("Components")]
    [SerializeField] private GameObject player;
    [SerializeField] private Inventory playerInventory;
    [SerializeField] private Animator anim;
    [SerializeField] private Camera cam;


    private void Start()
    {
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        if(playerInventory == null)
        {
            playerInventory = player.GetComponent<Inventory>();
        }
    }

    private void Raycast()
    {
        //Raycasters
        RaycastHit hit;
        RaycastHit npcHit;

        //ObjectLayers
        int buildingMask = 1 << 14;
        int npcMask = 1 << 10;

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward * 2), out npcHit, 1, npcMask))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * npcHit.distance, Color.blue);
            Debug.Log(npcHit.collider.gameObject.name);

            talkingBubble.SetActive(true);
 

            if (Input.GetKeyDown(KeyCode.E))
            {
                if (npcHit.collider.gameObject.TryGetComponent<InteractableObject>(out InteractableObject iO))
                {
                    iO.InteractWithObject(anim);
                }
            }
        }
        else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 1, buildingMask))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log(hit.collider.gameObject.name);

            interactionIcon.SetActive(true);

            if (Input.GetKeyDown(KeyCode.E))
            {
                if(hit.collider.gameObject.TryGetComponent<InteractableObject>(out InteractableObject iO))
                {                   
                    iO.InteractWithObject(anim);
                }
            }
        }
        else
        {
            interactionIcon.SetActive(false);
            talkingBubble.SetActive(false);
        }

        if (interactionIcon.activeSelf) interactionIcon.transform.LookAt(cam.transform);
        if (talkingBubble.activeSelf) talkingBubble.transform.LookAt(cam.transform);
    }

    private void Update()
    {
        if (GameManager.singleton.currentPlaystate != GameManager.PlayState.inOverworld ||
            CutsceneManager.singleton.currentState != CutsceneManager.CutsceneState.none)
        {
            interactionIcon.SetActive(false);
            talkingBubble.SetActive(false);
            return;
        }

        Raycast();      
    }
}
