using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class StatusManager : MonoBehaviour
{
    [SerializeField] private TMP_Text statusText;
    [SerializeField] private GameObject statusObject;

    public Transform startPos;
    public Transform endPos;
    public float movingSpeed = 500f;


    public List<string> statusQueue = new List<string>();
    public string currentString;

    bool playedSound;

    public static StatusManager singleton;

    private void Awake()
    {
        singleton = this;
    }

    public void InsertNewStatus(string status)
    {
        statusQueue.Add(status);       
    }

    public void Update()
    {
        if (statusQueue.Count <= 0 || GameManager.singleton.currentPlaystate != GameManager.PlayState.inOverworld)
        {
            statusObject.SetActive(false);
            return;
        }

        statusObject.SetActive(true);

        if (!playedSound)
        {
            playedSound = true;
            AudioManager.singleton.PlayStatusSFX();
        }

        currentString = statusQueue[0];
        statusText.text = currentString;
        statusObject.transform.position += Vector3.right * Time.deltaTime * movingSpeed;

        if(Vector3.Distance(statusObject.transform.position, endPos.transform.position) < 25f)
        {
            Debug.Log("Status text reached end destination.");
            playedSound = false;
            statusObject.transform.position = startPos.position;
            statusQueue.Remove(currentString);
        }
    }
}
