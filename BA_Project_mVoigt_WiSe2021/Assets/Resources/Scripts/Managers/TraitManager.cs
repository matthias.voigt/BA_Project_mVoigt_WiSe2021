using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TraitManager : MonoBehaviour
{
    public static TraitManager singleton;
    public int amountOfTraits = 3;
    public float grumpyRecoveryRate = 1.2f;
    public float happyNatureRecoveryRate = 1.2f;



    private void Awake()
    {
        singleton = this;
    }


    [Header("Job Values")]
    public Sprite[] JobSprites;
    public enum JobClass
    {
        undefined,
        Hero,
        Villager,
        Adventurer,
        Merchant,
        Guard,
        Researcher,
    }

    [Header("Trait Values")]
    public Sprite[] TraitSprites;
    public enum Traits
    {
        //Actions
        Pacifist,                   //Character always performs non aggressive actions          [Neutral]       [00]
        Aggressive,                 //Character always performs aggressive actions              [Neutral]       [01]       

        //Sleep
        NightOwl,                   //Character wakes up at 8:00 and goes to bed at 2:00        [Neutral]       [02]
        EarlyBird,                  //Character wakes up 4:00 and goes to bed at 22:00          [Neutral]       [03]
        SleepyHead,                 //Character wakes up at 10:00 and goes to bed at 22:00      [Negative]      [04]
        Sleepless,                  //Character wakes up at 4:00 and goes to bed at 24:00       [Positive]      [05]

        //Satisfaction Loss
        Workaholic,                 //Character looses less Satisfaction for actions            [Positive]      [06]
        Lazyhead,                   //Character looses more Satisfaction for actions            [Negative]      [07]

        //Tax based Traits
        Generous,                   //Character pays more taxes.                                [Positive]      [08]
        Miser,                      //Character pays less taxes.                                [Negative]      [9]

        //Satisfaction Recovery
        Grumpy,                     //Character recovers less Satisfaction from sleeping.       [Negative]      [10]
        HappyNature,                //Character recovers more Satisfaction from sleeping.       [Positive]      [11]
        undefined
    }

    public JobClass ReturnRandomClass()
    {
        JobClass job = (JobClass)Random.Range(2, System.Enum.GetValues(typeof(JobClass)).Length);
        return job;
    }


    public List<Traits> ReturnRandomTraits(List<Traits> characterTraits, AIStateManager aiStateManager)
    {
        characterTraits.Clear();
        List<Traits> traitList = GenerateTraitList();

        for (int i = 0; i < amountOfTraits; i++)
        {
            Traits trait = traitList[Random.Range(0, traitList.Count)];

            switch (trait)
            {
                case Traits.Aggressive:     
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);
                    traitList.Remove(Traits.Pacifist);
                    traitList.Remove(trait);
                    break;

                case Traits.Pacifist:
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);
                    traitList.Remove(trait);
                    traitList.Remove(Traits.Aggressive);
                    break;

                case Traits.EarlyBird:
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);

                    aiStateManager.SetSleepTrait(TraitManager.Traits.EarlyBird);
                    aiStateManager.character.wakeTime = TraitManager.singleton.GetWakeTime(trait);
                    aiStateManager.character.sleepTime = TraitManager.singleton.GetSleepTime(trait);

                    traitList.Remove(Traits.NightOwl);
                    traitList.Remove(Traits.EarlyBird);
                    traitList.Remove(Traits.Sleepless);
                    traitList.Remove(Traits.SleepyHead);
                    break;

                case Traits.NightOwl:
                    Debug.Log($"Rolling for trait: {trait}");
                    aiStateManager.SetSleepTrait(TraitManager.Traits.NightOwl);
                    characterTraits.Add(trait);

                    aiStateManager.character.wakeTime = TraitManager.singleton.GetWakeTime(trait);
                    aiStateManager.character.sleepTime = TraitManager.singleton.GetSleepTime(trait);

                    traitList.Remove(Traits.NightOwl);
                    traitList.Remove(Traits.EarlyBird);
                    traitList.Remove(Traits.Sleepless);
                    traitList.Remove(Traits.SleepyHead);
                    break;


                case Traits.Sleepless:
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);
                    aiStateManager.SetSleepTrait(TraitManager.Traits.Sleepless);
                    aiStateManager.character.wakeTime = TraitManager.singleton.GetWakeTime(trait);
                    aiStateManager.character.sleepTime = TraitManager.singleton.GetSleepTime(trait);

                    traitList.Remove(Traits.NightOwl);
                    traitList.Remove(Traits.EarlyBird);
                    traitList.Remove(Traits.Sleepless);
                    traitList.Remove(Traits.SleepyHead);
                    break;
                case Traits.SleepyHead:
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);
                    aiStateManager.SetSleepTrait(TraitManager.Traits.SleepyHead);

                    aiStateManager.character.wakeTime = TraitManager.singleton.GetWakeTime(trait);
                    aiStateManager.character.sleepTime = TraitManager.singleton.GetSleepTime(trait);

                    traitList.Remove(Traits.NightOwl);
                    traitList.Remove(Traits.EarlyBird);
                    traitList.Remove(Traits.Sleepless);
                    traitList.Remove(Traits.SleepyHead);
                    break;

                case Traits.Workaholic:
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);
                    traitList.Remove(trait);
                    traitList.Remove(Traits.Lazyhead);
                    break;

                case Traits.Lazyhead:
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);
                    traitList.Remove(Traits.Workaholic);
                    traitList.Remove(trait);
                    break;

                case Traits.Generous:
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);
                    traitList.Remove(trait);
                    traitList.Remove(Traits.Miser);
                    break;

                case Traits.Miser:
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);
                    traitList.Remove(Traits.Generous);
                    traitList.Remove(trait);
                    break;

                case Traits.HappyNature:
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);
                    aiStateManager.SetSatisfactionTrait(trait);
                    traitList.Remove(Traits.HappyNature);
                    traitList.Remove(Traits.Grumpy);
                    break;

                case Traits.Grumpy:
                    Debug.Log($"Rolling for trait: {trait}");
                    characterTraits.Add(trait);
                    aiStateManager.SetSatisfactionTrait(trait);
                    traitList.Remove(Traits.HappyNature);
                    traitList.Remove(Traits.Grumpy);                    
                    break;

                default:
                    Debug.Log("Something went wrong with generating trait list. No trait added.");
                    break;

            }
        }

        return traitList;
    }

    private List<Traits> GenerateTraitList()
    {
        List<Traits> _traitList = new List<Traits>();

        _traitList.Add(Traits.Aggressive);
        _traitList.Add(Traits.EarlyBird);
        _traitList.Add(Traits.Sleepless);
        _traitList.Add(Traits.Generous);
        _traitList.Add(Traits.Grumpy);
        _traitList.Add(Traits.HappyNature);
        _traitList.Add(Traits.Lazyhead);
        _traitList.Add(Traits.Miser);
        _traitList.Add(Traits.NightOwl);
        _traitList.Add(Traits.Pacifist);
        _traitList.Add(Traits.Workaholic);
        _traitList.Add(Traits.SleepyHead);

        return _traitList;
    }

    public float GetWakeTime(Traits trait)
    {

        float wakeTime = 21600f;

        switch (trait)
        {
            case TraitManager.Traits.NightOwl:
                wakeTime = 28800f;
                return wakeTime;
            case TraitManager.Traits.EarlyBird:
                wakeTime = 14400f;
                return wakeTime;
            case TraitManager.Traits.SleepyHead:
                wakeTime = 36000f;
                return wakeTime;
            case TraitManager.Traits.Sleepless:
                wakeTime = 14400f;
                return wakeTime;
            default:
                Debug.Log("No Sleep Trait found on " + gameObject.name + ". Setting normal wake time.");
                wakeTime = 21600f;
                return wakeTime;
        }
    }

    public float GetSleepTime(Traits trait)
    {
        float sleepTime = 79200f;
        switch (trait)
        {
            case TraitManager.Traits.NightOwl:
                sleepTime = 7200f;
                return sleepTime;
            case TraitManager.Traits.EarlyBird:
                sleepTime = 79200f;
                return sleepTime;
            case TraitManager.Traits.SleepyHead:
                sleepTime = 79200f;
                return sleepTime;
            case TraitManager.Traits.Sleepless:
                sleepTime = 86400;
                return sleepTime;
            default:
                Debug.Log("No Sleep Trait found on " + gameObject.name + ". Setting normal sleep time.");
                sleepTime = 79200f;
                return sleepTime;
        }
    }
}
