using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Cinemachine;

public class GameManager : MonoBehaviour
{
    public bool devMode = false;
    [Header("Settings")]
    public float merchWorkBeginTime = 36000;
    public float merchWorkEndTime = 64800;

    public float guardWorkBeginTime = 36000;
    public float guardWorkEndTime = 79200;

    public float merchRiseFactor = 1.35f;

    public float maxTimeTillItemSpawn = 129600f;
    public float minTimeTillItemSpawn = 86400f;

    public float satisfactionReduction = 0.05f;
    public float satisfactionMultiplicator = 0.2f;

    public float timeTillRecovery = 86400f;

    public float minTimeTillEvent = 43200f;
    public float maxTimeTillEvent = 129600f;

    public float maxDistanceFight = 50f;
    public float seightBuff = 50f;

    public Inventory PlayerInventory = null;

    [SerializeField] private TMP_Text money_text = null;
    [SerializeField] private TMP_Text globalMoney_text = null;

    [Header("Villagers")]
    public List<AIStateManager> villagers = new List<AIStateManager>();
    public List<AIStateManager> enemies = new List<AIStateManager>();
    public List<AIStateManager> neutrals = new List<AIStateManager>();
    public GameObject playerObj;

    [Header("Panels")]
    [SerializeField] private GameObject buildingInfoPanel = null;
    [SerializeField] private GameObject constructionPanel = null;
    [SerializeField] private GameObject characterInfoPanel = null;
    [SerializeField] public GameObject ingameMenuPanel = null;
    [SerializeField] private GameObject dialoguePanel = null;

    public Camera mainCamera = null;
    public Camera thirdPersonCam;

    public Sprite[] factionIcons;

    public static GameManager singleton;


    [Header("State Overview")]
    public PlayState currentPlaystate = PlayState.inOverworld;

    private void Awake()
    {
        singleton = this;

    }

    private void OnApplicationQuit()
    {
        Debug.Log("Resetting load settings.");
        PlayerPrefs.SetInt("load", 0);
    }

    public float GetSatisfactionReduction(Character c)
    {
        float v = satisfactionReduction * satisfactionMultiplicator;
        float reduction = 0;

        if (c.traits.Contains(TraitManager.Traits.Workaholic))
        {
            reduction = satisfactionReduction - v;
        }
        else if (c.traits.Contains(TraitManager.Traits.Lazyhead))
        {
            reduction = satisfactionReduction + v;
        }

        return reduction;
    }


    public enum PlayState
    {
        inOverworld,
        inMenu,
        spectationMode
    }


    public void TryChangePlayState(PlayState requestedState)
    {
        Debug.Log($"Try changing playstate from {currentPlaystate} to {requestedState}.");

        if (currentPlaystate == requestedState)
        {
            Debug.Log($"{this.name} is already in requested state.");
            return;
        }
        else if (currentPlaystate == PlayState.inOverworld)
        {
            if (requestedState == PlayState.inMenu)
            {
                thirdPersonCam.GetComponent<CinemachineBrain>().enabled = false;
                currentPlaystate = requestedState;              
            }           
        }
        else if (currentPlaystate == PlayState.inMenu)
        {
            if (requestedState == PlayState.inOverworld)
            {
                thirdPersonCam.GetComponent<CinemachineBrain>().enabled = true;
                currentPlaystate = requestedState;
            }
            else if (requestedState == PlayState.spectationMode)
            {
                ingameMenuPanel.SetActive(false);
                thirdPersonCam.GetComponent<CinemachineBrain>().enabled = true;
                currentPlaystate = requestedState;
            }
        }
        else if (currentPlaystate == PlayState.spectationMode)
        {
            if (requestedState == PlayState.inMenu)
            {
                ingameMenuPanel.SetActive(true);
                thirdPersonCam.GetComponent<CinemachineBrain>().enabled = false;
                currentPlaystate = requestedState;
            }
        }
        else
            Debug.LogError("Cannot change playstate!");
    }

    private void Update()
    {      
        money_text.text = InventoryManager.singleton.playerInventory.money.ToString();

        if (FlyManager.singleton.transformation == FlyManager.Transfomation.bat) return;
        if (MenuManager.singleton.IsMenuOpen()) return;

        if (Input.GetKeyDown(KeyCode.Alpha1) && GameManager.singleton.currentPlaystate == PlayState.inOverworld)
        {
            AllEnemiesDefeated();
            AudioManager.singleton.PlayBookPageSFX();

            if (ConstructionMenuAvailable())
            {
                ConstructionManager.singleton.constructButton.interactable = true;
            }
            else
            {
                ConstructionManager.singleton.constructButton.interactable = false;
            }

            if (!ingameMenuPanel.activeSelf)
            {
                GameManager.singleton.TryChangePlayState(PlayState.inMenu);
                ingameMenuPanel.SetActive(true);
            }

        }
        else if (Input.GetButtonDown("Fire2"))
        {
            if (GameManager.singleton.currentPlaystate == PlayState.inOverworld) return;

            if (ingameMenuPanel.activeSelf)
            {
                AudioManager.singleton.PlayBookPageSFX();
                GameManager.singleton.TryChangePlayState(PlayState.inOverworld);
                ingameMenuPanel.SetActive(false);
            }
        }
        else if (Input.GetKeyDown(KeyCode.F8) && devMode)
        {
            playerObj.GetComponent<Inventory>().AddMoney(9999);
        }
    }


    public void DisplayPlayerMoney(int playerMoney)
    {
        money_text.text = playerMoney.ToString();
        globalMoney_text.text = playerMoney.ToString();
    }

    public void CloseAllPanels()
    {
        buildingInfoPanel.SetActive(false);
        constructionPanel.SetActive(false);
        characterInfoPanel.SetActive(false);
        ingameMenuPanel.SetActive(false);
        dialoguePanel.SetActive(false);
    }

    public bool ConstructionMenuAvailable()
    {
        RaycastHit hit;
        int layerMask = LayerMask.GetMask("Ground");
        if (Physics.Raycast(playerObj.transform.position, Vector3.down, out hit, layerMask))
        {
            if (hit.collider.gameObject.name == "TownTerrain")
                return true;
            else return false;
        }
        else
            return false;
    }

    public void RemoveNPCFromList(AIStateManager ai)
    {
        if (ai.faction == AIStateManager.Faction.villager)
        {
            villagers.Remove(ai);
        }
        else if (ai.faction == AIStateManager.Faction.neutral)
        {
            neutrals.Remove(ai);
        }
        else if (ai.faction == AIStateManager.Faction.enemy)
        {
            enemies.Remove(ai);
        }
        UpdateIDs();
    }

    public void AddNPCToList(AIStateManager ai, AIStateManager.Faction newFaction)
    {
        ai.faction = newFaction;

        if (newFaction == AIStateManager.Faction.villager)
        {
            if (villagers.Contains(ai))
            {
                Debug.LogWarning($"{ai.character.GetCharacterName()} already is a villager. Return!");
                return;
            }


            if (enemies.Contains(ai)) enemies.Remove(ai);
            if (neutrals.Contains(ai)) neutrals.Remove(ai);

            Debug.Log($"Adding {ai.character.GetCharacterName()} to villager list.");


            villagers.Add(ai);
        }
        else if (newFaction == AIStateManager.Faction.neutral)
        {
            if (neutrals.Contains(ai))
            {
                Debug.LogWarning($"{ai.character.GetCharacterName()} already is neutral. Return!");
                return;
            }

            if (enemies.Contains(ai)) enemies.Remove(ai);
            if (villagers.Contains(ai)) villagers.Remove(ai);

            Debug.Log($"Adding {ai.character.GetCharacterName()} to neutral list.");


            neutrals.Add(ai);
        }
        else if (newFaction == AIStateManager.Faction.enemy)
        {
            if (enemies.Contains(ai))
            {
                Debug.LogWarning($"{ai.character.GetCharacterName()} already is an enemy. Return!");
                return;
            }

            if (neutrals.Contains(ai)) neutrals.Remove(ai);
            if (villagers.Contains(ai)) villagers.Remove(ai);

            Debug.Log($"Adding {ai.character.GetCharacterName()} to enemy list.");

            enemies.Add(ai);
        }

        UpdateIDs();
    }

    private void UpdateIDs()
    {

        for (int i = 0; i < villagers.Count; i++)
        {
            villagers[i].character.SetVillagerID(i);
        }

        for (int i = 0; i < neutrals.Count; i++)
        {
            neutrals[i].character.SetVillagerID(i);
        }

        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].character.SetVillagerID(i);
        }

    }

    public TMPro.TMP_Text questText;
    public bool AllEnemiesDefeated()
    {
        if (enemies.Count == 0)
        {
            questText.text = "Defeat the Evil Mage!";
            Debug.Log("No Enemies in enemy list. Returning true for boss scene init.");
            return true;
        }
        else
        {
            int amountOfEnemies = 0;

            for (int i = 0; i < enemies.Count; i++)
            {
                AIStateManager ai = enemies[i];

                if (ai.currentState == ai.faintState) amountOfEnemies++;
                else if (ai.currentState == ai.imprisonedState) amountOfEnemies++;
                else if (ai.isDead) amountOfEnemies++;
                else if (ai.currentState == ai.recoverState) amountOfEnemies++;
                else if (ai.currentState == ai.followState) amountOfEnemies++;
            }

            if (amountOfEnemies >= enemies.Count)
            {
                questText.text = "Defeat the Evil Mage!";
                Debug.Log("Enemies are dead or fainted. return true");
                return true;
            }

            else
            {
                questText.text = "Defeat or recruit all enemies!";
                return false;
            }
        }
    }
}
