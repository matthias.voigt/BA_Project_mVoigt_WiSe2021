using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class DialogueManager : MonoBehaviour
{
    [Header("UI Elements")]
    [SerializeField] public GameObject DialoguePanel = null;
    [SerializeField] private GameObject InteractionButtons = null;
    [SerializeField] private TMP_Text followText;
    [SerializeField] private GameObject MerchantButtons = null;
    [SerializeField] private GameObject ResearchButtons = null;
    [SerializeField] private GameObject PrisonerButtons = null;
    [SerializeField] private GameObject LootButtons = null;
    [SerializeField] private GameObject RuinButtons = null;
    [SerializeField] private GameObject HospitalButtons = null;
    [SerializeField] private GameObject AdventurerButtons = null;
    [SerializeField] private TMP_Text AdventurerRecruitText = null;

    //public TMP_Text nameText;
    [SerializeField] public TMP_Text dialogueText;
    public AIStateManager currentAI = null;
    [SerializeField] public AudioSource nextSentenceSFX = null;

    public Dialogue defaultDialogue = null;
    
    private Queue<string> sentences;
    
    [Header("Current State")]
    public Dialogue currentDialogue;
    public DialogueState currentDialogueState = DialogueState.none;

    [Header("Prison")]
    public TMP_Text freeForMoneyTxt = null;

    public static DialogueManager singleton;

    public enum DialogueState
    {
        none,
        inDialogue
    }

    private void Awake()
    {
        singleton = this;
    }

    private void Start()
    {
        sentences = new Queue<string>();
    }

    public void StartDialogue(AIStateManager currentAI)
    {
        if(currentAI.currentTextType == Dialogue.TextType.QG_Report)
        {
            if (currentAI.currentQuest.questType == QuestManager.QuestType.Scout)
            {
                if (currentAI.currentQuest.questType == QuestManager.QuestType.Scout)
                {
                    Debug.Log("Initiating random event!");
                    ScoutManager.singleton.SetRandomEvent(currentAI.scoutState.eventPlaces, currentAI.rP);
                }
            }

            Dialogue d = new Dialogue();
            d.displayMode = Dialogue.DisplayMode.none;
            d.textType = Dialogue.TextType.QG_Report;
            d.sentences = new List<string>(currentAI.rP.questReports).ToArray();
            d.raceType = Character.RaceType.undefined;
            d.faction = AIStateManager.Faction.villager;
            d.fromFile = "created through code";
            d.job = TraitManager.JobClass.undefined;
            d.trait = TraitManager.Traits.undefined;

            currentDialogue = d;


        }
        else
        {
            //Get dialogue
            Dialogue dialogue = GetDialogue(
                currentAI.GetCurrentTextType(),
                currentAI.GetCurrentDisplayMode(),
                currentAI.character.traits,
                currentAI.faction,
                currentAI.character.raceType,
                currentAI.character.jobClass);
            currentDialogue = dialogue;
        }
       

        Debug.Log($"Starting dialogue with {currentAI.character.GetCharacterName()}. \n" +
    $"Current Display Mode: {currentAI.currentDisplayMode}.\n" +
    $"Current Text Type: {currentAI.currentTextType}.\n" +
    $"Trait 1: {currentAI.character.traits[0]}.\n" +
    $"Trait 2: {currentAI.character.traits[1]}.\n" +
    $"Trait 3: {currentAI.character.traits[2]}.\n " +
    $"Faction: {currentAI.faction}.\n" +
    $"Race: {currentAI.character.race}.\n" +
    $"Job: {currentAI.character.jobClass}.\n" +
    $"Dialogue ID: {currentDialogue.id}.\n" +
    $"Dialogue Data: {currentDialogue.fromFile}");

        //Set current Character
        this.currentAI = currentAI;

        StartText();
    }

    private void StartText()
    {
        //Enabling Dialogue Panel
        DialoguePanel.SetActive(true);

        //Disabling Interaction Buttons
        InteractionButtons.SetActive(false);

        //nameText.text = dialogue.name;
        sentences = new Queue<string>();
        sentences.Clear();

        foreach (string sentence in currentDialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }


    public void StartTextOnly(Dialogue eventText)
    {
        Dialogue dialogue = eventText;
        DialoguePanel.SetActive(true);
        InteractionButtons.SetActive(false);
        //nameText.text = dialogue.name;
        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }



    public void DisplayNextSentence()
    {
        if (CharacterDisplayManager.singleton.currentDisplayState != CharacterDisplayManager.CharacterDisplayStates.none) return;

        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        nextSentenceSFX.Play();

        string sentence = sentences.Dequeue();
        StopAllCoroutines(); 
        string displayedText = ReplaceFillWords(sentence);
        dialogueText.text = displayedText;

        StartCoroutine(TypeSentence(displayedText));

    }

    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    void EndDialogue()
    {
        Debug.Log("End of conversation");
        if(currentAI != null)
            Debug.Log("Current Display Mode: " + currentAI.currentDisplayMode + " Current Text Type Mode: " + currentDialogue.textType);
        
        switch (currentDialogue.displayMode)
        {
            case Dialogue.DisplayMode.character:
                DisableAllButtons();
                CheckFollow();
                InteractionButtons.SetActive(true);
                break;
            case Dialogue.DisplayMode.merchant:
                DisableAllButtons();
                MerchantButtons.SetActive(true);
                break;
            case Dialogue.DisplayMode.researcher:
                DisableAllButtons();
                ResearchButtons.SetActive(true);
                break;
            case Dialogue.DisplayMode.none:
                DisableAllButtons();
                if (currentAI != null)
                    EndConversation();
                else
                    EndDescriptiveText();

                break;
            case Dialogue.DisplayMode.prisoner:
                DisableAllButtons();
                int m = currentAI.inventory.money;
                int mP = Mathf.RoundToInt((30f * m) / 100);
                freeForMoneyTxt.text = $"Get {mP} gold for freedom.";
                PrisonerButtons.SetActive(true);
                break;
            case Dialogue.DisplayMode.exitPrison:
                DisableAllButtons();
                             
                //Recruit
                if(currentAI.currentTextType == Dialogue.TextType.recruit)
                {
                    StatusManager.singleton.InsertNewStatus($"{currentAI.character.GetCharacterName()} is freed from prison and joins the town!");
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.character;
                    currentAI.currentTextType = Dialogue.TextType.standard;
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.villager);
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.recruitFromPrison, currentAI.character.raceType);
                    currentAI.character.AddSatisfaction(15);
                    
                    if (currentAI.hasQuest) currentAI.rP.AddQuestReport("Fortunately you helped me breaking out of prison! Thank you so much for that!");
                    if(currentAI.faction == AIStateManager.Faction.villager)
                    {
                        currentAI.rP.AddReport("I am free again!");
                    }
                    else
                    {
                        currentAI.rP.AddReport("I am free! And I have a new home!");
                    }

                    currentAI.imprisonedState.prison.ExitPrison();
                    TryChangeDialogueState(DialogueState.none, currentAI, null);

                }
                //recruit failed
                else if(currentAI.currentTextType == Dialogue.TextType.recruitFailed)
                {
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.recruitFromPrison, currentAI.character.raceType);
                    StatusManager.singleton.InsertNewStatus($"{currentAI.character.GetCharacterName()} is freed from prison!");
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.none;
                    currentAI.currentTextType = Dialogue.TextType.standard;

                    currentAI.rP.AddReport("I will never join you.");
                    currentAI.imprisonedState.prison.ExitPrison();
                    TryChangeDialogueState(DialogueState.none, currentAI, null);


                }
                //Recruit but full
                else if(currentAI.currentTextType == Dialogue.TextType.recruitFull)
                {
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.neutral);
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.recruitFromPrison, currentAI.character.raceType);
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.none;
                    currentAI.currentTextType = Dialogue.TextType.standard;
                    currentAI.rP.AddReport("I want to leave this island!");
                    currentAI.imprisonedState.prison.ExitPrison();
                    TryChangeDialogueState(DialogueState.none, currentAI, null);
                }
                //Free for Money success
                else if(currentAI.currentTextType == Dialogue.TextType.free)
                {
                    StatusManager.singleton.InsertNewStatus($"{currentAI.character.GetCharacterName()} is freed from prison!");
                    currentAI.character.AddSatisfaction(15);
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.freeForMoneyFromPrison, currentAI.character.raceType);
                    currentAI.rP.AddReport("I could bought myself out of prison! Finally I am free again!");

                    //Get 30% of money
                    int cM = currentAI.inventory.money;
                    int amount = Mathf.RoundToInt((30f * cM) / 100);
                    currentAI.inventory.AddMoney(-amount);
                    GameManager.singleton.PlayerInventory.AddMoney(amount);
                    

                    currentAI.currentTextType = Dialogue.TextType.standard;
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.none;

                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.neutral);

                    currentAI.imprisonedState.prison.ExitPrison();
                    TryChangeDialogueState(DialogueState.none, currentAI, null);
                }
                //Free failed
                else if(currentAI.currentTextType == Dialogue.TextType.freeFailed)
                {
                    StatusManager.singleton.InsertNewStatus($"{currentAI.character.GetCharacterName()} is freed from prison and escapes!");
                    currentAI.character.AddSatisfaction(15);
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.freeForMoneyFromPrison, currentAI.character.raceType);

                    //Get 30% of money
                    int cM = currentAI.inventory.money;
                    int amount = Mathf.RoundToInt((30f * cM) / 100);
                    currentAI.inventory.AddMoney(-amount);
                    GameManager.singleton.PlayerInventory.AddMoney(amount);
                    currentAI.rP.AddReport("I could bought myself out of prison! Finally I am free again!");

                    currentAI.currentTextType = Dialogue.TextType.standard;
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.none;

                    if (currentAI.character.traits.Contains(TraitManager.Traits.Aggressive))
                        GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.enemy);
                    else GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.neutral);

                    currentAI.imprisonedState.prison.ExitPrison();
                    TryChangeDialogueState(DialogueState.none, currentAI, null);
                }               
                DialoguePanel.SetActive(false);
                GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
                break;
            case Dialogue.DisplayMode.lootable:
                DisableAllButtons();
                LootButtons.SetActive(true);
                break;
            case Dialogue.DisplayMode.ressurect:
                DisableAllButtons();
                RuinButtons.SetActive(true);
                break;
            case Dialogue.DisplayMode.exitRuin:
                DisableAllButtons();
                DialoguePanel.SetActive(false);

                //Recruit
                if (currentAI.currentTextType == Dialogue.TextType.recruit)
                {
                    Debug.Log("Trying to recruit " + currentAI.character.GetCharacterName());
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.recruitFromRuin, currentAI.character.raceType);
                    currentAI.rP.AddReport("I got a second chance! I am a villager now!");

                    Debug.Log($"{currentAI.character.GetCharacterName()} joins town!");
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.villager);
                    StatusManager.singleton.InsertNewStatus($"{currentAI.character.GetCharacterName()} joins town.");
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.goHomeState);

                    
                }
                else if(currentAI.currentTextType == Dialogue.TextType.recruitFailed)
                {
                    currentAI.rP.AddReport("I will never join you!");
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.rescueSuccessful, currentAI.character.raceType);
                    
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.enemy);
                    
                    Debug.Log($"{currentAI.character.GetCharacterName()} refuses to join town!");
                    currentAI.currentTextType = Dialogue.TextType.standard;
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.none;
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.goHomeState);
                }
                else if(currentAI.currentTextType == Dialogue.TextType.free)
                {
                    //Get 30% of money
                    var _m = currentAI.inventory.money;
                    var _mP = Mathf.RoundToInt((30f * _m) / 100);
                    GameManager.singleton.PlayerInventory.AddMoney(_mP);
                    StatusManager.singleton.InsertNewStatus($"Obtained {_mP} gold.");

                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.releaseSoul, currentAI.character.formerRaceType);

                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.neutral);
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.goHomeState);
  
                }
                else if ((currentAI.currentTextType == Dialogue.TextType.freeFailed))
                {
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.releaseSoul, currentAI.character.raceType);
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.enemy);
                    currentAI.currentTextType = Dialogue.TextType.standard;
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.none;
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.goHomeState);
                }


                GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
                break;
            case Dialogue.DisplayMode.hospital:
                DisableAllButtons();
                HospitalButtons.SetActive(true);
                break;
            case Dialogue.DisplayMode.exitHospital:
                DisableAllButtons();
                DialoguePanel.SetActive(false);
                
                //Recruit
                if (currentAI.currentTextType == Dialogue.TextType.recruit)
                {
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.rescueSuccessful, currentAI.character.raceType);
                    currentAI.rP.AddReport("I got a second chance! I am a villager now!");

                    Debug.Log($"{currentAI.character.GetCharacterName()} joins town!");
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.villager);
                    StatusManager.singleton.InsertNewStatus($"{currentAI.character.GetCharacterName()} joins town.");
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.goHomeState);                
                }
                else if(currentAI.currentTextType == Dialogue.TextType.recruitFailed)
                {
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.rescueSuccessful, currentAI.character.raceType);
                    currentAI.rP.AddReport("I am loyal to The Evil Mage, I refuse to join town!");
                    Debug.Log($"{currentAI.character.GetCharacterName()} refuses to join town!");
                    currentAI.currentTextType = Dialogue.TextType.standard;
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.none;
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.enemy);
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.goHomeState);
                }
                else if (currentAI.currentTextType == Dialogue.TextType.free)
                {
                    //Change Displayer
                    currentAI.currentTextType = Dialogue.TextType.standard;
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.none;

                    //NPC becomes neutral
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.neutral);

                    //Get 30% of money
                    var _m = currentAI.inventory.money;
                    var _mP = Mathf.RoundToInt((30f * _m) / 100);
                    GameManager.singleton.PlayerInventory.AddMoney(_mP);
                    StatusManager.singleton.InsertNewStatus($"Obtained {_mP} gold.");

                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.releaseSoul, currentAI.character.raceType);
                                                                
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.goHomeState);

                }
                else if(currentAI.currentTextType == Dialogue.TextType.freeFailed)
                {
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.enemy);
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.releaseSoul, currentAI.character.raceType);
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.goHomeState);
                }
                //Villager comes back to normal
                else if(currentAI.faction == AIStateManager.Faction.villager)
                {
                    currentAI.gameObject.layer = 10;
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.villager);
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.character;
                    currentAI.currentTextType = Dialogue.TextType.standard;
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.idleState);
                }
                else
                {
                    Debug.LogError("Something went wrong!");
                    TryChangeDialogueState(DialogueState.none, currentAI, null);
                }
                break;
            case Dialogue.DisplayMode.adventurer:
                DisableAllButtons();
                AdventurerButtons.SetActive(true);
                break;
            case Dialogue.DisplayMode.exitAdventurer:
                DisableAllButtons();
                DialoguePanel.SetActive(false);
                if (currentAI.currentTextType == Dialogue.TextType.steal)
                {
                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.enemy);
                    currentAI.currentTextType = Dialogue.TextType.standard;
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.none;
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.goHomeState);
                }
                else if(currentAI.currentTextType == Dialogue.TextType.recruit)
                {
                    StatusManager.singleton.InsertNewStatus($"{currentAI.character.GetCharacterName()} joins town.");
                    currentAI.rP.AddReport("Let's go on a new adventure! I joined the town!");
                    PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.recruitFromRuin, currentAI.character.raceType);

                    GameManager.singleton.AddNPCToList(currentAI, AIStateManager.Faction.villager);
                    currentAI.currentTextType = Dialogue.TextType.standard;
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.character;
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.goHomeState);
                }
                else if(currentAI.currentTextType == Dialogue.TextType.recruitFailed)
                {
                    currentAI.currentTextType = Dialogue.TextType.standard;
                    currentAI.currentDisplayMode = Dialogue.DisplayMode.adventurer;
                    TryChangeDialogueState(DialogueState.none, currentAI, currentAI.idleState);
                }
                break;
            default:
                DisableAllButtons();
                CheckFollow();
                InteractionButtons.SetActive(true);
                break;

        }      
    }

    private void CheckFollow()
    {
        followText.text = "Follow me";
        if (currentAI.followState.objToFollow != null)
        {
            if (currentAI.followState.objToFollow == GameManager.singleton.playerObj)
            {
                followText.text = "Stop following";
            }
        }
    }
    public void DisableAllButtons()
    {
        LootButtons.SetActive(false);
        MerchantButtons.SetActive(false);
        InteractionButtons.SetActive(false);
        ResearchButtons.SetActive(false);
        PrisonerButtons.SetActive(false);
        RuinButtons.SetActive(false);
        HospitalButtons.SetActive(false);
        AdventurerButtons.SetActive(false);
    }

    private void Update()
    {
        if (currentDialogueState != DialogueState.inDialogue) return;

        if (Input.GetButtonDown("Fire1"))
        {
            DisplayNextSentence();
        }
    }

    public void ShowCharacterDetails()
    {
        CharacterDisplayManager.singleton.TryChangeCharacterDisplayState(CharacterDisplayManager.CharacterDisplayStates.jobClass, currentAI.character);
    }

    public void EndConversation() => TryChangeDialogueState(DialogueState.none, currentAI, currentAI.previewsState);
    
    private string ReplaceFillWords(string text)
    {
        if (currentAI == null) return text;
        string _sentence = text;
        _sentence = _sentence.
            Replace("{goalAmount}", currentAI.currentQuest.amountToGet.ToString()).
            Replace("{currentAmount}", currentAI.gatherState.gatheredItems.Count.ToString()).
            Replace("{object}", currentAI.currentQuest.GetObjectTag());         
           
        return _sentence;
    }    

    public void CheckQuestBoard()
    {
        TryChangeDialogueState(DialogueState.none, currentAI, currentAI.qBoardState);
    }

    public void FollowMe()
    {

        if (currentAI.followState.objToFollow == GameManager.singleton.playerObj)
        {
            Debug.Log($"{currentAI.character.GetCharacterName()} is unfollowing!");
            currentAI.followState.objToFollow = null;
            TryChangeDialogueState(DialogueState.none, currentAI, currentAI.idleState);
        }
        else if (currentAI.previewsState == currentAI.followState)
        {
            Debug.Log($"{currentAI.character.GetCharacterName()} is unfollowing!");
            currentAI.followState.objToFollow = null;
            TryChangeDialogueState(DialogueState.none, currentAI, currentAI.idleState);
        }
        else
        {
            Debug.Log($"{currentAI.character.GetCharacterName()} is following!");
            currentAI.followState.objToFollow = GameObject.FindGameObjectWithTag("Player");
            TryChangeDialogueState(DialogueState.none, currentAI, currentAI.followState);
        }
    }
    public GameObject UnfollowButton;
    public void UnfollowMe()
    {
        Debug.Log($"{currentAI.character.GetCharacterName()} is unfollowing!");
        currentAI.followState.objToFollow = null;
        TryChangeDialogueState(DialogueState.none, currentAI, currentAI.idleState);
    }



    public void DisplayDescription(Dialogue d)
    {
        if(currentDialogueState == DialogueState.inDialogue)
        {
            Debug.Log("Already in Dialogue state.");
            return;
        }
        else
        {
            Debug.Log("Displaying descriptive text.");
            currentDialogueState = DialogueState.inDialogue;
            GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
            currentDialogue = d;
            StartText();
        }
    }

    private void EndDescriptiveText()
    {
        DisableAllButtons();
        DialoguePanel.SetActive(false);

        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
        currentDialogueState = DialogueState.none;
    }

    public void TryChangeDialogueState(DialogueState requestedState, AIStateManager currentAI, AIState aiState)
    {
        Debug.Log($"Trying to change dialogue state from {currentDialogueState} to {requestedState}.");

        if(currentAI == null)
        {
            this.currentAI = currentAI;
        }

        #region Current State: None
        if (currentDialogueState == DialogueState.none)
        {
            if(requestedState == DialogueState.inDialogue)
            {
                AdventurerRecruitText.text = $"Recruit for {currentAI.inventory.favorAmount} {currentAI.inventory.favoriteItem.objectTag}.";

                Debug.Log($"{this.name} is trying to change to PlayState: Menu.");
                GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
                StartDialogue(currentAI);

                currentAI.currentState = currentAI.TryChangeState(aiState);

                currentDialogueState = requestedState;
            }
        }
        #endregion
        #region Current State: In Dialogue
        else if (currentDialogueState == DialogueState.inDialogue)
        {
            if(requestedState == DialogueState.none)
            {
                if(currentAI != null)
                {
                    if (currentAI.healedState.healed)
                    {
                        currentAI.healedState.healed = false;
                        currentAI.previewsState = currentAI.idleState;
                    }

                    if (currentAI.currentDisplayMode == Dialogue.DisplayMode.merchant || currentAI.currentDisplayMode == Dialogue.DisplayMode.researcher)
                    {
                        if (currentAI.specialBuilding != null)
                        {
                            currentAI.specialBuilding.StopInteractingWithBuilding();
                        }
                        else
                        {
                            Debug.Log($"{currentAI.character.GetCharacterName()} has no shop. Ending conversation.");
                        }
                    }
                    else if (currentAI.questEndState.readyToReport)
                    {
                        //End characters quest
                        aiState = currentAI.idleState;
                        currentAI.EndCurrentQuest();
                    }

                    //Ending Prison Cam
                    if (currentAI.imprisonedState.prison != null)
                    {
                        currentAI.imprisonedState.prison.StopInteractingWithPrison();
                    }

                    if (aiState != null)
                        currentAI.currentState = currentAI.TryChangeState(aiState);
                }



                currentAI = null;
                DisableAllButtons();
                DialoguePanel.SetActive(false);

                GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
                currentDialogueState = requestedState;
            }
            else if(requestedState == DialogueState.inDialogue)
            {
                Debug.Log("Trying to continue text.");
                DisableAllButtons();

                StartDialogue(currentAI);

                currentAI.currentState = currentAI.TryChangeState(aiState);

                currentDialogueState = requestedState;

            }
        }
        #endregion
        else
        {
            Debug.LogError($"Failed to transit from {currentDialogueState} to {requestedState}.");
        }
    }

    public void ShowNPCInventory()
    {
        Inventory npcInv = currentAI.gameObject.GetComponent<Inventory>();
        InventoryManager.singleton.DisplayInventory(npcInv, InventoryManager.DisplayMode.npcInventory);
    }

    public Dialogue GetDialogue(Dialogue.TextType dT, Dialogue.DisplayMode dM, List<TraitManager.Traits> t, AIStateManager.Faction f, Character.RaceType rt, TraitManager.JobClass j)
    {
        List<Dialogue> pD = new List<Dialogue>(); 
        foreach(Dialogue d in LoadDialougeData.singleton.dialogue)
        {
            if (d.textType == dT &&
            d.displayMode == dM &&
            d.faction == f)             
            {
                if ((t.Contains(d.trait) || d.trait == TraitManager.Traits.undefined)                   
                     && (d.job == j || d.job == TraitManager.JobClass.undefined)
                     && (d.raceType == rt || d.raceType == Character.RaceType.undefined))
                {
                    Debug.Log($"ID: {d.id}, from: {d.fromFile}");
                    Debug.Log($"{d.trait}, {d.raceType}, {d.job}");
                    pD.Add(d);
                }
            }

        }

        if (!pD.Any())
        {
            Debug.LogError("No Dialogue found!");
            return defaultDialogue;
        }
        else
        {
            List<Dialogue> _tempList = new List<Dialogue>();

            foreach (Dialogue d in pD)
            {
                if (t.Contains(d.trait) && d.raceType == rt && d.job == j)
                {
                    _tempList.Add(d);
                }
            }

            if (_tempList.Any()) 
                return _tempList[Random.Range(0, _tempList.Count)];

        }

        return pD[Random.Range(0, pD.Count)];
    }


    public void SellToMerchant()
    {
        InventoryManager.singleton.SellToMerchant(currentAI);
    }
    public void BuyFromMerchant()
    {
        InventoryManager.singleton.BuyFromMerchant(currentAI);
    }

    public void OpenResearchPanel()
    {
        ResearchManager.singleton.OpenResearchPanel(currentAI.specialBuilding);
    }

    public void RecruitFromRuin()
    {        
        Debug.Log($"Try recruiting {currentAI.character.GetCharacterName()} from ruin.");

        if (currentAI.character.traits.Contains(TraitManager.Traits.Aggressive))
            currentAI.currentTextType = Dialogue.TextType.recruitFailed;
        else
            currentAI.currentTextType = Dialogue.TextType.recruit;


        currentAI.currentDisplayMode = Dialogue.DisplayMode.exitRuin;

        TryChangeDialogueState(DialogueState.inDialogue, currentAI, currentAI.talkingState);
        
    }
    public void ReleaseSoul()
    {     
        Debug.Log($"Releasing soul {currentAI.character.GetCharacterName()} from ruin.");

        if (currentAI.character.traits.Contains(TraitManager.Traits.Aggressive))
            currentAI.currentTextType = Dialogue.TextType.freeFailed;
        else
            currentAI.currentTextType = Dialogue.TextType.free;

        currentAI.currentDisplayMode = Dialogue.DisplayMode.exitRuin;
        TryChangeDialogueState(DialogueState.inDialogue, currentAI, currentAI.talkingState);
    }

    public void BuyOutPrisoner() 
    {
        Debug.Log($"Freeing {currentAI.character.GetCharacterName()} from prison.");

        if (currentAI.character.traits.Contains(TraitManager.Traits.Aggressive))
            currentAI.currentTextType = Dialogue.TextType.freeFailed;
        else
            currentAI.currentTextType = Dialogue.TextType.free;

        if (currentAI.faction == AIStateManager.Faction.villager)
            currentAI.currentTextType = Dialogue.TextType.freeFailed;

        currentAI.currentDisplayMode = Dialogue.DisplayMode.exitPrison;

        TryChangeDialogueState(DialogueState.inDialogue, currentAI, null);
    }

    public void RecruitPrisoner()
    {
        //Not a villager
        if (!GameManager.singleton.villagers.Contains(currentAI))
        {
            //Full List
            if(GameManager.singleton.villagers.Count >= 15)
            {
                Debug.Log("No more recruit options possible");
                currentAI.currentTextType = Dialogue.TextType.recruitFull;
            }
            else //Place left
            {
                currentAI.currentTextType = Dialogue.TextType.recruit;
            }
            
        }
        //Is already a villager
        else if(GameManager.singleton.villagers.Contains(currentAI)) //is a villager
        {
            currentAI.currentTextType = Dialogue.TextType.recruit;
        }
        else //Is not a villager
        {
            if (currentAI.character.traits.Contains(TraitManager.Traits.Aggressive))
                currentAI.currentTextType = Dialogue.TextType.recruitFailed;
        }




        currentAI.currentDisplayMode = Dialogue.DisplayMode.exitPrison;


        Debug.Log($"Try recruiting {currentAI.character.GetCharacterName()} from prison.");
        TryChangeDialogueState(DialogueState.inDialogue, currentAI, null);
    }

    public void LootForMoney()
    {
        PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.lootForMoney, currentAI.character.raceType);

        currentAI.isDead = true;

        //Get 30% of money
        int m = currentAI.inventory.money;
        int mP = Mathf.RoundToInt((30f * m) / 100);
        currentAI.inventory.AddMoney(-mP);
        GameManager.singleton.PlayerInventory.AddMoney(mP);

        StatusManager.singleton.InsertNewStatus($"Obtained {mP} gold.");

        TryChangeDialogueState(DialogueState.none, currentAI, currentAI.faintState);
        currentAI.faintState.SpawnParticles();
        currentAI.faintState.DestroyThisObject();
    }

    public void LootForItems()
    {
        PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.lootForItems, currentAI.character.raceType);

        List<ObjectHandler> oH = new List<ObjectHandler>(currentAI.inventory.items);

        for(int i = 0; i < oH.Count; i++)
        {
            currentAI.inventory.DropItem(oH[i]);
        }

        currentAI.isDead = true;

        TryChangeDialogueState(DialogueState.none, currentAI, currentAI.faintState);
        currentAI.faintState.SpawnParticles();
        currentAI.faintState.DestroyThisObject();
    }

    public void RecruitFromHospital()
    {
        Debug.Log($"Try recruiting {currentAI.character.GetCharacterName()} from hospital.");

        if(currentAI.character.traits.Contains(TraitManager.Traits.Aggressive))
            currentAI.currentTextType = Dialogue.TextType.recruitFailed;
        else
            currentAI.currentTextType = Dialogue.TextType.recruit;
        

        currentAI.currentDisplayMode = Dialogue.DisplayMode.exitHospital;

        TryChangeDialogueState(DialogueState.inDialogue, currentAI, currentAI.talkingState);
    }
    public void ReleaseFromHospital()
    {
        if (currentAI.character.traits.Contains(TraitManager.Traits.Aggressive))
            currentAI.currentTextType = Dialogue.TextType.freeFailed;
        else
            currentAI.currentTextType = Dialogue.TextType.free;

        Debug.Log($"Releasing {currentAI.character.GetCharacterName()} from hospital.");
        currentAI.currentDisplayMode = Dialogue.DisplayMode.exitHospital;
        TryChangeDialogueState(DialogueState.inDialogue, currentAI, currentAI.talkingState);
    }

    public void StealFromAdventurer()
    {
        currentAI.currentTextType = Dialogue.TextType.steal;
        currentAI.currentDisplayMode = Dialogue.DisplayMode.exitAdventurer;

        var r = Random.Range(0, 2);
        if(r == 0)
        {
            //Steal Item
            if (currentAI.inventory.items.Count <= 0)
            {
                if (currentAI.inventory.items.Count > 3)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        currentAI.inventory.ChangeObjectAmount(currentAI.inventory.items[i].objectID, -1);
                        currentAI.inventory.DropItem(currentAI.inventory.items[i]);                       
                    }
                }
                else
                {
                    currentAI.inventory.ChangeObjectAmount(currentAI.inventory.items[0].objectID, -1);
                    currentAI.inventory.DropItem(currentAI.inventory.items[0]);
                }
            }
            else
            {
                //Get 30% of money
                var _m = currentAI.inventory.money;
                var _mP = Mathf.RoundToInt((30f * _m) / 100);
                GameManager.singleton.PlayerInventory.AddMoney(_mP);
                StatusManager.singleton.InsertNewStatus($"Obtained {_mP} gold.");
            }
        }
       
        if(r == 1)
        {
            //Get 30% of money
            var _m = currentAI.inventory.money;
            var _mP = Mathf.RoundToInt((30f * _m) / 100);
            GameManager.singleton.PlayerInventory.AddMoney(_mP);
            StatusManager.singleton.InsertNewStatus($"Obtained {_mP} gold.");           
        }
        PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.steal, currentAI.character.raceType);
        TryChangeDialogueState(DialogueState.inDialogue, currentAI, currentAI.talkingState);
    }

    public void RecruitAdventurer()
    {
        ObjectHandler oH = currentAI.inventory.favoriteItem;
        int amount = currentAI.inventory.favorAmount;
        int a = 0;

        List<ObjectHandler> items = new List<ObjectHandler>();

        foreach(ObjectHandler item in GameManager.singleton.PlayerInventory.items)
        {
            if (item.objectID == oH.objectID)
            {
                a++;
                items.Add(item);
            }
        }
        //Enough Items 
        if(a >= amount)
        {
            currentAI.currentTextType = Dialogue.TextType.recruit;

            for(int i = 0; i < amount; i++)
            {
                currentAI.inventory.AddItemToInventory(items[i]);
                GameManager.singleton.PlayerInventory.ChangeObjectAmount(oH.objectID, -1);
            }
        }
        else
        {
            currentAI.currentTextType = Dialogue.TextType.recruitFailed;
        }

        currentAI.currentDisplayMode = Dialogue.DisplayMode.exitAdventurer;
        TryChangeDialogueState(DialogueState.inDialogue, currentAI, currentAI.talkingState);
    }

   
}
