using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BuildingInfoManager : MonoBehaviour
{
    public static BuildingInfoManager singleton;

    [SerializeField] private GameObject buildingInfoPanel = null;

    [Header("UI Elements")]
    [SerializeField] TMP_Text owner = null;
    [SerializeField] TMP_Text buildingName = null;
    [SerializeField] TMP_Text buildingDesc = null;
    [SerializeField] Button assignVillagerButton = null;
    [SerializeField] Button sleepButton = null;
    [SerializeField] Button knockButton = null;
    [SerializeField] Button ownerButton = null;

    [Header("Villager List")]
    [SerializeField] private GameObject VillagerListItemPrefab = null;
    [SerializeField] private GameObject content = null;
    [SerializeField] private List<GameObject> villagerItems = new List<GameObject>();

    private Character currentlySelectedChar = null;
    [SerializeField] private TMP_Text assignedVillager = null;
    [SerializeField] private TMP_Text eventDesc = null;
    [SerializeField] private Button assignButton = null;
    [SerializeField] private Button profileButton = null;

    [Header("Pages")]
    [SerializeField] private GameObject StartPage = null;
    [SerializeField] private GameObject VillagerList = null;
    [SerializeField] private GameObject selectedPanel = null;

    public BuildingHandler currentBuilding = null;

    public CurrentBuildingInfoState currentInfoState = CurrentBuildingInfoState.none;

    public enum CurrentBuildingInfoState
    {
        none,
        main,
        assignList,
        assignListProfile,
        owner,
        buildingDetails
    }

    private void Awake()
    {
        singleton = this;
    }
    private void Start()
    {
        CloseAll();
    }

    private void SetMainInfo(BuildingHandler buildHandler)
    {
        if(buildHandler.GetOwner() == null)
        {
            owner.text = "owned by nobody";
            ownerButton.interactable = false;
            sleepButton.interactable = false;
        }
        else
        {
            ownerButton.interactable = true;
            owner.text = "owned by " + buildHandler.GetOwner().GetCharacterName();

            if (buildHandler.GetOwner().gameObject.tag == "Player")
            {
                sleepButton.interactable = true;
            }
            else
            {
                sleepButton.interactable = false;
            }
        }

        buildingName.text = buildHandler.GetConstruction().GetConstructName();
        buildingDesc.text = buildHandler.GetConstruction().GetConstructDesc();
    }

    private void Update()
    {
        if (GameManager.singleton.currentPlaystate == GameManager.PlayState.inOverworld) return;

        if (Input.GetButtonDown("Fire2"))
        {
            if (currentInfoState == CurrentBuildingInfoState.main) TryChangeBuildingInfoState(CurrentBuildingInfoState.none, currentBuilding);
            else if (currentInfoState == CurrentBuildingInfoState.assignList) TryChangeBuildingInfoState(CurrentBuildingInfoState.main, currentBuilding);        
        }
    }

    #region Buttons
    public void ShowVillagers()
    {
        TryChangeBuildingInfoState(CurrentBuildingInfoState.assignList, currentBuilding);
    }
    public void ShowProfileVillagerProfile()
    {
        TryChangeBuildingInfoState(CurrentBuildingInfoState.assignListProfile, currentBuilding);
    }
    public void ShowOwner()
    {
        TryChangeBuildingInfoState(CurrentBuildingInfoState.owner, currentBuilding);
    }
    public void ShowAssignVillagerList()
    {
        StartPage.SetActive(false);
        InstantiateVillagerList();
        VillagerList.SetActive(true);
        selectedPanel.SetActive(false);

        assignButton.interactable = false;
        profileButton.interactable = false;
    }
    #endregion



    /// <summary>
    /// Instantiates all villager items in a scroll view.
    /// </summary>
    private void InstantiateVillagerList()
    {
        foreach(GameObject obj in villagerItems)
        {
            Destroy(obj);
        }
        villagerItems.Clear();

        

        foreach(AIStateManager ai in GameManager.singleton.villagers)
        {
            GameObject villItem = Instantiate(VillagerListItemPrefab);
            villItem.transform.SetParent(content.transform);

            villItem.GetComponent<VillagerListItemHandler>().SetData(ai.character);

            villagerItems.Add(villItem);
        }
    }

    public void DisplayAssignDesc(Character charData)
    {
        currentlySelectedChar = charData;
        selectedPanel.SetActive(true);

        assignButton.interactable = true;
        profileButton.interactable = true;

        assignedVillager.text = charData.GetCharacterName();
        eventDesc.text = GetAssignDesc(charData);
    }

    private string GetAssignDesc(Character charData)
    {
        string eventDesc = "empty description";
        
        if (charData.GetCurrentHome() == null)   //Character has no home
        {
            if (currentBuilding.GetOwner() == null) //Building has no inhabitant
            {
                eventDesc = $"Shall {charData.GetCharacterName()} move to " +
                    $"{currentBuilding.GetConstruction().GetConstructName()}?";

            }
            else //Building has an inhabitant
            {
                eventDesc = $"Shall {charData.GetCharacterName()} move to " +
                        $"{currentBuilding.GetConstruction().GetConstructName()}? " +
                        $"{currentBuilding.GetOwner().GetCharacterName()} will lose this home!";
            }
        }
        else //Character has a home
        {
            if (currentBuilding.GetOwner() == null) //Building has no inhabitant
            {
                eventDesc = $"Shall {charData.GetCharacterName()} move from " +
                    $"{charData.GetCurrentHome().GetConstruction().GetConstructName()} to " +
                    $"{currentBuilding.GetConstruction().GetConstructName()}?";
            }
            else if (charData == currentBuilding.GetOwner())       //This is characters home
            {
                eventDesc = $"{charData.GetCharacterName()} is already living here.";
                assignButton.interactable = false;              
            }
            else //Building has an inhabitant
            {
                eventDesc = $"Shall {charData.GetCharacterName()} move from " +
                                    $"{charData.GetCurrentHome().GetConstruction().GetConstructName()} to " +
                                    $"{currentBuilding.GetConstruction().GetConstructName()}? " +
                                    $"{currentBuilding.GetOwner().GetCharacterName()} will lose this home!";
            }
        }
        Debug.Log(eventDesc);
        return eventDesc;
    }

    public void AssignVillager()
    {
        Debug.Log($"{currentlySelectedChar} is moving to {currentBuilding.GetConstruction().GetConstructName()}.");

        if(currentlySelectedChar.GetCurrentHome() != null)
        {
            Debug.Log($"{currentlySelectedChar} is moving out from {currentlySelectedChar.GetCurrentHome().GetConstruction().GetConstructName()}.");
            currentlySelectedChar.GetCurrentHome().MoveOut();
        }

        currentBuilding.SetOwner(currentlySelectedChar);

        SetMainInfo(currentBuilding);

        TryChangeBuildingInfoState(CurrentBuildingInfoState.main, currentBuilding);       
    }

    #region UI Navigation
    private void ShowMainPage()
    {
        AudioManager.singleton.PlayBookPageSFX();

        buildingInfoPanel.SetActive(true);

        StartPage.SetActive(true);
        VillagerList.SetActive(false);
        selectedPanel.SetActive(false);
    }
    private void ShowAssignList()
    {
        InstantiateVillagerList();

        buildingInfoPanel.SetActive(true);
        StartPage.SetActive(false);
        VillagerList.SetActive(true);
    }
    public void CloseAll()
    {
        buildingInfoPanel.SetActive(false);
        StartPage.SetActive(false);
        VillagerList.SetActive(false);
    }
    #endregion

    public void TryChangeBuildingInfoState(CurrentBuildingInfoState requestedState, BuildingHandler bH)
    {
        Debug.Log($"Trying to change from {currentInfoState} to {requestedState}.");
        if (currentInfoState == requestedState)
        {
            Debug.Log($"{gameObject.name} is already in requested State! Return");
            return;
        }


        currentBuilding = bH;

        #region Current State: None
        if (currentInfoState == CurrentBuildingInfoState.none)
        {
            if(requestedState == CurrentBuildingInfoState.main)
            {
                GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);

                ShowMainPage();
                SetMainInfo(bH);
                InstantiateVillagerList();

                currentInfoState = requestedState;
            }
        }
        #endregion
        #region Current State: Main
        else if (currentInfoState == CurrentBuildingInfoState.main)
        {
            if(requestedState == CurrentBuildingInfoState.none)
            {
                CloseAll();
                currentBuilding = null;
                GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
                currentInfoState = requestedState;
            }
            else if(requestedState == CurrentBuildingInfoState.assignList)
            {
                ShowAssignList();               
                currentInfoState = requestedState;
            }
            else if (requestedState == CurrentBuildingInfoState.owner)
            {
                CloseAll();
                CharacterDisplayManager.singleton.TryChangeCharacterDisplayState(CharacterDisplayManager.CharacterDisplayStates.jobClass, bH.GetOwner());
                currentInfoState = requestedState;
            }
        }
        #endregion
        #region Current State: AssignList
        else if (currentInfoState == CurrentBuildingInfoState.assignList)
        {
            if (requestedState == CurrentBuildingInfoState.main)
            {
                ShowMainPage();
                currentInfoState = requestedState;
            }
            else if (requestedState == CurrentBuildingInfoState.assignListProfile)
            {
                CloseAll();
                CharacterDisplayManager.singleton.TryChangeCharacterDisplayState(CharacterDisplayManager.CharacterDisplayStates.jobClass, currentlySelectedChar);
                currentInfoState = requestedState;
            }
        }
        #endregion
        #region Current State: AssignListProfile
        else if (currentInfoState == CurrentBuildingInfoState.assignListProfile)
        {
            if(requestedState == CurrentBuildingInfoState.assignList)
            {
                ShowAssignList();
                currentInfoState = requestedState;
            }
        }
        #endregion
        #region Current State: Owner 
        else if (currentInfoState == CurrentBuildingInfoState.owner)
        {
            if(requestedState == CurrentBuildingInfoState.main)
            {
                ShowMainPage();
                currentInfoState = requestedState;
            }
        }
        #endregion
    }
}

