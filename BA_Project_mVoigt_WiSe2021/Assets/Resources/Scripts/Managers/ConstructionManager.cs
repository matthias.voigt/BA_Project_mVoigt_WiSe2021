using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ConstructionManager : MonoBehaviour
{
    [Header("Ingame Menu Panel")]
    [SerializeField] private GameObject IngameMenuPanel = null;

    [Header("Construction Canvas")]
    [SerializeField] private GameObject constructionPanel = null;
    [SerializeField] private GameObject overviewPanel = null;
    [SerializeField] private Button buildButton = null;
    [SerializeField] private RawImage previewImage = null;
    [SerializeField] private TMP_Text constructionCostsText = null;
    [SerializeField] private GameObject currentMoneyInConstructMode = null;
    [SerializeField] private TMP_Text currentMoneyInConstructModeText = null;
    [SerializeField] public Button constructButton = null;

    [Header("Preview Scene")]
    [SerializeField] private GameObject previewScene = null;    
    [SerializeField] private Transform previewTransform = null;

    [Header("Main Description Scene")]
    [SerializeField] private GameObject descriptionPanel = null;
    [SerializeField] private TMP_Text descriptionTitle = null;
    [SerializeField] private Image descriptionImage = null;
    [SerializeField] private TMP_Text descriptionText = null;
    [SerializeField] private Sprite[] descImgSprites = null;
    public int descPageID = 0;

    #region Item Grids
    [Header("Item Grid")]
    [SerializeField] private GameObject homesGrid = null;
    public List<Construction> constructsHomes = new List<Construction>();
    [SerializeField] private GameObject specialGrid = null;
    public List<Construction> constructsSpecial = new List<Construction>();

    [Header("Base")]
    [SerializeField] private GameObject wallsGrid = null;
    public List<Construction> constructsWalls = new List<Construction>();
    [SerializeField] private GameObject fencesGrid = null;
    public List<Construction> constructsFences = new List<Construction>();
    [SerializeField] private GameObject pathsGrid = null;
    public List<Construction> constructsPaths = new List<Construction>();

    [Header("Nature")]
    [SerializeField] private GameObject flowersGrid = null;
    public List<Construction> constructsFlowers = new List<Construction>();
    [SerializeField] private GameObject rocksGrid = null;
    public List<Construction> constructsRocks = new List<Construction>();
    [SerializeField] private GameObject stonesGrid = null;
    public List<Construction> constructsStones = new List<Construction>();
    [SerializeField] private GameObject treesGrid = null;
    public List<Construction> constructsTrees = new List<Construction>();

    [Header("Miscellaneous")]
    [SerializeField] private GameObject benchesGrid = null;
    public List<Construction> constructsBenches = new List<Construction>();
    [SerializeField] private GameObject lanternsGrid = null;
    public List<Construction> constructsLanterns = new List<Construction>();
    [SerializeField] private GameObject diverseGrid = null;
    public List<Construction> constructsDiverse = new List<Construction>();
    #endregion

    [Header("ItemPrefab")]
    [SerializeField] private GameObject constructionItemPrefab = null;


    [Header("Cameras")]
    [SerializeField] private GameObject constructCamera = null;
    [SerializeField] private Camera thirdPersonCam = null;

    [Header("Player references")]
    [SerializeField] private Transform playerPos = null;

    [Header("Blueprint prefab")]
    [SerializeField] private GameObject blueprint = null;

    [Header("Construction State")]
    public ConstructionState currentConstructState = ConstructionState.none;


    //References
    private Construction constructionData = null;
    private GameObject currentConstructPreview = null;
    public GameObject currentBluePrintInScene = null;

    public static ConstructionManager singleton;

    public enum ConstructionState
    {
        none,
        inOverview,
        inBuildMenu,
        inPreview,
        inBlueprinting
    }

    private void Awake() => singleton = this;

    private void Start()
    {
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        InstantiateAllConstructionItems();
        currentMoneyInConstructMode.SetActive(false);
        DisableAllElements();

        int l = PlayerPrefs.GetInt("load", 0);
        if (l == 0)
        {
            Debug.Log("Setting construct Unlocks to default.");
            SetConstructionsToDefault();
        }
    }

    private void SetConstructionsToDefault()
    {
        foreach(Construction c in constructsHomes)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
        foreach (Construction c in constructsSpecial)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
        foreach (Construction c in constructsWalls)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
        foreach (Construction c in constructsFences)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false); ;
        }
        foreach (Construction c in constructsPaths)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
        foreach (Construction c in constructsFlowers)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
        foreach (Construction c in constructsRocks)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
        foreach (Construction c in constructsStones)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
        foreach (Construction c in constructsTrees)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
        foreach (Construction c in constructsBenches)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
        foreach (Construction c in constructsLanterns)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
        foreach (Construction c in constructsDiverse)
        {
            if (c.unlockedFromStart) c.UnlockBuilding(true);
            else c.UnlockBuilding(false);
        }
    }

    private void DisableAllElements()
    {
        homesGrid.SetActive(false);
        specialGrid.SetActive(false);
        wallsGrid.SetActive(false);
        fencesGrid.SetActive(false);
        pathsGrid.SetActive(false);
        flowersGrid.SetActive(false);
        rocksGrid.SetActive(false);
        stonesGrid.SetActive(false);
        treesGrid.SetActive(false);
        benchesGrid.SetActive(false);
        lanternsGrid.SetActive(false);
        diverseGrid.SetActive(false);

    }

    private void Update() 
    {
        GetInput();

        if(currentConstructState == ConstructionState.inBlueprinting)
            currentMoneyInConstructModeText.text = InventoryManager.singleton.playerInventory.money.ToString();
    }


    /// <summary>
    /// Handles Input.
    /// </summary>
    private void GetInput()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            switch (currentConstructState)
            {
                case ConstructionState.inOverview:
                    TryChangeConstructState(ConstructionState.none);
                    break;
                case ConstructionState.inBuildMenu:
                    TryChangeConstructState(ConstructionState.inOverview);
                    break;
                case ConstructionState.inPreview:
                    TryChangeConstructState(ConstructionState.inBuildMenu);
                    break;
                case ConstructionState.inBlueprinting:
                    TryChangeConstructState(ConstructionState.inPreview);
                    break;
            }
        }
    }

    public void OpenConstructionPanel()
    {
        TryChangeConstructState(ConstructionState.inOverview);
    }

    /// <summary>
    /// Sets preview scene of selected Construct.
    /// </summary>
    private void InstantiatePreview(Construction construct)
    {
        if (currentConstructPreview != null)
        {
            Debug.Log("Deleting preview cache.");
            DestroyImmediate(currentConstructPreview, true);
        }

        CheckIfConstructCanBeBuild(construct);

        EnablePreviewScene(true);
        Debug.Log($"Instantiating preview gameObject: {construct.GetConstructName()}");
        currentConstructPreview = Instantiate(construct.GetConstruct(), previewTransform);

        currentConstructPreview.layer = 9;
        foreach (Transform child in currentConstructPreview.transform)
        {
            child.gameObject.layer = 9;

            if(child.transform.childCount > 0)
            {
                foreach(Transform _child in child.transform)
                {
                    _child.gameObject.layer = 9;
                }
            }
        }
    }

    private void CheckIfConstructCanBeBuild(Construction construction)
    {
        constructionCostsText.text = construction.GetConstructCost().ToString();

        int cost = construction.GetConstructCost();
        int currentMoney = InventoryManager.singleton.playerInventory.money;

        if (construction.GetUnlockBool())
        {
            previewImage.color = Color.white;

            if (currentMoney > cost)
            {
                constructionCostsText.color = Color.green;
                buildButton.interactable = true;
            }
            else
            {
                constructionCostsText.color = Color.red;
                buildButton.interactable = false;
            }
        }
        else
        {
            previewImage.color = Color.black;
            buildButton.interactable = false;
            constructionCostsText.text = "???";
            constructionCostsText.color = Color.black;
        }
    }

    /// <summary>
    /// Instantiates all preassigned construction items for building menu.
    /// </summary>
    private void InstantiateAllConstructionItems()
    {
        Debug.Log("Instantiating all preassigned construction items.");
        for(int i = 0; i < constructsHomes.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(homesGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsHomes[i]);
        }
        for (int i = 0; i < constructsSpecial.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(specialGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsSpecial[i]);
        }
        for (int i = 0; i < constructsWalls.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(wallsGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsWalls[i]);
        }
        for (int i = 0; i < constructsFences.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(fencesGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsFences[i]);
        }
        for (int i = 0; i < constructsPaths.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(pathsGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsPaths[i]);
        }
        for (int i = 0; i < constructsTrees.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(treesGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsTrees[i]);
        }
        for (int i = 0; i < constructsFlowers.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(flowersGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsFlowers[i]);
        }
        for (int i = 0; i < constructsStones.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(stonesGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsStones[i]);
        }
        for (int i = 0; i < constructsRocks.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(rocksGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsRocks[i]);
        }
        for (int i = 0; i < constructsBenches.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(benchesGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsBenches[i]);
        }
        for (int i = 0; i < constructsLanterns.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(lanternsGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsLanterns[i]);
        }
        for (int i = 0; i < constructsDiverse.Count; i++)
        {
            GameObject newConstructItem = Instantiate(constructionItemPrefab, transform.position, Quaternion.identity);
            newConstructItem.transform.SetParent(diverseGrid.transform);
            newConstructItem.GetComponent<ConstructionItemHandler>().SetConstruct(constructsDiverse[i]);
        }


    }

    public void ShowBuildSectionHome()
    {
        TryChangeConstructState(ConstructionState.inBuildMenu);

        descriptionImage.sprite = descImgSprites[0];
        descriptionText.text = "A home is needed to recover a villagers satisfaction. The bigger the house, the better! You can assign villagers to a house by interacting with a house.";
        descriptionTitle.text = "Homes";

        homesGrid.SetActive(true);

        AudioManager.singleton.PlayBookPageSFX();
    }
    public void ShowBuildSectionSpecial()
    {
        TryChangeConstructState(ConstructionState.inBuildMenu);

        descriptionImage.sprite = descImgSprites[0];
        descriptionText.text = "These buildings are important for everyone in the village. Try interacting with those buildings!";
        descriptionTitle.text = "Special";

        specialGrid.SetActive(true);

        AudioManager.singleton.PlayBookPageSFX();
    }
    public void ShowBuildSectionFences()
    {
        {
            TryChangeConstructState(ConstructionState.inBuildMenu);

            descriptionImage.sprite = descImgSprites[3];
            descriptionText.text = "Base Components!";
            descriptionTitle.text = "Base components";

            fencesGrid.SetActive(true);

            AudioManager.singleton.PlayBookPageSFX();
        }
    }
    public void ShowBuildSectionWalls()
    {
        {
            TryChangeConstructState(ConstructionState.inBuildMenu);

            descriptionImage.sprite = descImgSprites[3];
            descriptionText.text = "Base Components!";
            descriptionTitle.text = "Base";
            wallsGrid.SetActive(true);

            AudioManager.singleton.PlayBookPageSFX();
        }
    }
    public void ShowBuildSectionPaths()
    {
        {
            TryChangeConstructState(ConstructionState.inBuildMenu);

            descriptionImage.sprite = descImgSprites[3];
            descriptionText.text = "Build up a base for your villagers!";
            descriptionTitle.text = "Base";

            pathsGrid.SetActive(true);

            AudioManager.singleton.PlayBookPageSFX();
        }
    }
    public void ShowBuildSectionTrees()
    {
        {
            TryChangeConstructState(ConstructionState.inBuildMenu);

            descriptionImage.sprite = descImgSprites[1];
            descriptionText.text = "Decorate your town with beautiful flowers and trees.";
            descriptionTitle.text = "Nature";

            treesGrid.SetActive(true);

            AudioManager.singleton.PlayBookPageSFX();
        }
    }
    public void ShowBuildSectionFlowers()
    {
        {
            TryChangeConstructState(ConstructionState.inBuildMenu);

            descriptionImage.sprite = descImgSprites[1];
            descriptionText.text = "Decorate your town with beautiful flowers and trees.";
            descriptionTitle.text = "Nature";

            flowersGrid.SetActive(true);

            AudioManager.singleton.PlayBookPageSFX();
        }
    }
    public void ShowBuildSectionStones()
    {
        {
            TryChangeConstructState(ConstructionState.inBuildMenu);

            descriptionImage.sprite = descImgSprites[1];
            descriptionText.text = "Decorate your town with beautiful flowers and trees.";
            descriptionTitle.text = "Nature";

            stonesGrid.SetActive(true);

            AudioManager.singleton.PlayBookPageSFX();
        }
    }
    public void ShowBuildSectionRocks()
    {
        {
            TryChangeConstructState(ConstructionState.inBuildMenu);

            descriptionImage.sprite = descImgSprites[1];
            descriptionText.text = "Decorate your town with beautiful flowers and trees.";
            descriptionTitle.text = "Nature";

            rocksGrid.SetActive(true);

            AudioManager.singleton.PlayBookPageSFX();
        }
    }
    public void ShowBuildSectionBenches()
    {
        {
            TryChangeConstructState(ConstructionState.inBuildMenu);

            descriptionImage.sprite = descImgSprites[2];
            descriptionText.text = "Miscellaneous Components!";
            descriptionTitle.text = "Miscellaneous";

            benchesGrid.SetActive(true);

            AudioManager.singleton.PlayBookPageSFX();
        }
    }
    public void ShowBuildSectionLanterns()
    {
        {
            TryChangeConstructState(ConstructionState.inBuildMenu);

            descriptionImage.sprite = descImgSprites[2];
            descriptionText.text = "Miscellaneous Components!";
            descriptionTitle.text = "Miscellaneous";

            lanternsGrid.SetActive(true);

            AudioManager.singleton.PlayBookPageSFX();
        }
    }
    public void ShowBuildSectionDiverse()
    {
        {
            TryChangeConstructState(ConstructionState.inBuildMenu);

            descriptionImage.sprite = descImgSprites[2];
            descriptionText.text = "Miscellaneous Components!";
            descriptionTitle.text = "Miscellaneous";

            diverseGrid.SetActive(true);

            AudioManager.singleton.PlayBookPageSFX();
        }
    }


    /// <summary>
    /// Handles construction menu navigation.
    /// </summary>
    public void TryChangeConstructState(ConstructionState requestedState)
    {
        Debug.Log($"Trying to change from {currentConstructState} to {requestedState}.");

        if(currentConstructState == ConstructionState.none)
        {
            if(requestedState == ConstructionState.inOverview)
            {
                AudioManager.singleton.PlayBookPageSFX();
                Debug.Log("Enabling construction menu. Disabling third person camera control.");
                EnablePreviewScene(false);
                constructionPanel.SetActive(true);
                IngameMenuPanel.SetActive(false);
                descriptionPanel.SetActive(false);
                

                currentConstructState = requestedState;
                GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
            }
        }
        else if(currentConstructState == ConstructionState.inOverview)
        {
            if(requestedState == ConstructionState.inBuildMenu)
            {
                DisableAllElements();
                overviewPanel.SetActive(false);
                descriptionPanel.SetActive(true);
                currentConstructState = requestedState;
            }
            else if(requestedState == ConstructionState.none)
            {
                Debug.Log("Closing construction menu. Enabling third person camera control.");
                AudioManager.singleton.PlayBookPageSFX();
                constructionPanel.SetActive(false);
                EnablePreviewScene(false);
                currentConstructState = requestedState;

                GameManager.singleton.TryChangePlayState(GameManager.PlayState.inOverworld);
            }
        }
        else if(currentConstructState == ConstructionState.inBuildMenu)
        {
            if(requestedState == ConstructionState.inPreview)
            {
                Debug.Log("Enabling preview scene with given construction data.");

                EnablePreviewScene(true);
                descriptionPanel.SetActive(false);
                InstantiatePreview(constructionData);

                currentConstructState = requestedState;
            }
            else if(requestedState == ConstructionState.inOverview)
            {
                overviewPanel.SetActive(true);
                DisableAllElements();
                EnablePreviewScene(false);
                descriptionPanel.SetActive(false);
                currentConstructState = requestedState;
            }
        }
        else if(currentConstructState == ConstructionState.inPreview)
        {
            if(requestedState == ConstructionState.inBuildMenu)
            {
                Debug.Log("Returning from preview to menu.");
                EnablePreviewScene(false);
                descriptionPanel.SetActive(true);
                currentConstructState = requestedState;
            }
            else if(requestedState == ConstructionState.inPreview)
            {
                Debug.Log("Changing from preview to preview. Updating construction data.");
                EnablePreviewScene(true);
                InstantiatePreview(constructionData);

                currentConstructState = requestedState;
            }
            else if(requestedState == ConstructionState.inBlueprinting)
            {
                Debug.Log("Changing to blueprinting. Deparenting construction camera.");
                constructCamera.transform.parent = null;
                constructCamera.transform.position = new Vector3(playerPos.position.x, constructCamera.transform.position.y, playerPos.position.z);
                ChangeToConstructionScene();
                currentConstructState = requestedState;
            }
        }
        else if(currentConstructState == ConstructionState.inBlueprinting)
        {
            if(requestedState == ConstructionState.inPreview)
            {
                currentMoneyInConstructMode.SetActive(false);             
                Debug.Log("Returning to preview scene. Destroying current blueprint gameObject in scene");
                Destroy(currentBluePrintInScene);                
                EnablePreviewScene(true);
                constructionPanel.SetActive(true);
                thirdPersonCam.gameObject.SetActive(true);
                constructCamera.gameObject.SetActive(false);
                NavigationBaker.singleton.UpdateNavMesh();
                currentConstructState = requestedState;
            }
        }
    }

    /// <summary>
    /// Changes to construction Camera and instantiates selected construction as blueprint.
    /// </summary>
    public void ChangeToConstructionScene()
    {
        EnablePreviewScene(false);
        constructionPanel.SetActive(false);
        thirdPersonCam.gameObject.SetActive(false);
        constructCamera.gameObject.SetActive(true);
        currentMoneyInConstructMode.SetActive(true);
        currentMoneyInConstructModeText.text = InventoryManager.singleton.playerInventory.money.ToString();

        currentBluePrintInScene = Instantiate(blueprint);
        Blueprint _blueprint = currentBluePrintInScene.GetComponent<Blueprint>();

        _blueprint.SetConstruct(constructionData);       
    }

    private void EnablePreviewScene(bool enable)
    {
        Debug.Log($"Turning preview scene to: {enable}");
        previewScene.SetActive(enable);        
    }

    public void InstantiateBlueprint()
    {
        TryChangeConstructState(ConstructionState.inBlueprinting);
    }

    public Camera GetConstructionCamera()
    {
        return constructCamera.GetComponentInChildren<Camera>();
    }

    public void SetConstruct(Construction construct)
    {
        constructionData = construct;
    }
}

