using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GameStates : MonoBehaviour
{
    /*
    public Gamestate currentGameState = Gamestate.inOverworld;
    [SerializeField] private Camera thirdPersonCam;

    public static GameStates singleton;

    private void Awake()
    {
        singleton = this;
    }
    
    public enum Gamestate
    {
        inOverworld,
        inDialogue,
        inMenu
    }

    public Gamestate GetCurrentGameState()
    {
        return currentGameState;
    }

    public void ChangeGameState(Gamestate requestedState)
    {
        Debug.Log($"Try changing game state from {currentGameState} to {requestedState}.");

        if (currentGameState == Gamestate.inOverworld)
        {
            if (requestedState == Gamestate.inMenu)
            {
                thirdPersonCam.GetComponent<CinemachineBrain>().enabled = false;
                currentGameState = requestedState;
            }
            else if(requestedState == Gamestate.inDialogue)
            {
                thirdPersonCam.GetComponent<CinemachineBrain>().enabled = false;
                currentGameState = requestedState;
            }
        }
        else if (currentGameState == Gamestate.inMenu)
        {
            if (requestedState == Gamestate.inOverworld)
            {
                thirdPersonCam.GetComponent<CinemachineBrain>().enabled = true;
                currentGameState = requestedState;
            }
        }
        else if(currentGameState == Gamestate.inDialogue)
        {
            if(requestedState == Gamestate.inOverworld)
            {
                thirdPersonCam.GetComponent<CinemachineBrain>().enabled = true;
                currentGameState = requestedState;
            }
        }
        else
            Debug.Log("Cannot change gamestate!");
    }
    */
}
