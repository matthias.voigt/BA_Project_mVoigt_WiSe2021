using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityManager : MonoBehaviour
{
    
    public static PersonalityManager singleton;
    public GameObject characterPrefab = null;

    string[] randomForenames = new string[] { "Charlie", "Justice", "Jackie", "Marison", "Frankie", "Jamie", "Sammie", "Milan", "Steve", "Leo", "Louis", "Jeffrey", "Moe", "Florian", "Marc", "Alex", "Matthew", "Sheena", "Peter", "Bob", "Leela", "Jean","Diluc","Link","Mario","Luigi","Bowser","Alan","Elon","Jeff", "Lee","Scott","Harry","Miles","Anduin","Sherlock","Little","Michael"};
    string[] randomLastnames = new string[] { "Smith", "Johnson", "Williams", "Brown", "Jones", "Garcia", "Miller", "Davis", "Jackson", "Wilson", "Dickson", "Berger","Skywalker","Parker","Jobs","Musk","Rodriguez","Lopez","Lee","Young","Allen","Potter","Evans","Collins","Murphy","Morales","Cooper","Kim","Holmes","Sanders","Myer","Ross","Wrynn"};

    private void Awake()
    {
        singleton = this;
    }

    public string GetRandomName()
    {
        string foreName = randomForenames[Random.Range(0, randomForenames.Length)];
        string backName = randomLastnames[Random.Range(0, randomLastnames.Length)];
        string randomName = foreName + " " + backName;

        return randomName;
    }

    public void CreateRandomCharacter(Vector3 position, GameObject prefab)
    { 
        GameObject newCharacter = Instantiate(prefab, position, transform.rotation * Quaternion.Euler(0f, 180f, 0f));          
    }  
}
