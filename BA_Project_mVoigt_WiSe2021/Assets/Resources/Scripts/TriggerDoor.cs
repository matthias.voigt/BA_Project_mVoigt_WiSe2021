using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDoor : MonoBehaviour
{
    Animator anim;
    private int colCount = 0;
    private bool isOpen;
    private void Start()
    {
        anim = GetComponentInParent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag =="NPC" || other.gameObject.tag == "Player")
        {
            if(!isOpen)
            {
                anim.Play("GateDoor");
                isOpen = true;
            }
            colCount++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "NPC" || other.gameObject.tag == "Player")
        {
            colCount--;
        }
        
        if(colCount <= 0)
        {
            colCount = 0;

            if (isOpen)
            {
                anim.Play("GateDoorClose");
                isOpen = false;
            }
        }
    }
}
