using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaintState : AIState
{
    public AIStateManager aiState;
    public GameObject skullPrefab;
    public GameObject deathEffect;
    public float timeUntilDie = 86400f; //24h
    public bool skullSpawned = false;

    public AIStateManager rescuer;
    public override AIState RunCurrentState()
    {
        aiState.character.satisfaction = 10;
        aiState.NPCUntargetable(true);
        aiState.currentDisplayMode = Dialogue.DisplayMode.lootable;
        aiState.currentTextType = Dialogue.TextType.fainted;
        aiState.previewsState = aiState.faintState;

        if (!QuestManager.singleton.faintedNPCs.Contains(aiState))
        {
            QuestManager.singleton.AddFaintedNPC(aiState);
        }

        if (!aiState.anim.GetCurrentAnimatorStateInfo(0).IsName("Die"))
        {
            aiState.anim.Play("Die");
            Debug.Log($"{aiState.character.GetCharacterName()} fainted.");
            return this;
        }
        if(!aiState.untargetable)
        {
            aiState.NPCUntargetable(true);
            Debug.Log($"Changing boolean of {aiState.character.GetCharacterName()}  to prevent targeting.");
            aiState.attackState.objToAttack = null;
        }
        

        if (timeUntilDie <= 0 && !skullSpawned)
        {
            Debug.Log($"{aiState.character.GetCharacterName()} died.");

            SpawnParticles();
            InstantiateSkull();

        }
        else if(skullSpawned)
        {
            DestroyThisObject();
        }
        else
        {
            timeUntilDie -= DayManager.singleton.speed * Time.deltaTime;
        }

        return this;
    }

    public void DestroyThisObject()
    {
        Debug.Log($"Disabling {aiState.character.GetCharacterName()} as it died.");
        aiState.isDead = true;

        if (aiState.hasQuest)
        {
            aiState.currentQuest.questFailed = true;
            aiState.EndCurrentQuest();
        }


        if (aiState.character.GetCurrentHome() != null)
        {
            aiState.character.currentHome.owner = null;
        }

        GameManager.singleton.RemoveNPCFromList(aiState);
        QuestManager.singleton.RemoveFaintedNPC(aiState);
        GameObject deadNPCList = GameObject.FindGameObjectWithTag("DeadNPCs");       
        aiState.gameObject.transform.SetParent(deadNPCList.transform);
        aiState.gameObject.SetActive(false);
    }

    public void SpawnParticles()
    {
        GameObject particle = Instantiate(deathEffect, aiState.gameObject.transform);
        particle.transform.SetParent(null);
    }

    private void InstantiateSkull()
    {
        //Instantiate Skull
        GameObject obj = Instantiate(skullPrefab, aiState.gameObject.transform);
        obj.transform.SetParent(null);

        //Get SkullHandler Component and set Character Data
        SkullHandler sk = obj.GetComponent<SkullHandler>();
        sk.SetCharacterData(aiState);

        //Was currently rescued
        if (rescuer != null)
        {
            Debug.Log($"{rescuer.character.GetCharacterName()} could not save {aiState.character.GetCharacterName()}. Quest failed.");

            rescuer.rescueState.skullObject = sk;
            rescuer.rescueState.npcDied = true;
        }

        skullSpawned = true;
    }

    public void SetRescuer(AIStateManager ai)
    {
        rescuer = ai;
    }
}
