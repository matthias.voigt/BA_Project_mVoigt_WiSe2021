using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchTowerState : AIState
{
    public AIStateManager aiStateManager;
    public WatchTower currentWatchTower;
    public bool isInTower = false;
    public override AIState RunCurrentState()
    {
        SetAgentSpeed();
        SetDisplayMode();

        //Is there a tower to inhabit?
        if (DoTowersExist())
        {
            //Is not in tower yet
            if (!isInTower)
            {
                //Setting tower Ref
                if (currentWatchTower == null)
                {
                    if (GetNeareastObject() == null) // All Towers already assigned
                    {
                        //REPORT
                        aiStateManager.rP.AddReport($"I wanted to guard at a watchtower, but they are all full.");
                        return aiStateManager.TryChangeState(aiStateManager.idleState);
                    }
                    else // Get empty tower
                    {
                        currentWatchTower = GetNeareastObject();
                        return this;
                    }
                }
                //Character is near tower
                else if (aiStateManager.IsCharacterNearbyObject(aiStateManager.gameObject, currentWatchTower.gameObject, 2f))
                {
                    //Tower is empty
                    if (currentWatchTower.currentObj == null)
                    {
                        currentWatchTower.EnterTower(aiStateManager.gameObject);             
                        isInTower = true;
                        return this;
                    }
                    else //Try search another
                    {
                        currentWatchTower = null;
                        return this;
                    }
                }
                else //Walking to tower
                {

                    //REPORT
                    aiStateManager.rP.AddReport($"Let's go to the watchtower.");

                    aiStateManager.agent.SetDestination(currentWatchTower.gameObject.transform.position);
                    if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                        aiStateManager.anim.Play("Walk");
                    return this;
                }
            }
            else //Character is in tower
            {
                if (DayManager.singleton.time > GameManager.singleton.guardWorkEndTime)
                {
                    Debug.Log($"{aiStateManager.character.GetCharacterName()} is done working.");
                    //REPORT
                    aiStateManager.rP.AddReport($"Another working day at the watchtower ends!");
                    currentWatchTower.ExitTower();
                    return aiStateManager.TryChangeState(aiStateManager.idleState);
                }
                else
                {
                    if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                        aiStateManager.anim.Play("Idle");

                    return this;
                }
                    
            }


        }
        else
        {
            Debug.Log($"{aiStateManager.character.GetCharacterName()} did not find any towers.");

            //REPORT
            aiStateManager.rP.AddReport($"I wanted to guard at a watchtower, but there is none...");

        }

        
        return aiStateManager.TryChangeState(aiStateManager.idleState);
    }

   
    private WatchTower GetNeareastObject()
    {
        WatchTower[] allTowers = FindObjectsOfType<WatchTower>();
        WatchTower closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = aiStateManager.gameObject.transform.position;

        foreach (WatchTower go in allTowers)
        {
            if(go.currentObj == null && go.faction == aiStateManager.faction)
            {
                Vector3 diff = go.transform.position - position;
                float curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
        }

        return closest;
    }
    
    public bool DoTowersExist()
    {
        if (FindObjectOfType<WatchTower>())
            return true;
        else
            return false;
    }

    public void SetAgentSpeed()
    {
        aiStateManager.agent.speed = aiStateManager.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplayMode()
    {
        aiStateManager.canTalk = false;
    }

}
