using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepState : AIState
{
    public SleepItemHandler sleepingPlace;
    [SerializeField] private Animator anim;
    [SerializeField] private AIStateManager aiStateManager;

    [SerializeField] private float sleepRecoveryOnGround = 0.01f;

    public override AIState RunCurrentState()
    {
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Sleep"))
            anim.Play("Sleep");


        aiStateManager.agent.isStopped = true;

        aiStateManager.character.AddSatisfaction(RecoverSatisfaction());

        if (aiStateManager.IsWakeUpTime())
        {
            Debug.Log($"{aiStateManager.gameObject.name} wakes up!");

            aiStateManager.agent.isStopped = false;
            if(sleepingPlace != null)
            {
                sleepingPlace.ExitSleeping();
                ResetSleepSettings();
                return aiStateManager.TryChangeState(aiStateManager.idleState);
            }
            else
            {
                ResetSleepSettings();
                return aiStateManager.TryChangeState(aiStateManager.idleState);
            }
        }
        else
            return this;
    }

    public void SetSleepingPlace(SleepItemHandler sleepPlace)
    {
        sleepingPlace = sleepPlace;
    }

    public float RecoverSatisfaction()
    {
        float recoveryRate;

        if (sleepingPlace != null)
        {
            if (aiStateManager.satisfactionTrait == TraitManager.Traits.HappyNature)
            {
                recoveryRate = (sleepingPlace.recoveryRate * TraitManager.singleton.happyNatureRecoveryRate) * Time.deltaTime;
            }
            else if (aiStateManager.satisfactionTrait == TraitManager.Traits.Grumpy)
            {
                recoveryRate = (sleepingPlace.recoveryRate / TraitManager.singleton.grumpyRecoveryRate) * Time.deltaTime;
            }
            else
                recoveryRate = sleepingPlace.recoveryRate * Time.deltaTime;
        }
        else
        {
            if (aiStateManager.satisfactionTrait == TraitManager.Traits.HappyNature)
            {              
                recoveryRate = (sleepRecoveryOnGround * TraitManager.singleton.happyNatureRecoveryRate) * Time.deltaTime;
            }
            else if (aiStateManager.satisfactionTrait == TraitManager.Traits.Grumpy)
            {
                recoveryRate = (sleepRecoveryOnGround / TraitManager.singleton.grumpyRecoveryRate) * Time.deltaTime;
            }
            else
                recoveryRate = sleepRecoveryOnGround * Time.deltaTime;
        }

        return Mathf.Abs(recoveryRate);
    }

    public void ResetSleepSettings()
    {
        aiStateManager.untargetable = false;

        if (aiStateManager.faction == AIStateManager.Faction.villager)
        {
            if(aiStateManager.character.jobClass == TraitManager.JobClass.Hero)
            {
                aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
                aiStateManager.currentTextType = Dialogue.TextType.standard;
            }
            else
            {
                aiStateManager.currentDisplayMode = Dialogue.DisplayMode.character;
                aiStateManager.currentTextType = Dialogue.TextType.standard;
            }

        }
        else if(aiStateManager.faction == AIStateManager.Faction.enemy)
        {
            aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
            aiStateManager.currentTextType = Dialogue.TextType.standard;
        }
        else if (aiStateManager.faction == AIStateManager.Faction.neutral)
        {
            if(aiStateManager.character.jobClass == TraitManager.JobClass.Merchant)
            {
                aiStateManager.currentDisplayMode = Dialogue.DisplayMode.merchant;
                aiStateManager.currentTextType = Dialogue.TextType.shop;
            }
            else if(aiStateManager.character.jobClass == TraitManager.JobClass.Adventurer)
            {
                aiStateManager.currentDisplayMode = Dialogue.DisplayMode.adventurer;
                aiStateManager.currentTextType = Dialogue.TextType.standard;
            }
            else
            {
                aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
                aiStateManager.currentTextType = Dialogue.TextType.standard;
            }

        }


        aiStateManager.agent.isStopped = false;
        aiStateManager.findSleepState.sleepingPlace = null;
        aiStateManager.findSleepState.placeToSleepFound = false;
    }
}
