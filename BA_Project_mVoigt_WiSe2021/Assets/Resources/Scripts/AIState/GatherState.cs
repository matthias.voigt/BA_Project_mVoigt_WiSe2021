using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GatherState : AIState
{
    public AIStateManager aiStateManager;

    //public Quest currentQuest;
    public ObjectHandler questObject;

    public ObjectHandler[] allItems;
    public List<ObjectHandler> relevantItems;

    public List<GameObject> gatheredItems;

    public float timeTillSkip = 36000f;

    private void OnEnable()
    {
        questObject = null;
        relevantItems.Clear();
    }

    public override AIState RunCurrentState()
    {
        SetAgentSpeed();
        SetDisplayMode();
        aiStateManager.character.ReduceSatisfactionBy(GameManager.singleton.GetSatisfactionReduction(aiStateManager.character));

        if (aiStateManager.hasQuest && !aiStateManager.sendQuestStartStatus) aiStateManager.SendQuestStartStatus();

        if(gatheredItems.Count >= aiStateManager.currentQuest.amountToGet)
        {
            //REPORT
            aiStateManager.rP.AddReport($"Collecting those items was fun!");
            aiStateManager.rP.AddQuestReport("Anyways, I could collect all items you asked for!");
            
            Debug.Log("Quest finished!");
            aiStateManager.currentQuest.questFinished = true;
        }

        //Enough items collected?
        if (gatheredItems.Count >= aiStateManager.currentQuest.amountToGet || aiStateManager.currentQuest.questFailed)
        {
            return aiStateManager.TryChangeState(aiStateManager.questEndState);
        }
        //Is QuestObject null?
        if(questObject == null)
        {
            FindQuestObject();
        }
        //Is character far away from quest object?
        else if(!aiStateManager.IsCharacterNearbyObject(aiStateManager.character.gameObject,questObject.gameObject, 2f))
        {
            if(questObject == null)
            {
                return aiStateManager.TryChangeState(aiStateManager.idleState);
            }

            timeTillSkip -= Time.deltaTime * DayManager.singleton.speed;
            if(timeTillSkip <= 0)
            {
                Debug.LogWarning($"{aiStateManager.character.GetCharacterName()} could not pick up {questObject} due to an unkown error. Picking up by force.");
                return PickUpObject_Normal();
            }
            aiStateManager.agent.SetDestination(questObject.transform.position);
        }
        //Character is nearby questObject!
        else
        {
            if (questObject == null)
            {
                return aiStateManager.TryChangeState(aiStateManager.idleState);
            }
            else if (aiStateManager.IsActionSuccessful())
            {
                //REPORT
                aiStateManager.rP.AddReport($"Let's pick up this {questObject.objectTag}.");
                return PickUpObject_Normal();
            }
            else if(!aiStateManager.IsActionSuccessful())
            {
                //REPORT
                aiStateManager.rP.AddReport($"I wanted to pick up this {questObject.objectTag}, but I am too tired...");
                aiStateManager.rP.AddQuestReport("When I started this quest, I thought I could do it...I was just too tired...I am sorry...");

                Debug.Log($"{aiStateManager.character.GetCharacterName()} failed an action due to lack of satisfaction.");
                aiStateManager.currentQuest.questFailed = true;
                return aiStateManager.TryChangeState(aiStateManager.idleState);
            }
        }

        if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Sprint"))
            aiStateManager.anim.Play("Sprint");

        return this;
    }

    private void FindQuestObject()
    {
        if (FindObjectOfType<ObjectHandler>() == null)
        {
            Debug.LogWarning("No Items found with tag " + aiStateManager.currentQuest.GetObjectTag());
            aiStateManager.currentQuest.questFailed = true;
            aiStateManager.TryChangeState(aiStateManager.idleState);
            return;
        }

        else if (FindObjectOfType<ObjectHandler>())
        {
            Debug.Log($"{aiStateManager.character.GetCharacterName()} found collectable items.");

            allItems = FindObjectsOfType<ObjectHandler>();
            relevantItems = new List<ObjectHandler>();

            foreach (ObjectHandler oH in allItems)
            {
                if (oH.objectTag == aiStateManager.currentQuest.GetObjectTag())
                {
                    relevantItems.Add(oH);
                }
            }
        }
        else
        {
            //REPORT
            aiStateManager.rP.AddReport($"I wanted to gather some {questObject.objectTag}s, but I could not find any...");
            aiStateManager.rP.AddQuestReport("I wanted to gather some items, but I just could not find any of them...");
            Debug.LogWarning("No Items found with tag " + aiStateManager.currentQuest.GetObjectTag());
            aiStateManager.currentQuest.questFailed = true;
            aiStateManager.TryChangeState(aiStateManager.idleState);
            return;
        }
        allItems = null;

        questObject = GetNeareastObject();
    }

    private ObjectHandler GetNeareastObject()
    {
        ObjectHandler closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = aiStateManager.gameObject.transform.position;

        foreach (ObjectHandler go in relevantItems)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;

            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }

        return closest;                
    }

    private AIState PickUpObject_Normal()
    {
        Debug.Log($"{aiStateManager.character.GetCharacterName()} is picking up a {questObject.objectTag}.");
        if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("PickUp"))
        {
            aiStateManager.anim.Play("PickUp");
            Debug.Log($"{aiStateManager.character.GetCharacterName()} picked up object of tag {aiStateManager.currentQuest.GetObjectTag()}");
            return this;
        }
        else if (aiStateManager.anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            Debug.Log($"{aiStateManager.character.GetCharacterName()} picked up a quest item.");
            aiStateManager.rP.AddQuestReport("So I picked up the item you asked for.");
            gatheredItems.Add(questObject.gameObject);
            relevantItems.Remove(questObject);
            questObject.gameObject.SetActive(false);
            questObject = null;
            return aiStateManager.TryChangeState(aiStateManager.idleState);
        }
        else
            return this;
    }

    public void SetAgentSpeed()
    {
        aiStateManager.agent.speed = aiStateManager.runningSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplayMode()
    {
        aiStateManager.canTalk = true;
        switch (aiStateManager.faction)
        {
            case AIStateManager.Faction.villager:
                if (aiStateManager.character.jobClass == TraitManager.JobClass.Hero)
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
                    aiStateManager.currentTextType = Dialogue.TextType.standard;
                }
                else
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.character;
                    aiStateManager.currentTextType = Dialogue.TextType.standard;
                }
                break;
        }
    }
}
