using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowState : AIState
{
    public AIStateManager aiStateManager;
    public GameObject objToFollow = null;
    public AIStateManager aiToFollow;

    public override AIState RunCurrentState()
    {
        if(objToFollow == null)
        {
            aiStateManager.NPCUntargetable(false);
            Debug.Log($"{aiStateManager.character.GetCharacterName()} has no object to follow. Return to idle.");
            return aiStateManager.TryChangeState(aiStateManager.idleState);
        }
        aiStateManager.canTalk = true;

        //REPORT
        if(objToFollow.tag != "Player")
            aiStateManager.rP.AddReport($"Where are we going, {objToFollow.name}?");
        else
            aiStateManager.rP.AddReport($"Where are we going?");

        if (aiStateManager.hasQuest)
        {
            if (objToFollow.tag == "Player")
                aiStateManager.rP.AddQuestReport("Well, and then you wanted me to follow you.");
            else
            {
                aiStateManager.rP.AddQuestReport($"Then I had to follow {objToFollow.name}...");
            }
            
        }



        //Character is nearby Object
        if (aiStateManager.IsCharacterNearbyObject(aiStateManager.character.gameObject, objToFollow, 2f))
        {
            aiStateManager.agent.isStopped = true;

            if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                aiStateManager.anim.Play("Idle");
        }
        //Character is far away from object
        else if (aiStateManager.IsCharacterTooFarAwayFromObject(aiStateManager.gameObject, objToFollow, 8f))
        {
            if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Sprint"))
                aiStateManager.anim.Play("Sprint");

            aiStateManager.agent.speed = aiStateManager.runningSpeed * Time.deltaTime * DayManager.singleton.speed; ;
            aiStateManager.agent.isStopped = false;
        }
        //Character is away from object
        else if(aiStateManager.IsCharacterTooFarAwayFromObject(aiStateManager.gameObject, objToFollow, 4f))
        {
            if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                aiStateManager.anim.Play("Walk");

            aiStateManager.agent.speed = aiStateManager.walkingSpeed * Time.deltaTime * DayManager.singleton.speed; ;
            aiStateManager.agent.isStopped = false;
        }

        aiStateManager.agent.SetDestination(objToFollow.transform.position);

        return this;
    }

    private void SetAnimation()
    {
        if (aiStateManager.agent.velocity.magnitude < 0.15f)
        {
            if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                aiStateManager.anim.Play("StandingIdle_noWeapon");
        }
        if (aiStateManager.agent.velocity.magnitude < 0.15f)
        {
            if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                aiStateManager.anim.Play("Walk");
        }
    }
}
