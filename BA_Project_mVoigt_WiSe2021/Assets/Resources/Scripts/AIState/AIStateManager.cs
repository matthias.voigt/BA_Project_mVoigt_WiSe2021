using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIStateManager : MonoBehaviour
{
    [Header("Current State")]
    public AIState currentState;

    [Header("Character Components")]
    public Character character;
    public NavMeshAgent agent;
    public Animator anim;
    public Inventory inventory;
    public GameObject camTransform;

    [Header("States")]
    public IdleState idleState;
    public WanderState wanderState;
    public FindSleepState findSleepState;
    public TalkingState talkingState;
    public SleepState sleepState;
    public GatherState gatherState;
    public QuestEndState questEndState;
    public FollowState followState;
    public CheckQuestBoardState qBoardState;
    public WatchTowerState watchTowerState;
    public MerchantState merchState;
    public AttackState attackState;
    public FaintState faintState;
    public ResearchState researchState;
    public EscortPrisonerState escortPrisonerState;
    public ImprisonedState imprisonedState;
    public ScoutState scoutState;
    public FleeState fleeState;
    public RecoverState recoverState;
    public RescueState rescueState;
    public GoHomeState goHomeState;
    public TradeState tradeState;
    public CookState cookState;
    public HealedState healedState;

    public bool untargetable = false;

    public AIState previewsState;

    [Header("Traits")]
    public TraitManager.Traits sleepTrait;
    public TraitManager.Traits satisfactionTrait;

    [Header("Current Quest")]
    public bool hasQuest = false;
    public Quest currentQuest;
    public bool sendQuestStartStatus = false;

    [Header("Others")]
    public bool canTalk = true;
    public SpecialBuilding specialBuilding = null;
    public ReportHandler rP = null;

    [Header("Rescue Related")]
    public bool isCurrentlyMissing;
    public bool isDead = false;

    [Header("Others")]
    public float walkingSpeed = 1f;
    public float runningSpeed = 4f;

    private void Start()
    {
        if(inventory == null)
        {
            inventory = GetComponent<Inventory>();
        }

        if(rP == null)
        {
            rP = GetComponent<ReportHandler>();
        }
    }

    public void SetSleepTrait(TraitManager.Traits _sleepTrait)
    {
        sleepTrait = _sleepTrait;
    }

    public void SetSatisfactionTrait(TraitManager.Traits _satisfactionTrait)
    {
        satisfactionTrait = _satisfactionTrait;
    }

    public bool ignoreCutscene = false;
    void Update()
    {
        if (CutsceneManager.singleton.currentState != CutsceneManager.CutsceneState.none && !ignoreCutscene) return;

        GetTargetInSeight();
        CheckSleepTime();

        RunStateMachine();
    }

    private void RunStateMachine()
    {
      
        AIState nextState = currentState?.RunCurrentState();

        if(nextState != null)
        {
            SwitchToTheNextState(nextState);
        }
    }

    private void CheckSleepTime()
    {
        if (DialogueManager.singleton.currentAI == this) return;
        if (currentState == recoverState) return;
        if (currentState == findSleepState) return;
        if (currentState == sleepState) return;
        if (currentState == faintState) return;
        if (currentState == imprisonedState) return;
        if (currentDisplayMode == Dialogue.DisplayMode.ressurect) return;
        if (currentState == goHomeState) return;

        if (IsSleepingTime())
        {
            Debug.Log($"{gameObject.name} is sleepy!");
            currentState = TryChangeState(findSleepState);
        }
    }

    private void SwitchToTheNextState(AIState nextState)
    {
        currentState = nextState;
    }

    public bool HasCharacterQuest()
    {
        if (hasQuest) return true;
        else return false;
    }

    public AIState ChangeToQuestingState()
    {
        Debug.Log($"{character.GetCharacterName()} has a quest. Finding quest type.");
        switch (currentQuest.questType)
        {
            case QuestManager.QuestType.Gather:
                Debug.Log($"Quest type for {character.GetCharacterName()} is {gatherState}.");
                return TryChangeState(gatherState);
            case QuestManager.QuestType.Become:
                switch (currentQuest.job)
                {
                    case TraitManager.JobClass.Villager:
                        Debug.Log($"{character.GetCharacterName()} is becoming a {currentQuest.job}.");
                        rP.AddQuestReport("Time for some rest! As a villager I have no duties anymore! And I even got paid for it, thank you!");
                        rP.AddReport("I am a villager now! Time to relax!");
                        character.jobClass = TraitManager.JobClass.Villager;
                        currentQuest.questFinished = true;
                        return questEndState;
                    case TraitManager.JobClass.Adventurer:
                        Debug.Log($"{character.GetCharacterName()} is becoming a {currentQuest.job}.");
                        rP.AddQuestReport("Well...You want me to be an adventurer? So will it be!");
                        rP.AddReport("I am an adventurer now! Time to go on adventures!");
                        character.jobClass = TraitManager.JobClass.Adventurer;
                        currentQuest.questFinished = true;
                        return questEndState;
                    case TraitManager.JobClass.Merchant:
                        Debug.Log($"{character.GetCharacterName()} is becoming a {currentQuest.job}.");
                        rP.AddQuestReport("Well, you need a merchant? Just give me a place to sell some items, and we are good to go!");
                        rP.AddReport("I am a merchant now! Time to get rich!");
                        character.jobClass = TraitManager.JobClass.Merchant;
                        currentQuest.questFinished = true;
                        return questEndState;
                    case TraitManager.JobClass.Guard:
                        Debug.Log($"{character.GetCharacterName()} is becoming a {currentQuest.job}.");

                        rP.AddQuestReport("Well, you want me to become a guard? I will try to protect our town!");
                        rP.AddReport("I am a guard now! Villians, stay away!");
                        character.jobClass = TraitManager.JobClass.Guard;
                        currentQuest.questFinished = true;
                        return questEndState;
                    case TraitManager.JobClass.Researcher:
                        rP.AddQuestReport("Well, you want me to do some research? Give me a lab to do my working, and I can research wonderful constructions for you!");
                        rP.AddReport("I am a researcher now! I feel so intelligent!");
                        Debug.Log($"{character.GetCharacterName()} is becoming a {currentQuest.job}.");
                        character.jobClass = TraitManager.JobClass.Researcher;
                        currentQuest.questFinished = true;
                        return questEndState;
                    default:
                        Debug.LogError("Something went wrong with changing jobs!");
                        return questEndState;
                }
            case QuestManager.QuestType.Scout:
                Debug.Log($"Quest type for {character.GetCharacterName()} is {scoutState}.");
                return TryChangeState(scoutState);
            case QuestManager.QuestType.Rescue:
                Debug.Log($"Quest type for {character.GetCharacterName()} is {rescueState}.");
                return TryChangeState(rescueState);
            default:
                Debug.LogWarning($"No quest type found for {character.GetCharacterName()}! Returning to {idleState}.");
                return idleState;
        }
    }

    public AIState TryChangeState(AIState requestedState)
    {
        Debug.Log($"{character.GetCharacterName()} is trying to change from {currentState} to {requestedState}.");

        if(requestedState == currentState)
        {
            Debug.Log("Already in requested state");
            return currentState;
        }

        // IDLE
        else if(requestedState == idleState)
        {
            canTalk = true;
            idleState.SetIdleTime();
            agent.speed = 0;
            agent.isStopped = true;

            previewsState = currentState;
            currentState = idleState;
        }
        // WANDER
        else if(requestedState == wanderState)
        {
            wanderState.SetRandomPos();
            canTalk = true;
            agent.speed = walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;

            //wanderState.SetWanderTimer();

            previewsState = currentState;
            currentState = wanderState;

        }
        //SLEEP
        else if (requestedState == sleepState)
        {
            canTalk = false;
            agent.speed = 0;
            agent.isStopped = true;

            previewsState = currentState;
            currentState = sleepState;
        }
        //FIND SLEEP
        else if (requestedState == findSleepState)
        {
            if(followState.objToFollow != null)
            {
                if(followState.objToFollow.TryGetComponent(out AIStateManager ai))
                {
                    if(ai.currentState == ai.sleepState)
                    {
                        if(ai.currentState != ai.imprisonedState)
                        {
                            if (character.satisfaction < 10) character.AddSatisfaction(15);
                            ai.escortPrisonerState.prison = null;
                            ai.escortPrisonerState.prisoner = null;

                            if (hasQuest)
                                rP.AddQuestReport("My kidnapper went to sleep, so I tried to take the chance and wanted to escape!");
                            rP.AddReport("My kidnapper is sleeping, thats my chance!!!");

                            currentState = goHomeState;
                            NPCUntargetable(false);
                        }                     
                    }

                    else if(ai.escortPrisonerState.prisoner == this)
                    {
                        if(hasQuest)
                            rP.AddQuestReport("It was getting late and I was tired, but I was held captive, I did not know what do to.");
                        rP.AddReport("I am so tired, but my kidnapper won't let me sleep...");
                        Debug.Log($"{character.GetCharacterName()} cannot change to sleep, as it is a prisoner of {ai.character.GetCharacterName()}");
                        currentState = followState;
                        previewsState = followState;
                    }
                }
            }
            else
            {
                canTalk = true;
                agent.speed = walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
                agent.isStopped = false;

                findSleepState.sleepingPlace = null;
                findSleepState.placeToSleepFound = false;

                previewsState = currentState;
                currentState = findSleepState;
            }

            if(escortPrisonerState.prisoner != null)
            {
                //POSSIBLE REPORT?
                //Sending Prisoner to sleep
                escortPrisonerState.prisoner.currentState = escortPrisonerState.prisoner.TryChangeState(escortPrisonerState.prisoner.findSleepState);               
            }



        } 
        else if(requestedState == healedState)
        {
            healedState.healed = true;
            canTalk = true;
            previewsState = currentState;
            currentState = talkingState;
        }
        //TALK
        else if (requestedState == talkingState)
        {
            canTalk = true;
            agent.speed = 0;
            agent.isStopped = true;

            previewsState = currentState;
            currentState = talkingState;
        }
        //GATHER
        else if(requestedState == gatherState)
        {
            canTalk = true;
            agent.speed = runningSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            gatherState.timeTillSkip = 36000f;
            previewsState = currentState;
            currentState = gatherState;
        }
        else if(requestedState == questEndState)
        {
            canTalk = true;
            agent.speed = walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;

            previewsState = currentState;
            currentState = questEndState;

        }
        else if(requestedState == followState)
        {
            canTalk = true;
            agent.speed = walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            previewsState = currentState;
            currentState = followState;
        }
        else if(requestedState == qBoardState)
        {
            canTalk = true;
            agent.speed = walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            previewsState = currentState;
            currentState = qBoardState;
        }
        else if(requestedState == watchTowerState)
        {
            canTalk = true;
            agent.speed = walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            previewsState = currentState;
            currentState = watchTowerState;

        }
        else if (requestedState == merchState)
        {
            canTalk = false;
            agent.speed = walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            previewsState = currentState;
            currentState = merchState;

         
        }
        else if(requestedState == attackState)
        {
            canTalk = false;
            agent.speed = runningSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;

            if(attackState.objToAttack == null)
            {
                currentState = idleState;
            }
            else
            {
                attackState.attackInitPos = attackState.objToAttack.gameObject.transform.position;
                attackState.initPos = transform.position;

                if (followState.objToFollow != GameManager.singleton.playerObj)
                    followState.objToFollow = null;

                previewsState = currentState;
                currentState = attackState;
            }



        }
        else if (requestedState == faintState)
        {
            canTalk = true;
            agent.speed = 0;
            agent.isStopped = true;
      
            if(escortPrisonerState.prisoner != null)
            {
                //Removing Prisoner
                Debug.Log($"Removing prisoner from {character.GetCharacterName()}. Possible Event report!!!");
          
                escortPrisonerState.prisoner.currentTextType = Dialogue.TextType.standard;
                escortPrisonerState.prisoner.currentDisplayMode = Dialogue.DisplayMode.character;
                
                escortPrisonerState.prisoner.followState.objToFollow = null;
                escortPrisonerState.prisoner = null;

                if (escortPrisonerState.prison != null)
                {
                    escortPrisonerState.prison.currentObj = null;
                    escortPrisonerState.prison = null;
                }

            }

            previewsState = currentState;
            currentState = faintState;

            currentTextType = Dialogue.TextType.fainted;
            currentDisplayMode = Dialogue.DisplayMode.lootable;

        }
        else if (requestedState == researchState)
        {
            canTalk = true;
            agent.speed = walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            previewsState = currentState;
            currentState = researchState;

        }
        else if (requestedState == escortPrisonerState)
        {
            canTalk = true;
            agent.speed = walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;

            previewsState = currentState;
            currentState = escortPrisonerState;

        }
        else if(requestedState == imprisonedState)
        {
            agent.enabled = false;
            canTalk = false;
            previewsState = currentState;
            currentState = imprisonedState;


        }
        else if (requestedState == scoutState)
        {
            canTalk = true;
            agent.speed = runningSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            previewsState = currentState;
            currentState = scoutState;

        }
        else if (requestedState == fleeState)
        {
            fleeState.destReached = true;
            canTalk = false;
            agent.speed = runningSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            previewsState = currentState;
            currentState = fleeState;

        }
        else if (requestedState == recoverState)
        {
            recoverState.timeTillRecovery = GameManager.singleton.timeTillRecovery;
            canTalk = false;
            agent.speed = 0;
            agent.isStopped = true;
            previewsState = currentState;
            currentState = recoverState;

        }
        else if (requestedState == rescueState)
        {
            canTalk = true;
            agent.speed = runningSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            previewsState = currentState;
            currentState = rescueState;

        }
        else if(requestedState == goHomeState)
        {
            goHomeState.isGoingHome = true;
            canTalk = true;
            agent.speed = runningSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            currentDisplayMode = Dialogue.DisplayMode.character;
            currentTextType = Dialogue.TextType.standard;
            previewsState = currentState;
            currentState = goHomeState;
        }
        else if (requestedState == tradeState)
        {
            canTalk = true;
            agent.speed = walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
            agent.isStopped = false;
            previewsState = currentState;
            currentState = tradeState;
        }
        else if(requestedState == cookState)
        {
            agent.isStopped = false;
            previewsState = currentState;
            currentState = cookState;
            canTalk = true;
        }


        return currentState;

    }

    public bool IsActionSuccessful()
    {
        float satisfaction = character.GetProsperitySatisfaction();
        float minSatisfactionRequired = 10;

        if (satisfaction > minSatisfactionRequired)
        {
            return true;
        }
        else return false;
    }

    public bool IsSleepingTime()
    {
        float currentTime = DayManager.singleton.GetCurrentTimeFloat();
        float wakeTime = character.wakeTime;
        float sleepTime = character.sleepTime;


        if (sleepTrait == TraitManager.Traits.NightOwl)
        {
            if (currentTime > sleepTime && currentTime < wakeTime) return true;
            else return false;
        }
        else if (sleepTrait == TraitManager.Traits.EarlyBird || sleepTrait == TraitManager.Traits.SleepyHead)
        {
            if (currentTime > sleepTime) return true;
            else return false;
        }
        else if (sleepTrait == TraitManager.Traits.Sleepless)
        {
            if (currentTime >= 0 && currentTime < wakeTime) return true;
            else return false;
        }
        else if (currentTime > sleepTime) return true;
        else return false;

    }

    public bool IsWakeUpTime()
    {
        float currentTime = DayManager.singleton.GetCurrentTimeFloat();
        float wakeTime = character.wakeTime;
        float sleepTime = character.sleepTime;

        if(sleepTrait == TraitManager.Traits.NightOwl)
        {
            if (currentTime >= wakeTime) return true;
            else return false;
        }
        else if (currentTime >= wakeTime && currentTime < sleepTime) return true;
        else return false;
    }

    public bool IsCharacterNearbyObject(GameObject ownObj, GameObject goalObj, float distance)
    {
        if (Vector3.Distance(ownObj.transform.position, goalObj.transform.position) < distance)
        {
            Debug.Log($"{ownObj.name} reached {goalObj.name}.");
            return true;
        }
        else
            return false;
    }

    public bool IsCharacterTooFarAwayFromObject(GameObject ownObj, GameObject goalObj, float distance)
    {
        if (Vector3.Distance(ownObj.transform.position, goalObj.transform.position) > distance)
        {
            Debug.Log($"{ownObj.name} is too far away from {goalObj.name}.");
            return true;
        }
        else
            return false;
    }


    public void EndCurrentQuest()
    {
        Debug.Log($"{character.GetCharacterName()} ends questing states. Resetting quest.");
        rP.ClearQuestReport();
        rP.AddReport("I am finally done with my quest!");

        currentDisplayMode = Dialogue.DisplayMode.character;
        currentTextType = Dialogue.TextType.standard;

        if(currentQuest.questFailed == true)
        {            
            int moneyBack = currentQuest.moneyReward - currentQuest.moneyReward / 4;
            GameManager.singleton.PlayerInventory.AddMoney(moneyBack);
            character.AddQuestExp(-1);
            inventory.AddMoney(currentQuest.moneyReward/4);

            Debug.Log($"{character.GetCharacterName()} failed the quest. " +
                $"Only getting quarter of money({moneyBack / 4}) as reward. " +
                $"Player gets back {moneyBack} coins. Loosing 1 Prestige point.");

            PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.questFailed, character.raceType);
            StatusManager.singleton.InsertNewStatus($"{character.GetCharacterName()} failed quest. Obtained {moneyBack} gold back.");
        }
        else if(currentQuest.questFailed == false)
        {
            PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.questsuccessful, character.raceType);
            inventory.AddMoney(currentQuest.moneyReward);
            character.AddQuestExp(1);
        }
        

        switch (currentQuest.questType)
        {
            case QuestManager.QuestType.Gather:
                Debug.Log($"{character.GetCharacterName()} drops gathered items.");
                if (gatherState.gatheredItems.Count > 0)
                {
                    foreach(GameObject item in gatherState.gatheredItems)
                    {
                        item.transform.position = new Vector3(character.gameObject.transform.position.x, character.gameObject.transform.position.y + 2, character.gameObject.transform.position.z);
                        item.SetActive(true);
                    }
                    gatherState.gatheredItems.Clear();
                }
                break;
            case QuestManager.QuestType.Scout:
                Debug.LogWarning("Scout Mission finished.");               
                scoutState.ResetSettings();
                break;
            case QuestManager.QuestType.Rescue:
                if (rescueState.pickedUpSkull)
                {
                    inventory.DropItem(rescueState.skullObject.GetComponent<ObjectHandler>());
                }
                break;
        }


        //Reset Quest
        hasQuest = false;
        sendQuestStartStatus = false;
        currentQuest.questFinished = false;
        currentQuest.questFailed = false;     

        //Reset Quest End State
        questEndState.readyToReport = false;
        questEndState.sendStatus = false;

        //Reset Gather State
        gatherState.allItems = null;
        gatherState.relevantItems.Clear();

        //Reset Rescue State
        rescueState.rescuedAi = null;
        rescueState.hp = null;
        rescueState.changedPopularity = false;

        rescueState.pickedUpSkull = false;
        rescueState.skullObject = null;
        rescueState.npcDied = false;

        //Clear Quest
        QuestManager.singleton.currentQuestsAssigned.Remove(currentQuest);
        currentQuest.questDisplayer.DestroySelf();
    }

    public void SendQuestStartStatus()
    {
        sendQuestStartStatus = true;
        StatusManager.singleton.InsertNewStatus($"{character.GetCharacterName()} is going on Quest: " + currentQuest.questName);
    }

    public void NPCUntargetable(bool turn)
    {
        untargetable = turn;
    }

    public void GetTargetInSeight()
    {
        if (currentState == goHomeState) return;
        if (untargetable) return;
        if (DialogueManager.singleton.currentAI == this) return;
        if (attackState.objToAttack != null) return;
        if (currentState == sleepState) return;        
        if (currentDisplayMode == Dialogue.DisplayMode.ressurect) return;

        //RaycastHit hit;
        Vector3 origin = gameObject.transform.position;
        Vector3 dir = transform.position;
        float sphereRad = 20f;
        //float maxDist = 10f;
        float seightBuff = 0f;

        if (currentState == watchTowerState && watchTowerState.isInTower)
        {
            seightBuff = GameManager.singleton.seightBuff;
        }
        else
        {
            seightBuff = 0f;
        }


        LayerMask layerMask = LayerMask.GetMask("Character");

        AIStateManager hitAi = null;


        Collider[] hitColliders = Physics.OverlapSphere(origin, sphereRad + seightBuff, layerMask);

        foreach (var hc in hitColliders)
        {
            if (hc.TryGetComponent(out AIStateManager aM))
            {
                if (aM.currentDisplayMode != Dialogue.DisplayMode.ressurect)
                {
                    hitAi = aM;

                    //This NPC is a villager
                    if (faction == Faction.villager)
                    {
                        if (hitAi.faction == Faction.enemy && !hitAi.untargetable)
                        {
                            AbandonAllBuldings();

                            rP.AddReport("Enemy in sight!");

                            if (hitAi.currentState == hitAi.sleepState)
                            {
                                hitAi.rP.AddReport("Yawn, what is going on...?");
                                hitAi.rP.AddQuestReport("I was sleeping, but woke up due to strange noises I heard...");
                                hitAi.sleepState.ResetSleepSettings();
                                if (hitAi.sleepState.sleepingPlace != null)
                                {
                                    hitAi.sleepState.sleepingPlace.ExitSleeping();
                                    hitAi.sleepState.ResetSleepSettings();
                                }

                                hitAi.currentState = hitAi.TryChangeState(hitAi.idleState);
                            }
                            

                            if (hasQuest)
                            {
                                if (currentQuest.questFinished)
                                {
                                    rP.AddQuestReport("I was already done with my quest, but then I got attacked by an enemy!");
                                }
                                else
                                {
                                    rP.AddQuestReport("I wanted to do my quest, but then I got attacked by an enemy!");
                                }
                            }

                            if (character.traits.Contains(TraitManager.Traits.Pacifist))
                            {
                                fleeState.fleeFromObj = hitAi;
                                currentState = TryChangeState(fleeState);
                            }
                            else
                            {
                                //Target is an enemy
                                attackState.objToAttack = hitAi;
                                currentState = TryChangeState(attackState);
                            }

                        }
                        else
                        {
                            //Debug.Log($"{character.GetCharacterName()} does not need to attack {hitAi.character.GetCharacterName()} as they are no enemies.");
                        }
                    }
                    //This NPC is an enemy
                    else if (faction == Faction.enemy)
                    {
                        //Target is a villager
                        if ((hitAi.faction == Faction.villager || hitAi.faction == Faction.neutral) && !hitAi.untargetable)
                        {
                            AbandonAllBuldings();

                            if (hitAi.currentState == hitAi.sleepState)
                            {
                                hitAi.rP.AddReport("Yawn, what is going on...?");
                                hitAi.rP.AddQuestReport("I was sleeping, but woke up due to strange noises I heard...");
                                hitAi.sleepState.ResetSleepSettings();
                                if (hitAi.sleepState.sleepingPlace != null)
                                {
                                    hitAi.sleepState.sleepingPlace.ExitSleeping();
                                    hitAi.sleepState.ResetSleepSettings();
                                }
                                
                                hitAi.currentState = hitAi.TryChangeState(hitAi.idleState);
                            }

                            //Target is an enemy
                            if (hitAi.faction == Faction.villager)
                                rP.AddReport("A companion of Hero?! Attack!");
                            else if (hitAi.faction == Faction.neutral)
                                rP.AddReport("A stranger on this island? Attack, before Hero recruits him!");

                            if (character.traits.Contains(TraitManager.Traits.Pacifist))
                            {
                                fleeState.fleeFromObj = hitAi;
                                currentState = TryChangeState(fleeState);
                            }
                            else
                            {
                                //Target is an enemy
                                attackState.objToAttack = hitAi;
                                currentState = TryChangeState(attackState);
                            }
                        }
                        else
                        {
                            //Debug.Log($"{character.GetCharacterName()} does not need to attack {hitAi.character.GetCharacterName()} as they are no enemies.");
                        }
                    }
                }
            }
        }
    }

   
    public Faction faction = Faction.villager;
    public FactionIcon fc;
    public enum Faction
    {
        enemy,          
        neutral,        
        villager         
    }


    public void SetRandomFaction()
    {
        faction = (Faction)Random.Range(0, System.Enum.GetValues(typeof(Faction)).Length);
    }
    public void SetFactionIcon()
    {
        fc.SetIcon(faction);
    }

    public void AbandonAllBuldings()
    {
        Debug.Log($"Checking {character.GetCharacterName()} for leaving building.");
        if(currentState == watchTowerState && watchTowerState.isInTower)
        {
            watchTowerState.currentWatchTower.ExitTower();
        }
        else if(currentState == merchState && merchState.ownedMerchBuild.isOpen)
        {
            merchState.ownedMerchBuild.ExitShop();
        }
        else if(currentState == researchState && researchState.ownedLabBuild.isOpen)
        {
            researchState.ownedLabBuild.ExitShop();
        }
        else if(currentState == sleepState)
        {
            if(sleepState.sleepingPlace != null && sleepState.sleepingPlace.currentObject == this.gameObject)
            {
                sleepState.sleepingPlace.ExitSleeping();
            }
        }
        currentState = TryChangeState(idleState);
    }

    public Dialogue.TextType currentTextType = Dialogue.TextType.standard;
    public Dialogue.TextType GetCurrentTextType()
    {
        return currentTextType;
    }
    public Dialogue.DisplayMode currentDisplayMode = Dialogue.DisplayMode.character;
    public Dialogue.DisplayMode GetCurrentDisplayMode()
    {
        return currentDisplayMode;
    }

    public int GetCurrentStateID()
    {
        if (currentState == idleState) return 0;
        else if (currentState == wanderState) return 1;
        else if (currentState == findSleepState) return 2;
        else if (currentState == talkingState) return 3;
        else if (currentState == sleepState) return 4;
        else if (currentState == gatherState) return 5;
        else if (currentState == questEndState) return 6;
        else if (currentState == followState) return 7;
        else if (currentState == qBoardState) return 8;
        else if (currentState == watchTowerState) return 9;
        else if (currentState == merchState) return 10;
        else if (currentState == attackState) return 11;
        else if (currentState == faintState) return 12;
        else if (currentState == researchState) return 13;
        else if (currentState == escortPrisonerState) return 14;
        else if (currentState == imprisonedState) return 15;
        else if (currentState == scoutState) return 16;
        else if (currentState == fleeState) return 17;
        else if (currentState == recoverState) return 18;
        else if (currentState == rescueState) return 19;
        else if (currentState == goHomeState) return 20;
        else if (currentState == tradeState) return 21;
        else if (currentState == cookState) return 22;
        else return 0;
    }
}
