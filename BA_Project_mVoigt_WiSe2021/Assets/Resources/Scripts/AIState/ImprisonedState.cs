using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImprisonedState : AIState
{
    public AIStateManager aM;
    public Prison prison = null;
    public float timeTillEscape = 18000f;
    public bool changePopularity = false;
    public override AIState RunCurrentState()
    {

        //SetDialogueTypes();

        aM.canTalk = false;
        if (!changePopularity)
        {
            changePopularity = true;
            PopularityManager.singleton.ChangePopularity(PopularityManager.PopularityAction.imprison, aM.character.raceType);
        }


        if (aM.IsSleepingTime())
        {
            if (!aM.anim.GetCurrentAnimatorStateInfo(0).IsName("Sleep"))
                aM.anim.Play("Sleep");

            aM.character.AddSatisfaction(aM.sleepState.RecoverSatisfaction());
        }
        else
        {
            if (!aM.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                aM.anim.Play("Idle");

            timeTillEscape -= Time.deltaTime * DayManager.singleton.speed;
            if(timeTillEscape <= 0)
            {
                timeTillEscape = 18000f;
                EscapePrison();
            }
        }


        return this;
    }

    public void SetDialogueTypes()
    {
        aM.currentDisplayMode = Dialogue.DisplayMode.prisoner;
        aM.currentTextType = Dialogue.TextType.standard;
    }

    private void EscapePrison()
    {
        if (aM.character.GetProsperitySatisfaction() >= 15)
        {
            Debug.Log($"{aM.character.GetCharacterName()} is trying to escape from prison.");
            prison.EscapePrison();
        }
        else
        {
            aM.character.AddSatisfaction(Random.Range(5, 20));
        }
    }
}
