using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscortPrisonerState: AIState
{
    public AIStateManager aiState;
    public Prison prison;
    public AIStateManager prisoner;

    public bool loweredCage = false;
    public bool pDelivered = false;
    public bool upperedCage = false;
    public override AIState RunCurrentState()
    {
        SetAgentSpeed();

        //Someone else is escorting prisoner
        if (prisoner.followState.objToFollow != aiState.gameObject)
        {
            if (aiState.hasQuest)
            {
                aiState.rP.AddQuestReport("I wanted to escort my prisoner, but someone else already did the task.");
            }
            aiState.rP.AddReport("I wanted to escort my prisoner, but someone else already did the task.");
            prisoner = null;
            return aiState.idleState;
        }
        if (prisoner.isDead)
        {
            aiState.rP.AddReport("Oh no! My prisoner died...I should've paid more attention...");
            if(aiState.hasQuest) aiState.rP.AddQuestReport("But when I tried escorting my prisoner, this villain unfortunately died in some way...I should've paid more attention.");
            Debug.Log($"{aiState.character.GetCharacterName()} lost its prisoner.");
            ResetEscortSettings();
            return aiState.TryChangeState(aiState.idleState);
        }

        if (!DoPrisonsExist()) 
        {
            ResetEscortSettings();
            aiState.rP.AddReport("I cannot escort a prisoner without a prison.");
            if (aiState.hasQuest) aiState.rP.AddQuestReport("I wanted to escort my attacker to a prison, but we had none, so I left the villain behind and continued my quest.");
            return aiState.TryChangeState(aiState.idleState);
        }
    
        //Find empty prison
        if (DoPrisonsExist() && prison == null)
        {
            prison = GetNeareastObject();

            if (prison == null)
            {
                Debug.Log($"{aiState.character.GetCharacterName()} tried to escort {prisoner.character.GetCharacterName()} to prison, but none of them are empty.");
                aiState.rP.AddReport("I cannot escort a prisoner without an empty prison.");
                if (aiState.hasQuest) aiState.rP.AddQuestReport("I wanted to escort my attacker to prison, but our prisons were already full, so I left the villain behind and continued my quest.");
                ResetEscortSettings();
                return aiState.TryChangeState(aiState.idleState);
            }
            else
            {
                aiState.rP.AddReport($"Escorting {prisoner.character.GetCharacterName()} to prison.");
                if (aiState.hasQuest) aiState.rP.AddQuestReport("Well, I escorted my attacker to prison.");
                prison.currentObj = prisoner;

                //Tell Prisoner to follow
                prisoner.currentTextType = Dialogue.TextType.escort;
                prisoner.currentDisplayMode = Dialogue.DisplayMode.none;
                prisoner.followState.objToFollow = aiState.gameObject;
                prisoner.currentState = prisoner.TryChangeState(prisoner.followState);
                QuestManager.singleton.RemoveFaintedNPC(prisoner);
            }
        }


        if (aiState.IsCharacterNearbyObject(aiState.gameObject, prison.EnterZoneEscort.gameObject, 5f))
        {
            if (!loweredCage)
            {
                StartCoroutine(LowerCage());            
            }
            else if (!pDelivered) // Cage is lowered, prisoner go in
            {
                StartCoroutine(PrisonAnim());

                /*
                prison.EnterCage(prisoner);
                pDelivered = true;
                prisoner.followState.objToFollow = prison.EnterZonePrisoner.gameObject;

                Debug.Log(Vector3.Distance(prisoner.transform.position, prison.EnterZonePrisoner.position));
                if (Vector3.Distance(prisoner.transform.position, prison.EnterZonePrisoner.position) <= 5f)
                {                   
                    prison.EnterCage(prisoner);
                    pDelivered = true;
                }*/
            }
            else if (!upperedCage)
            {
                upperedCage = true;
                prison.CloseCage();            
            }
            else if (upperedCage)
            {
                aiState.rP.AddReport($"I put {prisoner.character.GetCharacterName()} into prison.");
                if (aiState.hasQuest) aiState.rP.AddQuestReport("Finally at the end, I brought the villain to prison.");
                Debug.Log($"{aiState.character.GetCharacterName()} successfully delivered {prisoner.character.GetCharacterName()} to prison.");
                ResetEscortSettings();
                return aiState.TryChangeState(aiState.idleState);
            }
            else return this;

            
        }
        else //Go to empty prison
        {
            aiState.rP.AddReport($"Escorting {prisoner.character.GetCharacterName()} to prison.");
            if (aiState.hasQuest) aiState.rP.AddQuestReport("Well, I escorted my attacker to an empty prison at our town.");
            aiState.agent.SetDestination(prison.EnterZoneEscort.position);

            if (!aiState.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                aiState.anim.Play("Walk");
        }

        return this;
    }

    IEnumerator LowerCage()
    {
        if(prison != null)
            prison.LowerCage();
        yield return new WaitForSeconds(3f);
        loweredCage = true;

    }

    IEnumerator PrisonAnim()
    {
        if(prison != null)
            prison.EnterCage(prisoner);

        yield return new WaitForSeconds(3f);
        pDelivered = true;
    }
    public bool DoPrisonsExist()
    {
        if (FindObjectOfType<Prison>())
        {
            Debug.Log("Prisons do exist.");
            return true;
        }
        else
        {
            Debug.Log("No prisons existing.");
            return false;
        }
    }
    public Prison GetNeareastObject()
    {
        Prison[] allPrisons = FindObjectsOfType<Prison>();
        Prison closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = aiState.gameObject.transform.position;

        foreach (Prison go in allPrisons)
        {
            if (go.currentObj == null && go.faction == aiState.faction)
            {             
                Vector3 diff = go.transform.position - position;
                float curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
        }

        return closest;
    }

    public void ResetEscortSettings()
    {
        upperedCage = false;
        pDelivered = false;
        loweredCage = false;
        prison = null;
        prisoner.followState.objToFollow = null;
        prisoner = null;
    }

    public void SetAgentSpeed()
    {
        if(prisoner != null && prisoner.currentState == prisoner.talkingState && DialogueManager.singleton.currentDialogueState == DialogueManager.DialogueState.inDialogue)
        {
            aiState.agent.speed *= 0;
        }
        else
        {
            aiState.agent.speed = aiState.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
        }
    }

    public void SetDisplaymode()
    {
        aiState.canTalk = true;
        switch (aiState.faction)
        {
            case AIStateManager.Faction.villager:
                if (aiState.character.jobClass == TraitManager.JobClass.Hero)
                {
                    aiState.currentDisplayMode = Dialogue.DisplayMode.none;
                    aiState.currentTextType = Dialogue.TextType.standard;
                }
                else
                {
                    aiState.currentDisplayMode = Dialogue.DisplayMode.character;
                    aiState.currentTextType = Dialogue.TextType.standard;
                }
                break;
            case AIStateManager.Faction.enemy:
                aiState.currentDisplayMode = Dialogue.DisplayMode.none;
                aiState.currentTextType = Dialogue.TextType.standard;
                break;
            case AIStateManager.Faction.neutral:
                if (aiState.character.jobClass == TraitManager.JobClass.Adventurer)
                {
                    aiState.currentDisplayMode = Dialogue.DisplayMode.adventurer;
                    aiState.currentTextType = Dialogue.TextType.standard;
                }
                else if (aiState.character.jobClass == TraitManager.JobClass.Merchant)
                {
                    aiState.currentDisplayMode = Dialogue.DisplayMode.merchant;
                    aiState.currentTextType = Dialogue.TextType.shop;
                }
                else if (aiState.character.race == Character.Race.skeleton)
                {
                    aiState.currentDisplayMode = Dialogue.DisplayMode.ressurect;
                    aiState.currentTextType = Dialogue.TextType.standard;
                }
                break;
        }
    }
}
