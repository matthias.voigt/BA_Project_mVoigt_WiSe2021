using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class WanderState : AIState
{
    public AIStateManager aiStateManager;
    
    public float wanderRadius;
    public Vector3 randomPos;
    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

    public override AIState RunCurrentState()
    {
        SetDisplaymode();
        SetAgentSpeed();
        SetAnimation();
        Report();

        //Is there a quest to fullfill?
        if (aiStateManager.hasQuest)
        {
            return aiStateManager.TryChangeState(aiStateManager.ChangeToQuestingState());
        }

        
        aiStateManager.agent.SetDestination(randomPos);

        if(Vector3.Distance(aiStateManager.gameObject.transform.position, randomPos) < 1f)
        {
            return aiStateManager.TryChangeState(aiStateManager.idleState);
        }

        return this;
    }

    public void SetRandomPos()
    {
        //Get a random destination;
        randomPos = RandomNavSphere(aiStateManager.gameObject.transform.position, wanderRadius, -1);
    }

    private void SetAnimation()
    {
        if (aiStateManager.agent.velocity.magnitude < 0.15f)
        {
            if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                aiStateManager.anim.Play("Idle");
        }
        else
        {
            if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                aiStateManager.anim.Play("Walk");
        }
    }

    private void Report()
    {
        aiStateManager.rP.AddReport("I am going for a walk.");
    }

    public void SetAgentSpeed()
    {
       aiStateManager.agent.speed = aiStateManager.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplaymode()
    {
        aiStateManager.canTalk = true;
        switch (aiStateManager.faction)
        {
            case AIStateManager.Faction.villager:
                if (aiStateManager.character.jobClass == TraitManager.JobClass.Hero)
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
                    aiStateManager.currentTextType = Dialogue.TextType.standard;
                }
                else
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.character;
                    aiStateManager.currentTextType = Dialogue.TextType.standard;
                }
                break;
            case AIStateManager.Faction.enemy:
                aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
                aiStateManager.currentTextType = Dialogue.TextType.standard;
                break;
            case AIStateManager.Faction.neutral:
                if (aiStateManager.character.jobClass == TraitManager.JobClass.Adventurer)
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.adventurer;
                    aiStateManager.currentTextType = Dialogue.TextType.standard;
                }
                else if (aiStateManager.character.jobClass == TraitManager.JobClass.Merchant)
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.merchant;
                    aiStateManager.currentTextType = Dialogue.TextType.shop;
                }
                else if (aiStateManager.character.race == Character.Race.skeleton)
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.ressurect;
                    aiStateManager.currentTextType = Dialogue.TextType.standard;
                }
                break;
        }
    }
}
