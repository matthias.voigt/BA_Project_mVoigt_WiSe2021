using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealedState : AIState
{
    private GameObject qBoard;
    public AIStateManager ai;
    public bool healed = false;
    public override AIState RunCurrentState()
    {

        if (qBoard == null)
            qBoard = GameObject.FindGameObjectWithTag("Questboard");

        if(qBoard != null)
        {
            if (ai.IsCharacterTooFarAwayFromObject(ai.gameObject, qBoard.gameObject, 10f))
            {
                ai.agent.SetDestination(qBoard.transform.position);
                SetAgentSpeed();
            }
            else
            {
                ai.agent.speed *= 0;
            }
        }




        SetAnimation();
        DisplayMode();

        return this;
    }

    public void DisplayMode()
    {
        ai.currentDisplayMode = Dialogue.DisplayMode.exitHospital;
        ai.currentTextType = Dialogue.TextType.healed;
    }
    public void SetAnimation()
    {
        if (!ai.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            ai.anim.Play("Idle");
    }

    public void SetAgentSpeed()
    {
        ai.agent.speed = ai.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
    }
}
