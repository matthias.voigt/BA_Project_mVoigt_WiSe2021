using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class FindSleepState : AIState
{
    [Header("Components")]
    public AIStateManager aiStateManager;
    public SleepItemHandler sleepingPlace = null;

    [Header("Booleans")]
    public bool searchedForPlace = false;
    public bool placeToSleepFound = false;
    public bool noPlaceToSleep = false;

    [Header("Sight")]
    public float seightDistance = 50f;


    public GameObject[] gos;
    private List<GameObject> placesToSleep = new List<GameObject>();

    public override AIState RunCurrentState()
    {
        SetAgentSpeed();
        SetAnimation();
        SetDisplaymode();


        
        aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;

        aiStateManager.currentTextType = Dialogue.TextType.findSleep;


        if (aiStateManager.IsWakeUpTime())
        {
            aiStateManager.rP.AddReport("I am so tired...I didn't get any sleep at night...");
            ResetSettings();
            return aiStateManager.TryChangeState(aiStateManager.idleState);
        }
            

        aiStateManager.agent.isStopped = false;


        if (noPlaceToSleep)
        {
            aiStateManager.rP.AddQuestReport("Unfortunately there was no place to sleep, so I had to sleep on the ground.");
            ResetSettings();
            return aiStateManager.TryChangeState(aiStateManager.sleepState);
        }


        if (!placeToSleepFound)
        {
            aiStateManager.rP.AddQuestReport("It was getting late, so I decided to go to sleep.");
            FindAPlaceToSleep(); //Character searches for a place.
            return this;
        }
        else if (sleepingPlace != null) //Found a sleeping place
        {
            if (IsCharacterNearbySleepingPlace()) //Is sleeping place nearby
            {
                if (IsSleepingPlaceEmpty()) //Is sleeping place empty
                {                   
                    sleepingPlace.SleepOnObject(aiStateManager.gameObject, aiStateManager.anim);
                    aiStateManager.sleepState.SetSleepingPlace(sleepingPlace);
                    ResetSettings();
                    return aiStateManager.TryChangeState(aiStateManager.sleepState);
                }
                else //Sleeping place is not empty
                {
                    placesToSleep.Remove(sleepingPlace.gameObject);
                    placeToSleepFound = false;
                    return aiStateManager.TryChangeState(aiStateManager.idleState);
                }

            } 
            else //Going to sleeping place
            {
                aiStateManager.agent.SetDestination(sleepingPlace.transform.position);
                return this;
            } 
        }
        else return this;

        #region oldcode
        /*
        if (DayManager.singleton.GetCurrentTime() == DayManager.CurrentTimeState.morning)
        {
            searchedForPlace = false;
            sleepingPlace.ExitSleeping();
            aiStateManager.TryChangeAIState(aiStateManager.idleState);
            agent.speed = 1;
            return aiStateManager.idleState;
        }
        else if(DayManager.singleton.time > 86400 || DayManager.singleton.time < 21600)
        {
            if (!sleeping)
            {
                Debug.Log($"It is getting late! {aiStateManager.gameObject.name} is running to a place to sleep in.");
                agent.speed = 4;

                if (!anim.GetCurrentAnimatorStateInfo(0).IsName("NormalSprint_noWeapon"))
                    anim.Play("NormalSprint_noWeapon");
            }
        }
        else
        {
            if (!sleeping)
            {
                agent.speed = 1;
                if (!anim.GetCurrentAnimatorStateInfo(0).IsName("NormalWalk_noWeapon"))
                    anim.Play("NormalWalk_noWeapon");
            }
        }

        if (!placeToSleepFound)
        {

            if(aiStateManager.character.GetCurrentHome() != null)
            {
                Debug.Log($"{gameObject.name} is sleepy and heading home.");
                sleepingPlace = aiStateManager.character.GetCurrentHome().GetSleepHandlerItem();
                agent.SetDestination(sleepingPlace.enterPosition.position);

                placeToSleepFound = true;
            }

            else if (GameObject.FindGameObjectWithTag("Bench") != null)
            {
                Debug.Log($"{gameObject.name} is sleepy but has no home. Finding a place to sleep.");
                sleepingPlace = GameObject.FindGameObjectWithTag("Bench").GetComponent<SleepItemHandler>();
                placeToSleepFound = true;

                
                agent.SetDestination(FindClosestBench());

            }
            else
            {               
                aiStateManager.TryChangeAIState(aiStateManager.idleState);
                return aiStateManager.idleState;
            }
                
        }
        else if(sleepingPlace != null)
        {
            if (Vector3.Distance(transform.position, sleepingPlace.enterPosition.position) < 1.5f)
            {
                if (!sleeping)
                {
                    Debug.Log($"{gameObject} is near a place to sleep!");

                    if (sleepingPlace.currentObject == null)
                    {
                        agent.isStopped = true;
                        sleepingPlace.SleepOnObject(aiStateManager.gameObject, anim);
                        sleeping = true;
                        return this;
                    }
                    else
                    {
                        placeToSleepFound = false;
                        aiStateManager.TryChangeAIState(aiStateManager.idleState);
                        placesToSleep.Remove(sleepingPlace.gameObject);
                        return aiStateManager.idleState;
                    }
                }
                else
                {
                    if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Sleep_noWeapon"))
                        anim.Play("Sleep_noWeapon");
                }
            }
        }
        else
        {
            Debug.Log($"{aiStateManager.gameObject.name} cannot find a place to sleep!");           
            sleeping = true;

            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Sleep_noWeapon"))
                anim.Play("Sleep_noWeapon");
        }
        
        return this;
        */
        #endregion
    }

    public Vector3 FindClosestBench()
    {
        if (!searchedForPlace)
        {          
            gos = GameObject.FindGameObjectsWithTag("Bench");
            foreach(GameObject go in gos)
            {         
                placesToSleep.Add(go);
            }
            searchedForPlace = true;
        }
        else
        {
            gos = null;
            gos = placesToSleep.ToArray();
        }

        if (gos.Length <= 0)
        {
            Debug.Log("No benches left!");
            noPlaceToSleep = true;
            sleepingPlace = null;
            return gameObject.transform.position;
        }

        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;

        foreach (GameObject go in gos)
        {
            if(go != null)
            {
                Vector3 diff = go.transform.position - position;
                float curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
        }

        placesToSleep = gos.OrderBy(x => Vector3.Distance(x.transform.position, transform.position)).ToList();

        SleepItemHandler sleepHandler = closest.GetComponent<SleepItemHandler>();
        sleepingPlace = sleepHandler;

        sleepingPlace = closest.GetComponent<SleepItemHandler>();

        Vector3 enterPosition = sleepHandler.enterPosition.transform.position;

        Debug.Log($"{aiStateManager.name} Distance: {Vector3.Distance(aiStateManager.gameObject.transform.position, enterPosition)}");
        return enterPosition;
    }

    private void GoToSleepingPlace(Vector3 position)
    {
        placeToSleepFound = true;
        aiStateManager.agent.SetDestination(position);
    }

    public void FindAPlaceToSleep()
    {
        aiStateManager.rP.AddReport("Let's call it a day.");

        //This character has a home to go to
        if (aiStateManager.character.GetCurrentHome() != null)
        {          
            sleepingPlace = aiStateManager.character.GetCurrentHome().GetComponent<SleepItemHandler>();
            //Home is reachable
            if (IsCurrentPlaceInSeight(sleepingPlace.enterPosition.position))
            {
                //REPORT
                aiStateManager.rP.AddReport("I am tired, let's go home...");

                //Getting sleepItem of home
                sleepingPlace = aiStateManager.character.GetCurrentHome().GetSleepHandlerItem();
                //Go home
                GoToSleepingPlace(sleepingPlace.enterPosition.position);
            }  
        }
        else //No Home or home is too far away
        {
            if (aiStateManager.character.GetCurrentHome() != null)
            {
                //REPORT
                aiStateManager.rP.AddReport("I want to go home and sleep, but my home is so far away...");
            }
            else
            {
                aiStateManager.rP.AddReport("If I only had a home where I could sleep at...");
            }

            //This character has benches to go to instead
            if (GameObject.FindGameObjectWithTag("Bench") != null)
            {
                //Benches are nearby
                if(IsCurrentPlaceInSeight(FindClosestBench()))
                {                   
                    //REPORT
                    aiStateManager.rP.AddReport("Maybe there is a bench nearby where I can sleep...");
                    GoToSleepingPlace(FindClosestBench());
                }
                else //No Benches nearby
                {
                    //REPORT
                    aiStateManager.rP.AddReport("Nothing is nearby where I can sleep...I am too tired to go on, good night...");

                    Debug.Log($"No place to sleep found. Trying to sleep on ground. {aiStateManager.name}");
                    noPlaceToSleep = true;
                    searchedForPlace = true;
                }
            }
            else //No Benches available
            {
                //REPORT
                aiStateManager.rP.AddReport("Nothing is nearby where I can sleep...I am too tired to go on, good night...");

                Debug.Log("No place to sleep found. Trying to sleep on ground.");
                noPlaceToSleep = true;
                searchedForPlace = true;

            }
        }
    }



    private bool IsCurrentPlaceInSeight(Vector3 sleepPos)
    {
        if(Vector3.Distance(aiStateManager.gameObject.transform.position, sleepPos) < seightDistance)
        {       
            return true;
        }
        else
        {            
            return false;
        }
    }

    private bool IsCharacterNearbySleepingPlace()
    {
        if (Vector3.Distance(aiStateManager.gameObject.transform.position, sleepingPlace.enterPosition.position) < 1.5f)
        {
            return true; //Go to sleep mode!
        }
        else
            return false; //Reset Findings?
    }

    private bool IsSleepingPlaceEmpty()
    {
        if (sleepingPlace.currentObject == null)
            return true;
        else return false;
    }

    private void ResetSettings()
    {
        searchedForPlace = false;
        placeToSleepFound = false;
        noPlaceToSleep = false;
        sleepingPlace = null;

        aiStateManager.sleepState.ResetSleepSettings();
    }


    private void SetAnimation()
    {
        if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
            aiStateManager.anim.Play("Walk");
    }

    public void SetAgentSpeed()
    {
        aiStateManager.agent.speed = aiStateManager.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplaymode()
    {
        aiStateManager.canTalk = true;
        aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
        aiStateManager.currentTextType = Dialogue.TextType.findSleep;      
    }

}
