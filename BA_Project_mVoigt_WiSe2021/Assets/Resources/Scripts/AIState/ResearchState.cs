using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResearchState : AIState
{
    public AIStateManager aiStateManager;

    public SpecialBuilding ownedLabBuild;

    public SpecialBuilding selectedBuild;
    public override AIState RunCurrentState()
    {
        SetAgentSpeed();
        SetDisplayMode();

        //There is a lab building to inhabit
        if (DoResearchLabBuildsExist())
        {
            //Get nearest lab building or own building
            selectedBuild = GetNeareastObject();

            //Return if selectedBuild is still empty
            if (selectedBuild == null)
            {
                Debug.Log("No empty lab buildings available.");
                aiStateManager.rP.AddReport("I need a research lab to do my work!");
                return aiStateManager.TryChangeState(aiStateManager.idleState);
            }

            //Character is nearby build
            if (aiStateManager.IsCharacterNearbyObject(aiStateManager.character.gameObject, selectedBuild.gameObject, 3f))
            {
                //Build is not open yet
                if (!selectedBuild.isOpen)
                {
                    selectedBuild.EnterBuilding(aiStateManager);
                }
                else //Build is open
                {
                    if (DayManager.singleton.time > GameManager.singleton.merchWorkEndTime)
                    {
                        aiStateManager.rP.AddReport("Enough researching for today!");
                        Debug.Log($"{aiStateManager.character.GetCharacterName()} is done working.");
                        ownedLabBuild.ExitShop();
                        return aiStateManager.currentState = aiStateManager.TryChangeState(aiStateManager.idleState);
                    }
                    else if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                        aiStateManager.anim.Play("Idle");
                    return this;
                }
            }
            else //Still walking to lab built
            {
                if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                    aiStateManager.anim.Play("Walk");

                aiStateManager.rP.AddReport("Time to go to work.");
                aiStateManager.agent.SetDestination(selectedBuild.enterPosition.position);
            }
        }
        else //No lab builds available
        {
            aiStateManager.rP.AddReport("I need a research lab to do my work...");
            return aiStateManager.TryChangeState(aiStateManager.idleState);
        }

        return this;
    }


    public bool DoResearchLabBuildsExist()
    {
        if (FindObjectOfType<SpecialBuilding>())
            return true;
        else
            return false;
    }

    private SpecialBuilding GetNeareastObject()
    {
        if (ownedLabBuild != null)
        {
            return ownedLabBuild;
        }

        SpecialBuilding[] allMerchs = FindObjectsOfType<SpecialBuilding>();
        SpecialBuilding closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = aiStateManager.gameObject.transform.position;

        foreach (SpecialBuilding go in allMerchs)
        {
            if (go.owner == null && go.buildingType == SpecialBuilding.BuildingType.researchLab)
            {
                Vector3 diff = go.transform.position - position;
                float curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
        }

        return closest;
    }

    public void SetAgentSpeed()
    {
        aiStateManager.agent.speed = aiStateManager.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplayMode()
    {
        if(ownedLabBuild != null)
        {
            if(ownedLabBuild.isOpen == true)
            {
                aiStateManager.canTalk = false;
                aiStateManager.currentDisplayMode = Dialogue.DisplayMode.researcher;
                aiStateManager.currentTextType = Dialogue.TextType.shop;
            }
        }
        else
        {
            aiStateManager.canTalk = true;
            aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
            aiStateManager.currentTextType = Dialogue.TextType.standard;
        }

            
    }
}

