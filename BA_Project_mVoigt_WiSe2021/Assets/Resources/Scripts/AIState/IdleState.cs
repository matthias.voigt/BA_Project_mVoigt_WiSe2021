using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IdleState : AIState
{
    public AIStateManager aiStateManager;

    public float timeTillAction = 3f;
    public float timer;

    public void SetIdleTime()
    {
        timer = timeTillAction;
    }

    public override AIState RunCurrentState()
    {
        SetAnimation();
        SetDisplaymode();

        timer -= Time.deltaTime * DayManager.singleton.speed;

        if (aiStateManager.character.GetProsperitySatisfaction() <= 0 && !aiStateManager.untargetable)
        {
            return aiStateManager.faintState;
        }


        if (aiStateManager.character.race == Character.Race.mage) return this;

        if (aiStateManager.healedState.healed) return aiStateManager.healedState;
  
        if (timer < 0)
        {
            timer = timeTillAction;

            //Character is going home
            if (aiStateManager.goHomeState.isGoingHome)
            {
                return aiStateManager.TryChangeState(aiStateManager.goHomeState);
            }
            //Character has a prisoner to escort
            else if (aiStateManager.escortPrisonerState.prisoner != null)
            {
                return aiStateManager.TryChangeState(aiStateManager.escortPrisonerState);
            }
            //Character just defeated an enemy
            else if (aiStateManager.previewsState == aiStateManager.attackState && aiStateManager.attackState.objToAttack != null)
            {          
                    return aiStateManager.TryChangeState(aiStateManager.escortPrisonerState);
            }
            else if (aiStateManager.followState.objToFollow == GameManager.singleton.playerObj)
            {
                aiStateManager.rP.AddReport("Time to go back to " + aiStateManager.followState.objToFollow.name);
                Debug.Log($"{aiStateManager.character.GetCharacterName()} continues following player.");
                return aiStateManager.followState;
            }
                //Has Character a quest to fullfill?
            else if (aiStateManager.HasCharacterQuest())
            {
                if(aiStateManager.character.GetProsperitySatisfaction() < 15)
                {
                    aiStateManager.rP.AddQuestReport("I wanted to do my quest, but I felt so tired. That's why I wanted to eat something first.");
                    return aiStateManager.TryChangeState(aiStateManager.cookState);
                }
                return aiStateManager.TryChangeState(aiStateManager.ChangeToQuestingState());
            }
            //Character is a merchant
            else if(aiStateManager.character.jobClass == TraitManager.JobClass.Merchant && aiStateManager.faction == AIStateManager.Faction.villager)
            {
                if(DayManager.singleton.GetCurrentTimeFloat() > GameManager.singleton.merchWorkBeginTime &&
                    DayManager.singleton.GetCurrentTimeFloat() < GameManager.singleton.merchWorkEndTime)
                {
                    if (aiStateManager.merchState.DoMerchantBuildsExist())
                    {
                        aiStateManager.rP.AddReport("Let's go to work!");
                        Debug.Log($"{aiStateManager.character.GetCharacterName()} is going to work.");
                        return aiStateManager.TryChangeState(aiStateManager.merchState);
                    }
                    else
                    {
                        aiStateManager.rP.AddReport("I can't sell my items without a market...");
                        return WanderAround();
                    }
                }
                else
                {
                    return WanderAround();
                }
            }
            //Character is a guard
            else if (aiStateManager.character.jobClass == TraitManager.JobClass.Guard)
            {
                if (DayManager.singleton.GetCurrentTimeFloat() > GameManager.singleton.guardWorkBeginTime &&
                    DayManager.singleton.GetCurrentTimeFloat() < GameManager.singleton.guardWorkEndTime)
                {
                    if (aiStateManager.watchTowerState.DoTowersExist())
                    {
                        Debug.Log($"{aiStateManager.character.GetCharacterName()} is going to work.");
                        return aiStateManager.TryChangeState(aiStateManager.watchTowerState);
                    }
                    else
                    {
                        aiStateManager.rP.AddReport("If I had a watchtower, I could do my job even better!");
                        return WanderAround();
                    }
                }
                else
                {
                    return WanderAround();
                }
            }
            else if (aiStateManager.character.jobClass == TraitManager.JobClass.Researcher && aiStateManager.faction == AIStateManager.Faction.villager)
            {
                if (DayManager.singleton.GetCurrentTimeFloat() > GameManager.singleton.merchWorkBeginTime &&
                    DayManager.singleton.GetCurrentTimeFloat() < GameManager.singleton.merchWorkEndTime)
                {
                    if (aiStateManager.researchState.DoResearchLabBuildsExist())
                    {
                        Debug.Log($"{aiStateManager.character.GetCharacterName()} is going to work.");
                        return aiStateManager.TryChangeState(aiStateManager.researchState);
                    }
                    else
                    {
                        aiStateManager.rP.AddReport("I cannot do my research without a research lab!");
                        return WanderAround();
                    }
                }
                else
                {
                    return WanderAround();
                }
            }
            else if(aiStateManager.character.jobClass == TraitManager.JobClass.Adventurer)
            {
                //It is currently morning or midday
                if(DayManager.singleton.currentTimeState == DayManager.CurrentTimeState.morning ||
                    DayManager.singleton.currentTimeState == DayManager.CurrentTimeState.midday)
                {
                    //There are unassigned quests
                    if(QuestManager.singleton.currentQuestsUnassigned.Count > 0)
                    {
                        return aiStateManager.TryChangeState(aiStateManager.qBoardState);
                    }
                    else
                    {
                        aiStateManager.rP.AddReport("I wish there were some quests I could do!");
                        return WanderAround();
                    }
                }
                else
                {
                    return WanderAround();
                }
            }
            else //Go wander around if nothing else is do to.
            {
                return WanderAround();
            }
        }

        return this;        
    }

    private void SetAnimation()
    {
        if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            aiStateManager.anim.Play("Idle");
    }   

    private AIState WanderAround()
    {
        aiStateManager.agent.isStopped = false;

        //Return from here as this is not a villager
        if (aiStateManager.faction != AIStateManager.Faction.villager)
        {
            return aiStateManager.TryChangeState(aiStateManager.wanderState);
        }
        else
            return aiStateManager.TryChangeState(GoToRandomState());
    }


    private AIState GoToRandomState()
    {
        int r = Random.Range(0, 4);

        if (aiStateManager.faction == AIStateManager.Faction.enemy)
            r = 0;
        else if(aiStateManager.faction == AIStateManager.Faction.neutral)
        {
            r = Random.Range(0, 3);
        }

        //Wander around
        if(r == 0)
        {
            return aiStateManager.wanderState;
        }
        //Go to a shop
        else if(r == 1)
        {
            return aiStateManager.tradeState;
        }
        //Go cooking
        else if(r == 2)
        {
            return aiStateManager.cookState;
        }
        else if(r == 3)
        {
            if (aiStateManager.character.jobClass == TraitManager.JobClass.Hero ||
                aiStateManager.character.jobClass == TraitManager.JobClass.Villager)
                return aiStateManager.wanderState;
            else
                return aiStateManager.qBoardState;
        }
        //Wander around at default
        else
            Debug.Log("Random Action: wander at default");
            return aiStateManager.wanderState;
    }


    private AIStateManager GetNearestFainted()
    {
        List<AIStateManager> faintedNPC = new List<AIStateManager>();
        AIStateManager closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = aiStateManager.gameObject.transform.position;

        if (aiStateManager.faction == AIStateManager.Faction.villager)
        {
            if(aiStateManager.character.jobClass == TraitManager.JobClass.Guard)
            {
                foreach(AIStateManager ai in GameManager.singleton.enemies)
                {
                    if(ai.currentState == ai.faintState)
                    {
                        if(Vector3.Distance(aiStateManager.gameObject.transform.position, ai.transform.position) < 10f)
                        {
                            faintedNPC.Add(ai);
                        }
                    }
                }
            }
        }

        foreach(AIStateManager nearestEnemy in faintedNPC)
        {
            Vector3 diff = nearestEnemy.transform.position - position;
            float curDistance = diff.sqrMagnitude;

            if (curDistance < distance)
            {
                closest = nearestEnemy;
                distance = curDistance;
            }
        }

        return closest;
    }


    public void SetDisplaymode()
    {
        aiStateManager.canTalk = true;

        switch (aiStateManager.faction)
        {
            case AIStateManager.Faction.villager:
                if(aiStateManager.character.jobClass == TraitManager.JobClass.Hero)
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
                    aiStateManager.currentTextType = Dialogue.TextType.standard;
                }
                else
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.character;
                    aiStateManager.currentTextType = Dialogue.TextType.standard;
                }
                break;
            case AIStateManager.Faction.enemy:
                aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
                aiStateManager.currentTextType = Dialogue.TextType.standard;
                break;
            case AIStateManager.Faction.neutral:
                if(aiStateManager.character.jobClass == TraitManager.JobClass.Adventurer)
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.adventurer;
                    aiStateManager.currentTextType = Dialogue.TextType.standard;
                }
                else if(aiStateManager.character.jobClass == TraitManager.JobClass.Merchant)
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.merchant;
                    aiStateManager.currentTextType = Dialogue.TextType.shop;
                }
                else if(aiStateManager.character.race == Character.Race.skeleton)
                {
                    aiStateManager.currentDisplayMode = Dialogue.DisplayMode.ressurect;
                    aiStateManager.currentTextType = Dialogue.TextType.standard;
                }
                break;
        }
    }
}
