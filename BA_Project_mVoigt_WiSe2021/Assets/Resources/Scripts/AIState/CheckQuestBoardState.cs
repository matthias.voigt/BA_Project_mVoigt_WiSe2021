using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckQuestBoardState : AIState
{
    public AIStateManager aiStateManager;
    public GameObject questBoard;
    public override AIState RunCurrentState()
    {
        SetAgentSpeed();
        SetDisplayMode();

        if (aiStateManager.hasQuest)
        {
            //REPORT
            aiStateManager.rP.AddReport($"Checking the quest board? Why should I? I already have a quest.");
            aiStateManager.rP.AddQuestReport("When you told me to check the quest board, I was a bit confused...You already sent me out to do one, right? So I continued my quest...");
            return aiStateManager.TryChangeState(aiStateManager.idleState);
        }

        if(questBoard == null)
        {
            questBoard = GameObject.FindGameObjectWithTag("QuestBoard");
        }
      
        if(aiStateManager.IsCharacterNearbyObject(aiStateManager.character.gameObject, questBoard, 3f))
        {
            if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                aiStateManager.anim.Play("Idle");
            return TryGetRandomQuest();
        }
        else
        {
            aiStateManager.agent.SetDestination(questBoard.transform.position);
            if (!aiStateManager.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                aiStateManager.anim.Play("Walk");
        }

        return this;
    }

    private AIState TryGetRandomQuest()
    {
        if(QuestManager.singleton.currentQuestsUnassigned.Count <= 0)
        {
            //REPORT
            aiStateManager.rP.AddReport($"I wanted to do some quests, but there are no quests available.");

            Debug.Log($"{aiStateManager.character.GetCharacterName()} did not find any quests on the quest board.");
            return aiStateManager.TryChangeState(aiStateManager.idleState);
        }
        else
        {
            //REPORT
            aiStateManager.rP.AddReport($"Let's see what quests I could do today.");

            List<Quest> qs = new List<Quest>(QuestManager.singleton.currentQuestsUnassigned);
            foreach(Quest q in QuestManager.singleton.currentQuestsUnassigned)
            {
                //Removing all quests with too low prestige
                if(q.questPrestige < aiStateManager.character.GetPrestige())
                {
                    qs.Remove(q);
                }
            }
            //Return if no Quests available
            if(qs.Count <= 0)
            {
                //REPORT
                aiStateManager.rP.AddReport($"All these quests are under my prestige. Not worth my time!");

                Debug.Log($"{aiStateManager.character.GetCharacterName()} did not find any quests on the quest board.");
                return aiStateManager.TryChangeState(aiStateManager.idleState);
            }
            else
            {
                if (aiStateManager.IsActionSuccessful())
                {
                    int questID = Random.Range(0, qs.Count);
                    aiStateManager.currentQuest = qs[questID];

                    List<Quest> questList = new List<Quest>(QuestManager.singleton.currentQuestsUnassigned);

                    foreach (Quest _q in questList)
                    {
                        if (qs[questID] == _q)
                        {
                            Debug.Log("This is the quest!");
                            aiStateManager.currentQuest = _q;

                            _q.QuestHolder = aiStateManager;
                            QuestManager.singleton.currentQuestsUnassigned.Remove(aiStateManager.currentQuest);
                            QuestManager.singleton.currentQuestsAssigned.Add(aiStateManager.currentQuest);
                            aiStateManager.hasQuest = true;

                            //REPORT
                            aiStateManager.rP.AddReport($"This sounds interesting: {aiStateManager.currentQuest.questName}. Let's do this one!");
                            aiStateManager.rP.AddQuestReport($"I am here to report my mission: '{aiStateManager.currentQuest.questName}'!");
                        }
                    }
                }
                else
                {
                    aiStateManager.rP.AddReport("I feel too tired to do quests...");
                }

                
            }
        }

        return aiStateManager.TryChangeState(aiStateManager.idleState);
    }

    public void SetAgentSpeed()
    {
        aiStateManager.agent.speed = aiStateManager.walkingSpeed * Time.deltaTime * DayManager.singleton.speed;
    }

    public void SetDisplayMode()
    {
        aiStateManager.canTalk = true;
        if (aiStateManager.character.jobClass == TraitManager.JobClass.Hero)
        {
            aiStateManager.currentDisplayMode = Dialogue.DisplayMode.none;
            aiStateManager.currentTextType = Dialogue.TextType.standard;
        }
        else
        {
            aiStateManager.currentDisplayMode = Dialogue.DisplayMode.character;
            aiStateManager.currentTextType = Dialogue.TextType.standard;
        }
    }
}
