using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkingState : AIState
{
    public Animator anim;
    public AIStateManager ai;

    public override AIState RunCurrentState()
    { 
        if(CutsceneManager.singleton.currentState != CutsceneManager.CutsceneState.inDialogue)
        {
            ai.previewsState = ai.idleState;
            return ai.TryChangeState(ai.idleState);
        }

        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            anim.Play("Idle");

        return this;
    }
}
