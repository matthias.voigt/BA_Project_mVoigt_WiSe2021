using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkState : AIState
{
    public Animator anim;

    public override AIState RunCurrentState()
    {
        if(!anim.GetCurrentAnimatorStateInfo(0).IsName("NormalWalk_noWeapon"))
            anim.Play("NormalWalk_noWeapon");

        return this;
    }
}
