using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : AIState
{
    public AIStateManager aiState;
    public AIStateManager objToAttack;

    public Vector3 initPos;
    public Vector3 attackInitPos;
    bool targetEscaped = false;
    float distanceSeight;


    public override AIState RunCurrentState()
    {
        aiState.canTalk = false;

        //Return to Idle if no obj to attack is there
        if (objToAttack == null) return aiState.TryChangeState(aiState.idleState);

        //Reduce enemy satisfaction
        float authority = (float)aiState.character.GetAuthority() / 25;
        objToAttack.character.ReduceSatisfactionBy(authority);


        if (aiState.hasQuest)
        {
            if (aiState.currentQuest.questFinished)
            {
                aiState.rP.AddQuestReport("I was already done with my quest, but then I got attacked by an enemy!");
            }
            else
            {
                aiState.rP.AddQuestReport("I wanted to do my quest, but then I got attacked by an enemy!");
            }
        }


        //Check if own satisfaction is below zero
        if (aiState.character.GetProsperitySatisfaction() <= 0)
        {
            aiState.NPCUntargetable(true);
            Debug.Log($"{aiState.character.GetCharacterName()} has no more satisfaction left for figthing.");
            
            //Thought Reports
            aiState.rP.AddReport("I cannot fight any longer...urgs...");
            //Quest Reports
            if (aiState.hasQuest)
            {
                aiState.rP.AddQuestReport("I fought for my life, but I could not do it...I fainted and everything went black.");
            }
            return aiState.TryChangeState(aiState.faintState);
        }
        //Check if enemy satisfaction is below zero
        else if (objToAttack.character.GetProsperitySatisfaction() <= 0)
        {         
            //Thought Reports
            aiState.rP.AddReport("Hah! I fought and won! How dare you to challenge me!");

            //Quest Reports
            if (aiState.hasQuest)
            {
                if (objToAttack.character.traits.Contains(TraitManager.Traits.Pacifist))
                {
                    aiState.rP.AddQuestReport("The enemy tried to escape, but I could catch up and won the fight!");
                }
                else if(aiState.character.traits.Contains(TraitManager.Traits.Pacifist))
                {
                    aiState.rP.AddQuestReport("I ran for my life! Somehow I could escape.");
                }
                else
                {
                    aiState.rP.AddQuestReport("I fought the enemy in a dangerous battle. With all my power, I could defeat the villain and won!");
                }                
            }

            if (aiState.escortPrisonerState.prisoner == null)
            {
                aiState.escortPrisonerState.prisoner = objToAttack;
                objToAttack.followState.objToFollow = aiState.gameObject;
            }
                

            objToAttack = null;
            return aiState.TryChangeState(aiState.idleState);
        }

        if (targetEscaped)
        {
            //Obj to Attack ran away;
            aiState.rP.AddReport("I couldn't chase my target...It was too fast...");
            if (aiState.hasQuest)
                aiState.rP.AddQuestReport("I tried to attack my target, but it was too fast and escaped...");

            aiState.agent.SetDestination(initPos);

            if (Vector3.Distance(aiState.gameObject.transform.position, initPos) < 5f)
            {
                targetEscaped = false;
                objToAttack = null;
                return aiState.TryChangeState(aiState.idleState);
            }

            return this;
        }

        if (aiState.character.jobClass == TraitManager.JobClass.Guard)
        {
            distanceSeight = GameManager.singleton.seightBuff + GameManager.singleton.maxDistanceFight;
        }
        else
        {
            distanceSeight = GameManager.singleton.maxDistanceFight;
        }

        //Obj to attack is too far away
        if (Vector3.Distance(attackInitPos, objToAttack.gameObject.transform.position) > GameManager.singleton.maxDistanceFight)
        {
            targetEscaped = true;
        }

        //Character is nearby objToAttack
        if(aiState.IsCharacterNearbyObject(aiState.gameObject, objToAttack.gameObject, 2f))
        {
            aiState.agent.speed = 1f;

            if(objToAttack.currentState != objToAttack.attackState)
            {
                if(objToAttack.character.traits.Contains(TraitManager.Traits.Pacifist))
                {
                    if (objToAttack.currentState != objToAttack.fleeState)
                    {
                        objToAttack.fleeState.fleeFromObj = aiState;

                        if (objToAttack.currentState != objToAttack.faintState)
                            objToAttack.currentState = objToAttack.TryChangeState(objToAttack.fleeState);
                    }              
                }
                else
                {
                    objToAttack.attackState.objToAttack = aiState;

                    if(objToAttack.currentState != objToAttack.faintState)
                        objToAttack.currentState = objToAttack.TryChangeState(objToAttack.attackState);
                }
            }

            aiState.gameObject.transform.LookAt(objToAttack.gameObject.transform);
            if (!aiState.anim.GetCurrentAnimatorStateInfo(0).IsName("Attack01") && !aiState.anim.GetCurrentAnimatorStateInfo(0).IsName("Attack02")
                && !aiState.anim.GetCurrentAnimatorStateInfo(0).IsName("AttackIdle"))
                aiState.anim.Play("Attack01");
        }
        else //Character is going to objToAttack
        {
            aiState.agent.SetDestination(objToAttack.transform.position);

            if (!aiState.anim.GetCurrentAnimatorStateInfo(0).IsName("Sprint"))
                aiState.anim.Play("Sprint");

            aiState.agent.speed = 4;
        }

        return this;
    }

}
