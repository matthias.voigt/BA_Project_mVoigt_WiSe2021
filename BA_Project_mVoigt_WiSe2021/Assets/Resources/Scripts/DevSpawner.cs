using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevSpawner : MonoBehaviour
{
    public List<GameObject> randomItem = new List<GameObject>();

    public bool sendStatus = false;

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.D))
        {
            if (!sendStatus)
            {
                sendStatus = true;
                StatusManager.singleton.InsertNewStatus("Dev Mode activated.");

            }
            GameManager.singleton.devMode = true;
        }

        if (Input.GetKeyUp(KeyCode.F1) && GameManager.singleton.devMode)
        {
            Vector3 pos = new Vector3(transform.position.x, transform.position.y + 3f, transform.position.z);
            
            GameObject newObj = Instantiate(randomItem[Random.Range(0,randomItem.Count)], pos, Quaternion.identity);
        }
        else if(Input.GetKeyUp(KeyCode.F2) && GameManager.singleton.devMode)
        {
            if(GameManager.singleton.villagers.Count < 15)
            {
                RandomEventManager.singleton.SetUpRandomEvent(RandomEventManager.RandomEvent.villager);
            }
        }
        else if(Input.GetKeyUp(KeyCode.F3) && GameManager.singleton.devMode)
        {
            if (GameManager.singleton.enemies.Count < 15)
            {
                RandomEventManager.singleton.SetUpRandomEvent(RandomEventManager.RandomEvent.travelingEnemy);
            }
        }
        else if (Input.GetKeyUp(KeyCode.F4))
        {
            RandomEventManager.singleton.SetUpRandomEvent(RandomEventManager.RandomEvent.getmoney);
        }
        else if (Input.GetKeyUp(KeyCode.F5))
        {
            StatusManager.singleton.InsertNewStatus("Defeated all enemies!");
            foreach(AIStateManager ai in GameManager.singleton.enemies)
            {
                ai.character.satisfaction = 0;
                ai.currentState = ai.TryChangeState(ai.faintState);
            }
        }
        else if (Input.GetKeyUp(KeyCode.F6))
        {
            StatusManager.singleton.InsertNewStatus("Researched all buildings.");
            
            foreach(Construction c in SaveManager.singleton.constructions)
            {
                c.UnlockBuilding(true);
            }
        }
    }
}
