using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullHandler : MonoBehaviour
{
    public string cName = "";
    public Character.RaceType formerRaceType;
    public List<TraitManager.Traits> traits = new List<TraitManager.Traits>();
    public AIStateManager.Faction faction = AIStateManager.Faction.neutral;
    public TraitManager.JobClass job = TraitManager.JobClass.Adventurer;

    public void SetCharacterData(AIStateManager ai)
    {
        cName = ai.character.GetCharacterName();
        traits = new List<TraitManager.Traits>(ai.character.traits);
        faction = ai.faction;
        job = ai.character.jobClass;
        formerRaceType = ai.character.raceType;

    }
}
