using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prison : MonoBehaviour
{
    public AIStateManager currentObj = null;
    public Transform EnterZonePrisoner;
    public Transform EnterZoneEscort;
    public Transform IndoorPos;

    public GameObject cage;
    private Animator anim;
    public GameObject prisonCam;

    public AIStateManager.Faction faction = AIStateManager.Faction.villager;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void LowerCage()
    {
        Debug.Log("Lowering cage.");
        anim.Play("Prison_PullLevel");
    }
    public void CloseCage()
    {
        Debug.Log("Closing cage.");
        anim.Play("Prison_PullLevelUp");
    }

    public void EnterCage(AIStateManager ai)
    {
        ai.NPCUntargetable(true);
        Debug.Log($"{ai.character.GetCharacterName()} got imprisoned.");
        ai.currentState = ai.TryChangeState(ai.imprisonedState);
        currentObj = ai;
        ai.character.AddSatisfaction(5);

        ai.currentDisplayMode = Dialogue.DisplayMode.prisoner;
        ai.currentTextType = Dialogue.TextType.standard;
        ai.imprisonedState.prison = this;
        //ai.agent.enabled = false;
        ai.gameObject.transform.position = IndoorPos.transform.position;
        ai.gameObject.transform.rotation = IndoorPos.transform.rotation;
        ai.gameObject.transform.SetParent(cage.transform);

        //REPORT
        ai.rP.AddReport($"I am imprisoned...What should I do?");

        if(ai.hasQuest)
            ai.rP.AddQuestReport("At the end I got imprisoned...");

    }

    public void InteractWithPrison()
    {
        GameManager.singleton.TryChangePlayState(GameManager.PlayState.inMenu);
        //prisonCam.SetActive(true);
        currentObj.previewsState = currentObj.imprisonedState;
        currentObj.imprisonedState.SetDialogueTypes();
        DialogueManager.singleton.TryChangeDialogueState(DialogueManager.DialogueState.inDialogue, currentObj, currentObj.imprisonedState);
    }

    public bool IsPrisonEmpty()
    {
        if (currentObj == null) return true;
        else return false;
    }
    public bool IsPrisonerSleeping()
    {
        if (currentObj.IsSleepingTime())
        {
            Debug.Log("Prisoner is currently sleeping.");
            return true;
        }
        else
            return false;
    }
    public bool EnemiesNearby()
    {
        if (faction == AIStateManager.Faction.villager) return false;

        else if (Vector3.Distance(gameObject.transform.position, GetNeareastEnemy()) < 25f)
        {
            currentObj.rP.AddReport("Someone wanted to talk to me in prison, but I can't. There are enemies nearby...");
            Debug.Log("Cannot interact with prison, enemies nearby.");
            return true;
        }
        else
            return false;
    }

    public void StopInteractingWithPrison()
    {
        prisonCam.SetActive(false);
    }

    public void ExitPrison()
    {
        currentObj.NPCUntargetable(false);
        StartCoroutine(ExitPrison_Escape());
        prisonCam.SetActive(false);
        currentObj.transform.SetParent(null);       
    }

    public void EscapePrison()
    {
        currentObj.rP.AddReport("I did it! I found a way to break out of prison! Let's escape!");
        if (currentObj.hasQuest) currentObj.rP.AddQuestReport("With all my last strength remaining, I tried to escape from prison!");
        StatusManager.singleton.InsertNewStatus($"{currentObj.character.GetCharacterName()} is trying to escape from prison!");
        currentObj.NPCUntargetable(false);
        StartCoroutine(ExitPrison_Escape());
    }

    IEnumerator ExitPrison_Escape()
    {
        Debug.Log($"{currentObj.character.GetCharacterName()} is escaping prison.");
        LowerCage();
        yield return new WaitForSeconds(5f);

        currentObj.agent.enabled = true;
        currentObj.agent.Warp(EnterZonePrisoner.position);

        currentObj.currentState = currentObj.TryChangeState(currentObj.goHomeState);
        
        currentObj.transform.SetParent(null);
        currentObj = null;
        
        CloseCage();
    }


    private Vector3 GetNeareastEnemy()
    {
        AIStateManager[] allEnemies = FindObjectsOfType<AIStateManager>();
        AIStateManager closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = gameObject.transform.position;

        foreach (AIStateManager go in allEnemies)
        {
            if (go.faction == AIStateManager.Faction.enemy && go != currentObj)
            {
                Vector3 diff = go.transform.position - position;
                float curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
        }

        if(closest == null)
        {        
            return new Vector3(1000, 1000, 1000);
        }
        return closest.gameObject.transform.position;
    }

}
