using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactionIcon : MonoBehaviour
{
    public SpriteRenderer sr;
    public GameObject model;
    private Camera cam;
    private AIStateManager ai;

    private void Start()
    {
        ai = GetComponentInParent<AIStateManager>();
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    public void SetIcon(AIStateManager.Faction f)
    {
        sr.sprite = GameManager.singleton.factionIcons[(int)f];
    }

    private void Update()
    {
        if (GameManager.singleton.currentPlaystate != GameManager.PlayState.inOverworld) return;

        
        if (Input.GetKey(KeyCode.Tab))
        {
            SetIcon(ai.faction);
            sr.gameObject.SetActive(true);
            sr.gameObject.transform.LookAt(cam.transform);
        }
        else
        {
            sr.gameObject.SetActive(false);
        }
    }
}
