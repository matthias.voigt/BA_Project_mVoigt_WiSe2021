using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newEventDesc", menuName = "New Event Description")]
public class EventDescription : ScriptableObject
{
    public int eventID;
    public string description;
    public bool unlockedFromStart;
    public string desc;
}
