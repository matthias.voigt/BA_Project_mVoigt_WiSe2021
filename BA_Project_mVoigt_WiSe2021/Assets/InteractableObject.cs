using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    public Interaction interaction = Interaction.undefined;

    private Chest chest;
    private ObjectHandler item;
    private SpecialBuilding specialBuilding;
    private Prison prison;
    private BuildingHandler buildingHandler;
    private BossCutsceneHandler bossCutsceneHandler;
    private AIStateManager ai;
    private SleepItemHandler sleepItemHandler;
    private TownBell townbell;


    [SerializeField] private Dialogue buildingEmpty;
    [SerializeField] private Dialogue buildingClosed;
    [SerializeField] private Dialogue enemiesNearby;
    public enum Interaction
    {
        undefined,
        chest,
        item,
        questBoard,
        shop,
        prison,
        house,
        evilMage,
        npc,
        bench,
        townbell,
        outworldHouse
    }

    private void Start()
    {
        Debug.Log($"Setting {gameObject.name} as interactable target.");
        if (interaction == Interaction.npc) gameObject.layer = 10;
        else gameObject.layer = 14;
        
        GetScript();
    }
    public void InteractWithObject(Animator anim)
    {
        Debug.Log($"Interacting with {gameObject.name}");
        CheckInteractionType(anim);

        if (interaction == Interaction.undefined) return;
    }


    private void CheckInteractionType(Animator anim)
    {
        switch (interaction)
        {
                //CHEST
            case Interaction.chest:
                chest.OpenChest();
                anim.Play("PickUp_noWeapon");
                break;
                //ITEM
            case Interaction.item:
                anim.Play("PickUp_noWeapon");
                AudioManager.singleton.PlayPickUpSFX();
                GameManager.singleton.PlayerInventory.AddItemToInventory(item);
                gameObject.SetActive(false);
                break;
                //QUEST BOARD
            case Interaction.questBoard:
                QuestManager.singleton.OpenQuestBoard();
                break;
                //RESEARCH LAB//MARKET
            case Interaction.shop:
                anim.Play("Idle");
                if (specialBuilding.isOpen && specialBuilding.owner != null)
                    specialBuilding.InteractWithBuilding();
                else
                {
                    if(specialBuilding.owner == null) DialogueManager.singleton.DisplayDescription(buildingEmpty);
                    else if (!specialBuilding.isOpen) DialogueManager.singleton.DisplayDescription(buildingClosed);
                }                  
                break;
                //PRISON
            case Interaction.prison:
                
                if(prison.IsPrisonEmpty())
                {
                    DialogueManager.singleton.DisplayDescription(buildingEmpty);
                }
                else if(prison.EnemiesNearby())
                {
                    DialogueManager.singleton.DisplayDescription(enemiesNearby);
                }
                else if (prison.IsPrisonerSleeping())
                {
                    DialogueManager.singleton.DisplayDescription(buildingClosed);
                }
                else
                    prison.InteractWithPrison();
                break;
            case Interaction.house:
                BuildingInfoManager.singleton.TryChangeBuildingInfoState(BuildingInfoManager.CurrentBuildingInfoState.main, buildingHandler);
                break;
            case Interaction.evilMage:
                bossCutsceneHandler.Interact();
                break;
            case Interaction.npc:
                if (ai.canTalk)
                {
                    if (ai.currentState == ai.faintState)
                        DialogueManager.singleton.TryChangeDialogueState(DialogueManager.DialogueState.inDialogue, ai, ai.faintState);
                    else
                    {
                        ai.gameObject.transform.LookAt(anim.gameObject.transform);
                        DialogueManager.singleton.TryChangeDialogueState(DialogueManager.DialogueState.inDialogue, ai, ai.talkingState);
                    }
                }
                break;
            case Interaction.bench:
                if(sleepItemHandler.currentObject != null) DialogueManager.singleton.DisplayDescription(buildingClosed);
                else sleepItemHandler.Interact(GameManager.singleton.playerObj, anim);
                break;
            case Interaction.townbell:
                townbell.Interact();
                break;
            case Interaction.outworldHouse:
                DialogueManager.singleton.DisplayDescription(buildingEmpty);
                break;

        }
    }



    private void GetScript()
        {
            switch (interaction)
            {
                case Interaction.chest:
                    chest = GetComponent<Chest>();
                    break;
                case Interaction.item:
                    item = GetComponent<ObjectHandler>();
                    break;
                case Interaction.shop:
                    specialBuilding = GetComponent<SpecialBuilding>();
                    break;
                case Interaction.prison:
                    prison = GetComponent<Prison>();
                    break;
                case Interaction.house:
                    buildingHandler = transform.parent.gameObject.GetComponent<BuildingHandler>();
                    break;
                case Interaction.evilMage:
                    bossCutsceneHandler = GetComponent<BossCutsceneHandler>();
                    break;
                case Interaction.questBoard:
                    break;
                case Interaction.npc:
                    ai = GetComponent<AIStateManager>();
                    break;
            case Interaction.bench:
                sleepItemHandler = GetComponent<SleepItemHandler>();
                break;
            case Interaction.townbell:
                townbell = GetComponent<TownBell>();
                break;
                default:
                    Debug.LogError($"Something went wrong with {gameObject.name}");
                    break;

            }
        }
}
