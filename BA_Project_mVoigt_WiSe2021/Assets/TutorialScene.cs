using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScene : MonoBehaviour
{
    public CutsceneDialogue tutorial;
    public BoxCollider bc;

    void Start()
    {
        int l = PlayerPrefs.GetInt("load", 0);

        if (l == 1 || CutsceneManager.singleton.skipIntro)
        {
            Debug.Log("Destroying tutorial scene.");
            Destroy(gameObject);
        }
        else if(l == 2)
        {
            for(int i = 0; i < 3; i++)
            {
                RandomEventManager.singleton.SetUpRandomEvent(RandomEventManager.RandomEvent.villager);
            }
            StatusManager.singleton.InsertNewStatus("Press 1 to open Inventory.");
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (CutsceneManager.singleton.skipIntro) return;

        if (other.gameObject.tag == "Player")
        {
            bc.enabled = false;
            CutsceneManager.singleton.StartEvent(tutorial);
        }
    }
}
