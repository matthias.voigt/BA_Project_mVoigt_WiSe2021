using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CharacterCreationManager : MonoBehaviour
{
    #region REFERENCES

    [Header("Body Parts")]
    [SerializeField] private List<GameObject> Belts = new List<GameObject>();
    [SerializeField] private List<GameObject> Cloths = new List<GameObject>();
    [SerializeField] private List<GameObject> Crowns = new List<GameObject>();
    [SerializeField] private List<GameObject> Faces = new List<GameObject>();
    [SerializeField] private List<GameObject> Gloves = new List<GameObject>();
    [SerializeField] private List<GameObject> Hairs = new List<GameObject>();
    [SerializeField] private List<GameObject> Helmets = new List<GameObject>();
    [SerializeField] private List<GameObject> Shoes = new List<GameObject>();
    [SerializeField] private List<GameObject> ShoulderPads = new List<GameObject>();
    [SerializeField] private List<GameObject> HairsHalf = new List<GameObject>();

    [Header("On Click Sound")]
    [SerializeField] private AudioSource soundOnClick = null;

    #endregion

    private PostProcessManipulator ppM;

    private void Start()
    {
        //LoadCharacterAppearance();
        ppM = GetComponent<PostProcessManipulator>();
    }
    bool loading = false;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !loading)
        {
            loading = true;
            ppM.LoadLevel(0);
        }
    }

    #region HELMET CHANGE

    private int helmetID = -1;
    [Header("Text References")]
    [SerializeField] TMP_Text helmetText = null;
    public void ChangeHelmet()
    {
        soundOnClick.Play();

        helmetID++;
        DeactivateAllHelmets();
        DeactivateAllHair();

        if (helmetID >= Helmets.Count)
        {
            helmetID = -1;
            helmetText.text = "Helmet: none";

            Hairs[hairID].SetActive(true);
        }
        else
        {
            HairsHalf[hairID].SetActive(true);
            Helmets[helmetID].SetActive(true);
            helmetText.text = "Helmet: " + (helmetID + 1).ToString();
        }

        if (helmetID >= 7)
        {
            DeactivateAllFaces();
            DeactivateAllHair();
        }
        else
        {
            Faces[faceID].SetActive(true);
        }
    }

    private void DeactivateAllHelmets()
    {
        foreach (GameObject helmet in Helmets)
        {
            helmet.SetActive(false);
        }
    }
    #endregion

    #region FACE CHANGE
    private int faceID = 0;
    [SerializeField] TMP_Text faceText = null;
    public void ChangeFace()
    {
        soundOnClick.Play();

        faceID++;
        DeactivateAllFaces();

        if (faceID >= Faces.Count)
        {
            faceID = 0;
        }

        if (helmetID < 7)
        {
            Faces[faceID].SetActive(true);
        }

        faceText.text = "Face: " + (faceID + 1).ToString();
    }
    private void DeactivateAllFaces()
    {
        foreach (GameObject face in Faces)
        {
            face.SetActive(false);
        }
    }
    #endregion

    #region HAIR CHANGE

    private int hairID = 0;
    [SerializeField] TMP_Text hairText = null;
    public void ChangeHair()
    {
        soundOnClick.Play();

        hairID++;
        DeactivateAllHair();

        if (hairID >= Hairs.Count)
        {
            hairID = 0;
        }

        if (helmetID == -1)
        {
            Hairs[hairID].SetActive(true);
        }
        else if (helmetID < 7)
        {
            HairsHalf[hairID].SetActive(true);
        }

        hairText.text = "Hair: " + (hairID + 1).ToString();
    }
    private void DeactivateAllHair()
    {
        foreach (GameObject hair in Hairs)
        {
            hair.SetActive(false);
        }

        foreach (GameObject hair in HairsHalf)
        {
            hair.SetActive(false);
        }
    }
    #endregion

    #region CLOTH CHANGE

    private int clothID = 0;
    [SerializeField] TMP_Text clothText = null;
    public void ChangeCloth()
    {
        soundOnClick.Play();

        clothID++;
        DeactivateAllCloths();

        if (clothID >= Cloths.Count)
        {
            clothID = 0;
        }

        Cloths[clothID].SetActive(true);
        clothText.text = "Cloth: " + (clothID + 1).ToString();
    }

    private void DeactivateAllCloths()
    {
        foreach (GameObject cloth in Cloths)
        {
            cloth.SetActive(false);
        }
    }
    #endregion

    #region SHOE CHANGE

    private int shoeID = 0;
    [SerializeField] TMP_Text shoeText = null;
    public void ChangeShoe()
    {
        soundOnClick.Play();

        shoeID++;
        DeactivateAllShoes();

        if (shoeID >= Shoes.Count)
        {
            shoeID = 0;
        }

        Shoes[shoeID].SetActive(true);
        shoeText.text = "Shoes: " + (shoeID + 1).ToString();
    }

    private void DeactivateAllShoes()
    {
        foreach (GameObject shoe in Shoes)
        {
            shoe.SetActive(false);
        }
    }
    #endregion

    #region CHANGE GLOVE
    private int gloveID = 0;
    [SerializeField] TMP_Text gloveText = null;
    public void ChangeGloves()
    {
        soundOnClick.Play();

        gloveID++;
        DeactivateAllGloves();

        if (gloveID >= Gloves.Count)
        {
            gloveID = 0;
        }

        Gloves[gloveID].SetActive(true);
        gloveText.text = "Gloves: " + (gloveID + 1).ToString();
    }

    private void DeactivateAllGloves()
    {
        foreach (GameObject glove in Gloves)
        {
            glove.SetActive(false);
        }
    }
    #endregion

    #region CHANGE BELT
    private int beltID = -1;
    [SerializeField] TMP_Text beltText = null;
    public void ChangeBelt()
    {
        soundOnClick.Play();

        beltID++;
        DeactivateAllBelts();

        if (beltID >= Belts.Count)
        {
            beltID = -1;
            beltText.text = "Belt: none";
        }
        else
        {
            Belts[beltID].SetActive(true);
            beltText.text = "Belt: " + (beltID + 1).ToString();
        }
    }

    private void DeactivateAllBelts()
    {
        foreach (GameObject belt in Belts)
        {
            belt.SetActive(false);
        }
    }
    #endregion

    #region CHANGE SHOULDERPAD
    private int shoulderID = -1;
    [SerializeField] TMP_Text shoulderText = null;
    public void ChangeShoulderPad()
    {
        soundOnClick.Play();

        shoulderID++;
        DeactivateAllShoulderPads();

        if (shoulderID >= ShoulderPads.Count)
        {
            shoulderID = -1;
            shoulderText.text = "Shoulder: none";
        }
        else
        {
            ShoulderPads[shoulderID].SetActive(true);
            shoulderText.text = "Shoulders: " + (shoulderID + 1).ToString();
        }
    }

    private void DeactivateAllShoulderPads()
    {
        foreach (GameObject shoulder in ShoulderPads)
        {
            shoulder.SetActive(false);
        }
    }
    #endregion

    #region CHANGE CROWN
    private int crownID = -1;
    [SerializeField] TMP_Text crownText = null;
    public void ChangeCrown()
    {
        soundOnClick.Play();

        crownID++;
        DeactivateAllCrowns();

        if (crownID >= Crowns.Count)
        {
            crownID = -1;
            crownText.text = "Crown: none";
        }
        else
        {
            Crowns[crownID].SetActive(true);
            crownText.text = "Crown: " + (crownID + 1).ToString();
        }
    }

    private void DeactivateAllCrowns()
    {
        foreach (GameObject crown in Crowns)
        {
            crown.SetActive(false);
        }
    }
    #endregion


    public void SaveCharacterAppearance()
    {
        Debug.Log("Saving character appearance.");

        PlayerPrefs.SetInt("helmetID", helmetID);
        PlayerPrefs.SetInt("faceID", faceID);
        PlayerPrefs.SetInt("hairID", hairID);
        PlayerPrefs.SetInt("clothID", clothID);
        PlayerPrefs.SetInt("shoeID", shoeID);
        PlayerPrefs.SetInt("gloveID", gloveID);
        PlayerPrefs.SetInt("beltID", beltID);
        PlayerPrefs.SetInt("shoulderID", shoulderID);
        PlayerPrefs.SetInt("crownID", crownID);
    }

    public void LoadCharacterAppearance()
    {
        Debug.Log("Loading character appearance.");

        helmetID = PlayerPrefs.GetInt("helmetID", helmetID);
        faceID = PlayerPrefs.GetInt("faceID", faceID);
        hairID = PlayerPrefs.GetInt("hairID", hairID);
        clothID = PlayerPrefs.GetInt("clothID", clothID);
        shoeID = PlayerPrefs.GetInt("shoeID", shoeID);
        gloveID = PlayerPrefs.GetInt("gloveID", gloveID);
        beltID = PlayerPrefs.GetInt("beltID", beltID);
        shoulderID = PlayerPrefs.GetInt("shoulderID", shoulderID);
        crownID = PlayerPrefs.GetInt("crownID", crownID);
        //--------------------------------------
        //Helmet
        DeactivateAllHelmets();

        if (helmetID != -1)
        {
            Helmets[helmetID].SetActive(true);
        }
        //--------------------------------------
        //Hair
        DeactivateAllHair();

        if (helmetID == -1)
        {
            Hairs[hairID].SetActive(true);
        }
        else if (helmetID < 7)
        {
            HairsHalf[hairID].SetActive(true);
        }
        //--------------------------------------
        //Face
        DeactivateAllFaces();
        if (helmetID < 7)
        {
            Faces[faceID].SetActive(true);
        }
        //--------------------------------------
        //Cloth
        DeactivateAllCloths();
        Cloths[clothID].SetActive(true);
        //--------------------------------------
        //Glove
        DeactivateAllGloves();
        Gloves[gloveID].SetActive(true);
        //--------------------------------------
        //Belt
        DeactivateAllBelts();
        if (beltID != -1)
        {
            Belts[beltID].SetActive(true);
        }
        //--------------------------------------
        //Shoe
        DeactivateAllShoes();
        Shoes[shoeID].SetActive(true);
        //--------------------------------------
        //Crown
        DeactivateAllCrowns();
        if (crownID != -1)
        {
            Crowns[crownID].SetActive(true);
        }
        //--------------------------------------
        //Shoulder
        DeactivateAllShoulderPads();
        if (shoulderID != -1)
        {
            ShoulderPads[shoulderID].SetActive(true);
        }
    }


    public void StartGame()
    {
        SaveCharacterAppearance();
        ppM.LoadLevel(1);
    }
}
