# README #

# Bachelor Graduate Program - University of Europe For Applied Sciences

## Town Of Forgotten Heroes
Copyright (c) 2022 by Matthias Voigt matthias.voigt@ue-germany.de
* Version 0.9

### What is this repository for? ###

* This repository contains the bachelor proposal and thesis.
* The official repository for the project was moved [here](https://gitlab.com/matthias.voigt/BA_Project_mVoigt_WiSe2021).


Required Software

This project uses Unity 2032.1.15f1

Get it from [Unity Webpage](https://unity3d.com/)


This project can freely be used for UE teaching and learning purposes.

The official repo is to be find [here](https://gitlab.com/matthias.voigt/BA_Project_mVoigt_WiSe2021)

___

## Relevant Links

[Itch.io](https://koskoheiwa.itch.io/tofh)

[Miro Board](https://miro.com/app/board/o9J_lqSkMjc=/)

[Project](https://bitbucket.org/btkgamedesign/p3_truestory_dsaylik_mvoigt/commits/)

